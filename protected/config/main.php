<?php
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Магазин детской мебели Комфорт',
    'aliases' => array(
        'bootstrap' => 'ext.bootstrap',
    ),
    'sourceLanguage' => 'ru_RU',
    'language' => 'ru',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
        'application.helpers.*',
		'application.models.*',
        'application.widgets.*',
		'application.components.*',
        'bootstrap.behaviors.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*',
        'editable.*' //easy include of editable classes
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'admin',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'5474823456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('77.244.36.194','::1'),
            'generatorPaths' => array('bootstrap.gii'),
		),

	),

	// application components
	'components'=>array(
    
        'clientScript' => array(
            'packages' => array(
                'jquery' => array(
                    'baseUrl' => Yii::app()->request->baseUrl . '/js_new',
                        'js' => array(
                            'jquery-2.1.4.js'
                    )
                )
            ) 
        ),
        
        'imagemod' => array(
                       //alias to dir, where you unpacked extension
            'class' => 'application.extensions.imagemodifier.CImageModifier',
        ),
    
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            'driver'=>'GD',
            'params'=>array('directory'=>'/opt/local/bin'),
        ),
        'editable' => array(
            'class'     => 'editable.EditableConfig',
            'form'      => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain'
            'mode'      => 'popup',            //mode: 'popup' or 'inline'
            'defaults'  => array(              //default settings for all editable elements
                'emptytext' => 'Click to edit'
            )
        ),
        'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        'class' => 'bootstrap.components.BsApi',

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName'=>false,
		),
		

		// database settings are configured in database.php
		'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=u0068489_default',
            'emulatePrepare' => true,
            'username' => 'u0068489_default',
            'password' => '!T3_BeMu',
//            'connectionString' => 'mysql:host=127.0.0.1;dbname=comfort_beby',
//            'emulatePrepare' => true,
//            'username' => 'root',
//            'password' => '',
//            'connectionString' => 'mysql:host=localhost;dbname=u0072831_d',
//            'emulatePrepare' => true,
//            'username' => 'u0072831_d',
//            'password' => '3c6qca2Y',
            'charset' => 'utf8',
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

        'mobileDetect' => array(
            'class' => 'ext.MobileDetect.MobileDetect'
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'serjeku@mail.ru',
	),
);
