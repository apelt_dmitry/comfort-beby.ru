<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/reset.css')
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
    ->registerCssFile($pt.'css/main.css')
    ->registerCssFile($pt.'css/preLoader.css')
    ->registerCssFile($pt.'css/lightview.css')
    ->registerCssFile($pt.'css/slider.css');

$cs
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/main.js')
    ->registerScriptFile($pt.'js/lightview.js')
    ->registerScriptFile($pt.'js/spinners.min.js')
    ->registerScriptFile($pt.'js/jquery.slides.min.js')
    ->registerScriptFile($pt.'js/slider.js');

$model = new Product();
$banner = Banner::model()->findByPk(1);
$capha = Capha::model()->findByPk(1);
$j_height = 750;

if (!$capha->capha) {$j_height = 600;}
?>


<!DOCTYPE html>

<html>
    <head>
    	<link rel="icon" type="image/png" href="/images/faw.png" />
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<meta name="language" content="en">
    	<title><?= Yii::app()->name;?></title>
    </head>
    
    <body>
        <a name="top"></a>
        
        <script>
            $(function(){
                // Подсказки для поисковой строки
                $('body').click(function(){$('.hide_search').hide();});
                
                $('.search_inp').on("input propertychange", function() {
                    var data_search = $.trim($(this).val());
                    
                    if (data_search.length > 0)
                    {
                        $.ajax({
                            url: "/site/hints", 
                            data: {data_search: data_search},
                            type: "GET",
                            success: function(result){
                                if (result != 0) {$('.hide_search').show().html(result);}
                            }
                        });
                    }
                });
            });
        </script>

        <div id="banner1"><?=$banner->banner1 ?></div>
        <div id="banner2"><?=$banner->banner2 ?></div>

        <?php
        $connection = new Connection();
        
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id' => 'mydialog',
            'options' => array(
                'width' => 464,
                'height' => $j_height,
                'title' => false,
                'autoOpen' => false,
                'modal' => true,
                'resizable' => true,
                'closeOnEscape'=> false,
                'open' => 'js:function(){
                    $(\'.ui-widget-overlay\').click(function(){
                        $(\'#mydialog\').dialog(\'close\');
                    })
                }',
                'hide' => array(
                    'effect' => 'explode',
                    'duration' => 500,
                ),
            ),
        ));
        ?>
        
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'quick-form',
                'enableClientValidation' => true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                    'validateOnChange'=>true,
                ),
                'action' => array('site/connection'),
            ));
            ?>

                <div class="font_bold font_size-24" style="margin: 12px 0 30px 0; color: #fff;">
                    Заполните форму и наш менеджер свяжется с вами в ближайшее время
                </div>
    
                <?= $form->textField($connection, 'name', array('placeHolder' => 'Ваше имя'));?>
                <?= $form->error($connection, 'name'); ?>
    
                <?= $form->textField($connection, 'phone', array('placeHolder' => 'Ваш телефон')); ?>
                <?= $form->error($connection, 'phone'); ?>
    
                <?= $form->textField($connection, 'email', array('placeHolder' => 'Ваш e-mail')); ?>
                <?= $form->error($connection, 'email'); ?>
    
                <div id="lsa">
                    <?php if ($capha->capha == 1): ?>
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <?php $this->widget('CCaptcha')?>
                            <?= CHtml::activeTextField($connection, 'verifyCode', array('placeHolder' => 'Код проверки'))?>
                            <?= $form->error($connection, 'verifyCode'); ?>
                        <?php endif?>
                    <?php endif?>
                </div>
    
                <?= CHtml::submitButton('ОТПРАВИТЬ', array('id' => 'connection_form_button', 'padding' => '10px')); ?>
                
                <div style="color: #fff; font-size: 20px; margin: 0 15px;">Мы гарантируем безопасность ваших данных</div>
                
            <?php $this->endWidget(); ?> 

        <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>


        <div id="loader-wrapper">
            <div id="loader"></div>
        </div>

        <div id="loader-wrapper2">
        	<div style="position:relative;display:table;left: 50%;top: 50%;">
                <img src="/images/close_conf.png" id="close_conf" style="width: 35px;position: absolute;top: -270px;right: -365px;cursor:pointer" />
            </div>
            <div id="loader3">
        		Интернет-магазин Комфорт (далее по тексту ИМ Комфорт) высоко ценит доверие, оказываемое нам нашими Покупателями, и осознает 
                ответственность за обеспечение конфиденциальности персональных данных. Мы соблюдаем правила защиты персональных данных от 
                несанкционированного доступа третьих лиц. При обработке персональных данных Покупателей ИМ Комфорт руководствуется Федеральным 
                законом РФ «О персональных данных» от 27 июля 2006 г. N 152-ФЗ.
                </br></br>
                Согласно статье 3 Федерального закона «О персональных данных» под персональными данными понимается любая информация, относящаяся 
                к прямо или косвенно определенному или определяемому физическому лицу, а под обработкой персональных данных — любое 
                действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без 
                использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, 
                уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), 
                обезличивание, блокирование, удаление, уничтожение персональных данных.
                </br></br>
                Согласно пункту 5 части 1 статьи 6 Федерального закона «О персональных данных» согласие субъекта персональных данных 
                на обработку персональных данных не требуется, если обработка персональных данных необходима для исполнения договора, 
                стороной которого является субъект персональных данных.
                </br>
        	</div>
        </div>

        <div class="row" style="height: 123px; background: #2b2b2b">
            <div class="my_container margin_top-20">
                <div class="row">
                    <div class="my-50">
                        <a href="/"><img src="/images/new_logo.png" style="margin-top: -36px;"/></a>
                    </div>
                    <div class="my-50" style="text-align: right;letter-spacing: 0.45px;">
                        <?php $adress = Adress::model()->find(); ?>
                        <div class="row color_white font_light">Адрес: <?=$adress->adress; ?></div>
                        <div class="row">
                            <div>
                                <?php 
                                $phone_model = Phone::model()->findAll();
                                
                                $i = 1;
                                foreach($phone_model as $val)
                                {
                                    if ($i == 3) {break;}
                                    
                                    $phone[$i] = $val->phone;
                                    $i++;
                                }
                                ?>
                                <div class="row color_white font_light" style="letter-spacing: 1.45px;">Телефон: <?=$phone[1]; ?></div>
                                <div class="row color_white font_light" style="letter-spacing: 1.45px;">Бесплатный звонок по РФ: <?=$phone[2]; ?></div>
                            </div>
                            <?= CHtml::link('Заказать звонок', '#', array('onclick' => '$("#mydialog").dialog("open"); return false;', 'class'=>'link_phone font_italic')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row_menu_header">
            <div class="my_container">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'activeCssClass' => 'active',
                    'id' => 'main_menu_header',
                    'items'=>array(
                        array(
                            'label'=>'Главная',
                            'url'=>array('/site/index'),
                            'linkOptions'=>array(
                                'class'=>'first_a',
                            ),
                        ),
                        array(
                            'label'=>'Каталог',
                            'url'=>array('/site/catalog')
                        ),
                        array(
                            'label'=>'Новости',
                            'url'=>array('/site/news')
                        ),
                        array(
                            'label'=>'О нас',
                            'url'=>array('/site/about')
                        ),
                        array(
                            'label'=>'Как сделать заказ',
                            'url'=>array('/site/how'),
                        ),
                        array(
                            'label'=>
                                '<div id="basket">Корзина</div>
                                    <img src="/images/for_basket.png" class="img_for_basket float_left">
                                    <img src="/images/basket.png" class="float_left">
                                    <div class="circle_basket">0</div>
                                </div>',
                            'encodeLabel'=>false,
                            'url'=>array('/site/basket'),
                            'linkOptions'=>array(
                                'class'=>'last_a',
                            ),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>

        <!-- Хлебные крошки и Выбор города -->
        <div class="header_line">
            <div class="bread_div">
                <?php echo $this->urlvar; ?> &nbsp;
            </div>
            <div class="city_div">
                <div class="city_div_1">
                    Ваш город:
                </div>
                <div class="city_div_2">
                    <?php $this->widget('application.components.CityWidget'); ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        
        <!-- Поиск по сайту и остальное -->
        <div class="header_line">
            <div class="header_input_line">
                <form method="get" action="/site/search">
                    <input type="text" name="data_search" class="search_inp" autocomplete="off" placeholder="Что вы хотите найти?" 
                    value="<?php
                    if (isset($_GET['data_search']) && !empty($_GET['data_search']))
                    {
                        $search_title = new CHtmlPurifier();
                        $search_title = $search_title->purify($_GET['data_search']);
                        $search_title = str_replace("&amp;", "&", $search_title);
                        $search_title = str_replace("'", "", $search_title);
                        $search_title = str_replace('"', "", $search_title);
                        $search_title = trim($search_title);
                        
                        echo $search_title;
                    }
                    ?>">
                    <input type="submit" name="" value=" " class="search_butt">
                </form>
                <div class="hide_search"></div>
            </div>
        </div>

        <!-- Большой ротатер баннеров -->
        <?php if (Yii::app()->controller->action->id != 'catalog' && Yii::app()->controller->action->id != 'productDetail' && Yii::app()->controller->action->id != 'search'): ?>
            
            <?php $act = Act::model()->findAll(); $i = 1; ?>
            
            <?php if (count($act) > 1): ?>
                <div class="row margin_top-24" id="hide_slides">
                    <div id="slides" style="position: relative">
                        <?php foreach($act as $val): ?>
                            <div act_id="<?=$val->id ?>" position="<?= $i ?>" class="slide_id"><?=$val->act_description ?></div>
                        <?php $i++; endforeach; ?>
            
                    </div>
                </div>
            <?php else: ?>
                <div class="row margin_top-40">
                    <?php foreach($act as $val): ?>
                        <div><?=$val->act_description ?></div>
                    <?php endforeach; ?>
                </div>
            <?php endif ?>
            
        <?php else: ?>
            <div class="margin_top-20"></div>
        <?php endif; ?>

        <script>
            function start_slide(){
                var position = 0;
                $('.slide_id').each(function(){
                    if( $(this).attr('act_id')==$('.act_product_id').attr('act_id')){
                        position = $(this).attr('position');
                    }
                });
                return position;
            }
            $(function() {
                var bg = start_slide();
                if( bg==0 ){
                    bg=1;
                }
                $('#slides').slidesjs({
                    //width: 940,
                    height: 286,
                    navigation: {
                        active: true
                    },
                    start: bg,
                    callback: {
                        loaded: function (number) {
                            resize_slider();
                            $('.slidesjs-pagination-item').css({
                                'float':'left',
                                'margin': '0 5px'
                            });
                            $('.slidesjs-navigation').html('');
                            $('.slidesjs-previous').css({
                                'position': 'absolute',
                                'top': '115px',
                                'left': '7%',
                                'width': '30px',
                                'height': '57px',
                                'z-index': '10000',
                                'background': 'url(/images/slider_str.png) center no-repeat rgba(0,0,0,0.7)'
                            });
                            $('.slidesjs-next').css({
                                'position': 'absolute',
                                'top': '115px',
                                'left': '89%',
                                'width': '30px',
                                'height': '57px',
                                'z-index': '10000',
                                'background': 'url(/images/slider_str2.png) center no-repeat rgba(0,0,0,0.7)'
                            });
                            $('.slidesjs-pagination').css({
                                'margin': 'auto',
                                'display': 'table',
                                //'margin-top': '-75px',
                                'z-index': 1000
                            });
                            $('.slidesjs-pagination-item a').html('');
                            $('.slidesjs-container').css('height', 305);
                            $('.slidesjs-control').css('height', 305);
                            $('.slidesjs-pagination-item a').css({
                                'width': '20px',
                                'height': '12px',
                                'display': 'block',
                                'background': '#dcdcdc'
                                //'border': '2px solid #1d6887',
                                //'border-radius': '10px'
                            });
                            $(window).resize(function(){
                                $('.slidesjs-pagination-item').css({
                                    'float':'left',
                                    'margin': '0 5px'
                                });
                                $('.slidesjs-pagination').css({
                                    'margin': 'auto',
                                    'display': 'table',
                                    //'margin-top': '-75px',
                                    'z-index': 1000
                                })
                                $('.slidesjs-pagination-item a').html('');
                                $('.slidesjs-container').css('height', 305);
                                $('.slidesjs-control').css('height', 305);
                                $('.slidesjs-pagination-item a').css({
                                    'width': '20px',
                                    'height': '12px',
                                    'display': 'block',
                                    'background': '#dcdcdc'
                                    //'border': '2px solid #1d6887',
                                    //'border-radius': '10px'
                                });
                            })
        
                        }
                    }
                });
            });
        </script>

        <!-- Блок - Просмотренные товары -->
        
        <?php 
        // Определяем мобильная это система или ПК
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile_detect = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
	
        if (!$mobile_detect): 
        ?>

            <?php
            if ($_COOKIE['show_product'] != null) 
            {
                $model = json_decode($_COOKIE['show_product']);
                $i = 0; $j = 1;
                
                foreach ($model as $val)
                {
                    $product = Product::model()->findByPk($val->product_id);
                    $result[$i]['product_id'] = $product->id;
                    $result[$i]['name'] = $product->product_name;
                    
                    $realPrice = $product->price;
                    
                    if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($product->productCityPrice) && !empty($product->productCityPrice))
                    {
                        foreach ($product->productCityPrice as $city_p)
                        {
                            if ($city_p->id_city == $_COOKIE['city'])
                            {
                                $realPrice = $city_p->price;
                            }
                        }
                    }
                    
                    if ($product->productDiscount->discount != null)
                    {
                        $result[$i]['price'] = round((100 - $product->productDiscount->discount*1)*$realPrice/100,0);
                    }
                    else
                    {
                        $result[$i]['price'] = $realPrice;
                    }
                    
                    $result[$i]['image'] = $product->mainImage->image;
            		$result[$i]['id'] = $product->id;
                    $i++;
                }
            }
            ?>
    
            <div id="nSP">
                <div class="color_nSP">Просмотренные товары:</div>
                <div style="overflow-y: scroll; height: 360px">
                    <?php foreach(array_reverse($result) as $val): ?>
                        <div class="view_product" style="margin-bottom: 20px;  margin-left: 44px;position: relative">
                            <table class="table_name">
                                <tr>
                                    <td style="text-align: center" class="font_bold"><?= $val['name'] ?></td>
                                </tr>
                            </table>
                            <div class="view_img">
                                <a href="/site/productDetail/<?=$val['id'] ?>"><?= BsHtml::image('/uploads/product/preview/'.$val['image']) ?></a>
                            </div>
                            <table class="table_detail">
                                <tr>
                                    <td class="table_price"><?= number_format($val['price'], 0, '.', ' ') ?> р.</td>
                                    <td class="table_link"><a href="/site/productDetail/<?=$val['id']; ?>">Подробнее</a></td>
                                </tr>
                            </table>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
    
            <script>
            $(document).ready(function () {
                $('#nSP').click(function () {
                    if( $(this).css('right') == '-249px' ){
                        $(this).animate({
                            'right': 0
                        })
                    }
                    else{
                        $(this).animate({
                            right: -249
                        })
                    }
        
                });
                setTimeout(function(){
                    $('#nSP').animate({
                        'right': 0
                    })
                },120000)
            });
            </script>

        <?php endif; ?>

        <!-- Основной контент -->
        <div class="my_container special">
            <?= $content ?>
        </div>

        <!-- Футер -->
        <div id="footer" class="margin_top-78" style="padding: 25px 0 0 0;margin-top: -2px;position:relative">
            <div class="my_container">
            
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'activeCssClass' => 'active',
                    'id' => 'main_menu2',
                    'items'=>array(
                        array(
                            'label'=>'Главная',
                            'url'=>array('/site/index')
                        ),
                        array(
                            'label'=>'Каталог',
                            'url'=>array('/site/catalog')
                        ),
                        array(
                            'label'=>'Новости',
                            'url'=>array('/site/news')
                        ),
                        array(
                            'label'=>'О нас',
                            'url'=>array('/site/about')
                        ),
                        array(
                            'label'=>'Как сделать заказ',
                            'url'=>array('/site/how')
                        ),
                    ),
                ));
                ?>
                
                <div class="my-36 margin_left-83 footer_phone">
                    <div class="row">
                        <div class="my-47" style="margin-right: 6px;">
                            <div class="row"><?=$phone[1]; ?></div>
                            <div class="row"><?=$phone[2]; ?></div>
                        </div>
        
                        <table class="my-49"><tr class="orange_phone"><td><?= CHtml::link('<div style="padding: 10px;">Заказать звонок</div>', '#', array('onclick' => '$("#mydialog").dialog("open"); return false;', 'class'=>'link_phone2')); ?></td></tr></table>
        
                    </div>
                    <div class="row"><?=$adress->adress; ?></div>
                </div>
                
        		<script>
    			$(document).ready(function(){
    				$('#conf').click(function(){
    					$('#loader-wrapper2').show();
    					return false;
    				})
    				$('#close_conf').click(function(){
    					$('#loader-wrapper2').hide();
    				})
    			});
        		</script>
                
                <div class="row" style="color: #fff; position: absolute;bottom: 20px;">
                    Сайт разработан: 
                    <a href="http://webarchitector.net/" id="link_arch" target="_blank" class="font_bold">WEBARCHITECTOR</a> 
                </div>
                
                <div class="row copy_l">
                    copyright&copy &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Комфорт. 2015. 
                    <a href="#" id="conf">Политика конфиденциальности</a> 
                </div>
            </div>
        </div>

        <div class="row">
            <div style="text-align: center;margin: 5px 0;">
            
                <!-- Yandex.Metrika informer -->
                <a href="https://metrika.yandex.ru/stat/?id=29500155&amp;from=informer"
                target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/29500155/1_1_FFFFFFFF_EFFFE1FF_0_uniques"
                style="width:80px; height:15px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (уникальные посетители)" /></a>
                <!-- /Yandex.Metrika informer -->
                
                <!-- Yandex.Metrika counter -->
                <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter29500155 = new Ya.Metrika({id:29500155,
                                    webvisor:true,
                                    clickmap:true,
                                    accurateTrackBounce:true});
                        } catch(e) { }
                    });
                
                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
                
                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
                </script>
                <noscript><div><img src="//mc.yandex.ru/watch/29500155" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                <!-- /Yandex.Metrika counter -->
                
            </div>
        </div>

        <a href="#top" class="page_up" title="Вверх">
            <img src="/images/page_up.png">
        </a>
    
    </body>
</html>