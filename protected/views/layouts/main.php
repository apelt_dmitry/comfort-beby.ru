<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$model = new Product();
$banner = Banner::model()->findByPk(1);
$capha = Capha::model()->findByPk(1);
$j_height = 750;

if (!$capha->capha) {$j_height = 600;}
?>
<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">

    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    
    <?php Yii::app()->clientScript->registerCssFile('/css_new/reset.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/animate.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/slick.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/slick-theme.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/jquery.arcticmodal-0.3.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/jquery.fancybox.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/yii.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/jquery-ui.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/validetta.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/main.css'); ?> 
        
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.fancybox.pack.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery-migrate-1.2.1.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/custom-form-elements.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/slick.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.arcticmodal-0.3.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/smooth_scrolling.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.cookie.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/validetta.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery-ui.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/main.js'); ?>
    
    <title><?= Yii::app()->name;?></title>
    
<script type="text/javascript">
function animate_cart_my(obj) {
    src="/images/button_buy_fly.png";
    $('<img src="' + src + '" id="temp_cart_animate" style="z-index: 2000; position: absolute; top:' + Math.ceil($(obj).offset().top) + 'px; left:' + Math.ceil($(obj).offset().left) + 'px;">').prependTo('body');
    $('#temp_cart_animate').animate(
        {
            top: -1000 + $(window).scrollTop(),
            left: $('body').width()
        },
        700,
        function () {
            $('#temp_cart_animate').remove();
        });
}
</script>
</head>

<body>

<a name="top"></a>

<?php Yii::app()->clientScript->registerScript('for_socail_reg', ''); ?>

<!-- Обратная связь -->
<div style="display: none;">
    <?php $connection = new Connection(); ?>

    <div class="box-modal" id="phoneModal">

        <div class="phone_modal clear-fix">
            <div class="box-modal_close arcticmodal-close phone_modal_close"></div>
            <div class="phone_modal_title">
                Заказать звонок
            </div>
            <div class="phone_modal_under_title">
                Заполните форму и отправте нам. Наш менеджер<br/>
                свяжется с Вами в ближайшее время
            </div>
            
	        <?php $form = $this->beginWidget('CActiveForm', array(
                    'action' => array('site/connection'),
    				'id' => 'quick-form',
    				'enableAjaxValidation' => true,
                    'enableClientValidation'=>false, 
    				'clientOptions'=>array(
    					'validateOnSubmit'=>true,
                        'validateOnChange'=>false,
    				),
    			));
	        ?> 
                <div class="form_name_bgr">
                    <?= $form->textField($connection, 'name', array('placeHolder' => 'Ваше имя', 'class' => 'form_name cb'));?>
                    <div class="red_star phone_red_star"></div>
                    <?= $form->error($connection, 'name'); ?>
                </div>
                <div class="form_phone_bgr">
                    <?= $form->textField($connection, 'phone', array('placeHolder' => 'Ваш телефон', 'class' => 'form_name cb'));?>
                    <div class="red_star phone_red_star"></div>
                    <?= $form->error($connection, 'phone'); ?>
                </div>
                <div class="form_email_bgr">
                    <?= $form->textField($connection, 'email', array('placeHolder' => 'Ваш e-mail', 'class' => 'form_name cb'));?>
                    <div class="red_star phone_red_star"></div>
                    <?= $form->error($connection, 'email'); ?>
                </div>

                <div class="warning3"><span>* </span>обязательные поля для заполнения</div>
                
                <?= CHtml::submitButton('', array('class' => 'phone_submit')); ?>
            
            <?php $this->endWidget(); ?> 

        </div>
    </div>
    <?php if(Yii::app()->user->hasState('thx_for_contac_form_submt')):?>
    <div class="box-modal" id="TYModal">
        <div class="phone_modal clear-fix">
            <div class="box-modal_close arcticmodal-close phone_modal_close"></div>
            <div class="smile_women"></div>
            <div class="smile_text">
                Благодарим Вас за прояленный<br/>
                интерес. Наш менеджер свяжется<br/>
                с Вами в ближайшее время<br/>
            </div>
        </div>
    </div>
    <?php Yii::app()->user->setState('thx_for_contac_form_submt', null); endif;?>
    <div class="box-modal" id="exampleModal">
        <div class="main_modal clear-fix">
            <div class="box-modal_close arcticmodal-close main_modal_close"></div>
            <div class="main_modal_title">Товар добавлен в корзину</div>
            <div class="main_modal_description">Вы можете продолжить покупки или <br/>оформить заказ</div>
            <a href="/site/basket"><div class="main_modal_button"></div></a>
            <div class="arcticmodal-close button_go_to_buy"></div>
        </div>
    </div>
</div>

<div id="header">
    <?php 
    $adress = Adress::model()->find();
    
    $phone_model = Phone::model()->findAll();
    
    $i = 1;
    foreach($phone_model as $val)
    {
        if ($i == 3) {break;}
        
        $phone[$i] = $val->phone;
        $i++;
    }
    ?>
    <div class="container">
        <a id="header_logo" href="/"></a>
        <div id="header_contacts">
            <div id="header_addr"><img src="/images/mini_map.png"><?php echo $adress->adress; ?></div>
            <div id="header_phone"><img src="/images/mini_phone.png"><?php echo $phone[1]; ?></div>
            <div id="header_car"><img src="/images/mini_car.png">Доставка в любой город России</div>
        </div>
        <div id="header_contacts_right">
            <div id="header_phone_right"><img src="/images/mini_phone.png">Бесплатный звонок по РФ:</div>
            <div id="header_phone_2"><?php echo $phone[2]; ?></div>
            <a href="#" id="header_call_back">Заказать звонок</a>
        </div>
    </div>
</div>

<div id="header_menu">
    <div class="menu_table">
        <ul id="main_menu_header">
            <li><a href="/">Главная</a></li>
            <li class="open_drop"><a href="/site/catalog">Каталог</a>
                <ul>
                    <?php $this->widget('application.widgets.CatalogWidget', array('wid_view'=>'catalog_drop_widget')); ?>  
                </ul>
            </li>
            

            <li><a href="/site/article">Статьи</a></li>
            <li><a href="/site/about">О нас</a></li>
            <li><a href="/site/how">Как сделать заказ</a></li>
            <li><a href="/site/delivery">Оплата и доставка</a></li>
            <li><a href="/site/basket">Корзина<img id="menu_cart" src="/images/menu_cart.png"><div id="menu_count" class="menu_count_basket">0</div></a></li>

            <div class="drop_count">
                <div class="drop_count_top clear-fix">
                    <div class="drop_count_top_close"></div>
                    <div class="drop_count_top_count">Выбранно товаров: <span class="drop_count_count">0</span><br/> На сумму: <span class="drop_count_cost">0</span> руб.</div>
                </div>
                <a class="no_border" href="/site/basket"><div class="drop_count_bot"></div></a>
            </div>


        </ul>

    </div>
</div>


    <div id="header_menu_2">
        <div class="menu_table_2">
            <ul id="main_menu_header_2">
                <li><a href="/">Главная</a></li>
                <li class="open_drop_2"><a href="/site/catalog">Каталог</a>
                    <ul>
                        <?php $this->widget('application.widgets.CatalogWidget', array('wid_view'=>'catalog_drop_widget')); ?>
                    </ul>
                </li>
                <li><a href="#"><img src="/images/green_phone.png"/><?php echo $phone[1]; ?>, <?php echo $phone[2]; ?></a></li>
                <li><a id="alwaystop_call_back" href="#">Заказать звонок</a></li>
                <li><a id="cart" href="/site/basket">Корзина<img id="menu_cart_2" src="/images/menu_cart.png">
    
                        <div id="menu_count_2" class="menu_count_basket">0</div>
                    </a></li>
                <div class="drop_count">
                    <div class="drop_count_top clear-fix">
                        <div class="drop_count_top_close"></div>
                        <div class="drop_count_top_count">Выбранно товаров: <span class="drop_count_count">0</span><br/> На сумму: <span class="drop_count_cost">0</span> руб.</div>
                    </div>
                    <a class="no_border" href="/site/basket"><div class="drop_count_bot"></div></a>
                </div>
            
            </ul>
        </div>
    </div>
<?php if (Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index'): ?>

    <div class="container search_and_city clear-fix">
    
        <script>
            
        </script>
    
        <form method="get" action="/site/catalog" class="searchform">
            <input type="text" name="product_name" class="search_input autocomplete" placeholder="Что вы хотите найти?" value="">
            <input type="submit" value="" class="search_button">
            <div class="hide_search"></div>
        </form>
        
        <?php $this->widget('application.components.CityWidget'); ?>
    </div>
    
<?php else: ?>

    
    <div class="breadcrumbs_box">
        <div class="container clear-fix">
            <div class="breadcrumbs">Навигация: <?php echo $this->urlvar; ?></div>
        </div>
    </div>
    
    <div class="container search_and_city_2 clear-fix">
        <form class="search_form" method="get" action="/site/catalog">
            <input type="text" name="product_name" class="new_search_input autocomplete" placeholder="Что вы хотите найти?" value="<?php  if(isset($_REQUEST['product_name'])) echo $_REQUEST['product_name']; ?>" />
    
            <div class="img_new_search_input"></div>
            <input type="submit" name="" value="Найти" class="new_search_button">
        </form>
        <form class="search_form ml_10" method="get" action="/site/catalog">
            <!-- <input type="text" name="" class="new_search_input_2" placeholder="Выберите производителя" value=""> -->
            
            <select class="new_search_input_2" name="manuf">
            
                <?php if(!isset($_REQUEST['manuf'])): ?> 
                    <?php $manuf = NULL ?>                      
                    <option value="" disabled selected>Выберите производителя</option>
                <?php else: ?>
                    <?php $manuf = $_REQUEST['manuf']; ?>  
                <?php endif; ?>
                
                <?php $production_all = Production::getAllProduction2();
                foreach($production_all as $key => $val):?>
                    <option value="<?php echo $key;?>" <?php echo ($key == $manuf)?'selected': ''; ?>><?php echo $val; ?></option>
                <?php endforeach; ?>
            </select>
    
            <div class="img_new_search_input_2"></div>
            <input type="submit" name="" value="Найти" class="new_search_button_2">
        </form>
        <!--<div class="category_furniture"></div> -->
    </div>

<?php endif; ?>
<?php echo $content; ?>

<div class="footer clear-fix">
    <div class="container">
        <div class="footer_main_box_left">
        
            <?php $this->widget('application.widgets.CatalogWidget', array('wid_view'=>'catalog_footer')); ?>  
        
        <div class="footer_main_box_right">
            <div class="footer_main_box_right_top">
                <div class="footer_phone_top"><img src="/images/footer_phone.png">8 (800) 555-87-06</div>
                <div class="free_rf">Бесплатный звонок по РФ</div>
                <div class="footer_phone_bottom">+7 (495) 374-95-78</div>
                <a href="#" class="button_call_back"></a>
            </div>
            <div class="footer_main_box_right_bottom">
                <div class="footer_map"><img src="/images/footer_map.png">Москва,</div>
                <div class="footer_under_map">ул. Рябиновая, 41, корп.1</div>
            </div>
        </div>
        <div class="footer_main_box_bottom">
            copyright © Комфорт. 2015. Политика конфиденциальности
        </div>
        </div>
    </div>
</div>

    <!-- Блок - Просмотренные товары -->
     <?php 
        // Определяем мобильная это система или ПК
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $mobile_detect = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
 
        if (!$mobile_detect): 
        ?>

            <?php
            if ($_COOKIE['show_product'] != null) 
            {
                $model = json_decode($_COOKIE['show_product']);
                $i = 0; $j = 1;
                
                foreach ($model as $val)
                {
                    $product = Product::model()->findByPk($val->product_id);
                    $result[$i]['product_id'] = $product->id;
                    $result[$i]['name'] = $product->product_name;
                    
                    $realPrice = $product->price;
                    
                    if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($product->productCityPrice) && !empty($product->productCityPrice))
                    {
                        foreach ($product->productCityPrice as $city_p)
                        {
                            if ($city_p->id_city == $_COOKIE['city'])
                            {
                                $realPrice = $city_p->price;
                            }
                        }
                    }
                    
                    if ($product->productDiscount->discount != null)
                    {
                        $result[$i]['price'] = round((100 - $product->productDiscount->discount*1)*$realPrice/100,0);
                    }
                    else
                    {
                        $result[$i]['price'] = $realPrice;
                    }
                    
                    $result[$i]['image'] = $product->mainImage->image;
            		$result[$i]['id'] = $product->id;
                    $i++;
                }
            }
            ?>

<div class="flex-tab">
    <div class="flex_open"></div>
    <div class="flex_income">
        <div class="flex_banner"></div>
        <div class="flex_slider">
            <div class="flex_slider_big_box">
                <?php $i= 0; foreach(array_reverse($result) as $val): ?> 
                <div class="flex_slider_box">
                    <div class="flex_slider_title"><?= $val['name'] ?></div>
                    <!-- <a class="fancybox db" rel="group4" href="/uploads/product/<?php echo $val['image'];?>">-->
                    <a class="fancybox db"  href="/site/productDetail/<?php echo $val['id'];?>">
                        <div class="slider_yee"></div>
                        <img class="flex_slider_img" src="/uploads/product/preview/<?php echo $val['image'];?>">
                    </a>
                    <div class="flex_slider_price">Цена:<p><?= number_format($val['price'], 0, '.', ' ') ?> р.</p></div>
                    <div class="flex_slider_button buy_btn_main" onclick="animate_cart_my(this);" product_id="<?=$val['id']; ?>"></div>
                </div>
                    <?php $i++; if($i>=3): ?>
            </div>
            <div class="flex_slider_big_box">                    
                    <?php $i = 0; endif;?>
            
                <?php endforeach; ?>

            </div>
        </div>
        <div class="flex_grey_zone"></div>
    </div>

</div>

<?php endif; ?> <!-- end Просмотренные товары -->

<!-- script scroll to top -->
<script type="text/javascript" src="http://arrow.scrolltotop.com/arrow26.js"></script>


<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=29500155&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/29500155/1_1_FFFFFFFF_EFFFE1FF_0_uniques"
style="width:80px; height:15px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (уникальные посетители)" /></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter29500155 = new Ya.Metrika({id:29500155,
                    webvisor:true,
                    clickmap:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/29500155" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>

</html>