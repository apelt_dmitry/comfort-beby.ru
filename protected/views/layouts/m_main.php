<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$model = new Product();
$banner = Banner::model()->findByPk(1);
$capha = Capha::model()->findByPk(1);
$j_height = 750;

if (!$capha->capha) {$j_height = 600;}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php Yii::app()->clientScript->registerCssFile('/css_new/reset.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/animate.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/slick.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/slick-theme.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/jquery.arcticmodal-0.3.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/jquery.fancybox.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/drawer.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/yii.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/jquery-ui.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/css_new/mobile_main.css'); ?>

    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.fancybox.pack.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery-2.1.4.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery-migrate-1.2.1.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/custom-form-elements.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/slick.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.arcticmodal-0.3.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.drawer.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/smooth_scrolling.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery.cookie.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/validetta.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/jquery-ui.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('//cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.1/iscroll-min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('//cdn.rawgit.com/ungki/bootstrap.dropdown/3.3.1/dropdown.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile('/js_new/mobile_main.js'); ?>

    <title><?= Yii::app()->name;?></title>
</head>

<body class="drawer drawer-right">
<script>
    $(document).ready(function() {
        $(".drawer").drawer();
        $(".drawer-brand a").click(function() {
            $('.drawer').drawer('close');
        });
    });

</script>

<header role="banner">
    <div class="drawer-header">
        <button type="button" class="drawer-toggle drawer-hamburger">
            <span class="sr-only">toggle navigation</span>
            <span class="drawer-hamburger-icon"></span>
        </button>
    </div>

    <div class="drawer-main drawer-default">
        <nav class="drawer-nav" role="navigation">
            <div class="drawer-brand"><a href="#">Меню<img class="m_dawer_close" src="/images/m_dawer_close.png"></a></div>

            <?php $this->widget('application.widgets.CatalogWidget', array('wid_view'=>'catalog_mobile_menu')); ?>

            <div class="drawer-footer"><span></span></div>
        </nav>
    </div>
</header>

<div class="drawer-overlay">
    <!-- content -->
</div>

<div class="m_nav clear-fix">
    <a href="/" class="m_logo"></a>
    <div class="m_menu_box">
        <div class="m_menu_text">Меню:</div>
        <img src="/images/m_menu_lines.png">
    </div>
    <div class="m_cart_box">
        <div class="m_cart_text">Корзина:</div>
        <img src="/images/m_menu_cart.png">

        <a href="/site/basket" class="m_cart_count"><span><p class="menu_count_basket">0</p></span></a>
    </div>
</div>
<div class="m_banner">
    <a href="/site/catalog/27" class="pr">
        <img style="position: absolute" src="/images/m_scarry_girl.png">
        <img class="confeti" src="/images/confeti.png">
        <div class="m_container m_banner_box clear-fix">
            <div class="m_banner_text">
                РАСПРОДАЖА детской мебели
            </div>
            <div class="m_banner_sale"></div>
        </div>
    </a>
    <a href="/site/catalog/1" class="pr">
        <div class="m_banner_slide_2_left"></div>
        <div class="m_banner_slide_2_right"></div>
    </a>
    <a href="/site/catalog/32" class="pr">
        <div class="m_banner_slide_3_left"></div>
        <div class="m_banner_slide_3_hit"></div>
        <div class="m_banner_slide_3_right"></div>
    </a>
</div>
<div class="m_search_box">
    <div class="m_container clear-fix">
<!--        <form method="get" action="/site/catalog" class="searchform">-->
<!--            <input type="text" name="product_name" class="search_input autocomplete" placeholder="Что вы хотите найти?" value="">-->
<!--            <input type="submit" value="" class="search_button">-->
<!--            <div class="hide_search"></div>-->
<!--        </form>-->
        <form method="get" action="/site/catalog" class="clear-fix">
            <input type="text" name="product_name" class="m_search_input autocomplete" placeholder="Что вы хотите найти?" value="">
            <input type="submit" value="" class="m_search_button">
            <div class="hide_search"></div>
        </form>
        <div class="m_u_search">
            Например: детская кровать, комната Корсар, диван
        </div>
        <div class="m_button_call_us button_call_back"></div>
        <a href="/site/delivery" class="m_button_delivery_and_pay"></a>
    </div>

<!--<div class=" m_container m_breadcrumbs">-->
<!--    Главная > Доставка и оплата-->
<!--</div>-->

<?php echo $content; ?>

<div class="m_full_box clear-fix">
    <div class="m_full_box_bgr"></div>
    <div class="m_container clear-fix">
        <div class="m_mac"></div>
        <div class="m_full_button"></div>
    </div>
</div>
    <?php
    $adress = Adress::model()->find();

    $phone_model = Phone::model()->findAll();

    $i = 1;
    foreach($phone_model as $val)
    {
        if ($i == 3) {break;}

        $phone[$i] = $val->phone;
        $i++;
    }
    ?>

<div class="m_footer clear-fix">
    <div class="m_footer_left">
        <a href="/site/catalog">Каталог</a>
        <a href="/site/about">О нас</a>
        <a href="/site/delivery">Доставка и оплата</a>
        <a href="/site/how">Как сделать заказ</a>
    </div>
    <div class="m_footer_right">
        <div class="m_footer_right_box">
            <img src="/images/m_mini_map.png">

            <div class="m_footer_right_text">
                <?php echo $adress->adress; ?>
            </div>
        </div>
        <div class="m_footer_right_box">
            <img src="/images/m_mini_phone.png">

            <div class="m_footer_right_text">
                <?php echo $phone[1]; ?><br/>

                <p>Бесплатный звонок по РФ:</p>
            </div>
        </div>
        <div class="m_footer_right_box mt_20">
            <div class="m_footer_right_text ">
                <?php echo $phone[2]; ?>
            </div>
        </div>
    </div>
    <div class="m_container">
        <div class="m_copyright">copyright © Комфорт. 2015. <span>Политика конфиденциальности</span></div>
    </div>
</div>

<div style="display: none;">
    <?php $connection = new Connection(); ?>

    <div class="box-modal" id="phoneModal">

        <div class="phone_modal clear-fix">
            <div class="box-modal_close arcticmodal-close phone_modal_close"></div>
            <div class="phone_modal_title ">
                Заказать звонок
            </div>
            <div class="phone_modal_under_title">
                Заполните форму и отправте нам. Наш менеджер<br/>
                свяжется с Вами в ближайшее время
            </div>

            <?php $form = $this->beginWidget('CActiveForm', array(
                'action' => array('site/connection'),
                'id' => 'quick-form',
                'enableAjaxValidation' => true,
                'enableClientValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                    'validateOnChange'=>false,
                ),
            ));
            ?>
            <div class="form_name_bgr">
                <?= $form->textField($connection, 'name', array('placeHolder' => 'Ваше имя*', 'class' => 'form_name cb'));?>
                <div class="red_star phone_red_star"></div>
                <?= $form->error($connection, 'name'); ?>
            </div>
            <div class="form_phone_bgr">
                <?= $form->textField($connection, 'phone', array('placeHolder' => 'Ваш телефон*', 'class' => 'form_name cb'));?>
                <div class="red_star phone_red_star"></div>
                <?= $form->error($connection, 'phone'); ?>
            </div>
            <div class="form_email_bgr">
                <?= $form->textField($connection, 'email', array('placeHolder' => 'Ваш e-mail*', 'class' => 'form_name cb'));?>
                <div class="red_star phone_red_star"></div>
                <?= $form->error($connection, 'email'); ?>
            </div>

            <div class="warning3"><span>* </span>обязательные поля для заполнения</div>

            <?= CHtml::submitButton('', array('class' => 'phone_submit')); ?>

            <?php $this->endWidget(); ?>

        </div>
    </div>
    <?php if(Yii::app()->user->hasState('thx_for_contac_form_submt')):?>
        <div class="box-modal" id="TYModal">
            <div class="phone_modal clear-fix">
                <div class="box-modal_close arcticmodal-close phone_modal_close"></div>
                <div class="smile_women"></div>
                <div class="smile_text">
                    Благодарим Вас за прояленный<br/>
                    интерес. Наш менеджер свяжется<br/>
                    с Вами в ближайшее время<br/>
                </div>
            </div>
        </div>
        <?php Yii::app()->user->setState('thx_for_contac_form_submt', null); endif;?>
    <div class="box-modal" id="exampleModal">
        <div class="main_modal clear-fix">
            <div class="box-modal_close arcticmodal-close main_modal_close"></div>
            <div class="main_modal_title">Товар добавлен в корзину</div>
            <div class="main_modal_description">Вы можете продолжить покупки или <br/>оформить заказ</div>
            <a href="/site/basket"><div class="main_modal_button"></div></a>
            <div class="arcticmodal-close button_go_to_buy"></div>
        </div>
    </div>
</div>

</body>
</html>
