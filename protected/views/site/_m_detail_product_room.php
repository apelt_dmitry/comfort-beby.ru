<?php
$comp = InRoom::model()->findAllByAttributes(array(
    'room_id' => $model->id,
))
?>
<div class="m_popular_product_title" style="margin-top: 60px">
    <span>Все элементы данного гарнитура</span>
</div>
<div class="m_box_all_elements clear-fix">
    <?php foreach($comp as $val): ?>
        <?php $this->renderPartial('_m_viewAll', array('data'=>$val->ProductInRoom)); ?>
    <?php endforeach; ?>
</div>
