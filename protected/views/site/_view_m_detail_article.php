<!--<div class=" m_container m_breadcrumbs">-->
<!--    Главная > Новости-->
<!--</div>-->
<?php
Yii::app()->name = $data->meta_title;
?>

<!-- внизу новый код -->
<div class=" m_container m_breadcrumbs">
    <a href="/" style="text-decoration: none">Главная</a> > <a href="<?= Yii::app()->request->requestUri ?>" style="text-decoration: none">Новость</a>
</div>
<div class="m_news">
    <div class="m_container clear-fix">
        <div class="m_news_img_box">
            <?=BsHtml::image('/uploads/product/'.$data->image); ?>
        </div>
        <div class="m_news_title">
            <?=$data->title ?>
        </div>
        <div class="m_news_description">
            <?=$data->description ?>
        </div>
    </div>
</div>