

<?php
mb_internal_encoding("UTF-8");
$description = '';
if(strlen($data->description)<=150){
    $description = $data->description;
}
else{
    $description = mb_substr($data->description, 0, 140).'...';
}
?>

<div class="margin_left-21 margin_bottom-18">
    <div class="row  padding-14" style="border: 1px solid #d7d7d7">
        <div class="my-22"><?= BsHtml::image('/uploads/product/preview/'.$data->image,'', array('class' => 'news_img'))?></div>
        <div class="description_news my-77">
            <div><?=$description ?></div>
            <div><a href="/site/article/<?=$data->id ?>" class="link_news" style="text-align: right;  margin-top: 0px;">Подробнее</a></div>
        </div>
    </div>
</div>