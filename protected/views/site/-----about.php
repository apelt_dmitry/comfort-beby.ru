<?php
$meta = Meta::model()->findByPk(4);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
$config = ConfigMain::model()->findAll();
        $i = 1;
        foreach($config as $val){
            $text[$i]['title'] = $val->title;
            $text[$i]['description'] = $val->description;
            $i++;
        }
?>

<div class="row margin_top-20">
    <div class="my-24">
        <div id="comment">
            <div id="slider1">
                <table>
                    <tr>
                        <td class="comment_padding">
                            <a class="buttons prev" href="#"></a>
                        </td>
                        <td>
                            <div id="comment_header" class="font_bold"><a href="/site/comment" style="color: #fff">Все отзывы</a></div>
                        </td>
                        <td class="comment_padding">
                            <a class="buttons next" href="#"></a>
                        </td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="viewport">
                    <ul class="overview">
                        <?php foreach($comment as $val): ?>
                            <li><?= $val ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>

        <div style="display: table;margin-bottom: 40px;">
			<?php $comment_image = ImageComment::model()->findByPk(1) ?>
			<img src="/uploads/product/<?=$comment_image->image ?>" class="my-35" style="height: 80px;border-radius: 50px">
			<div class="my-60-ml-10"><?=$comment_image->title ?></div>
		</div>

        </div>

        <div class="my-76">
            <div class="row">
                <div class="my-100 chose_parents">
                    <div style="width: 75%;display: block;" class="font_bold"><?=$text[1]['title'] ?></div>
                </div>
                <div class="my-100 margin_top-30"><?=$text[1]['description'] ?></div>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="row margin_top-58">

        <div class="my-333">
            <img src="/images/fast.png">
            <div class="margin_top-28 font_bold"><?=$text[2]['title'] ?></div>
            <img src="/images/under.png" style="margin: 13px 0">
            <div style="padding: 0 15px"><?=$text[2]['description'] ?></div>
        </div>

        <div class="my-333">
            <img src="/images/risk.png">
            <div class="margin_top-28 font_bold"><?=$text[3]['title'] ?></div>
            <img src="/images/under.png" style="margin: 13px 0">
            <div style="padding: 0 15px"><?=$text[3]['description'] ?></div>
        </div>

        <div class="my-333">
            <img src="/images/product.png">
            <div class="margin_top-28 font_bold"><?=$text[4]['title'] ?></div>
            <img src="/images/under.png" style="margin: 13px 0">
            <div style="padding: 0 15px"><?=$text[4]['description'] ?></div>
        </div>

    </div>

    </div>

    <div class="row margin_top-58">

        <div class="my-23">
            <?=$result_categories ?>
        </div>

        <div class="my-77">
            <?php
                for ($i = 1; $i <= 6; $i++):
                     $ar_rand = rand(0, count($all_product['name'])-1);
            ?>

                <div class="view_product margin_left-25" style="margin-bottom: 20px;position: relative">
                    <table class="table_name">
                        <tr>
                            <td style="text-align: center" class="font_bold"><?= $all_product['name'][$ar_rand] ?></td>
                        </tr>
                    </table>
                    <div class="view_img">
                        <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                    </div>
                    <table class="table_detail">
                        <tr>
                            <?php if ($all_product['act'][$ar_rand] != 0): ?>
                                <img src="/images/act.png" class="act_img"/>
                            <?php endif ?>
                            
                            <?php if ($all_product['discount'][$ar_rand] != 0): ?>
                                <div class="discount md">-<?=$all_product['discount'][$ar_rand] ?>%</div>
                                <td class="table_price"><?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</td>
                            <?php else: ?>
                                <td class="table_price"><?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</td>
                            <?php endif; ?>

                            <td class="table_link"><a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>">Подробнее</a></td>
                        </tr>
                    </table>
                </div>

            <?php endfor ?>
        </div>
    </div>


    <div class="row margin_top-58">

        <div class="my-27" style="padding: 44px 0 0 24px">
            <img src="/images/shopping.png">
        </div>

        <div class="my-73">

            <div class="font_size-24 font_bold"><img src="/images/many_element.png" style="margin-right: 12px"><?=$text[5]['title'] ?></div>
            <div class="margin_top-20"><?=$text[5]['description'] ?></div>

            <div style="font-size: 24px" class="margin_top-30 font_bold"><img src="/images/many_element.png" style="margin-right: 12px"><?=$text[6]['title'] ?></div>
            <div class="margin_top-20"><?=$text[6]['description'] ?></div>

        </div>

    </div>
</div>

