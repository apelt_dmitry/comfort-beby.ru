<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs->registerCssFile($pt.'css/slider.css');
$cs->registerScriptFile($pt.'js/slider.js');

$meta = Meta::model()->findByPk(2);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
mb_internal_encoding("UTF-8");
?>

<script>
    $(document).ready(function(){
        new_css();
        category_tree();
    });
	
	function top_body(){	
		$('body').animate({scrollTop: 448},1000);
	}
	
    function category_tree(){
        if( $.get('id')!='undefined' ){
            if($('.sortable a[category_id="'+ $.get('id')+'"]').parent().next().hasClass('sortable')){
                $('.sortable a[category_id="'+ $.get('id')+'"]').parent().next().show().find('ul').show();
            }
            $('.sortable a[category_id="'+ $.get('id')+'"]').parent().parent().show();
            $('.sortable a[category_id="'+ $.get('id')+'"]').parent().parent().parent().show();
            $('.sortable').find('a[category_id="'+$.get('id')+'"]').css('color', '#1d6887');
        }
    }

    function new_css(){
        var i = 0;
        $('.width100').each(function(){
            if(i%4==0){
                //$(this).css('width','100%');
                $(this).find('.table_name td').css({
                    'background': '#fff',
                    'color': '#000',
                    'text-align': 'left'
                });
                $(this).find('.view_img').css({
                    'width': '288px',
                    'height': '210px'
                });
                $(this).find('.view_img img').css({
                    'width': '288px',
                    'height': '210px'
                });
                $(this).find('.table_name').css('width', '288px');
                $(this).find('.table_detail').css({
                    'position': 'absolute',
                    'left': '320px',
                    'top': '228px'
                });
                $(this).find('.description_js').css({
                    'margin-top': '55px',
                    'width': '406px',
                    'margin-left': '320px',
                    'font-size': '16px',
                    'display': 'block'
                });
                $(this).find('.dc').css({
                    'left': '222px',
                    'top': '43px',
                    'width': '99px',
                    'height': '101px',
                    'padding-left': '31px',
                    'padding-top': '35px',
                    'background': 'url("/images/discount_popa.png") 0 0 no-repeat'
                });
                $(this).find('.act_img').attr('src', '/images/ak.png');
                $(this).find('.act_img').addClass('pz');
                $(this).find('.act_img').removeClass('act_img');

            }
            i++;
        })
    }
</script>

<div class="row" style="margin-top:10px">
    <div id="product" name="product"></div>
    <div class="my-24">
        <ul id="catalog_main">
            <?=$result_categories ?>
        </ul>

        <div style="width: 98%;" class="margin_bottom-15">
            <?php for ($i = 1; $i <= 2; $i++): ?>
            
                <?php $ar_rand = rand(0, count($all_product['name'])-1); ?>

                <div style="text-align: center; color: #939393;" class="font_bold margin_bottom-15 margin_top-40"><?= $all_product['name'][$ar_rand] ?></div>

                <div class="special_img">
                    <a href="/site/productDetail/<?=$all_product['id'][$ar_rand]?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                </div>

                <div style="text-align: center;font-size:16px;color:#bfbfbf;margin-top: 5px" class="margin_bottom-25"><?= mb_substr($all_product['description'][$ar_rand], 0, 400).'...'; ?></div>

                <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="margin_bottom-25 description_left">Подробнее</a>

            <?php endfor ?>
        </div>
    </div>

    <div class="my-76_no_padding">
        <?php
		$sum = '<a href="/site/catalog">Каталог</a>';
	
        if (isset($_GET['id']))
        {
			$category_description = Category::model()->findByPk($_GET['id']);
			$desC = $category_description->category_description;
			
			$catP = Category::model()->findByPk($_GET['id']);
			$t3 = ' - ';
			$t3 .= '<a href="/site/catalog/?id='.$catP->id.'">'.$catP->name.'</a>';
            
			if ($catP->category_id != 0)
            {
				$secP = Category::model()->findByAttributes(array('id' => $catP->category_id));
				$t2 =' - ';
				$t2 .= '<a href="/site/catalog/?id='.$secP->id.'">'.$secP->name.'</a>';
				
				if ($secP->category_id != 0)
                {
					$frP = Category::model()->findByAttributes(array('id' => $secP->category_id));
					$t1 =' - ';
					$t1 .= '<a href="/site/catalog/?id='.$frP->id.'">'.$frP->name.'</a>';
				}
			}
        }
        else
        {
            $catD = ConfigMain::model()->findByPk(7);
            $desC = $catD->description;	
        }
        
		$sum .= $t1;
		$sum .= $t2;
		$sum .= $t3;
            
        if (isset($_GET['production']))
        {
            $production = Production::model()->findByPk($_GET['production']);
            $sum .= ' - Все товары производителя &ldquo;'.$production->name.'&rdquo;';
        }	
        
        $this->urlvar = $sum;
        ?>
        
        
        
        <!--
        <div id="filter">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'filter_form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                ),
                'action' => array('site/catalog'),
                'method' => 'GET',
            ));
            ?>

                <div class="name_filter">Цена:</div>
    			
    			<?=CHtml::hiddenField('id', (isset($_GET['id'])?$_GET['id']:'')) ?>
    
                <?= CHtml::textField('from', (isset($_GET['from'])?$_GET['from']:''), array('placeHolder' => 'от', 'class' => 'price_filter')) ?>
    
                <?= CHtml::textField('to', (isset($_GET['to'])?$_GET['to']:''), array('placeHolder' => 'до', 'class' => 'price_filter')) ?>
    
                <div class="clear"></div>
    
                <div class="name_filter">Производитель:</div>
    
                <?php $productions = Production::model()->allProduction; ?>
    
                <?= CHtml::dropDownList('productions', (isset($_GET['productions'])?$_GET['productions']:''), $productions, array('empty' => 'Все производители')); ?>
    
                <?= CHtml::submitButton('Найти', array(
                    'id' => 'submit_filter',
                    'name' => ''
                )); ?>

            <?php $this->endWidget(); ?>

        </div>
        -->
        
        <?php
        if ($scenario == 'category')
        {
            Yii::app()->clientScript->registerScript('category', "
                $('#catalogsort').change(function(){
                    var type = $(this).val();
                    $.fn.yiiListView.update('prod_cat', {
                        data: {catalogsort: type}
                    });
                    return false;
                });
            ");
        }
        else
        {
            Yii::app()->clientScript->registerScript('category', "
                $('#catalogsort').change(function(){
                    var type = $(this).val();
                    $.fn.yiiListView.update('prod_all', {
                        data: {catalogsort: type}
                    });
                    return false;
                });
            ");
        }
        ?>
        
        <div style="font-size: 14px; color: #868686; margin-bottom: 24px; padding-bottom: 7px; border-bottom: 1px solid #868686;">
            <div style="width: 114px; float: left; height: 23px; line-height: 23px;">Сортировать по: </div>
            <div style="width: 170px; float: left; height: 23px; line-height: 23px;">
                <div class="select_style select_style_st width170">
                    <select id="catalogsort" class="select_select select_select_st width170">
                        <option value="">умолчанию</option>
                        <option value="price_up">возрастанию цены</option>
                        <option value="price_down">убыванию цены</option>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        	
        <div class="row margin_bottom-24"><?= $desC; ?></div>
         
        <?php
        if ($scenario == 'category')
        {
            $this->widget('bootstrap.widgets.BsListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'id' => 'prod_cat',
                'afterAjaxUpdate' => 'function(){new_css();top_body()}',
                'pager' => array(
					//'afterAjaxUpdate' => 'function(){top_body()}',
                    'header' => '',
                    'prevPageLabel' => '',
                    'nextPageLabel' => '',
                    'maxButtonCount' => '6',
                    'firstPageLabel' => false,
                    'lastPageLabel' => false,
                    'htmlOptions' => array(
                        'class' => 'pagination',
                    )

                ),
            ));
        }
        
        if ($scenario == 'all')
        {
            $this->widget('bootstrap.widgets.BsListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_viewAll',
                'id' => 'prod_all',
                'afterAjaxUpdate' => 'function(){new_css();top_body()}',
                'pager' => array(
					//'afterAjaxUpdate' => 'function(){top_body()}',
                    'header' => '',
                    'prevPageLabel' => '',
                    'nextPageLabel' => '',
                    'maxButtonCount' => '6',
                    'firstPageLabel' => false,
                    'lastPageLabel' => false,
                    'htmlOptions' =>array(
                        'class' => 'pagination',
                    )
                ),
            ));
        }
        ?>
    </div>
</div>
