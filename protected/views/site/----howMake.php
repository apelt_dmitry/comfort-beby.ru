<?php
$meta = Meta::model()->findByPk(5);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>

<div class="row" style="margin-top:10px">

    <div class="my-24">
        <ul id="catalog_main">
            <?=$result_categories ?>
        </ul>

        <div id="popular" style="margin-top: 55px">

            <div id="slider2">
                <div class="header_popular">
                    <table>
                        <tr>
                            <td class="comment_padding">
                                <a class="buttons prev" href="#"></a>
                            </td>
                            <td>
                                <div id="comment_header" class="font_bold">Популярное</div>
                            </td>
                            <td class="comment_padding">
                                <a class="buttons next" href="#"></a>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="clear"></div>
                <div class="viewport">
                    <ul class="overview">
                        <?php foreach($popular as $val): ?>
                            <?php $product = $val->product?>
                            <?php if($product->mainImage!=null): ?>
                                <li>
                                    <a href="/site/productDetail/<?=$product->id ?>"><?= BsHtml::image('/uploads/product/preview/'.$product->mainImage->image) ?></a>
                                    <div class="popular_footer">
                                        <table>
                                            <tr>
                                                <td>
                                                    <a href="/site/productDetail/<?=$product->id ?>">Подробнее</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>

        <div style="width: 98%;" class="margin_bottom-15">
            <?php
            for ($i = 1; $i <= 2; $i++):
                $ar_rand = rand(0, count($all_product['name'])-1);
                ?>

                <div style="text-align: center; color: #939393;" class="font_bold margin_bottom-15 margin_top-40"><?= $all_product['name'][$ar_rand] ?></div>


                <div class="special_img">
                    <a href="/site/productDetail/<?=$all_product['id'][$ar_rand]?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                </div>

                <div style="text-align: center;font-size:16px;color:#bfbfbf;margin-top: 5px" class="margin_bottom-25"><?= substr($all_product['description'][$ar_rand], 0, 760).'...'; ?></div>

                <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="margin_bottom-25 description_left">Подробнее</a>

            <?php endfor ?>
        </div>
    </div>
    <div class="my-76">
        <div class="font_bold" style="font-size: 30px">Как сделать заказ:</div>
        <img src="/images/about2.png">
    </div>
</div>