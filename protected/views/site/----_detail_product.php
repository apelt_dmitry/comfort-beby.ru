<?php $this->urlvar = $sum; ?>

<div class="row">

    <div id="add_basket">Товар добавлен</div>

    <div class="my-61">

        <div class="row" style="height: 361px">
            <div class="my-70">
                <a href="/uploads/product/<?=$model->mainImage->image ?>" class='lightview' data-lightview-group='detail'><img src="/uploads/product/<?=$model->mainImage->image ?>" style="height: 310px;width: 410px"></a>
            </div>
            <div class="my-30" style="padding-left: 10px;margin-top: -24px;">

                <?php
                $i = 0;
                foreach($model->productImage as $val): ?>
                    <?php if($i>0 and $i<=3): ?>
                        <div><a href="/uploads/product/<?=$val->image ?>" class='lightview' data-lightview-group='detail'><img src="/uploads/product/preview/<?=$val->image ?>" style="width: 115px;height: 87px;margin-top: 24px"></a></div>
                    <?php endif ?>
                    <?php
                    if($i>3){
                        echo  '<div style="display: none"><a href="/uploads/product/'.$val->image.'" class="lightview" data-lightview-group="detail"><img src="/uploads/product/preview/'.$val->image.'" style="display:none;"></a></div>';
                    }
                    $i++;
                endforeach;?>

            </div>
        </div>

		<div class="row margin_top-30" style="width:90%"><?=$model->left_description ?></div>
		
    </div>

    <?php
    $cityPrice = $model->price;
    
    if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($model->productCityPrice) && !empty($model->productCityPrice))
    {
        foreach ($model->productCityPrice as $city_p)
        {
            if ($city_p->id_city == $_COOKIE['city'])
            {
                $cityPrice = $city_p->price;
            }
        }
    }
    ?>

    <div class="my-39 margin_bottom-25">
        <div class="row font_bold font_size-24" id="product_id" product_id="<?=$model->id ?>"><?=$model->product_name ?></div>
        <div class="row"><?=$model->description ?></div>
        <?php if( $model->act_id!=0 ): ?>
            <img src="/images/act.png" class="act_img adp"/>
        <?php endif ?>
        
        <?php if ($model->productDiscount->discount != null): ?>
            <div class="discount op">-<?=$model->productDiscount->discount ?>%</div>
            <div class="margin_top-20 float_left font_bold font_size-24 price_js" discount="<?=$model->productDiscount->discount ?>" base_price="<?=round((100-$model->productDiscount->discount*1)*$cityPrice/100, 0) ?>">
                Цена: <span style="text-decoration: line-through; color: #ff0000;font-size: 12px"><?=$cityPrice ?> р. </span>
                <?= round((100-$model->productDiscount->discount*1)*$cityPrice/100,0) ?> р.
            </div>
        <?php else: ?>
            <div class="margin_top-20 float_left font_bold font_size-24 price_js" base_price="<?=$cityPrice ?>">Цена: <?= $cityPrice ?> р.</div>
        <?php endif ?>

        <div class="detail_product margin_top-20 float_left font_size-20 act_product_id" style="margin-left: 48px;" act_id="<?= $model->act_id ?>"><a href="#" class="basket_ajax">В корзину</a> </div>
        <div class="clear"></div>
        <div>
            <?php
            $kuku = '';
            $i = 0;

            foreach($mas_pole['val'] as $key=>$val){
                echo '<div class="font_bold font_size-24 margin_top-40">'.$key.'</div>';

                if( $model->type_pole==1 ){
                    //echo '<input pole_id="'.$mas_pole['id'][$i].'" price_change="'.$mas_pole['price_change'][$key][$key2].'" value="'.$key2.'" type="radio" name="'.$key.'">';
                    echo '<div style="display: none"><input pole_id="" price_change="" value="" type="radio" name="" checked="checked" style="display:none" class="type_pole_input"></div>';
                    echo '<select class="listPole form-control" name="'.$key.'[]">';
                }
                //echo BsHtml::radioButtonListControlGroup($key,$mas_pole['id'][$j],$val,array('pole_id'=>$mas_pole['id'][$i], 'price_change'=>$mas_pole['price_change'][$i]));
                foreach($val as $key2=>$val2){

                    $checked = 'no';
                    if($key!= $kuku){
                        $j=$i;
                        $checked = 'yes';
                    }
                    $kuku = $key;
                    if( $model->type_pole==0 ){
                        if( $checked=='yes' ){
                            echo
                                '<div class="radio">
                            <label>
                                <input pole_id="'.$mas_pole['id'][$i].'" price_change="'.$mas_pole['price_change'][$key][$key2].'" value="'.$key2.'" type="radio" name="'.$key.'" checked="checked">
                                '.$val2.'
                            </label>
                        </div>';
                        }
                        else{
                            echo
                                '<div class="radio">
                            <label>
                                <input pole_id="'.$mas_pole['id'][$i].'" price_change="'.$mas_pole['price_change'][$key][$key2].'" value="'.$key2.'" type="radio" name="'.$key.'">
                                '.$val2.'
                            </label>
                        </div>';
                        }
                    }
                    else{
                        echo
                        '<option value="'.$key2.'" price_change="'.$mas_pole['price_change'][$key][$key2].'" class="opt_pole" name="'.$key.'">'.$val2.'</option>';
                    }

                }
                $i++;

                if( $model->type_pole==1 ){
                    echo '</select>';
                }
            }

            ?>

            <script>
                $('input[type="radio"]:checked').parent().css('background','url(/images/sf.png) no-repeat 0 7px');
            </script>
        </div>

        <?php if($model->production_id!=0): ?>

            <div class="production" style="margin-top: 10px">
                Производитель: <a href="/site/catalog?production=<?=$model->production->id;?>"><?=$model->production->name; ?></a>
            </div>

        <?php endif; ?>

		<div class="row margin_top-30"><?=$model->right_description ?></div>


    </div>

</div>
<div class="row" style="margin-top:40px">
<div class="row font_size-24 float_left">С этой моделью выбирают</div>
<hr class="float_left" style="border-top: 1px solid #d7d7d7; margin-left: 10px; margin-top: 19px;width: 645px"/>
</div>
<div class="row margin_top-20">
	
    <?php
	$ar_rang2 = array(); $ar_rand2 = range(0, count($new_all_product['name'])-1); shuffle($ar_rand2); 
    for ($i = 1; $i <= 6; $i++):
		if(count($new_all_product['name'])<6){
			$ar_rand = rand(0, count($new_all_product['name'])-1);
		}
		else{
			$ar_rand = $ar_rand2[$i];
		}
        //$ar_rand = rand(0, count($new_all_product['name'])-1);
        ?>

        <div class="my-50 margin_top-30">
            <a href="/site/productDetail/<?=$new_all_product['id'][$ar_rand] ?>"><img src="/uploads/product/preview/<?=$new_all_product['image'][$ar_rand] ?>" style="width: 150px;height: 125px;float: left"></a>
            <div style="float: left" class="margin_left-15">
                <div><?=$new_all_product['name'][$ar_rand] ?></div>

                <?php if( $new_all_product['discount'][$ar_rand]!=0 ){ ?>
                    <div class="discount small_size">-<?=$new_all_product['discount'][$ar_rand] ?>%</div>
                    <div>Цена: <span style="text-decoration: line-through; color: #ff0000;font-size: 12px"><?=number_format($new_all_product['price'][$ar_rand], 0, '.', ' ') ?> р. </span><?= number_format((100-$new_all_product['discount'][$ar_rand]*1)*$new_all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>
                <?php }
                else{
                    ?>

                    <div>Цена: <?= number_format((100-$new_all_product['discount'][$ar_rand]*1)*$new_all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>

                <?php } ?>
                <div class="margin_top-30"><a href="/site/productDetail/<?=$new_all_product['id'][$ar_rand] ?>" class="jopa">Подробнее</a></div>
            </div>
        </div>

    <?php endfor ?>
</div>

<!--end detail product-->



<div class="row margin_top-140 spss" style="background: #f8f8f8;padding: 20px 0;">
    <div class="row msps">
        <div class="my-30 margin_right" style="text-align: center">
            <img src="/images/machinka.png">
            <div class="margin_top-20 font_size-24">Срок доставки: <span style="color:#ff0000">24 часа</span></div>
        </div>

        <div class="my-30 margin_right" style="text-align: center">
            <img src="/images/shit.png">
            <div class="margin_top-20 font_size-24">Гарантия 18 месяцев</div>
        </div>

        <div class="my-30 margin_right" style="text-align: center">
            <img src="/images/sh.png">
            <div class="margin_top-20 font_size-24"> Стоимость сборки мебели: <span style="color:#ff0000">10%</span></div>
        </div>
    </div>
</div>


<div class="row margin_top-40">
    <div class="row">
        <div class="my-24 font_size-24">Мы рекомендуем:</div>
        <hr class="my-69" style="border-top: 1px solid #d7d7d7;  margin-top: 19px">
    </div>

    <div class="row">
        <div id="slider5">

            <div style="position: absolute;margin-left:-50px;margin-top: 105px;">
                <a class="buttons prev" href="#"></a>
                <a class="buttons next" href="#" style="margin-left: 910px;"></a>
            </div>

            <div class="clear"></div>
            <div class="viewport">
                <ul class="overview">
                    <?php
                    foreach($allR as $val): ?>
                        <li style="position: relative">
                            <a href="/site/productDetail/<?=$val['id'] ?>"><?= BsHtml::image('/uploads/product/preview/'.$val['image']) ?></a>

                            <div style="position: absolute; top: 190px;left: 62px;">

                                <?php if( $val['discount']!=0 ){ ?>
                                    <div class="discount sldops"><?=$val['discount'] ?>%</div>
                                    <div class="sps sp" style="width: 105px"><?= number_format((100-$val['discount']*1)*$val['price']/100, 0, '.', ' ') ?> р.</div>
                                <?php }
                                else{
                                    ?>

                                    <div class="sps sp" style="width: 105px"><?=number_format($val['price'], 0, '.', ' ') ?> р.</div>

                                <?php } ?>

                                <div class="sps" style="width: 105px"><a href="/site/productDetail/<?=$val['id'] ?>">Подробнее</a></div>
                            </div>

                        </li>
                    <?php
                    endforeach ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row margin_top-20">
    <?php
    for ($i = 1; $i <= 6; $i++):
        $ar_rand = rand(0, count($all_product['name'])-1);
        ?>

        <div class="my-50 margin_top-30">
            <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>"><img src="/uploads/product/preview/<?=$all_product['image'][$ar_rand] ?>" style="width: 150px;height: 125px;float: left"></a>
            <div style="float: left" class="margin_left-15">
                <div><?=$all_product['name'][$ar_rand] ?></div>

                <?php if( $all_product['discount'][$ar_rand]!=0 ){ ?>
                    <div class="discount small_size">-<?=$all_product['discount'][$ar_rand] ?>%</div>
                    <div>Цена: <span style="text-decoration: line-through; color: #ff0000;font-size: 12px"><?=number_format($all_product['price'][$ar_rand], 0, '.', ' ') ?> р. </span><?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>
                <?php }
                else{
                    ?>

                    <div>Цена: <?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>

                <?php } ?>
                <div class="margin_top-30"><a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="jopa">Подробнее</a></div>
            </div>
        </div>

    <?php endfor ?>
</div>