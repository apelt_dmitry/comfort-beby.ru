<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerCssFile($pt.'css/interActive.css');

$cs
    ->registerScriptFile($pt.'js/interActive.js');

$cords = array(
    'man' => array(
        '1' => array(
            'top' => '24.8%',
            'left' => '2.8%',
        ),
        '2' => array(
            'top' => '61.8%',
            'left' => '7.8%',
        ),
        '3' => array(
            'top' => '28.8%',
            'left' => '19.7%',
        ),
        '4' => array(
            'top' => '18%',
            'left' => '20.9%',
        ),
        '5' => array(
            'top' => '37.8%',
            'left' => '25%',
        ),
        '6' => array(
            'top' => '68%',
            'left' => '30.8%',
        ),
        '7' => array(
            'top' => '45%',
            'left' => '38.7%',
        ),
        '8' => array(
            'top' => '26.8%',
            'left' => '39.8%',
        ),
        '9' => array(
            'top' => '28.5%',
            'left' => '49.6%',
        ),
        '10' => array(
            'top' => '55.5%',
            'left' => '55.6%',
        ),

        '11' => array(
            'top' => '44%',
            'left' => '64.8%',
        ),
        '12' => array(
            'top' => '24.4%',
            'left' => '65.6%',
        ),
        '13' => array(
            'top' => '29%',
            'left' => '79.1%',
        ),


    )
);
$meta = Meta::model()->findByPk(7);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
$image = InterImage::model()->findByPk(2);
?>

<script>
    $(document).ready(function(){
        $('.bg_image_main').hide();
        $('body').scrollTop(124);
    })
</script>
<div class="man_room size_room">
    <img src="/uploads/product/<?=$image->image ?>" style="position: absolute;left: 76%;"/>
    <div class="name_room" scenario="man">
        <div>Комната для мальчиков «Корсар»</div>
        <a href="/site/catalog#product" class="another_room">Посмотрите другие комнаты</a>
    </div>
    <?php
    foreach($cords['man'] as $key => $val):
        $top = $val['top'];
        $left = $val['left'];
        ?>
        <div class="plus_room" id="<?= $key ?>" style="position: absolute;top:<?= $top ?>;left:<?= $left ?>"><img class="plus_room" src="/images/plus_room.png"></div>
    <?php endforeach ?>
    <div class="view">
        <div class="cross"></div>
        <div class="st"></div>
        <div class="description_room">

            <div class="description_text">
                Полка навесная «Леди»
                предоставляет возможность по максимуму использовать пространство детской
                комнаты, занимая свободное место на стенах. Легкая полка «Леди» непринужденно
                монтируется на любую стену
                и удобна во всём.
            </div>

            <div style="background: #006886;padding-bottom: 20px;">
                <!--<div class="after_description size">Размер</div>-->
                <div class="after_description price">Цена:</div>
                <a href="#" class="link_2 basket2 basket_href">В корзину</a>
                <a href="/site/catalog#product" class="link_2">Каталог</a>
            </div>

        </div>
    </div>
</div>