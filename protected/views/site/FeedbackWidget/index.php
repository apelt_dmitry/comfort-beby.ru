<div style="display: none;">
    <div class="box-modal" id="phoneModal">

        <div class="phone_modal clear-fix">
            <div class="box-modal_close arcticmodal-close phone_modal_close"></div>
            <div class="phone_modal_title">
                Заказать звонок
            </div>
            <div class="phone_modal_under_title">
                Заполните форму и отправте нам. Наш менеджер<br/>
                свяжется с Вами в ближайшее время
            </div>
            <form class="phone_form" name="" action="">
                <div class="form_name_bgr">
                    <input class="form_name cb" placeholder="Ваше Имя" name="phone_name">

                    <div class="red_star name_red_star"></div>
                </div>
                <div class="form_phone_bgr">
                    <input class="form_phone cb" placeholder="Ваш телефон" name="phone_phone">

                    <div class="red_star phone_red_star"></div>
                </div>
                <div class="form_email_bgr">
                    <input class="form_email cb" placeholder="Ваш email" name="phone_email">

                    <div class="red_star email_red_star"></div>
                </div>

                <div class="warning3"><span>* </span>обязательные поля для заполнения</div>
                <input formaction="" type="submit" value="" class="phone_submit" name="ordering_submit">
            </form>

        </div>
    </div>
    <div class="box-modal" id="TYModal">

        <div class="phone_modal clear-fix">
            <div class="box-modal_close arcticmodal-close phone_modal_close"></div>
            <div class="smile_women"></div>
            <div class="smile_text">
                Благодарим Вас за прояленный<br/>
                интерес. Наш менеджер свяжется<br/>
                с Вами в ближайшее время<br/>
            </div>
        </div>
    </div>
    <div class="box-modal" id="exampleModal">

        <div class="main_modal clear-fix">
            <div class="box-modal_close arcticmodal-close main_modal_close"></div>
            <div class="main_modal_title">Товар добавлен в корзину</div>
            <div class="main_modal_description">Вы можете продолжить покупки или <br/>оформить заказ</div>
            <div class="main_modal_button"></div>
        </div>
    </div>
</div>