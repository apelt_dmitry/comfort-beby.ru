<!-- NSV file -->
<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

//$cs->registerCssFile($pt.'css/slider.css');
//$cs->registerScriptFile($pt.'js/slider.js');

$meta = Meta::model()->findByPk(2);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
mb_internal_encoding("UTF-8");
?>


<?php
$sum = '<a href="/site/catalog">Каталог</a>';

if (isset($_GET['id'])) {
    $id_current = $_GET['id'];
    $category_description = Category::model()->findByPk($_GET['id']);
    $desC = $category_description->category_description;
    $nameC = $category_description->name;

    $catP = Category::model()->findByPk($_GET['id']);
    $t3 = ' - ';
    $t3 .= '<a href="/site/catalog/?id=' . $catP->id . '">' . $catP->name . '</a>';

    if ($catP->category_id != 0) {
        $secP = Category::model()->findByAttributes(array('id' => $catP->category_id));
        $t2 = ' - ';
        $t2 .= '<a href="/site/catalog/?id=' . $secP->id . '">' . $secP->name . '</a>';

        $nameC = $secP->name . ', ' . $nameC;

        if ($secP->category_id != 0) {
            $frP = Category::model()->findByAttributes(array('id' => $secP->category_id));
            $t1 = ' - ';
            $t1 .= '<a href="/site/catalog/?id=' . $frP->id . '">' . $frP->name . '</a>';
        }
    }
} else {
    $catD = ConfigMain::model()->findByPk(7);
    $desC = $catD->description;
}

$sum .= $t1;
$sum .= $t2;
$sum .= $t3;

$nameC = mb_convert_case($nameC, MB_CASE_TITLE, "UTF-8");

if (isset($_GET['production'])) {
    $production = Production::model()->findByPk($_GET['production']);
    $sum .= ' - Все товары производителя &ldquo;' . $production->name . '&rdquo;';
}

$this->urlvar = $sum;


$criteria = new CDbCriteria;
$criteria->condition = 'category_id= 0';
$criteria->order = 't.id DESC';
$base_category = Category::model()->findAll($criteria);

$criteria = new CDbCriteria;
$criteria->condition = 'category_id!= 0';
//$criteria->order = 't.id DESC';
$pod_category = Category::model()->findAll($criteria);
$pod_category = CHtml::listData($pod_category, 'id', 'name', 'category_id');

$hit_prodaj = Yii::app()->db->createCommand('SELECT  p.product_name, pi.image, d.discount, p.id, p.price
                                        FROM ProductCategory pc
                                        LEFT JOIN product p ON p.id=pc.product
                                        LEFT JOIN product_image pi ON pi.product_id = pc.product
                                        LEFT JOIN discount d ON d.id= p.discount_id
                                        WHERE pc.category=32
                                        GROUP BY pi.product_id
                                        LIMIT 4')->queryAll();
?>

<div class=" m_container m_breadcrumbs">
    <a href="/" style="text-decoration: none">Главная</a> ><?php if (isset($nameC)): ?><?= $nameC ?><?php endif; ?>
</div>

<div class="m_dropSlide_catalog">
    <div class="clear-fix m_container">
        <div class="m_drop_slide_box ">
            <div class="m_drop_slide_title clear-fix">
                <span>Подкатегории</span>
                <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
            </div>
            <div class="m_drop_slide_content clear-fix" style="display: none">
                <div class="m_under_category">
                    <span><a href="<?php echo Yii::app()->createUrl('/site/catalog',array('id'=>$id_current));?>"><?php if (isset($nameC)): ?><?= $nameC ?><?php endif; ?></a></span><br/>
                    <?php foreach($pod_category[$id_current] as $key=>$val):?>
                    <a style="text-transform: capitalize;" href="<?php echo Yii::app()->createUrl('/site/catalog',array('id'=>$key));?>"><?php echo $val;?></a><br/>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="m_drop_slide_box ">
                <div class="m_drop_slide_title clear-fix">
                    <span>Описание</span>
                    <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
                </div>
                <div class="m_drop_slide_content clear-fix" style="display: none">
                    <div class="m_some_review">
                        <?php if (isset($desC)): ?><?= $desC ?><?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="m_catalog_filter">
    <div class="m_container clear-fix">
        <div class="m_catalog_filter_list clear-fix">
            <?php
            $dataProvider->pagination->pageSize= 10;
            if ($scenario == 'category') {
                $this->widget('bootstrap.widgets.BsListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_m_view',
                    'id' => 'prod_cat',
                    'afterAjaxUpdate' => 'function(){new_css();top_body()}',
                    'template' => '
                        <div class="catalog_top_pagination">{pager}</div>
                        {items}
                        <div class="catalog_bot_pagination"> {pager}</div>',
                        'pager' => array(
                        //'afterAjaxUpdate' => 'function(){top_body()}',
                        'header' => '',
                        'prevPageLabel' => '<',
                        'previousPageCssClass' => 'm_catalog_filter_button m_next',
                        'nextPageLabel' => '>',
                        'nextPageCssClass' => 'm_catalog_filter_button m_next',
                        'maxButtonCount' => '6',
                        'firstPageLabel' => '|<',
                        'firstPageCssClass' => 'm_catalog_filter_button',
                        'lastPageLabel' => '>|',
                        'lastPageCssClass' => 'm_catalog_filter_button',
                        'internalPageCssClass' => 'm_catalog_filter_button',
                        'htmlOptions' => array(
                            'class' => 'm_catalog_filter_navigate',
                        )
                    ),
                ));
            }
            if ($scenario == 'all') {
                $this->widget('bootstrap.widgets.BsListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_m_viewAll',
                    'id' => 'prod_all',
                    'afterAjaxUpdate' => 'function(){new_css();top_body()}',
                    'template' => '
                    <div class="catalog_top_pagination">{pager}</div>
                        {items}
                    <div class="catalog_bot_pagination"> {pager}</div>',
                    'pager' => array(
                        //'afterAjaxUpdate' => 'function(){top_body()}',
                        'header' => '',
                        'prevPageLabel' => '<',
                        'previousPageCssClass' => 'article_navigate_button',
                        'nextPageLabel' => '>',
                        'nextPageCssClass' => 'article_navigate_button',
                        'maxButtonCount' => '6',
                        'firstPageLabel' => '|<',
                        'firstPageCssClass' => 'article_navigate_button',
                        'lastPageLabel' => '>|',
                        'lastPageCssClass' => 'article_navigate_button',
                        'internalPageCssClass' => 'article_navigate_button',
                        'htmlOptions' => array(
                            'class' => 'article_navigate',
                        )
                    ),
                    //'pagerCssClass' => 'catalog_bot_pagination',
                ));
            }
            ?>

        </div>
    </div>
</div>

<div class="m_popular_product_box">
    <div class="m_container clear-fix">
        <div class="m_popular_product_title">
            <span>Популярные товары</span>
        </div>
        <div class="m_popular_slider">
            <?php foreach ($hit_prodaj as $prod):?>
                <div class="m_product_box">
                    <a href="/site/productDetail/<?=$prod['id'] ?>" class="product_image">
                        <img class="m_product_box_image" src="/uploads/product/preview/<?= $prod['image'] ?>">
                    </a>
                    <div class="m_product_box_name"><?= $prod['product_name'] ?></div>
                    <div class="m_product_box_price"><?= $prod['price'] ?> р.-</div>
                    <div class="m_product_line"></div>
                    <div class="m_product_button buy_btn_main" product_id="<?=$prod['id'] ?>"></div>
                    <div class="m_hit"></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="m_main_why">
    <div class="clear-fix m_container">
        <div class="m_main_why_yellow">Почему вам нужно покупать детскую</div>
        <div class="m_main_why_black">мебель в нашем интернет магазине</div>
        <img class="m_note_cart" src="/images/m_note_cart.png">
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_1.png">

    <div class="m_main_advantage_text_box">
        <span>Гарантия 18 месяцев</span>
        Мы даем официальную гарантию на любой вид
        детской мебели. Можете не переживать, когда
        ваши детки прыгают на новой кровати
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_2.png">

    <div class="m_main_advantage_text_box">
        <span>Быстрая доставка</span>
        Если вы проживаете в Москве или области, то
        детскую мебель привезут и соберут уже на
        следующий день. В другие районы России заказ
        прибудет с надежной транспортной компанией
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_3.png">

    <div class="m_main_advantage_text_box">
        <span>Вы ничем не рискуете</span>
        Потому что платите за детскую комнату или
        мебель только после того, как вам ее доставят.
        Никакой предоплаты!
    </div>
</div>
<div style="margin-top: 50px"></div>