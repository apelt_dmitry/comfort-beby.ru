<!-- NSV file -->
<?php
mb_internal_encoding("UTF-8");
if (strlen($data->Product->description) <= 350) {
    $description = $data->Product->description;
} else {
    $description = mb_substr($data->Product->description, 0, 350) . '...';
}
?>

<div class="m_product_box">
    <a href="/site/productDetail/<?= $data->Product->id ?>">
        <img class="m_product_box_image" src="/uploads/product/preview/<?php echo $data->Product->mainImage->image ?>">
    </a>

    <div class="m_product_box_name"><?= $data->Product->product_name ?></div>
    <div class="m_product_box_price">
        <?php
        $cityPrice = $data->Product->price;
        if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->Product->productCityPrice) && !empty($data->productCityPrice)) {
            foreach ($data->Product->productCityPrice as $city_p) {
                if ($city_p->id_city == $_COOKIE['city']) {
                    $cityPrice = $city_p->price;
                }
            }
        }
        ?>
        <?php if ($data->Product->productDiscount->discount != null): ?>
            <strike ><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike>
            <span style="color: darkred;"><?= number_format((100 - $data->Product->productDiscount->discount * 1) * $cityPrice / 100, 0, '.', ' ') ?>
                р.</span>
        <?php else: ?>
            <?= number_format($cityPrice, 0, '.', ' ') ?> р.
        <?php endif; ?>
    </div>
    <div class="m_product_line"></div>
    <div class="m_product_button buy_btn_main" product_id="<?= $data->id ?>"></div>
    <?php if ($data->Product->productDiscount->discount != null): ?>
        <div class="m_sale"></div>
    <?php endif; ?>
    <?php ProductCategory::model()->findByAttributes(array(
        'category' => $category,
        'product' => $product,
    ))?>
    <?php if (ProductCategory::model()->findByAttributes(array(
        'category' => 32,
        'product' => $data->Product->id,
    ))
    ): ?>
        <div class="m_hit"></div>
    <?php endif; ?>
    <?php if (ProductCategory::model()->findByAttributes(array(
        'category' => 33,
        'product' => $data->Product->id,
    ))
    ): ?>
        <div class="m_new"></div>
    <?php endif; ?>

</div>

