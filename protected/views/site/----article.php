<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerCssFile($pt.'css/slider.css');


$cs
    ->registerScriptFile($pt.'js/slider.js');



$meta = Meta::model()->findByPk(3);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>
<?php
$view = '_view_article';
if($scenario == 'detail_news'){
    $view = '_view_detail_article';
}
mb_internal_encoding("UTF-8");
?>
<div id="news" name="news"></div>
<div class="row" style="margin-top:10px">
    <div id="product" name="product"></div>
    <div class="my-24">
        <ul id="catalog_main">
            <?=$result_categories ?>
        </ul>

        <div style="width: 98%;" class="margin_bottom-15">
            <?php
            for ($i = 1; $i <= 1; $i++):
                $ar_rand = rand(0, count($all_product['name'])-1);
                ?>

                <div style="text-align: center; color: #939393;" class="font_bold margin_bottom-15 margin_top-40"><?= $all_product['name'][$ar_rand] ?></div>


                <div class="special_img">
                    <a href="/site/productDetail/<?=$all_product['id'][$ar_rand]?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                </div>

                <div style="text-align: center;font-size:16px;color:#bfbfbf;margin-top: 5px" class="margin_bottom-25"><?= mb_substr($all_product['description'][$ar_rand], 0, 350).'...'; ?></div>

                <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="margin_bottom-25 description_left">Подробнее</a>



            <?php endfor ?>
        </div>
    </div>

    <div class="my-76_no_padding">
        <?php $this->widget('bootstrap.widgets.BsListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>$view,
            'afterAjaxUpdate' => 'function(){new_css()}',
            'pager'=>array(
                'header' => '',
                'prevPageLabel' => '',
                'nextPageLabel' => '',
                'maxButtonCount' => '4',
                'firstPageLabel'=> false,
                'lastPageLabel'=> false,
                'htmlOptions'=>array(
                    'class' => 'pagination',
                )

            ),
        )); ?>
    </div>
</div>