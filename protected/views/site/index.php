<!-- NSV file -->
<div class="main_slider clear-fix">
    <div class="top_slider">
        <?php foreach($slide as $val) :?>
        <div class="top_slider_slide" >
            <!-- Задний фон вынесен для возмжности замены из бд -->
            <div style="height: 369px; background: url('/uploads/Slide/<?= $val->preview ?>') center center no-repeat"></div>
            <a href="/site/catalog" class="button_want_present" style="z-index: 1"></a>
        </div>
        <?php endforeach; ?>
    </div>

</div>
<div class="promo_bgr clear-fix">
    <div class="container">
        <?php $banner = Banner::model()->findByPk(1); ?>
        <div class="promo_box">
            <?php if(!empty($banner->banner1)):?>
                <?php echo $banner->banner1;?>
            <?php endif;?>

        </div>
        <div class="promo_box">
            <?php if(!empty($banner->banner2)):?>
                <?php echo $banner->banner2;?>
            <?php endif;?>

        </div>
        <div class="promo_box">
            <?php if(!empty($banner->banner3)):?>
                <?php echo $banner->banner3;?>
            <?php endif;?>

        </div>
        <div class="promo_box">
            <?php if(!empty($banner->banner4)):?>
                <?php echo $banner->banner4;?>
            <?php endif;?>

        </div>
    </div>
</div>

<?php  
    //$arr = CHtml::listData($category_publish, 'publish_id', 'publish_id');
    //echo '<pre>'; var_dump($arr); 
?>  

<div class="main_catalog clear-fix">
    <div class="container">
        <?php $this->widget('application.widgets.CatalogWidget'); ?>
        <div class="main_catalog_right">
        
            <?php for ($i = 1; $i <= 30; $i++):?>
                <?php $this->renderPartial('_all_product_view', array('all_product'=>$all_product)); ?>
            <?php endfor; ?>

        </div>
    </div>
</div>
<div class="why_we">
    <div class="container">
        <div class="why_we_bgr"></div>
        <div class="why_we_box">
            <div class="word_box">
                <div class="angle_top"></div>
                <div class="angle_down"></div>
                <div class="why_we_word">Почему вам нужно покупать<br/>детскую мебель в нашем<br/>интернет магазине
                </div>
            </div>
            <div class="why_we_title"><img src="/images/ok.png">Вы ничем не рискуете</div>
            <div class="why_we_text">Потому что платите за детскую комнату или <br/> мебель только после того, как вам
                ее <br/> Никакой предоплаты!
            </div>
            <div class="why_we_title"><img src="/images/ok.png">Гарантия 18 месяцев</div>
            <div class="why_we_text">Мы даем официальную гарантию на любой вид<br/> детской мебели. Можете не
                переживать, когда<br/> ваши детки прыгают на новой кровати
            </div>
            <div class="why_we_title"><img src="/images/ok.png">Быстрая доставка</div>
            <div class="why_we_text">Если вы проживаете в Москве или области, то<br/> детскую мебель привезут и соберут
                уже на<br/> следующий день. В другие районы России заказ<br/> прибудет с надежной транспортной компанией
            </div>
        </div>
    </div>
</div>
<div class="reviews clear-fix">
    <div class="container clear-fix">
        <div class="reviews_word_box">
            <div class="angle_top"></div>
            <div class="angle_down"></div>
            <div class="why_we_word">Что говорят о нашей<br/>работе наши покупатели</div>
        </div>
        <img class="review_img animated " src="/uploads/icon/preview/c062d10fadabac80e5d4e9a0d5a7.png" />

        <div class="reviews_slider">
        
        <?php foreach($comment as $val): ?>
            <div class="review_box">
                <div class="review_name">
                    <?php echo $val->name; ?>
                </div>
                <div class="review_date">
                    <?php echo Yii::app()->dateFormatter->formatDateTime($val->date, 'long', false); ?>
                </div>
                <div class="big_skoba">“</div>
                <div class="review_review">
                <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $val->comment)), 0, 300).'...'; ?>
                </div>
            </div>
        <?php endforeach; ?>

        </div>

    </div>
</div>
<div class="ledy clear-fix">
    <div class="container">
        <div class="ledy_bgr"></div>
        <div class="ledy_column_1">
        
            <!-- Статьи -->
            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <div class="ledy_box">
                <?= BsHtml::image('/uploads/product/preview/'.$article[$ar_rand]['image']) ?>

                <div class="ledy_box_text">
                    <a href="#" class="ledy_box_title"><?php echo $article[$ar_rand]['title']; ?></a>

                    <div class="ledy_box_description">
                    <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $article[$ar_rand]['description'])), 0, 100).'...'; ?>
                    </div>
                    <a href="/site/article/<?php echo $article[$ar_rand]['id']?>"><div class="ledy_box_see"></div></a>
                </div>
            </div>
            
            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <div class="ledy_box">
                <?= BsHtml::image('/uploads/product/preview/'.$article[$ar_rand]['image']) ?>

                <div class="ledy_box_text">
                    <a href="#" class="ledy_box_title"><?php echo $article[$ar_rand]['title']; ?></a>

                    <div class="ledy_box_description">
                    <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $article[$ar_rand]['description'])), 0, 100).'...'; ?>
                    </div>
                    <a href="/site/article/<?php echo $article[$ar_rand]['id']?>"><div class="ledy_box_see"></div></a>
                </div>
            </div>
        </div>
        <div class="ledy_column_2">
            <div class="ledy_box_word">
                <div class="word_box">
                    <div class="angle_top"></div>
                    <div class="angle_down"></div>
                    <div class="why_we_word">Полезные статьи, которые<br/>помогут вам выбрать мебель</div>
                </div>
            </div>

            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <div class="ledy_box_right">
                <?= BsHtml::image('/uploads/product/preview/'.$article[$ar_rand]['image']) ?>

                <div class="ledy_box_text_right">
                    <a href="#" class="ledy_box_title"><?php echo $article[$ar_rand]['title']; ?></a>

                    <div class="ledy_box_description">
                    <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $article[$ar_rand]['description'])), 0, 100).'...'; ?>
                    </div>
                    <a href="/site/article/<?php echo $article[$ar_rand]['id']?>"><div class="ledy_box_see"></div></a>
                </div>
            </div>
        </div>
    </div>
</div>