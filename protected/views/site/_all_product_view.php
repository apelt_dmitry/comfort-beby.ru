<!-- NSV FILE -->
<!-- Файл одного продукта, используется в списках рендомного выбора:
на главной, в корзине
-->

<?php $ar_rand = rand(0, count($all_product['name']) - 1); ?>

<div class="product_box">
    <a href="/site/productDetail/<?= $all_product['id'][$ar_rand] ?>" class="product_image">
        <?php if ($all_product['discount'][$ar_rand] != 0): ?>
            <div class="stock"><?= $all_product['discount'][$ar_rand]; ?>%</div>
        <?php endif; ?>
        <!-- <div class="new"></div>  Для новинок, использовать без верхнего дива stock-->
        <div class="glaz"></div>
        <img src="/uploads/product/preview/<?= $all_product['image'][$ar_rand] ?>"/>
    </a>

    <div class="product_name"><?= $all_product['name'][$ar_rand] ?></div>
    <?php if ($all_product['discount'][$ar_rand] != 0) { ?>
        <div class="product_price">
            <strike>
                <?= number_format($all_product['price'][$ar_rand], 0, '.', ' ') ?>р.
            </strike>
            <span>
                <?= number_format((100 - $all_product['discount'][$ar_rand] * 1) * $all_product['price'][$ar_rand] / 100, 0, '.', ' ') ?>р.
            </span>
        </div>

    <?php
    } else {
        ?>
        <div class="product_price">
            <?= number_format((100 - $all_product['discount'][$ar_rand] * 1) * $all_product['price'][$ar_rand] / 100, 0, '.', ' ') ?>р.
        </div>
    <?php } ?>

    <div class="product_line"></div>
    <div class="button_buy buy_btn_main" product_id="<?= $all_product['id'][$ar_rand] ?>" onclick="animate_cart_my(this);"></div>
    <!-- <div class="hit"></div> Для хита продаж-->
</div>