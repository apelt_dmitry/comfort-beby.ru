<!-- NSV file -->
<?php
$meta = Meta::model()->findByPk(4);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
$config = ConfigMain::model()->findAll();
        $i = 1;
        foreach($config as $val){
            $text[$i]['title'] = $val->title;
            $text[$i]['description'] = $val->description;
            $i++;
        }
?>

<div class=" m_container m_breadcrumbs">
    <a href="/" style="text-decoration: none">Главная</a> > <a href="/site/about" style="text-decoration: none">О нас</a>
</div>

<div class="m_about_us_family m_container clear-fix">
    <div class="m_family_top_black">Интернет магазин «Комфорт»</div>
    <div class="m_family_bot_black">выбирают родители, которые</div>
    <div class="m_family_bot_yellow">думают о счастье своих детей</div>
</div>

<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_1" src="/images/m_advantage_img_1.png">
    <div class="m_advantage_text_box">
        <span>10 лет на рынке</span>
        За 10 лет, мы приобрели огромный опыт работы. Это позволило нам выстроить идеальную схему сотрудничества с клиентами
    </div>
</div>
<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_2" src="/images/m_advantage_img_2.png">
    <div class="m_advantage_text_box">
        <span>Быстрая доставка</span>
        Если вы проживаете в Москве или области, то детскую мебель привезут и соберут уже на следующий день. В другие районы России заказ прибудет с надежной транспортной компанией
    </div>
</div>
<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_3" src="/images/m_advantage_img_3.png">
    <div class="m_advantage_text_box">
        <span>Гарантия 18 месяцев</span>
        Мы даем официальную гарантию на любой вид детской мебели. Можете не переживать, когда ваши детки прыгают на новой кровати
    </div>
</div>
<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_4" src="/images/m_advantage_img_4.png">
    <div class="m_advantage_text_box">
        <span>Наличие товара</span>
        Вы не узнаете никогда, что такое выбрать понравившуюся детскую мебель и не купить ее, потому что ее нет в наличии. Мы выставляем на продажу только ту детскую мебель, которая есть в наличии здесь и сейчас
    </div>
</div>

<div class="m_clients_trust_box m_container clear-fix">
    <div class="m_clients_trust_black">Клиенты нам доверяют</div>
    <div class="m_yoga_women"></div>
    <div class="m_clients_trust_text">
        <span>89% покупателей рекомендуют нашу детскую мебель своим родственникам или друзьям.</span>
        Магазин «Комфорт» на рынке уже более 10 лет. Наши покупатели прекрасно понимают, что только качественная детская мебель, обслуживание на высшем уровне, гарантия 18 месяцев и доступные цены могут удержать компанию на плаву столько лет. Но, главное, вся мебель для детской комнаты изготовлена из экологически чистого материала, поэтому вы можете не переживать за здоровье своего малыша, пока он находится дома. Богатый выбор и доступные цены.
    </div>
</div>

<div class="m_company_recvizits">
    <div class="clear-fix m_container">
        <div class="m_company_recvizits_text">
            <span>Реквизиты компании: </span>
            Магазин "Комфорт", ул. Рябиновая, 41, корп.1,<br/>
            ОГРН: 1097746103388, ИНН: 7723707852, ООО "МебТорг"<br/>
            ЮР.АДРЕС: МОСКВА, ул. АЛЕКСЕЯ ДИКОГО,<br/>
            д. 5, пом. 1, shop@comfort-beby.ru<br/>
            тел.: +7 (495) 374-95-78, 8 (800) 555-87-06
        </div>
    </div>
</div>
