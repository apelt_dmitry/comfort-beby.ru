<?php 
Yii::app()->name = $data->meta_title;
?>

<!-- внизу новый код -->
<div class="news_right_box clear-fix">
    <div class="box_news">
        <?=BsHtml::image('/uploads/product/'.$data->image); ?>

        <div class="news_box_text">
            <div class="news_box_title">
                <?=$data->title ?>
            </div>
            <div class="news_box_description">
                <?=$data->description ?>
            </div>
        </div>

    </div>

</div>