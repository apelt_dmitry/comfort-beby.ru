<?php
$meta = Meta::model()->findByPk(6);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>

<script>
    $(document).ready(function () {
        $('.basket_count').click(function () {
            var cost = $('.cost[product_id=' + $(this).attr('product_id') + ']').html();
            var count = $('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html();
            if ($(this).hasClass('bsr')) {
                if ($('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html() * 1 >= 2) {
                    $('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html($('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html() * 1 - 1);
                }
            }
            else {
                $('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html($('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html() * 1 + 1);
            }
            $('.cost[product_id=' + $(this).attr('product_id') + ']').html((cost / count) * $('.product_count_new[product_id=' + $(this).attr('product_id') + ']').html());
            count_down();
            count_post2();
        });
        $('#next_order').click(function () {
            $('#next_step').show();
            $('body').animate({scrollTop: 1150}, 1000);
            $('html').animate({scrollTop: 1150}, 1000)
        });
        count_down();

        $('.button_client').click(function () {
            $.cookie('basket', null, {path: '/'});
        });
        $('.kokoko[kokoko="1"]').css({
            'border-top': 'none',
            'padding-top': 0
        });
        count_post();
        delete_from_basket();
        new_input();


    });

    function count_post2() {
        var i = 0;
        var count_post = [];
        $('.product_count_new').each(function () {
            count_post[$(this).attr('product_id')] = $(this).html();
            i++;
        });
        var id = product_id();
        for (var j in id) {
            $('input[name="product_count_new[' + id[j] + ']"').attr('value', count_post[id[j]])
        }
        /*var j = 0;
         $('input[name="product_count_new[]"]').each(function(){
         $(this).attr('value',count_post[j]);
         $(this).attr('name','product_count_new['+$(this).attr('product_id')+']' );
         j++;
         })*/

    }


    function count_post() {
        var i = 0;
        var count_post = [];
        $('.product_count_new').each(function () {
            count_post[i] = $(this).html();
            i++;
        });
        var j = 0;
        $('input[name="product_count_new[]"]').each(function () {
            $(this).attr('value', count_post[j]);
            $(this).attr('name', 'product_count_new[' + $(this).attr('product_id') + ']');
            j++;
        })

    }

    function delete_from_basket() {
        $('.delete_from_basket').click(function () {
            $(this).parent().parent().hide();
            var product_id = $(this).attr('product_id');
            $('.cost[product_id=' + product_id + ']').html('0');
            $('.product_count_new[product_id=' + product_id + ']').html('0');
            count_down();
            var new_product = [];
            var j = 0;
            var products = JSON.parse($.cookie('basket'));
            for (var i in products) {
                if (product_id != products[i]['product_id']) {
                    new_product[j] = products[i];
                    j++;
                }
            }
            $.cookie('basket', JSON.stringify(new_product), {expires: 30, path: '/'});
            basket();
            if (JSON.parse($.cookie('basket')) == '') {
                $('.spss').hide();
                $('.wfg').show();
                $.cookie('basket', null, {path: '/'});
            }
        });
    }


    function count_down() {
        var html = 0 / 1;
        $('.cost').each(function () {
            html += $(this).html() * 1;
        });
        $('#count_down').html(html);
    }

    function pole_basket() {
        var pole_basket = [];
        var id = 0;
        var i = 0;
        $('.pole_basket').each(function () {
            if (id != $(this).attr('product_id')) {
                i++;
                pole_basket[i] = '';
            }
            id = $(this).attr('product_id');
            pole_basket[i] += $(this).html() + '; ';
        });
        return pole_basket;
    }

    function new_input() {
        var id = product_id();
        var pole = pole_basket();
        for (var i in id) {
            $('input[name="pole[]"]').each(function () {
                if ($(this).attr('product_id') == id[i]) {
                    $(this).attr('value', pole[i]);
                    $(this).attr('name', 'pole[' + id[i] + ']');
                }
            })
        }
    }

    function product_id() {
        var product_id = [];
        var id = 0;
        var i = 0;
        $('.pole_basket').each(function () {
            if (id != $(this).attr('product_id')) {
                i++;
                product_id[i] = $(this).attr('product_id');
            }
            id = $(this).attr('product_id');
        })
        return product_id;
    }

</script>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'contact-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
)); ?>
<div class=" m_container m_breadcrumbs">
    <a href="/">Главная</a> > <a href="/site/basket">Корзина</a>
</div>

<div class="m_basket_product_box">
    <div class="m_container clear-fix">

        <?php
        $product_id = array();
        $kokoko = 1;
        if ($scenario == 'basket'): ?>
            <div class="m_basket_title">
                КОРЗИНА
            </div>
            <?php foreach ($model as $val): ?>
                <?php
                $product = Product::model()->findByPk($val['product_id']);
                $product_id[] = $product->id;
                ?>

                <div class="m_basket_product kokoko" kokoko="<?= $kokoko ?>">
                    <img class="m_basket_product_img" src="/uploads/product/preview/<?= $product->mainImage->image ?>">

                    <div class="m_basket_product_title">
                        <?= $product->product_name ?>
                    </div>
                    <div class="m_basket_product_count">
                        Колличество:

                        <div class="m_basket_product_count_minus basket_count bsr" product_id="<?= $product->id ?>"></div>
                        <div class="m_basket_product_count_count box_count">
                            <span class="product_count_new" product_id="<?= $product->id ?>"><?= $val['count']; ?></span>
                        </div>
                        <div class="m_basket_product_count_plus basket_count bsl" product_id="<?= $product->id ?>"></div>
                    </div>
                    <div class="m_basket_product_price">
                        Цена:
                        <span>
                            <?php
                                $cityPrice = $product->price;
                                if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($product->productCityPrice) && !empty($product->productCityPrice)) {
                                    foreach ($product->productCityPrice as $city_p) {
                                        if ($city_p->id_city == $_COOKIE['city']) {
                                            $cityPrice = $city_p->price;
                                        }
                                    }
                                }
                            ?>
                            <?php
                                if ($action == '+') {
                                    $base_price = round((100 - $product->productDiscount->discount * 1) * ($cityPrice + $price_change * 1) / 100, 0);
                                } else {
                                    $base_price = round((100 - $product->productDiscount->discount * 1) * ($cityPrice - $price_change * 1) / 100, 0);
                                }
                            ?>
                            <?php
                                if ($product->productDiscount->discount != null) { ?>
                                    <!-- <div class="discount ops"><?= $product->productDiscount->discount ?>%</div> -->
                                    <?= $base_price ?>
                            <?php
                                } else {
                                    ?>
                                    <?= $base_price ?>
                            <?php } ?>
                            р.-
                        </span>
                    </div>
                    <div class="cart_price_price" style="display: none">
                        <span class="cost" product_id="<?= $product->id ?>"><?= $base_price * ($val['count']) ?></span>
                        <span>руб.</span>
                    </div>
                    <a href="/site/productDetail/<?= $product->id ?>" class="m_button_product_card"></a>
                    <div class="m_button_delete_from_basket">
                        <div style="width: 100%; height: 100%" class="delete_from_basket" product_id="<?= $product->id ?>"></div>
                    </div>
                </div>
                <?php $kokoko++; endforeach; ?>
        <?php endif; ?>
        <?php if ($scenario == 'no_basket'): ?>
            <div class="row font_size-24">ПУСТО :(</div>
        <?php endif; ?>

        <div class="m_summ_basket_box">
            <span>Сумма заказа:</span><span id="count_down"></span><span>&nbspруб.</span>
        </div>
        <div class="m_notif_basket">
            В сумме заказа не учтены: акции на доставку<br/>
            и сборку; cтоимость доставки за пределами МКАД
        </div>
        <div class=" m_container fl">
            <div class="m_promo_cod_box">
                <div class="m_i_have_promo" >У меня есть промокод</div>
                <input class="m_promo" placeholder="Введите промокод" >
                <input type="button"value="Ввести"class="m_promo_success">
            </div>
        </div>
        <div class="m_button_go_oformlenie" id="next_order"></div>
    </div>
</div>

<div class="m_we_recomended_box">
    <div class="m_container clear-fix">
        <div class="m_we_recomended_title">Мы рекомендуем</div>
        <div class="m_we_recomended_slider">
            <?php for ($i = 1; $i <= 6; $i++): ?>
                <?php $this->renderPartial('_m_all_product_view', array('all_product' => $all_product)); ?>
            <?php endfor; ?>
        </div>
    </div>
</div>
<div class="m_ordering " id="next_step" style="display: none">
    <div class="m_ordering_line_1"></div>
    <div class="m_ordering_line_2"></div>
    <div class="m_ordering_line_3"></div>
    <div class="clear-fix m_container">
        <div class="m_ordering_title">
            Оформление заказа
        </div>
        <div class="m_ordering_info">
            Заполните поля с данными и выберите удобный для Вас
            способ доставки. После подтверждения заказа наш
            менеджер свяжеться с вами для уточнения деталей
        </div>
        <div class="m_ordering_personal_data">
            <img src="/images/m_male.png">
            Ваши данные
        </div>
        <form class="m_ordering_form" name="" action="">

            <div class="ordering_form" name="" action="">

                <?php
                $spPopa = 0;
                foreach($product_id as $key=>$val): ?>
                    <input type="hidden" name="product_id[]" style="display: none" value="<?=$val ?>"/>
                    <input type="hidden" name="product_count_new[]" style="display: none" value="" product_id="<?=$val ?>"/>
                    <input type="hidden" name="product_pole[]" style="display: none" value=""/>
                    <input type="hidden" name="pole[]" product_id="<?=$val ?>" style="display: none" value=""/>

                    <input type="hidden" name="pole_change[<?=$val ?>]" product_id="<?=$val ?>" style="display: none" value="<?=$price_change_id[$val] ?>"/>
                <?php endforeach ?>

            <div class="m_form_name_bgr">
                <?php echo $form->textField($contact_client, 'name', array('maxlength' => 64, 'placeHolder' => 'Ваше Имя', 'class' => 'm_form_name qwe form_name cb')); ?>
                <div class="m_red_star m_name_red_star"></div>
                <?php echo $form->error($contact_client, 'name'); ?>
            </div>
            <div class="m_form_phone_bgr">
                <?php echo $form->textField($contact_client, 'phone', array('maxLength' => 64, 'placeHolder' => 'Ваш телефон', 'class' => 'm_form_phone qwe form_phone cb')); ?>
                <?php echo $form->error($contact_client, 'phone'); ?>
                <div class="m_red_star m_phone_red_star"></div>
            </div>
            <div class="m_form_email_bgr">
                <?php echo $form->textField($contact_client, 'email', array('maxLength' => 64, 'placeHolder' => 'Ваш email', 'class' => 'm_form_email qwe form_email')); ?>
                <?php echo $form->error($contact_client, 'email'); ?>
                <div class="m_red_star m_email_red_star"></div>
            </div>
            <div class="m_form_text_bgr">
                <?php echo $form->textArea($contact_client, 'comment', array('maxLength' => 64, 'placeHolder' => 'Здесь Вы можете написать Ваш комментарий к заказу', 'class' => 'm_form_text qwe form_text cb')); ?>
                <?php echo $form->error($contact_client, 'comment'); ?>
            </div>
            <div class="m_warning"><span>* </span>обязательные поля для заполнения</div>
            <div class="m_ordering_logistic">
                <img src="/images/m_logistic.png">
                Выберите способ доставки
            </div>
            <div class="form-group m_radio_box">
                <label class="control-label" for="region"></label>
                <div style="position: relative">
                    <div class="m_radio">
                        <label>
                            <input  value="Москва(За МКАД + 25руб/км)" id="region_0" checked="checked" type="radio" name="region">
                            <p><span></span></p>
                            <div class="m_text_inp_moss">Москва(за МКАД + 25 руб/км)<br><span>Доставка: 950 рублей</span></div>
                        </label>
                    </div>
                    <div class="m_radio">
                        <label>
                            <input value="Самовывоз" id="region_1" type="radio" name="region">
                            <p><span></span></p>
                            <div class="m_text_inp_sam">Самовывоз<br><span>Доставка: бесплатно</span></div>
                        </label>
                    </div>
                    <div class="m_radio">
                        <label>
                            <input value="До ТК(для регионов)" id="region_2" type="radio" name="region">
                            <p><span></span></p>
                            <div class="m_text_inp_tk">До ТК(для регионов)<br><span>Доставка: 1000 руб.)</span></div>
                        </label>
                    </div>
                </div>
            </div>
        </form>
        <?php echo CHtml::SubmitButton('', array('class' => 'm_button_zakaz ordering_submit')); ?>
        <script>
            $(document).ready(function(){
                $('.m_button_zakaz').click(function(){
                    $('#m_ty_Modal').arcticmodal();
                });
            });
        </script>
        <!-- модальное окно при нажатии кнопки оформить заказ-->
        <div style="display: none;">
            <div class="m_ty_box" id="m_ty_Modal">
                <div class="m_ty_close arcticmodal-close"></div>

            </div>
        </div>
        <!-- модальное окно-->
    </div>
</div>
<div class="m_main_why">
    <div class="clear-fix m_container">
        <div class="m_main_why_yellow">Почему вам нужно покупать детскую</div>
        <div class="m_main_why_black">мебель в нашем интернет магазине</div>
        <img class="m_note_cart" src="/images/m_note_cart.png">
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_1.png">

    <div class="m_main_advantage_text_box">
        <span>Гарантия 18 месяцев</span>
        Мы даем официальную гарантию на любой вид
        детской мебели. Можете не переживать, когда
        ваши детки прыгают на новой кровати
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_2.png">

    <div class="m_main_advantage_text_box">
        <span>Быстрая доставка</span>
        Если вы проживаете в Москве или области, то
        детскую мебель привезут и соберут уже на
        следующий день. В другие районы России заказ
        прибудет с надежной транспортной компанией
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_3.png">

    <div class="m_main_advantage_text_box">
        <span>Вы ничем не рискуете</span>
        Потому что платите за детскую комнату или
        мебель только после того, как вам ее доставят.
        Никакой предоплаты!
    </div>
</div>
<div style="margin-top: 50px"></div>

<?php $this->endWidget(); ?>

