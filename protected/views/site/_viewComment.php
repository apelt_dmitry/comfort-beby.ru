<?php
    if( $data->state!=0 ):
?>
<div class="row margin_top-20">

    <div class="my-14 lpl">
        <?=BsHtml::image('/uploads/icon/preview/'.$data->icon->icon); ?>
    </div>

    <div class="my-86">
        <div class="row">
            <span class="font_bold"><?=$data->name ?>&nbsp&nbsp</span>
            <span><?=Yii::app()->dateFormatter->formatDateTime($data->date, 'long', false); ?></span>
        </div>
        <div class="row" style="margin-top: 10px">
            <?=$data->comment ?>
        </div>
    </div>

</div>
<?php endif; ?>
