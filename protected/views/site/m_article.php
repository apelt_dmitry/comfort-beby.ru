<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs->registerCssFile($pt . 'css/slider.css');

$cs->registerScriptFile($pt . 'js/slider.js');

$meta = Meta::model()->findByPk(3);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>

<?php
$view = '_view_article';
if ($scenario == 'detail_news') {
    $view = '_view_m_detail_article';
}
?>

<?php
$this->widget('bootstrap.widgets.BsListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => $view,
    //'afterAjaxUpdate' => 'function(){new_css()}',
    'template' => '
            {items}
            <div class="article_navigate_box"> {pager}</div>',
    'pager' => array(
        //'afterAjaxUpdate' => 'function(){top_body()}',
        'header' => '',
        'prevPageLabel' => '<',
        'previousPageCssClass' => 'article_navigate_button',
        'nextPageLabel' => '>',
        'nextPageCssClass' => 'article_navigate_button',
        'maxButtonCount' => '6',
        'firstPageLabel' => '|<',
        'firstPageCssClass' => 'article_navigate_button',
        'lastPageLabel' => '>|',
        'lastPageCssClass' => 'article_navigate_button',
        'internalPageCssClass' => 'article_navigate_button',
        'htmlOptions' => array(
            'class' => 'article_navigate',
        )
    ),
));
?>