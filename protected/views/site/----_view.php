<?php
mb_internal_encoding("UTF-8");
if (strlen($data->Product->description) <= 350)
{
    $description = $data->Product->description;
}
else
{
    $description = mb_substr($data->Product->description, 0, 350).'...';
}
?>

<div style="float: left;  position: relative;" class="width100">
    <div class="view_product margin_left-21" style="margin-bottom: 20px; display: table;">
        <div>
            <table class="table_name">
                <tr>
                    <td style="text-align: center" class="product_name_js font_bold"><?= $data->Product->product_name ?></td>
                </tr>
            </table>
            <div class="view_img" style="position: relative">
                <a href="/site/productDetail/<?=$data->Product->id?>">
                    <?= BsHtml::image('/uploads/product/preview/'.$data->Product->mainImage->image) ?>
                    <div class="shadow_img"></div>
                </a>
            </div>
        </div>
        <table class="table_detail">
            <tr>
                <?php if( $data->Product->act_id!=0 ): ?>
                    <img src="/images/act.png" class="act_img"/>
                <?php endif ?>
                
                <?php
                $cityPrice = $data->Product->price;
            
                if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->Product->productCityPrice) && !empty($data->Product->productCityPrice))
                {
                    foreach ($data->Product->productCityPrice as $city_p)
                    {
                        if ($city_p->id_city == $_COOKIE['city'])
                        {
                            $cityPrice = $city_p->price;
                        }
                    }
                }
                ?>
                
                <?php if ($data->Product->productDiscount->discount != null): ?>
                    <div class="discount dc"><?=$data->Product->productDiscount->discount ?>%</div>
                    <td class="table_price"><?= number_format((100-$data->Product->productDiscount->discount*1)*$cityPrice/100, 0, '.', ' ') ?> р.</td>
                <?php else: ?>
                    <td class="table_price"><?= number_format($cityPrice, 0, '.', ' ') ?> р.</td>
                <?php endif; ?>

                <td class="table_link"><a href="/site/productDetail/<?=$data->Product->id ?>">Подробнее</a></td>
            </tr>
        </table>
    </div>
    <div style="display: none" class="description_js"><?= BsHtml::encode(preg_replace('/\<br(\s*)?\/?\>/i', "\n", ($description))); ?></div>
</div>