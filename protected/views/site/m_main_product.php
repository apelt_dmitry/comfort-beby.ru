<?php
Yii::app()->name = $model->meta_title;
Yii::app()->clientScript->registerMetaTag($model->meta_content, 'description');
?>
<style>
    input[type="radio"] {
        display: inline;
    }

    .complect_wrap {
        padding: 16px;
        margin-bottom: 20px;
    }
</style>

<script>
    $(document).ready(function () {
        /* Добовление в корзину!!!! */
        $('.basket_ajax').click(function () {
            var pole_name = [];
            var pole_id = [];
            var i = 0;
            $('input[type="radio"]:checked').each(function () {
                pole_name[i] = $(this).attr('name');
                pole_id[i] = $(this).attr('value');
                i++;
            });
            var product = JSON.stringify([{
                'product_id': $('#product_id').attr('product_id'),
                'pole_name': pole_name,
                'pole_id': pole_id,
                'count': 1
            }]);
            //alert($.cookie('basket'));
            if ($.cookie('basket') == 'null' || !IsJsonString($.cookie('basket'))) {
                $.cookie('basket', product, {expires: 30, path: '/'});
            }
            else {
                var products = JSON.parse($.cookie('basket'));
                var hasProduct = false;
                for (var i in products) {
                    if ($('#product_id').attr('product_id') == products[i]['product_id']) {
                        products[i]['count'] = products[i]['count'] * 1 + 1;
                        hasProduct = true;
                    }
                }
                if (!hasProduct) {
                    products.push({
                        'product_id': $('#product_id').attr('product_id'),
                        'pole_name': pole_name,
                        'pole_id': pole_id,
                        'count': 1
                    });
                }
                $.cookie('basket', JSON.stringify(products), {expires: 30, path: '/'});
                $('#add_basket').css({
                    'left': ($(window).width() / 2) - 120,
                    'top': ($(window).height() / 2) - 100
                });
            }
            basket();
            return false;
        });

        /* для слайдера товара */
        $(".fancybox").fancybox();


    });
    /* для красивой анимации при покупке товара */
    function animate_cart_my(obj) {
        src = "/images/button_buy_fly.png";
        $('<img src="' + src + '" id="temp_cart_animate" style="z-index: 2000; position: absolute; top:' + Math.ceil($(obj).offset().top) + 'px; left:' + Math.ceil($(obj).offset().left) + 'px;">').prependTo('body');
        $('#temp_cart_animate').animate(
            {
                top: -1000 + $(window).scrollTop(),
                left: $('body').width()
            },
            700,
            function () {
                $('#temp_cart_animate').remove();
            });
    }
</script>
<div id="add_basket" style="display: none;">Товар добавлен</div>
<!-- ############################################################################### -->
<?php $this->urlvar = $sum;
$cityPrice = $model->price;
if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($model->productCityPrice) && !empty($model->productCityPrice)) {
    foreach ($model->productCityPrice as $city_p) {
        if ($city_p->id_city == $_COOKIE['city']) {
            $cityPrice = $city_p->price;
        }
    }
}
?>


<!-- new part-->
<div class=" m_container m_breadcrumbs">
    Навигация: <?php echo $this->urlvar; ?>
</div>
<div class="m_product_images">
    <div class="m_container clear-fix">
        <div class="m_tree_product_box">
            <?php
            $i = 0;
            foreach ($model->productImage as $val): ?>
                <?php if ($i > 0 and $i <= 3): ?>
                    <a class="fancybox" rel="group" href="/uploads/product/<?= $val->image ?>">
                        <div class="m_fliper"></div>
                        <img src="/uploads/product/<?= $val->image ?>" alt="">

                        <div class="mb_33px"></div>
                    </a>
                <?php endif; ?>
                <?php if ($i <= 3): ?>
                    <a style="display: none;" class="fancybox" rel="group" href="/uploads/product/<?= $val->image ?>">
                        <div class="m_fliper"></div>
                        <img src="/uploads/product/<?= $val->image ?>" alt="">

                        <div class="mb_33px"></div>
                    </a>
                <?php endif; ?>
                <?php $i++; ?>
            <?php endforeach; ?>
        </div>
        <div class="m_big_product_image">
            <a class="fancybox" rel="group" href="/uploads/product/<?= $model->mainImage->image ?>">
                <div class="m_fliper"></div>
                <img src="/uploads/product/<?= $model->mainImage->image ?>" alt="">
<!--                <div class="m_hit m_hit_big"></div>-->
            </a>
        </div>
    </div>
</div>
<div class="m_product_info">
    <div class="m_container clear-fix">
        <div class="m_product_name">
            <?= $model->product_name ?>
        </div>

        <div class="m_price_count_pay">
            <div class="m_price">
                ЦЕНА:
        <span id="product_id" product_id="<?= $model->id ?>">
            <?php if ($model->productDiscount->discount != null): ?>
                <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike>
                <span><?= number_format((100 - $model->productDiscount->discount * 1) * $cityPrice / 100, 0, '.', ' ') ?>
                    р.</span>
            <?php else: ?>
                <?= number_format($cityPrice, 0, '.', ' ') ?> р.
            <?php endif; ?>
         </span>
            </div>
            <!--  В десктопной версии отсуцтвует здесь пока тоже  -->
            <!--    <div class="m_count">-->
            <!--        <div class="m_count_minus"></div>-->
            <!--        <div class="m_count_count">1</div>-->
            <!--        <div class="m_count_plus"></div>-->
            <!--    </div>-->
            <div class="m_pay basket_ajax"></div>
        </div>
        <div class="m_description">
            <img src="/images/m_description.png">
            <span>Описание:</span>
            <?php echo $model->description ?>
        </div>
        <?php if (count($img_pole['name']) > 0): ?>
            <div class="m_drop_slide_title clear-fix">
                <img class="m_drop_1" src="/images/m_drop_1.png">
                <span class="ml_60px">Выберите цвет</span>
                <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
            </div>
            <div class="m_drop_slide_product clear-fix" style="display: none">
                <div class="choise_color_colors clear-fix">
                    <?php foreach ($img_pole['name'] as $key => $val): ?>
                        <div style="text-transform: capitalize"
                             class="choise_color_colors_title_<?php echo $key; ?> choise_color_colors_title">
                            <?php echo $val; ?>: <span></span>
                        </div>
                        <div class="box_colors clear-fix">
                            <?php foreach ($img_pole['val'][$val] as $key2 => $val2): ?>
                                <div class="box_color" for_title="choise_color_colors_title_<?php echo $key; ?>">
                                    <div style="display: none;">
                                        <input value="<?= $key2; ?>" type="radio" name="<?= $val; ?>"/>
                                    </div>
                                    <div color="<?php echo $val2; ?>" class="color">
                                        <img src="/uploads/pole/<?php echo $img_pole['img'][$val][$key2]; ?>"/>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (count($mas_pole['val']) > 0): ?>

            <div class="m_drop_slide_title clear-fix">
                <img class="m_drop_2" src="/images/m_drop_2.png">
                <span class="ml_60px">Выберите комплектацию</span>
                <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
            </div>
            <div class="m_drop_slide_product clear-fix " style="display: none; height:auto;">
                <div class="complect_wrap">
                    <?php
                    $kuku = '';
                    $i = 0;
                    foreach ($mas_pole['val'] as $key => $val) {
                        echo '<div class="font_bold font_size-24 margin_top-40">' . $key . '</div>';
                        if ($model->type_pole == 1) {
                            echo '<div style="display: none"><input pole_id="" price_change="" value="" type="radio" name="" checked="checked" style="display:none" class="type_pole_input"></div>';
                            echo '<select class="listPole form-control" name="' . $key . '[]">';
                        }
                        foreach ($val as $key2 => $val2) {
                            $checked = 'no';
                            if ($key != $kuku) {
                                $j = $i;
                                $checked = 'yes';
                            }
                            $kuku = $key;
                            if ($model->type_pole == 0) {
                                if ($checked == 'yes') {
                                    echo
                                        '<div class="radio">
                                <label>
                                    <input pole_id="' . $mas_pole['id'][$i] . '" price_change="' . $mas_pole['price_change'][$key][$key2] . '" value="' . $key2 . '" type="radio" name="' . $key . '" checked="checked">
                                    ' . $val2 . '
                                </label>
                            </div>';
                                } else {
                                    echo
                                        '<div class="radio">
                                <label>
                                    <input pole_id="' . $mas_pole['id'][$i] . '" price_change="' . $mas_pole['price_change'][$key][$key2] . '" value="' . $key2 . '" type="radio" name="' . $key . '">
                                    ' . $val2 . '
                                </label>
                            </div>';
                                }
                            } else {
                                echo
                                    '<option value="' . $key2 . '" price_change="' . $mas_pole['price_change'][$key][$key2] . '" class="opt_pole" name="' . $key . '">' . $val2 . '</option>';
                            }
                        }
                        $i++;
                        if ($model->type_pole == 1) {
                            echo '</select>';
                        }
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>


       <?php
           $matras = Yii::app()->db->createCommand('SELECT  p.product_name, pi.image, d.discount, p.id, p.price
                                                        FROM ProductCategory pc
                                                        LEFT JOIN product p ON p.id=pc.product
                                                        LEFT JOIN product_image pi ON pi.product_id = pc.product
                                                        LEFT JOIN discount d ON d.id= p.discount_id
                                                        WHERE pc.category=10
                                                        GROUP BY pi.product_id
                                                        LIMIT 2')->queryAll();
       ?>
        <div class="m_drop_slide_title clear-fix">
            <img class="m_drop_3" src="/images/m_drop_3.png">
            <span class="ml_60px">Выберите матрас</span>
            <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
        </div>
        <div class="m_drop_slide_product clear-fix" style="display: none">
            <div class="add_nth">
                <?php foreach($matras as $mat):?>
                <div class="m_product_box">
                    <img class="m_product_box_image" src="/uploads/product/preview/<?= $mat['image']?>">

                    <div class="m_product_box_name"><?= $mat['product_name']?></div>
                    <div class="m_product_box_price"><?= $mat['price']?> р.-</div>
                    <div class="m_product_line"></div>
                    <div class="m_product_button buy_btn_main" product_id="<?=$mat['id'] ?>"></div>
                </div>
                <?php endforeach;?>

            </div>

        </div>
        <?php if (!empty($model->left_description)): ?>
        <div class="m_drop_slide_title clear-fix">
            <img class="m_drop_4" src="/images/m_drop_4.png">
            <span class="ml_60px">Характеристики</span>
            <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
        </div>
        <div class="m_drop_slide_product clear-fix" style="display: none">
            <div class="m_haracter">
                <?php echo $model->left_description ?>
            </div>
        </div>
        <?php endif; ?>
        <?php if (!empty($model->palette)): ?>
            <div class="m_drop_slide_title clear-fix">
                <img class="m_drop_5" src="/images/m_drop_5.png">
                <span class="ml_60px">Цветовая палитра</span>
                <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
            </div>
            <div class="m_drop_slide_product clear-fix" style="display: none">
                <div class="m_choise_size_title">Нажмите на фото, что бы увеличить</div>
                <a class="fancybox" href="/uploads/palette/<?= $model->palette; ?>">
                    <div class="m_fliper"></div>
                    <img src="/uploads/palette/<?= $model->palette; ?>" alt="">
                </a>
            </div>
        <?php endif; ?>
        <?php if (!empty($model->delivery)): ?>
        <div class="m_drop_slide_title clear-fix">
            <img class="m_drop_6" src="/images/m_drop_6.png">
            <span class="ml_60px">Доставка и сборка</span>
            <img class="m_drop_slide_arrow rotate-reset" src="/images/m_slidedrop_arrow.png">
        </div>
        <div class="m_drop_slide_product clear-fix" style="display: none">
            <div class="m_dostavka_is_borka">
                <?php echo $model->delivery; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<div class="m_popular_product_box">
    <div class="m_container clear-fix">
        <div class="m_popular_product_title">
            <span>пОХОЖИЕ ТОВАРЫ</span>
        </div>
        <?php
        /** ДЛЯ Похожие товары */
        $cat_id = $model->oneProductCategory->category;
        $arr_mdl_ids = ProductCategory::model()->findAllByAttributes(array('category' => $cat_id), array('limit' => 7));
        $arr_ids = CHtml::listData($arr_mdl_ids, 'id', 'product');
        $str_ids = implode(',', $arr_ids);
        $arr_mdl_prod = Product::model()->findAll('id IN(' . $str_ids . ')');
        ?>
        <div class="m_popular_slider">
            <?php foreach ($arr_mdl_prod as $data): ?>
                <div class="m_product_box">
                    <a href="/site/productDetail/<?php echo $data->id; ?>">
                        <?php echo BsHtml::image('/uploads/product/preview/' . $data->mainImage->image, $data->product_name, array('class' => 'm_product_box_image')) ?>
                    </a>
                    <div class="m_product_box_name"><?= $data->product_name ?></div>
                    <?php
                    $cityPrice = $data->price;
                    if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->productCityPrice) && !empty($data->productCityPrice)) {
                        foreach ($data->productCityPrice as $city_p) {
                            if ($city_p->id_city == $_COOKIE['city']) {
                                $cityPrice = $city_p->price;
                            }
                        }
                    }
                    ?>
                    <div class="m_product_box_price">
                        <?php if ($data->productDiscount->discount != null): ?>
                            <!-- <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike> -->
                            <span>
                                <?= number_format((100 - $data->productDiscount->discount * 1) * $cityPrice / 100, 0, '.', ' ') ?>р.
                            </span>
                        <?php else: ?>
                            <?= number_format($cityPrice, 0, '.', ' ') ?> р.
                        <?php endif; ?></div>
                    <div class="m_product_line"></div>
                    <div class="m_product_button buy_btn_main" product_id="<?= $data->id ?>"></div>
                </div>
            <?php endforeach; ?>
        </div>


        <?php
        // Это кусок из старого кода, в передаваемых данных можно оставить только $model
        if ($model->room == 1) {
            //например товар 225 это сюда
            $this->renderPartial('_m_detail_product_room', array(
                'mas_pole' => $mas_pole,
                'all_product' => $all_product,
                'popular' => $popular,
                'model' => $model,
                'allR' => $allR,
                'sum' => $sum
            ));?>

        <?php
        } else {
            //например товар 190 это сюда
            $this->renderPartial('_detail_product', array(
                'mas_pole' => $mas_pole,
                'all_product' => $all_product,
                'popular' => $popular,
                'model' => $model,
                'allR' => $allR,
                'new_all_product' => $new_all_product,
                'sum' => $sum
            ));
        }
        ?>


    </div>
</div>

<div class="m_main_why">
    <div class="clear-fix m_container">
        <div class="m_main_why_yellow">Почему вам нужно покупать детскую</div>
        <div class="m_main_why_black">мебель в нашем интернет магазине</div>
        <img class="m_note_cart" src="/images/m_note_cart.png">
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_1.png">

    <div class="m_main_advantage_text_box">
        <span>Гарантия 18 месяцев</span>
        Мы даем официальную гарантию на любой вид
        детской мебели. Можете не переживать, когда
        ваши детки прыгают на новой кровати
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_2.png">

    <div class="m_main_advantage_text_box">
        <span>Быстрая доставка</span>
        Если вы проживаете в Москве или области, то
        детскую мебель привезут и соберут уже на
        следующий день. В другие районы России заказ
        прибудет с надежной транспортной компанией
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_3.png">

    <div class="m_main_advantage_text_box">
        <span>Вы ничем не рискуете</span>
        Потому что платите за детскую комнату или
        мебель только после того, как вам ее доставят.
        Никакой предоплаты!
    </div>
</div>
<div style="margin-top: 50px"></div>