<?php
mb_internal_encoding("UTF-8");
$description = '';
if(strlen($data->description)<=150){
    $description = $data->description;
}
else{
    $description = mb_substr($data->description, 0, 140).'...';
}
?>
            <div class="article_news_box">
                <?= BsHtml::image('/uploads/product/preview/'.$data->image,'', array('class' => 'news_img'))?>

                <div class="more_news_title">
                    <?php echo $data->title; ?>
                </div>
                <a href="/site/article/<?=$data->id ?>" class="more_news_more"> Читать далее</a>
            </div>