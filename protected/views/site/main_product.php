<?php
Yii::app()->name = $model->meta_title;
Yii::app()->clientScript->registerMetaTag($model->meta_content, 'description');
?>
<style>
input[type="radio"]
{
    display: inline;
}
.complect_wrap
{
    padding: 16px;
    margin-bottom: 20px;
}
</style>

<script>
    $(document).ready(function(){

        /* Добовление в корзину!!!! */
        $('.basket_ajax').click(function(){
            var pole_name = [];
            var pole_id = [];
            var i = 0;
            $('input[type="radio"]:checked').each(function(){
                pole_name[i] = $(this).attr('name');
                pole_id[i] = $(this).attr('value');
                i++;
            });

            var product =  JSON.stringify([{
                'product_id': $('#product_id').attr('product_id'),
                'pole_name': pole_name,
                'pole_id': pole_id,
                'count': 1
            }]);

            //alert($.cookie('basket'));

            if($.cookie('basket')=='null' || !IsJsonString($.cookie('basket'))){
                $.cookie('basket', product, { expires: 30, path: '/' });
            }
            else{
                var products = JSON.parse($.cookie('basket'));
                var hasProduct = false;
                for(var i in products){
                    if($('#product_id').attr('product_id')==products[i]['product_id']){
                        products[i]['count'] = products[i]['count']*1+1;
                        hasProduct = true;
                    }
                }
                if(!hasProduct){
                    products.push({
                        'product_id': $('#product_id').attr('product_id'),
                        'pole_name': pole_name,
                        'pole_id': pole_id,
                        'count': 1
                    });
                }
                $.cookie('basket', JSON.stringify(products), { expires: 30, path: '/' });
                $('#add_basket').css({
                    'left': ($(window).width()/2)-120,
                    'top': ($(window).height()/2)-100
                });

            }

        /* Старая анимация доб. товара
            $('#add_basket').show();
            $('#add_basket').animate({
                'opacity': 1
            },300,function(){
                setTimeout(function(){
                    $('#add_basket').animate({
                        'opacity': 0
                    },300,function(){
                        $('#add_basket').hide();
                    })
                },1500)
            });
            */

            basket();
            return false;
        });

        /* для просмотренных товаров */
		var product_id =  JSON.stringify([{
            'product_id': $('#product_id').attr('product_id')
        }]);

        if($.cookie('show_product')==null){
            $.cookie('show_product', product_id, { expires: 1, path: '/' });
        }
        else{
            var products = JSON.parse($.cookie('show_product'));

            var hasProduct = false;
            for(var i in products){
                if($('#product_id').attr('product_id')==products[i]['product_id']){
                    hasProduct = true;
                }
            }
            if(!hasProduct){
                products.push({
                    'product_id': $('#product_id').attr('product_id'),
                });
            }
            $.cookie('show_product', JSON.stringify(products), { expires: 1, path: '/' });
        }


    /* для слайдера товара */
    $(".fancybox").fancybox();

    /* для табов */
    $(function() {
        $( "#tabs" ).tabs();
    });

    });
     /* для красивой анимации при покупке товара */
    function animate_cart_my(obj) {
        src="/images/button_buy_fly.png";
        $('<img src="' + src + '" id="temp_cart_animate" style="z-index: 2000; position: absolute; top:' + Math.ceil($(obj).offset().top) + 'px; left:' + Math.ceil($(obj).offset().left) + 'px;">').prependTo('body');
        $('#temp_cart_animate').animate(
            {
                top: -1000 + $(window).scrollTop(),
                left: $('body').width()
            },
            700,
            function () {
                $('#temp_cart_animate').remove();
            });
    }
</script>
    <div id="add_basket" style="display: none;">Товар добавлен</div>

<!-- ############################################################################### -->
<?php $this->urlvar = $sum;

$cityPrice = $model->price;

if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($model->productCityPrice) && !empty($model->productCityPrice))
{
    foreach ($model->productCityPrice as $city_p)
    {
        if ($city_p->id_city == $_COOKIE['city'])
        {
            $cityPrice = $city_p->price;
        }
    }
}
?>

<div class="product_view_box">
    <div class="container clear-fix">

        <div class="product_images">
            <div class="tree_product_box">
                <?php
                $i = 0;
                foreach($model->productImage as $val): ?>
                    <?php if($i>0 and $i<=3):?>
                        <a class="fancybox" rel="group" href="/uploads/product/<?=$val->image ?>">
                            <div class="fliper"></div>
                            <img src="/uploads/product/<?=$val->image ?>" alt="" />
                        </a>
                    <?php endif; ?>

                    <?php if($i<=3):?>
                        <a style="display: none;"  class="fancybox" rel="group" href="/uploads/product/<?=$val->image ?>">
                            <div class="fliper"></div>
                            <img src="/uploads/product/<?=$val->image ?>" alt="" />
                        </a>
                    <?php endif; ?>

                    <?php  $i++; ?>
                    <!--
                    <a class="fancybox" rel="group" href="/images/product_image.jpg">
                        <div class="fliper"></div>
                        <img src="/images/product_image.jpg" alt="" />
                    </a>
                    <a class="fancybox" rel="group" href="/images/product_image.jpg">
                        <div class="fliper"></div>
                        <img src="/images/product_image.jpg" alt="" />
                    </a>
                    -->
                <?php endforeach; ?>
            </div>
            <div class="big_product_image">
                <a class="fancybox" rel="group" href="/uploads/product/<?=$model->mainImage->image ?>">
                    <div class="fliper"></div>
                    <img src="/uploads/product/<?=$model->mainImage->image ?>" alt="" width="331px" height="268px" />
                </a>
            </div>
        </div>

        <div class="product_information">
        <div class="product_title" >
            <?=$model->product_name ?>
        </div>
        <?php if(!empty($model->production->id)): ?>
            <div class="product_fabric">
                <a href="/site/catalog?manuf=<?=$model->production->id;?>">
                    <img src="/images/product_fabric.png"> “<?php echo $model->production->name; ?>”
                </a>
            </div>
        <?php endif; ?>
        <div class="product_box_price">
            <div class="product_box_price_price">ЦЕНА: <span id="product_id" product_id="<?=$model->id ?>">
                    <?php if ($model->productDiscount->discount != null): ?>
                        <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike>
                        <span><?= number_format((100-$model->productDiscount->discount*1)*$cityPrice/100, 0, '.', ' ') ?> р.</span>
                    <?php else: ?>
                        <?= number_format($cityPrice, 0, '.', ' ') ?> р.
                    <?php endif; ?>
                 </span></div>
            <div class="button_buy_product basket_ajax" onclick="animate_cart_my(this);"></div>
            <!-- Кол-во товара, пока что закомитил NSV
            <div class="product_count_minus"></div>
            <div class="product_count">1</div>
            <div class="product_count_plus"></div>
            -->
        </div>

        <?php if(count($img_pole['name']) > 0): ?>
            <div class="product_choice_color">
                <div class="choice_color_title">
                    Выберите цвет
                    <img class="choise_arrow rotate-reset" src="/images/choise_arrow.png"/>
                    <img class="choise_color_img" src="/images/choise_color_img.png">
                </div>
                <div class="choise_color_colors clear-fix" style="display: none">

                    <?php foreach($img_pole['name'] as $key=>$val):?>
                        <div class="choise_color_colors_title_<?php echo $key;?> choise_color_colors_title">
                            <?php echo $val;?>: <span></span>
                        </div>
                        <div class="box_colors clear-fix">
                            <?php foreach($img_pole['val'][$val] as $key2=>$val2):?>
                                <div class="box_color" for_title="choise_color_colors_title_<?php echo $key;?>">
                                    <div style="display: none;"><input value="<?=$key2;?>" type="radio" name="<?=$val;?>" /></div>
                                    <div color="<?php echo $val2; ?>"  class="color"  ><img src="/uploads/pole/<?php echo $img_pole['img'][$val][$key2]; ?>"/></div>
                                </div>
                            <?php endforeach; ?>
                            <!--
                            <div class="box_color">
                                <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #3CB7E7" color="Голубой"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #3CB7E7" color="Голубой"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #3CB7E7" color="Голубой"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                            </div>
                            <div class="box_color">
                                <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                            </div>

                            <div class="choise_color_colors_title_fasad">
                                Цвет фасада: <span></span>
                            </div>
                            <div class="box_colors clear-fix">
                                <div class="box_color_fasad">
                                    <div color="Голубой"  style="background-color: #3CB7E7" class="color"  ></div>
                                </div>
                                <div class="box_color_fasad">
                                    <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                                </div>
                                <div class="box_color_fasad">
                                    <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                                </div>
                                <div class="box_color_fasad">
                                    <div class="color" style="background-color: #3CB7E7" color="Голубой"></div>
                                </div>
                                <div class="box_color_fasad">
                                    <div class="color" style="background-color: #cd2e26" color="Красный"></div>
                                </div>
                                <div class="box_color_fasad">
                                    <div class="color" style="background-color: #FF7500" color="Апельсин"></div>
                                </div>
                                <div class="box_color_fasad">
                                    <div class="color" style="background-color: #3CB7E7" color="Голубой"></div>
                                </div>
                            </div>
                            -->
                        </div>
                    <?php endforeach;?>



                </div>

            </div>
        <?php endif; ?>

        <?php if(count($mas_pole['val']) > 0 ): ?>
            <div class="product_choice_size">
                <div class="choice_size_title">
                    Выберите комплектацию
                    <img class="choise_arrow_size rotate-reset" src="/images/choise_arrow.png"/>
                    <img class="choise_size_img" src="/images/size_img.png">
                </div>
                <div class="choice_size_sizes" style="display: none; height:auto;">
                    <!--
                    <div class="choise_size_sizes_title">
                        Цвет корпуса: <span></span>
                    </div>
                    <div class="box_sizes clear-fix">

                    <select name="size" id="size">
                        <option value="" >70x60см</option>
                        <option value="1">70x60см</option>
                        <option value="2">710x60см</option>
                        <option value="3">20x20см</option>
                    </select>

                    </div>
                     -->
                    <div class="complect_wrap">
                        <?php
                        $kuku = '';
                        $i = 0;

                        foreach($mas_pole['val'] as $key=>$val){
                            echo '<div class="font_bold font_size-24 margin_top-40">'.$key.'</div>';

                            if( $model->type_pole==1 ){
                                //echo '<input pole_id="'.$mas_pole['id'][$i].'" price_change="'.$mas_pole['price_change'][$key][$key2].'" value="'.$key2.'" type="radio" name="'.$key.'">';
                                echo '<div style="display: none"><input pole_id="" price_change="" value="" type="radio" name="" checked="checked" style="display:none" class="type_pole_input"></div>';
                                echo '<select class="listPole form-control" name="'.$key.'[]">';
                            }
                            //echo BsHtml::radioButtonListControlGroup($key,$mas_pole['id'][$j],$val,array('pole_id'=>$mas_pole['id'][$i], 'price_change'=>$mas_pole['price_change'][$i]));
                            foreach($val as $key2=>$val2){

                                $checked = 'no';
                                if($key!= $kuku)
                                {
                                    $j=$i;
                                    $checked = 'yes';
                                }
                                $kuku = $key;
                                if( $model->type_pole==0 )
                                {
                                    if( $checked=='yes' )
                                    {
                                        echo
                                            '<div class="radio">
                                <label>
                                    <input pole_id="'.$mas_pole['id'][$i].'" price_change="'.$mas_pole['price_change'][$key][$key2].'" value="'.$key2.'" type="radio" name="'.$key.'" checked="checked">
                                    '.$val2.'
                                </label>
                            </div>';
                                    }
                                    else
                                    {
                                        echo
                                            '<div class="radio">
                                <label>
                                    <input pole_id="'.$mas_pole['id'][$i].'" price_change="'.$mas_pole['price_change'][$key][$key2].'" value="'.$key2.'" type="radio" name="'.$key.'">
                                    '.$val2.'
                                </label>
                            </div>';
                                    }
                                }
                                else
                                {
                                    echo
                                        '<option value="'.$key2.'" price_change="'.$mas_pole['price_change'][$key][$key2].'" class="opt_pole" name="'.$key.'">'.$val2.'</option>';
                                }

                            }
                            $i++;

                            if( $model->type_pole==1 )
                            {
                                echo '</select>';
                            }
                        }

                        ?>

                        <script>
                            /* $('input[type="radio"]:checked').parent().css('background','url(/images/sf.png) no-repeat 0 7px'); */
                        </script>
                    </div>

                </div>
            </div>
        <?php endif; ?>
        </div>


        <div class="product_description">
            <div class="product_description_top">
                <img src="/images/product_description_img.png">Описание:
            </div>
            <div class="product_description_bot">
                <?php echo $model->description ?>
            </div>
        </div>
    </div>
</div>
<div class="tabs">
    <div class="container">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Характеристики</a></li>
                <li><a href="#tabs-2">Цветовая палитра</a></li>
                <li><a href="#tabs-3">Доставка и сборка</a></li>
                <a href="#"><img src="/images/product_percent.png" class="prod_percent"></a>
            </ul>
            <div id="tabs-1" >
                <?php echo $model->left_description ?>
                <!--
                <p>• Габаритные размеры: 1018 х 960 х 2151мм;</p>
                <p>• Спальное место: 900х2000мм;</p>
                <br/>
                <p>Транспортировка:</p>
                <br/>
                <p>• Масса: 58.5 кг;</p>
                <p>• Объём: 0,179 м3;</p>
                <p>• Кол-во упаковок: 4шт.</p>
                <br/>
                <p>Нажмите на фото, что бы увеличить</p>
                <div class="clear-fix" style="position: relative">
                    <div class="character clear-fix">
                        <div>70х160 см</div>
                        <a class="fancybox" rel="group2" href="/images/char_img.jpg"><img src="/images/char_img.jpg" alt="" /></a>
                    </div>
                    <div class="character">
                        <div>70х160 см</div>
                        <a class="fancybox" rel="group2" href="/images/char_img.jpg"><img src="/images/char_img.jpg" alt="" /></a>
                    </div>
                    <div class="character">
                        <div>70х160 см</div>
                        <a class="fancybox" rel="group2" href="/images/char_img.jpg"><img src="/images/char_img.jpg" alt="" /></a>
                    </div>
                </div>
                -->
            <div style="margin-bottom: 80px;"></div><!-- Временный тег, что бы скрыть баг верстки-->
            </div>
            <div id="tabs-2">
                <?php if(!empty($model->palette)): ?>
                    <p style="text-align: center;">Нажмите на фото, что бы увеличить</p>
                    <p style="text-align: center;"><a class="fancybox" rel="group3" href="/uploads/palette/<?=$model->palette;?>"><img width="400" src="/uploads/palette/<?=$model->palette;?>" alt="" /></a></p>
                <?php endif; ?>
            </div>
            <div id="tabs-3">
                <?php echo  $model->delivery; ?>
            </div>
        </div>
    </div>
</div>

<?php
// Это кусок из старого кода, в передаваемых данных можно оставить только $model
if ($model->room == 1)
{
    //например товар 225 это сюда
    $this->renderPartial('_detail_product_room', array(
        'mas_pole' => $mas_pole,
        'all_product' => $all_product,
        'popular' => $popular,
        'model' => $model,
        'allR' => $allR,
        'sum' => $sum
    ));
}
else
{
    //например товар 190 это сюда
    $this->renderPartial('_detail_product',array(
        'mas_pole' => $mas_pole,
        'all_product' => $all_product,
        'popular' => $popular,
        'model' => $model,
        'allR' => $allR,
        'new_all_product' => $new_all_product,
        'sum' => $sum
    ));
}
?>

<div class="similar_products_box" style="margin-bottom: 60px;">
    <div class="container similar_products clear-fix">
        <div class="similar_products_title">Похожие товары:</div>

<?php
/** ДЛЯ Похожие товары */
$cat_id = $model->oneProductCategory->category;
$arr_mdl_ids = ProductCategory::model()->findAllByAttributes(array('category'=>$cat_id), array('limit'=>7));
$arr_ids = CHtml::listData($arr_mdl_ids, 'id', 'product');
$str_ids = implode(',', $arr_ids);
$arr_mdl_prod = Product::model()->findAll('id IN('.$str_ids.')');
//var_dump($arr_mdl_prod);
//$criteria = new CDbCriteria;
?>

        <div class="similarSlider">

        <?php foreach($arr_mdl_prod as $data): ?>
            <div class="similar_sl_box">
                <div class="similar_sl_box_img">
                    <a href="/site/productDetail/<?php echo $data->id;?>"><?php echo BsHtml::image('/uploads/product/preview/'.$data->mainImage->image, $data->product_name, array('class'=>'img_popular_prod')) ?></a>
                    <!-- <img src="/images/similar_img.jpg" /> -->
                </div>
                <div class="similar_name"><?= $data->product_name ?></div>

                <?php
                $cityPrice = $data->price;

                if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->productCityPrice) && !empty($data->productCityPrice))
                {
                    foreach ($data->productCityPrice as $city_p)
                    {
                        if ($city_p->id_city == $_COOKIE['city'])
                        {
                            $cityPrice = $city_p->price;
                        }
                    }
                }
                ?>
                <div class="similar_price">
                    <?php if ($data->productDiscount->discount != null): ?>
                            <!-- <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike> -->
                            <span><?= number_format((100-$data->productDiscount->discount*1)*$cityPrice/100, 0, '.', ' ') ?> р.</span>
                    <?php else: ?>
                        <?= number_format($cityPrice, 0, '.', ' ') ?> р.
                    <?php endif; ?>
                </div>
                <a class="button_similar button_buy buy_btn_main" product_id="<?=$data->id ?>" onclick="animate_cart_my(this);" ></a>
            </div>
        <?php endforeach; ?>

        <!--
            <div class="similar_sl_box">
                <div class="similar_sl_box_img"><img src="/images/similar_img.jpg"></div>
                <div class="similar_name">Кровать для детей корсар-3</div>
                <div class="similar_price">12 000 р.</div>
                <a class="button_similar" href="#"></a>
            </div>
            <div class="similar_sl_box">
                <div class="similar_sl_box_img"><img src="/images/similar_img.jpg"></div>
                <div class="similar_name">Кровать для детей корсар-3</div>
                <div class="similar_price">12 000 р.</div>
                <a class="button_similar" href="#"></a>
            </div>
            <div class="similar_sl_box">
                <div class="similar_sl_box_img"><img src="/images/similar_img.jpg"></div>
                <div class="similar_name">Кровать для детей корсар-3</div>
                <div class="similar_price">12 000 р.</div>
                <a class="button_similar" href="#"></a>
            </div>
            <div class="similar_sl_box">
                <div class="similar_sl_box_img"><img src="/images/similar_img.jpg"></div>
                <div class="similar_name">Кровать для детей корсар-3</div>
                <div class="similar_price">12 000 р.</div>
                <a class="button_similar" href="#"></a>
            </div>
            <div class="similar_sl_box">
                <div class="similar_sl_box_img"><img src="/images/similar_img.jpg"></div>
                <div class="similar_name">Кровать для детей корсар-3</div>
                <div class="similar_price">12 000 р.</div>
                <a class="button_similar" href="#"></a>
            </div>
            -->
        </div>

    </div>
</div>



<div class="ledy clear-fix">
    <div class="container">
        <div class="ledy_bgr"></div>
        <div class="ledy_column_1">

            <!-- Статьи -->
            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <div class="ledy_box">
                <?= BsHtml::image('/uploads/product/'.$article[$ar_rand]['image'],$article[$ar_rand]['title'] , array('class'=>'img_article')) ?>

                <div class="ledy_box_text">
                    <a href="#" class="ledy_box_title"><?php echo $article[$ar_rand]['title']; ?></a>

                    <div class="ledy_box_description">
                    <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $article[$ar_rand]['description'])), 0, 100).'...'; ?>
                    </div>
                    <a href="/site/article/<?php echo $article[$ar_rand]['id']?>"><div class="ledy_box_see"></div></a>
                </div>
            </div>

            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <div class="ledy_box">
                <?= BsHtml::image('/uploads/product/'.$article[$ar_rand]['image'],$article[$ar_rand]['title'] , array('class'=>'img_article')) ?>

                <div class="ledy_box_text">
                    <a href="#" class="ledy_box_title"><?php echo $article[$ar_rand]['title']; ?></a>

                    <div class="ledy_box_description">
                    <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $article[$ar_rand]['description'])), 0, 100).'...'; ?>
                    </div>
                    <a href="/site/article/<?php echo $article[$ar_rand]['id']?>"><div class="ledy_box_see"></div></a>
                </div>
            </div>
        </div>
        <div class="ledy_column_2">
            <div class="ledy_box_word">
                <div class="word_box">
                    <div class="angle_top"></div>
                    <div class="angle_down"></div>
                    <div class="why_we_word">Полезные статьи, которые<br/>помогут вам выбрать мебель</div>
                </div>
            </div>

            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <div class="ledy_box_right">
                <?= BsHtml::image('/uploads/product/'.$article[$ar_rand]['image'],$article[$ar_rand]['title'] , array('class'=>'img_article')) ?>

                <div class="ledy_box_text_right">
                    <a href="#" class="ledy_box_title"><?php echo $article[$ar_rand]['title']; ?></a>

                    <div class="ledy_box_description">
                    <?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $article[$ar_rand]['description'])), 0, 100).'...'; ?>
                    </div>
                    <a href="/site/article/<?php echo $article[$ar_rand]['id']?>"><div class="ledy_box_see"></div></a>
                </div>
            </div>
        </div>
    </div>
</div>