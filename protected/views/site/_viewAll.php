<!-- NSV file -->
<?php
mb_internal_encoding("UTF-8");
$description = BsHtml::encode(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $data->description)); ;
if (strlen($description) >= 350)
{
    $description = mb_substr($description, 0, 300).'...';
}
?>

            <div class="product_box">
                <a href="/site/productDetail/<?=$data->id ?>" class="product_image">
                 <?php if ($data->productDiscount->discount != null): ?>
                    <div class="stock"><?= $data->productDiscount->discount; ?>%</div>
                    <?php endif; ?>
                    <div class="glaz"></div>
                    <?= BsHtml::image('/uploads/product/preview/'.$data->mainImage->image) ?>
                </a>
                <div class="product_name"><?= $data->product_name ?></div>
                <div class="product_price">
                
                <?php
                $cityPrice = $data->price;
            
                if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->productCityPrice) && !empty($data->productCityPrice))
                {
                    foreach ($data->productCityPrice as $city_p)
                    {
                        if ($city_p->id_city == $_COOKIE['city'])
                        {
                            $cityPrice = $city_p->price;
                        }
                    }
                }
                ?>
                
                
                
                    <?php if ($data->productDiscount->discount != null): ?>
                            <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike> 
                            <span><?= number_format((100-$data->productDiscount->discount*1)*$cityPrice/100, 0, '.', ' ') ?> р.</span>
                    <?php else: ?>
                        <?= number_format($cityPrice, 0, '.', ' ') ?> р.
                    <?php endif; ?>
                
                </div>
                <div class="product_line"></div>
                <div class="button_buy buy_btn_main" product_id="<?=$data->id ?>" onclick="animate_cart_my(this);"></div>
            </div>