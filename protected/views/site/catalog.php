<!-- NSV file -->
<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

//$cs->registerCssFile($pt.'css/slider.css');
//$cs->registerScriptFile($pt.'js/slider.js');

$meta = Meta::model()->findByPk(2);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
mb_internal_encoding("UTF-8");
?>


<?php
$sum = '<a href="/site/catalog">Каталог</a>';

if (isset($_GET['id'])) {
    $category_description = Category::model()->findByPk($_GET['id']);
    $desC = $category_description->category_description;
    $nameC = $category_description->name;

    $catP = Category::model()->findByPk($_GET['id']);
    $t3 = ' - ';
    $t3 .= '<a href="/site/catalog/?id=' . $catP->id . '">' . $catP->name . '</a>';

    if ($catP->category_id != 0) {
        $secP = Category::model()->findByAttributes(array('id' => $catP->category_id));
        $t2 = ' - ';
        $t2 .= '<a href="/site/catalog/?id=' . $secP->id . '">' . $secP->name . '</a>';

        $nameC = $secP->name . ', ' . $nameC;

        if ($secP->category_id != 0) {
            $frP = Category::model()->findByAttributes(array('id' => $secP->category_id));
            $t1 = ' - ';
            $t1 .= '<a href="/site/catalog/?id=' . $frP->id . '">' . $frP->name . '</a>';
        }
    }
} else {
    $catD = ConfigMain::model()->findByPk(7);
    $desC = $catD->description;
}

$sum .= $t1;
$sum .= $t2;
$sum .= $t3;

$nameC = mb_convert_case($nameC, MB_CASE_TITLE, "UTF-8");

if (isset($_GET['production'])) {
    $production = Production::model()->findByPk($_GET['production']);
    $sum .= ' - Все товары производителя &ldquo;' . $production->name . '&rdquo;';
}

$this->urlvar = $sum;
?>

<div class="category_more">
    <div class="container clear-fix">
        <div class="category_more_catalog">
            <div class="category_more_catalog_category">
                <?php //$catP->name?>
            </div>
            <div class="category_more_catalog_title">
                <?php if (isset($nameC)): ?>
                    <?= $nameC ?>
                <?php endif; ?>
            </div>
            <div class="category_more_catalog_description">
                <?= $desC ?>
            </div>
        </div>
        <div class="category_more_slider">
            <div class="category_more_slider_title clear-fix">
                <p>САМЫЕ ПОПУЛЯРНЫЕ МОДЕЛИ</p>
            </div>
            <div class="catalog_slider">
                <?php for ($i = 1; $i <= 6; $i++): ?>
                    <?php $this->renderPartial('_all_product_view', array('all_product' => $all_product)); ?>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</div>

<div class="main_catalog clear-fix">
    <div class="container">
        <?php $this->widget('application.widgets.CatalogWidget', array('wid_view' => 'catalog_widget')); ?>
        <div class="main_catalog_right">

            <?php
            if ($scenario == 'category') {
                $this->widget('bootstrap.widgets.BsListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_view',
                    'id' => 'prod_cat',
                    'afterAjaxUpdate' => 'function(){new_css();top_body()}',
                    'template' => '
                    <div class="catalog_top_pagination">{pager}</div>
                        {items}
                    <div class="catalog_bot_pagination"> {pager}</div>',
                    'pager' => array(
                        //'afterAjaxUpdate' => 'function(){top_body()}',
                        'header' => '',
                        'prevPageLabel' => '<',
                        'previousPageCssClass' => 'article_navigate_button',
                        'nextPageLabel' => '>',
                        'nextPageCssClass' => 'article_navigate_button',
                        'maxButtonCount' => '6',
                        'firstPageLabel' => '|<',
                        'firstPageCssClass' => 'article_navigate_button',
                        'lastPageLabel' => '>|',
                        'lastPageCssClass' => 'article_navigate_button',
                        'internalPageCssClass' => 'article_navigate_button',
                        'htmlOptions' => array(
                            'class' => 'article_navigate',
                        )
                    ),
                ));
            }

            if ($scenario == 'all') {
                $this->widget('bootstrap.widgets.BsListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_viewAll',
                    'id' => 'prod_all',
                    'afterAjaxUpdate' => 'function(){new_css();top_body()}',
                    'template' => '
                    <div class="catalog_top_pagination">{pager}</div>
                        {items}
                    <div class="catalog_bot_pagination"> {pager}</div>',
                    'pager' => array(
                        //'afterAjaxUpdate' => 'function(){top_body()}',
                        'header' => '',
                        'prevPageLabel' => '<',
                        'previousPageCssClass' => 'article_navigate_button',
                        'nextPageLabel' => '>',
                        'nextPageCssClass' => 'article_navigate_button',
                        'maxButtonCount' => '6',
                        'firstPageLabel' => '|<',
                        'firstPageCssClass' => 'article_navigate_button',
                        'lastPageLabel' => '>|',
                        'lastPageCssClass' => 'article_navigate_button',
                        'internalPageCssClass' => 'article_navigate_button',
                        'htmlOptions' => array(
                            'class' => 'article_navigate',
                        )
                    ),
                    //'pagerCssClass' => 'catalog_bot_pagination',
                ));
            }
            ?>

        </div>
    </div>
</div>
</div>