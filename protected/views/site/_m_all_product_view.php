<!-- NSV FILE -->
<!-- Файл одного продукта, используется в списках рендомного выбора:
на главной, в корзине
-->

<?php $ar_rand = rand(0, count($all_product['name']) - 1); ?>
<div class="m_we_recomended_slider_box">
    <a href="/site/productDetail/<?= $all_product['id'][$ar_rand] ?>" class="m_we_recomended_slider_image_box">
        <img class="m_we_recomended_slider_img" src="/uploads/product/preview/<?= $all_product['image'][$ar_rand] ?>"/>
    </a>
    <div class="m_we_recomended_slider_name">
        <?= $all_product['name'][$ar_rand] ?>
    </div>
</div>

