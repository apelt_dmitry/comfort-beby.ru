<?php
mb_internal_encoding("UTF-8");

$meta = Meta::model()->findByPk(8);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>
<script>

    $(document).ready(function(){
        $('.link_icon').click(function(){
            $('#UserComment_icon_id').val($(this).attr('icon_id'));
            $('#icon_select').attr('src', $(this).find('img').attr('src'));
            $('#myModal').modal('toggle');
            return false;
        })
    })

</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Выбрать фото</h4>
            </div>
            <div class="modal-body">
                <?php
                    $icon_site = IconComment::model()->findAllByAttributes(array('icon_user' => 1));
                    foreach( $icon_site as $val ){
                        echo BsHtml::link(BsHtml::image('/uploads/icon/preview/'.$val->icon), '#', array('class' => 'link_icon', 'icon_id' => $val->id));
                    }
                ?>
            </div>

            <div class="modal-footer">
                <?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
                    array(
                        'id'=>'uploadFile',
                        'config'=>array(
                            'action'=>Yii::app()->createUrl('site/upload'),
                            'allowedExtensions'=>array("jpg", "jpeg", "gif", "png"),//array("jpg","jpeg","gif","exe","mov" and etc...
                            'sizeLimit'=>10*1024*1024,// maximum file size in bytes
                            //'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
                            'onComplete'=>"js:function(id, fileName, responseJSON){
                                $('#icon_select').attr({'src':'/uploads/icon/preview/'+fileName});
                                $('#myModal').modal('toggle');
                                $('#icon_name_input').val(fileName);
                            }",
                            //'messages'=>array(
                            //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                            //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                            //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                            //                  'emptyError'=>"{file} is empty, please select files again without it.",
                            //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                            //                 ),
                            //'showMessage'=>"js:function(message){ alert(message); }"
                        )
                    )); ?>
                <div>Размер фото до 2mb</div>
            </div>

        </div>
    </div>
</div>


    <?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'form_comment',
        'options' => array(
            'width' => 699,
            'height' => 325,
            'title' => false,
            'autoOpen' => false,
            'modal' => true,
            'resizable'=> false,
            'closeOnEscape'=> true,
            'open'=>'js:function(){
                    $(\'.ui-widget-overlay\').click(function(){
                        $(\'#form_comment\').dialog(\'close\');
                    })
                }',
            'hide'=>array(
                'effect'=>'explode',
                'duration'=>500,
            ),
        ),
    ));
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quick-form_comment',
        'enableClientValidation' => true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>true,
        ),
        'action' => array('site/comment'),
    ));
    ?>



<div class="my-14 text-center">
    <img src="/images/empty_user.png" style="width: 77px; height: 77px" id="icon_select">
    <a href="#" id="addFoto" data-toggle="modal" data-target="#myModal">Выберите фото</a>
</div>

<div class="my-86">

    <?= $form->hiddenField($form_comment, 'icon_id') ?>
    <input type="hidden" name="name_image" value="no" id="icon_name_input">

    <div class="row">
        <?= $form->textField($form_comment,'name', array(
            'placeHolder' => 'Ваше имя',
            'id' => 'comment_name',
        ));
        ?>
    </div>

    <div class="row">
        <?= $form->textArea($form_comment,'comment', array(
            'placeHolder' => 'Ваш комментарий',
            'id' => 'comment_area',
        ));
        ?>
    </div>

</div>

<div class="row text-right">
    <?= CHtml::submitButton('ОСТАВИТЬ КОММЕНТАРИЙ', array(
        'id' => 'comment_form_button',

        'padding' => '10px',
    ));
    ?>
</div>


<?php
$this->endWidget();
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<div class="row" style="margin-top:10px">

    <div class="my-24">
        <ul id="catalog_main">
            <?=$result_categories ?>
        </ul>

        <div style="width: 98%;" class="margin_bottom-15">
            <?php
            for ($i = 1; $i <= 2; $i++):
                $ar_rand = rand(0, count($all_product['name'])-1);
                ?>

                <div style="text-align: center; color: #939393;" class="font_bold margin_bottom-15 margin_top-40"><?= $all_product['name'][$ar_rand] ?></div>


                <div class="special_img">
                    <a href="/site/productDetail/<?=$all_product['id'][$ar_rand]?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                </div>

                <div style="text-align: center;font-size:16px;color:#bfbfbf;margin-top: 5px" class="margin_bottom-25"><?= mb_substr($all_product['description'][$ar_rand], 0, 400).'...'; ?></div>

                <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="margin_bottom-25 description_left">Подробнее</a>

            <?php endfor ?>
        </div>

    </div>

    <div class="my-76">

        <div class="row" style="padding-bottom: 20px; border-bottom: 2px solid #d7d7d7">
            <div id="button_comment">
                <?= CHtml::link('Оставить отзыв', '#', array('onclick' => '$("#form_comment").dialog("open"); return false;', 'class'=>'link_comment')); ?>
            </div>
        </div>

        <div class="row">
            <?php
                $this->widget('bootstrap.widgets.BsListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_viewComment',
                    'pager'=>array(
                        'header' => '',
                        'prevPageLabel' => '',
                        'nextPageLabel' => '',
                        'maxButtonCount' => '4',
                        'firstPageLabel'=> false,
                        'lastPageLabel'=> false,
                        'htmlOptions'=>array(
                            'class' => 'pagination',
                        )

                    ),
                ));
            ?>
        </div>
    </div>

</div>