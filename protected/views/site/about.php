<!-- NSV file -->
<?php
$meta = Meta::model()->findByPk(4);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
$config = ConfigMain::model()->findAll();
        $i = 1;
        foreach($config as $val){
            $text[$i]['title'] = $val->title;
            $text[$i]['description'] = $val->description;
            $i++;
        }
?>

<div class="about_us_family">
    <div class="container family_box">
        <div class="about_us_family_bgr"></div>
        
        <?php $title_about  = explode(',', $text[1]['title']); ?>
        <div class="about_us_family_black_text"><p><?php echo $title_about[0]; ?>,</p></div>
        <div class="about_us_family_yellow_text"><p><?php echo $title_about[1]; ?></p></div>
    </div>
</div>
<div class="about_us_balls">
    <div class="container about_us_balls_box">
        <div class="about_us_balls_bgr"></div>

        <div class="about_us_balls_text_box">
            <div class="about_us_balls_title"><img src="/images/about_us_img_1.png">10 лет на рынке</div>
            <div class="about_us_balls_description">За 10 лет, мы приобрели огромный опыт работы. Это позволило нам
                выстроить идеальную схему сотрудничества с клиентами
            </div>
            <div class="about_us_balls_title"><img src="/images/about_us_img_2.png">Быстрая доставка</div>
            <div class="about_us_balls_description">Если вы проживаете в Москве или области, то детскую мебель привезут
                и соберут уже на следующий день. В другие районы России заказ прибудет с надежной транспортной компанией
            </div>
            <div class="about_us_balls_title"><img class="corr" src="/images/about_us_img_3.png">Гарантия 18 месяцев
            </div>
            <div class="about_us_balls_description">Мы даем официальную гарантию на любой вид детской мебели. Можете не
                переживать, когда ваши детки прыгают на новой кровати
            </div>
            <div class="about_us_balls_title"><img class="corr_2" src="/images/about_us_img_4.png">Наличие товара</div>
            <div class="about_us_balls_description">Вы не узнаете никогда, что такое выбрать понравившуюся детскую
                мебель и не купить ее, потому что ее нет в наличии. Мы выставляем на продажу только ту детскую мебель,
                которая есть в наличии здесь и сейчас
            </div>
        </div>
    </div>
</div>
<div class="clients_trust">
    <div class="container clients_trust_box clear-fix">
        <div class="about_us_boys"></div>
        <div class="clients_trust_women"></div>
        <div class="clients_trust_title">Клиенты нам доверяют</div>
        <div class="clients_trust_description">
            <?=$text[1]['description'] ?>
        </div>
        <a href="<?php echo Yii::app()->createUrl('/site/catalog'); ?>"><div class="clients_trust_button"></div></a>
    </div>
</div>
<div class="oculus">
    <div class="container clear-fix">
        <div class="oculus_bgr"></div>
        <div class="oculus_title">Реквизиты компании:</div>
        <div class="oculus_description">Магазин "Комфорт", ул. Рябиновая, 41, корп.1,<br/>
            ОГРН: 1097746103388, ИНН: 7723707852, ООО "МебТорг"<br/>
            Юр.адрес: МОСКВА, ул. АЛЕКСЕЯ ДИКОГО, д. 5, пом. 1,<br/> shop@comfort-beby.ru<br/>
            тел.: +7 (495) 374-95-78, 8 (800) 555-87-06"<br/>
        </div>
    </div>
</div>