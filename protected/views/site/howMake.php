<?php
$meta = Meta::model()->findByPk(5);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>
<div class="order">
    <div class="container clear-fix">
        <div class="order_bgr"></div>
        <div class="order_text_yellow clear-fix">
            <p>
                ЗАКаЗАТЬ ДЕТСКУЮ МЕБЕЛЬ В НАШЕМ
            </p>
        </div>
        <div class="order_text_black clear-fix">
            <p>
                ИНТЕРНЕТ МАГАЗИНЕ ОЧЕНЬ ЛЕГКО
            </p>
        </div>
    </div>
</div>
<div class="order_boxes">
    <div class="container clear-fix">
        <div class="boxes">
            <div class="boxes_text_1">
                Выберите понравившуюся Вам
                модель и нажмите на кнопку
                “купить”. Товар появится в Вашей
                корзине, где Вы сможете выбрать
                колличество товара и ввести
                промокод на скидку.
            </div>
            <div class="boxes_text_uppercase_1">
                Карточка товара
            </div>
            <div class="boxes_text_4">
                Вы можете выбрать цвет и
                комплектацию на странице товара.
                Для этого достаточно нажать на фото
                понравившейся Вам модели
            </div>
            <div class="boxes_text_uppercase_2">
                Оформление заказа
            </div>
            <div class="boxes_text_5">
                Будьте внимательны во время ввода
                своих данных. В противном случае
                мы не сможем с Вами связаться
            </div>
            <div class="boxes_text_2">
                На странице “Корзины” нажмите
                на кнопку ”Оформить заказ”.
                Введите свои данные. Наш
                менеджер свяжется с Вами
                для уточнения деталей Вашего
                заказа.
            </div>
            <div class="boxes_text_3">
                Выберите понравившуюся Вам
                модель и нажмите на кнопку
                “купить”. Товар появится в Вашей
                корзине, где Вы сможете выбрать
                колличество товара и ввести
                промокод на скидку.
            </div>
            <div class="boxes_text_uppercase_3">
                Оплата

            </div>
            <div class="boxes_text_6">
                Оплата любым удобным для вас
                способом: в рублях при получении(
                для Москвы и МО) или по
                безналичному расчету для регионов.
            </div>
        </div>
        <div class="boxes_mac"></div>
        <div class="boxes_line"></div>
    </div>
</div>
<div class="order_boy">
    <div class="container clear-fix">
        <div class="order_boy_bgr"></div>
        <a href="/site/catalog"><div class="order_boy_button"></div></a>
        <div class="word_white_box">
            <div class="angle_white_top"></div>
            <div class="angle_white_down"></div>
            <div class="white_word">ТаК просто, что даже<br/>ребенок сможет</div>
        </div>
    </div>

</div>