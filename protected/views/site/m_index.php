<?php

$hit_prodaj = Yii::app()->db->createCommand('SELECT  p.product_name, pi.image, d.discount, p.id, p.price
                                        FROM ProductCategory pc
                                        LEFT JOIN product p ON p.id=pc.product
                                        LEFT JOIN product_image pi ON pi.product_id = pc.product
                                        LEFT JOIN discount d ON d.id= p.discount_id
                                        WHERE pc.category=32
                                        GROUP BY pi.product_id
                                        LIMIT 4')->queryAll();
$novinki = Yii::app()->db->createCommand('SELECT p.product_name, pi.image, d.discount, p.id, p.price
                                        FROM ProductCategory pc
                                        LEFT JOIN product p ON p.id=pc.product
                                        LEFT JOIN product_image pi ON pi.product_id = pc.product
                                        LEFT JOIN discount d ON d.id= p.discount_id
                                        WHERE pc.category=33
                                        GROUP BY pi.product_id
                                        LIMIT 4')->queryAll();
$rasprodaja= Yii::app()->db->createCommand('SELECT p.product_name, pi.image, d.discount, p.id, p.price
                                        FROM ProductCategory pc
                                        LEFT JOIN product p ON p.id=pc.product
                                        LEFT JOIN product_image pi ON pi.product_id = pc.product
                                        LEFT JOIN discount d ON d.id= p.discount_id
                                        WHERE pc.category=27
                                        GROUP BY pi.product_id
                                        LIMIT 4')->queryAll();
?>
<div class="m_main_category">
    <div class="clear-fix m_container">
        <div class="m_main_category_box">
            <div class="m_main_category_title drawer-toggle">Категории мебели</div>
        </div>
        <div id="m_box_main_category">

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 27)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Распродажа</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 32)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Популярные товары</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 33)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Новинки</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 1)); ?>"class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Детские комнаты</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 34)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Модульная мебель</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 4)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Кровать - чердаки</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 16)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Детские<br/> кровати</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 7)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">2-х ярусные кровати</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 10)); ?>"  class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Матрасы</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 6)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Детские<br/> диваны</div>
                </div>
            </a>

            <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 5)); ?>" class="m_box_category">
                <img class="m_box_category_img" src="/images/product_img.png">

                <div class="m_main_name_box">
                    <div class="m_main_name">Кровать машина</div>
                </div>
            </a>

        </div>
    </div>
</div>

<div class="m_main_why">
    <div class="clear-fix m_container">
        <div class="m_main_why_yellow">Почему вам нужно покупать детскую</div>
        <div class="m_main_why_black">мебель в нашем интернет магазине</div>
        <img class="m_note_cart" src="/images/m_note_cart.png">
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_1.png">

    <div class="m_main_advantage_text_box">
        <span>Гарантия 18 месяцев</span>
        Мы даем официальную гарантию на любой вид
        детской мебели. Можете не переживать, когда
        ваши детки прыгают на новой кровати
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_2.png">

    <div class="m_main_advantage_text_box">
        <span>Быстрая доставка</span>
        Если вы проживаете в Москве или области, то
        детскую мебель привезут и соберут уже на
        следующий день. В другие районы России заказ
        прибудет с надежной транспортной компанией
    </div>
</div>
<div class="m_container clear-fix m_main_advantage">
    <img class="m_main_advantage_img" src="/images/m_yellow_mini_3.png">

    <div class="m_main_advantage_text_box">
        <span>Вы ничем не рискуете</span>
        Потому что платите за детскую комнату или
        мебель только после того, как вам ее доставят.
        Никакой предоплаты!
    </div>
</div>

<div class="m_main_slider_box">
    <div class="m_container clear-fix">
        <div class="m_main_slider_header"></div>
        <div class="m_main_slider">
            <div class="m_main_slider_slide">
                <a class="m_product_link" href="/site/catalog/32">Популярные товары</a>
                <?php foreach ($hit_prodaj as $prod): ?>
                    <div class="m_product_box">
                        <a href="/site/productDetail/<?= $prod['id'] ?>">
                            <img class="m_product_box_image" src="/uploads/product/preview/<?= $prod['image'] ?>">
                        </a>


                        <div class="m_product_box_name"><?= $prod['product_name'] ?></div>
                        <div class="m_product_box_price"><?= $prod['price'] ?> р.-</div>
                        <div class="m_product_line"></div>
                        <div class="m_product_button buy_btn_main" product_id="<?= $prod['id'] ?>"></div>
                        <div class="m_hit"></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="m_main_slider_slide">
                <a class="m_product_link" href="/site/catalog/33">Новинки</a>
                <?php foreach ($novinki as $nov): ?>
                    <div class="m_product_box">
                        <a href="/site/productDetail/<?= $nov['id'] ?>">
                            <img class="m_product_box_image" src="/uploads/product/preview/<?= $nov['image'] ?>">
                        </a>


                        <div class="m_product_box_name"><?= $nov['product_name'] ?></div>
                        <div class="m_product_box_price"><?= $nov['price'] ?> р.-</div>
                        <div class="m_product_line"></div>
                        <div class="m_product_button buy_btn_main"  product_id="<?= $nov['id'] ?>"></div>
                        <div class="m_new"></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="m_main_slider_slide">
                <a class="m_product_link" href="/site/catalog/27">Распродажа</a>
                <?php foreach ($rasprodaja as $raspr): ?>
                    <div class="m_product_box">
                        <a href="/site/productDetail/<?= $raspr['id'] ?>">
                            <img class="m_product_box_image" src="/uploads/product/preview/<?= $raspr['image'] ?>">
                        </a>

                        <div class="m_product_box_name"><?= $raspr['product_name'] ?></div>
                        <div class="m_product_box_price">
                            <strike>
                                <?= number_format($raspr['price'], 0, '.', ' ') ?>р.
                            </strike>
                        <span style="color: #AE0000"> 
                            <?= number_format((100 - $raspr['discount'] * 1) * $raspr['price'] / 100, 0, '.', ' ') ?>р.
                        </span>
                        </div>
                        <div class="m_product_line"></div>
                        <div class="m_product_button buy_btn_main" product_id="<?= $raspr['id'] ?>"></div>
                        <div class="m_sale"></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class="m_go_article">
    <div class="m_container clear-fix">
        <div class="m_go_article_yellow">Полезные стать, которые помогут</div>
        <div class="m_go_article_black">Вам выбрать детскую мебель</div>
        <div class="m_big_article_box">
            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <?= BsHtml::image('/uploads/product/preview/'.$article[$ar_rand]['image']) ?>

            <a href="/site/article/<?php echo $article[$ar_rand]['id']?>" class="m_big_article_inside">
                <?php echo $article[$ar_rand]['title']; ?>
            </a>
        </div>
        <div class="m_big_article_box">
            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <?= BsHtml::image('/uploads/product/preview/'.$article[$ar_rand]['image']) ?>

            <a href="/site/article/<?php echo $article[$ar_rand]['id']?>" class="m_big_article_inside">
                <?php echo $article[$ar_rand]['title']; ?>
            </a>
        </div>
        <div class="m_big_article_box">
            <?php  $ar_rand = rand(0, count($article)-1); ?>
            <?= BsHtml::image('/uploads/product/preview/'.$article[$ar_rand]['image']) ?>

            <a href="/site/article/<?php echo $article[$ar_rand]['id']?>" class="m_big_article_inside">
                <?php echo $article[$ar_rand]['title']; ?>
            </a>
        </div>
    </div>
</div>