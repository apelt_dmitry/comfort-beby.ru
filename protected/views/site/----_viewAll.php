<?php
mb_internal_encoding("UTF-8");
$description = BsHtml::encode(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $data->description)); ;
if (strlen($description) >= 350)
{
    $description = mb_substr($description, 0, 300).'...';
}
?>
<div style="float: left;  position: relative;" class="width100">
    <div class="view_product margin_left-21" style="margin-bottom: 20px; display: table;">
        <div>
            <table class="table_name">
                <tr>
                    <td style="text-align: center" class="product_name_js font_bold"><?= $data->product_name ?></td>
                </tr>
            </table>
            <div class="view_img" style="position: relative">
                <a href="/site/productDetail/<?=$data->id ?>">
                    <?= BsHtml::image('/uploads/product/preview/'.$data->mainImage->image) ?>
                    <div class="shadow_img"></div>
                </a>
            </div>
        </div>
        <table class="table_detail" >
            <tr>
                <?php if( $data->act_id!=0 ): ?>
                    <img src="/images/act.png" class="act_img"/>
                <?php endif ?>
                
                <?php
                $cityPrice = $data->price;
            
                if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->productCityPrice) && !empty($data->productCityPrice))
                {
                    foreach ($data->productCityPrice as $city_p)
                    {
                        if ($city_p->id_city == $_COOKIE['city'])
                        {
                            $cityPrice = $city_p->price;
                        }
                    }
                }
                ?>
                
                <?php if ($data->productDiscount->discount != null): ?>
                    <div class="discount dc"><?= $data->productDiscount->discount ?>%</div>
                    <td class="table_price"><?= number_format((100-$data->productDiscount->discount*1)*$cityPrice/100, 0, '.', ' ') ?> р.</td>
                <?php else: ?>
                    <td class="table_price"><?= number_format($cityPrice, 0, '.', ' ') ?> р.</td>
                <?php endif; ?>
                <td class="table_link"><a href="/site/productDetail/<?=$data->id ?>">Подробнее</a></td>
            </tr>
        </table>
    </div>
    <div style="display: none" class="description_js"><?=  ($description);  ?></div>
</div>