<?php
$meta = Meta::model()->findByPk(5);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>
<div class=" m_container m_breadcrumbs">
    <a href="/" style="text-decoration: none">Главная</a> > <a href="/site/how" style="text-decoration: none">Как сделать заказ</a>
</div>

<div class="m_order">
    <div class="clear-fix m_container">
        <div class="m_order_yellow">ЗАКаЗАТЬ ДЕТСКУЮ МЕБЕЛЬ В НАШЕМ</div>
        <div class="m_order_black">ИНТЕРНЕТ МАГАЗИНЕ ОЧЕНЬ ЛЕГКО</div>
        <div class="m_order_mac"></div>
        <div class="m_order_step_box">
            <img src="/images/m_order_step_1.png">

            <div class="m_order_step_text">
                <span>ШАГ 1</span>
                Выберите понравившуюся Вам модель и нажмите на кнопку“купить”. Товар появится в Вашейкорзине, где Вы
                сможете выбратьколличество товара и ввести промокод на скидку.
            </div>
            <div class="m_order_step_text_grey">
                <span>Карточка товара </span>
                Вы можете выбрать цвет и комплектацию на странице товара.Для этого достаточно нажать на фото
                понравившейся Вам модели
            </div>
        </div>
        <div class="m_order_step_box">
            <img src="/images/m_order_step_2.png">

            <div class="m_order_step_text">
                <span>ШАГ 2</span>
                На странице “Корзины” нажмите на кнопку ”Оформить заказ”. Введите свои данные. Наш менеджер свяжется с Вами для уточнения деталей Вашего заказа.
            </div>
            <div class="m_order_step_text_grey">
                <span> Оформление заказа </span>

                Будьте внимательны во время вводасвоих данных.
                В противном случае мы не сможем с Вами связаться
            </div>
        </div>
        <div class="m_order_step_box">
            <img src="/images/m_order_step_3.png">

            <div class="m_order_step_text">
                <span>ШАГ 3</span>
                Выберите понравившуюся Вам модель и нажмите на кнопку “купить”. Товар появится в Вашей корзине, где Вы сможете выбрать колличество товара и ввести промокод на скидку.
            </div>
            <div class="m_order_step_text_grey">
                <span>ОПЛАТА </span>
                Оплата любым удобным для вас способом: в рублях при получении( для Москвы и МО) или по безналичному расчету для регионов.
            </div>
        </div>
    </div>
</div>