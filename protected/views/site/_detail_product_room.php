<?php
$comp = InRoom::model()->findAllByAttributes(array(
    'room_id' => $model->id,
))
?>
<div class="all_elements">
    <div class="container clear-fix">
        <div class="all_elements_title">Все элементы данного гарнитура</div>
        <div class="all_elements_box clear-fix">
        <?php foreach($comp as $val): ?>
            <?php $this->renderPartial('_viewAll', array('data'=>$val->ProductInRoom)); ?>
        <?php endforeach; ?>
        </div>
    </div>
</div>
