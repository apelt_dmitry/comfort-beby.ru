<!-- NSV file -->
<?php
mb_internal_encoding("UTF-8");
if (strlen($data->Product->description) <= 350) {
    $description = $data->Product->description;
} else {
    $description = mb_substr($data->Product->description, 0, 350) . '...';
}
?>

<div class="product_box">
    <a href="/site/productDetail/<?= $data->Product->id ?>" class="product_image">
        <?php if ($data->Product->productDiscount->discount != null): ?>
            <div class="stock"><?= $data->Product->productDiscount->discount; ?>%</div>
        <?php endif; ?>
        <div class="glaz"></div>
        <?= BsHtml::image('/uploads/product/preview/' . $data->Product->mainImage->image) ?>
    </a>

    <div class="product_name"><?= $data->Product->product_name ?></div>
    <div class="product_price">

        <?php
        $cityPrice = $data->Product->price;

        if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->Product->productCityPrice) && !empty($data->productCityPrice)) {
            foreach ($data->Product->productCityPrice as $city_p) {
                if ($city_p->id_city == $_COOKIE['city']) {
                    $cityPrice = $city_p->price;
                }
            }
        }
        ?>

        <?php if ($data->Product->productDiscount->discount != null): ?>
            <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike>
            <span><?= number_format((100 - $data->Product->productDiscount->discount * 1) * $cityPrice / 100, 0, '.', ' ') ?>
                р.</span>
        <?php else: ?>
            <?= number_format($cityPrice, 0, '.', ' ') ?> р.
        <?php endif; ?>

    </div>
    <div class="product_line"></div>
    <div class="button_buy buy_btn_main" product_id="<?= $data->id ?>" onclick="animate_cart_my(this);"></div>
</div>