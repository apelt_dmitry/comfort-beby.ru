<?php 
Yii::app()->name = $data->news_publish->meta_title;
?>
<div class="margin_left-21 margin_bottom-18">

    <div>
        <div class="font_bold font_size-22 margin_bottom-16" style="margin-top: 5px;" ><?=$data->news_publish->title ?></div>
        <div class="margin_bottom-16"><?=$data->news_publish->description ?></div>
        <div class="font_size-18"><?=BsHtml::image('/uploads/product/'.$data->news_publish->image) ?></div>
    </div>
    <a href="/site/news#news" class="all_news">Посмотреть все новости</a>

    <div class="social_bar">
        <script type="text/javascript">(function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();</script>
        <div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,nocounter,theme=08" data-services="vkontakte,odnoklassniki,facebook,twitter,moimir"></div>
        <!--<div class="say"><img src="/images/say.jpg"></div>-->
    </div>
</div>