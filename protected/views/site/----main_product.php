<?php
Yii::app()->name = $model->meta_title;
Yii::app()->clientScript->registerMetaTag($model->meta_content, 'description');
?>

<script>
    $(document).ready(function(){
        $('.listPole').each(function(){
            if( $(this).find('option[value="'+$(this).val()+'"]').attr('price_change')!='+0' ){
                $(this).prev().find('.type_pole_input').attr({
                    'name': $(this).find('option[value="'+$(this).val()+'"]').attr('name'),
                    'price_change': $(this).find('option[value="'+$(this).val()+'"]').attr('price_change'),
                    'value': $(this).val()
                });
                console.log('!')
                new_price();
            }
            else{
                $(this).prev().find('.type_pole_input').attr({
                    'name': $(this).find('option[value="'+$(this).val()+'"]').attr('name'),
                    'price_change': $(this).find('option[value="'+$(this).val()+'"]').attr('price_change'),
                    'value': $(this).val()
                });
                new_price();
            }
        });

        $('.listPole').change(function(){
            //alert($(this).find('option[value="'+$(this).val()+'"]').html());
            /*$('.type_pole_input').attr({
                'name': $(this).find('option[value="'+$(this).val()+'"]').attr('name'),
                'price_change': $(this).find('option[value="'+$(this).val()+'"]').attr('price_change'),
                'value': $(this).val()
            });*/
            $(this).prev().find('.type_pole_input').attr({
                'name': $(this).find('option[value="'+$(this).val()+'"]').attr('name'),
                'price_change': $(this).find('option[value="'+$(this).val()+'"]').attr('price_change'),
                'value': $(this).val()
            });
            new_price();
        });
        
        $('.basket_ajax').click(function(){
            var pole_name = [];
            var pole_id = [];
            var i = 0;
            $('input[type="radio"]:checked').each(function(){
                pole_name[i] = $(this).attr('name');
                pole_id[i] = $(this).attr('value');
                i++;
            });

            var product =  JSON.stringify([{
                'product_id': $('#product_id').attr('product_id'),
                'pole_name': pole_name,
                'pole_id': pole_id,
                'count': 1
            }]);

            if($.cookie('basket')==null){
                $.cookie('basket', product, { expires: 30, path: '/' });
            }
            else{
                var products = JSON.parse($.cookie('basket'));
                var hasProduct = false;
                for(var i in products){
                    if($('#product_id').attr('product_id')==products[i]['product_id']){
                        products[i]['count'] = products[i]['count']*1+1;
                        hasProduct = true;
                    }
                }
                if(!hasProduct){
                    products.push({
                        'product_id': $('#product_id').attr('product_id'),
                        'pole_name': pole_name,
                        'pole_id': pole_id,
                        'count': 1
                    });
                }
                $.cookie('basket', JSON.stringify(products), { expires: 30, path: '/' });
                $('#add_basket').css({
                    'left': ($(window).width()/2)-120,
                    'top': ($(window).height()/2)-100
                });

            }

            $('#add_basket').show();
            $('#add_basket').animate({
                'opacity': 1
            },300,function(){
                setTimeout(function(){
                    $('#add_basket').animate({
                        'opacity': 0
                    },300,function(){
                        $('#add_basket').hide();
                    })
                },1500)
            });

            basket();
            return false;
        })
		
		var product_id =  JSON.stringify([{
            'product_id': $('#product_id').attr('product_id')
        }]);

        if($.cookie('show_product')==null){
            $.cookie('show_product', product_id, { expires: 1, path: '/' });
        }
        else{
            var products = JSON.parse($.cookie('show_product'));
            
            var hasProduct = false;
            for(var i in products){
                if($('#product_id').attr('product_id')==products[i]['product_id']){
                    hasProduct = true;
                }
            }
            if(!hasProduct){
                products.push({
                    'product_id': $('#product_id').attr('product_id'),
                });
            }
            $.cookie('show_product', JSON.stringify(products), { expires: 1, path: '/' });
        }
    })
</script>

<?php
    if ($model->room == 1)
    {
        $this->renderPartial('_detail_product_room', array(
            'mas_pole' => $mas_pole,
            'all_product' => $all_product,
            'popular' => $popular,
            'model' => $model,
            'allR' => $allR,
            'sum' => $sum
        ));
    }
    else
    {
        $this->renderPartial('_detail_product',array(
            'mas_pole' => $mas_pole,
            'all_product' => $all_product,
            'popular' => $popular,
            'model' => $model,
            'allR' => $allR,
			'new_all_product' => $new_all_product,
            'sum' => $sum
        ));
    }
?>