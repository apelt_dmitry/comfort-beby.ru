<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs->registerCssFile($pt.'css/slider.css');
$cs->registerScriptFile($pt.'js/slider.js');

Yii::app()->name = 'Комфорт - поиск по магазину';
Yii::app()->clientScript->registerMetaTag('Комфорт - поиск по магазину', 'description');
mb_internal_encoding("UTF-8");
?>

<script>
    $(document).ready(function(){
        new_css();
        category_tree();
    });
	
	function top_body(){	
		$('body').animate({scrollTop: 448},1000);
	}
	
    function category_tree(){
        if( $.get('id')!='undefined' ){
            if($('.sortable a[category_id="'+ $.get('id')+'"]').parent().next().hasClass('sortable')){
                $('.sortable a[category_id="'+ $.get('id')+'"]').parent().next().show().find('ul').show();
            }
            $('.sortable a[category_id="'+ $.get('id')+'"]').parent().parent().show();
            $('.sortable a[category_id="'+ $.get('id')+'"]').parent().parent().parent().show();
            $('.sortable').find('a[category_id="'+$.get('id')+'"]').css('color', '#1d6887');
        }
    }

    function new_css(){
        var i = 0;
        $('.width100').each(function(){
            if(i%4==0){
                //$(this).css('width','100%');
                $(this).find('.table_name td').css({
                    'background': '#fff',
                    'color': '#000',
                    'text-align': 'left'
                });
                $(this).find('.view_img').css({
                    'width': '288px',
                    'height': '210px'
                });
                $(this).find('.view_img img').css({
                    'width': '288px',
                    'height': '210px'
                });
                $(this).find('.table_name').css('width', '288px');
                $(this).find('.table_detail').css({
                    'position': 'absolute',
                    'left': '320px',
                    'top': '228px'
                });
                $(this).find('.description_js').css({
                    'margin-top': '55px',
                    'width': '406px',
                    'margin-left': '320px',
                    'font-size': '16px',
                    'display': 'block'
                });
                $(this).find('.dc').css({
                    'left': '222px',
                    'top': '43px',
                    'width': '99px',
                    'height': '101px',
                    'padding-left': '31px',
                    'padding-top': '35px',
                    'background': 'url("/images/discount_popa.png") 0 0 no-repeat'
                });
                $(this).find('.act_img').attr('src', '/images/ak.png');
                $(this).find('.act_img').addClass('pz');
                $(this).find('.act_img').removeClass('act_img');

            }
            i++;
        })
    }
</script>

<div class="row" style="margin-top:10px">
    <div id="product" name="product"></div>
    <div class="my-24">
        <div style="width: 98%;" class="margin_bottom-15">
            <?php for ($i = 1; $i <= 2; $i++): ?>
            
                <?php $ar_rand = rand(0, count($all_product['name'])-1); ?>

                <div style="text-align: center; color: #939393;" class="font_bold margin_bottom-15 margin_top-40"><?= $all_product['name'][$ar_rand] ?></div>

                <div class="special_img">
                    <a href="/site/productDetail/<?=$all_product['id'][$ar_rand]?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                </div>

                <div style="text-align: center;font-size:16px;color:#bfbfbf;margin-top: 5px" class="margin_bottom-25"><?= mb_substr($all_product['description'][$ar_rand], 0, 400).'...'; ?></div>

                <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="margin_bottom-25 description_left">Подробнее</a>

            <?php endfor ?>
        </div>
    </div>
    <div class="my-76_no_padding">
        <div class="row" style="position:absolute; top:-30px;">
            <a href="/site/search">Поиск по магазину</a>
        </div>
        <?php if (!empty($result)): ?>
            <div style="display: table; width: 100%;">
                <?php $i=0; foreach ($result as $data): ?>
                    <?php 
                    $this->renderPartial('_viewAll', array(
                        'data' => Product::model()->findByPk($data["attrs"]['idproduct']),
                        'num' => $num+$i,
                    )); 
                    ?>
                <?php $i++; endforeach; ?>
            </div>
            <div>
                <?php
                /*
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header' => '',
                    'maxButtonCount' => 7,
                    'prevPageLabel' => 'предыдущая',
                    'nextPageLabel' => 'следующая',
                    'firstPageLabel' => 'первая',
                    'lastPageLabel' => 'последняя',
                    'cssFile' => false
                ));*/
                ?>
            </div>
        <?php else: ?>
            <div>Нет результатов</div>
        <?php endif; ?>
    </div>
</div>