
<!--<div class="m_all_elements">-->
    <div class="m_product_box">
        <a href="/site/productDetail/<?=$data->id ?>" class="product_image">
            <?= BsHtml::image('/uploads/product/preview/'.$data->mainImage->image,array('class'=>'m_product_box_image')) ?>
        </a>

        <div class="m_product_box_name"><?= $data->product_name ?></div>
        <div class="m_product_box_price">
            <?php
            $cityPrice = $data->price;

            if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->productCityPrice) && !empty($data->productCityPrice))
            {
                foreach ($data->productCityPrice as $city_p)
                {
                    if ($city_p->id_city == $_COOKIE['city'])
                    {
                        $cityPrice = $city_p->price;
                    }
                }
            }
            ?>
            <?php if ($data->productDiscount->discount != null): ?>
                <strike><?= number_format($cityPrice, 0, '.', ' ') ?> р.</strike>
                <span><?= number_format((100-$data->productDiscount->discount*1)*$cityPrice/100, 0, '.', ' ') ?> р.</span>
            <?php else: ?>
                <?= number_format($cityPrice, 0, '.', ' ') ?> р.
            <?php endif; ?> р.-
        </div>
        <div class="m_product_line"></div>
        <div class="m_product_button buy_btn_main" product_id="<?=$data->id ?>"></div>
    </div>
<!--</div>-->
