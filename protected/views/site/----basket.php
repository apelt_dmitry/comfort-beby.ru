<?php
$meta = Meta::model()->findByPk(6);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>

<script>
    $(document).ready(function(){
        $('.basket_count').click(function(){
            var cost = $('.cost[product_id='+$(this).attr('product_id')+']').html();
            var count =$('.product_count[product_id='+$(this).attr('product_id')+']').html();
            if($(this).hasClass('bsr')){
                if($('.product_count[product_id='+$(this).attr('product_id')+']').html()*1>=2){
                    $('.product_count[product_id='+$(this).attr('product_id')+']').html($('.product_count[product_id='+$(this).attr('product_id')+']').html()*1-1);
                }
            }
            else{
                $('.product_count[product_id='+$(this).attr('product_id')+']').html($('.product_count[product_id='+$(this).attr('product_id')+']').html()*1+1);
            }
            $('.cost[product_id='+$(this).attr('product_id')+']').html((cost/count)*$('.product_count[product_id='+$(this).attr('product_id')+']').html());
            count_down();
            count_post2();
        });
        $('#next_order').click(function(){
            $('#next_step').show();
            $('body').animate({scrollTop: 1500},1000);
			$('html').animate({scrollTop: 1500},1000)
        });
        count_down();

        $('.button_client').click(function(){
            $.cookie('basket', null, { path: '/' });
        });
        $('.kokoko[kokoko="1"]').css({
            'border-top':'none',
            'padding-top': 0
        });
        count_post();
        delete_from_basket();
        new_input();



    });

    function count_post2(){
        var i = 0;
        var count_post = [];
        $('.product_count').each(function(){
            count_post[$(this).attr('product_id')] = $(this).html();
            i++;
        });
        var id = product_id();
        for(var j in id){
            $('input[name="product_count['+id[j]+']"').attr('value',count_post[id[j]])
        }
        /*var j = 0;
         $('input[name="product_count[]"]').each(function(){
         $(this).attr('value',count_post[j]);
         $(this).attr('name','product_count['+$(this).attr('product_id')+']' );
         j++;
         })*/

    }


    function count_post(){
        var i = 0;
        var count_post = [];
        $('.product_count').each(function(){
            count_post[i] = $(this).html();
            i++;
        });
        var j = 0;
        $('input[name="product_count[]"]').each(function(){
            $(this).attr('value',count_post[j]);
            $(this).attr('name','product_count['+$(this).attr('product_id')+']' );
            j++;
        })

    }

    function delete_from_basket(){
        $('.delete_from_basket').click(function(){
            $(this).parent().parent().hide();
            var product_id = $(this).attr('product_id');
            $('.cost[product_id='+product_id+']').html('0');
            $('.product_count[product_id='+product_id+']').html('0');
            count_down();
            var new_product = [];
            var j=0;
            var products = JSON.parse($.cookie('basket'));
            for(var i in products){
                if(product_id!=products[i]['product_id']){
                    new_product[j] = products[i];
                    j++;
                }
            }
            $.cookie('basket', JSON.stringify(new_product), { expires: 30, path: '/' });
            basket();
            if(JSON.parse($.cookie('basket'))==''){
                $('.spss').hide();
                $('.wfg').show();
                $.cookie('basket', null, { path: '/' });
            }
        });
    }


    function count_down(){
        var html = 0/1;
        $('.cost').each(function(){
             html += $(this).html()*1;
        });
        $('#count_down').html(html);
    }

    function pole_basket(){
        var pole_basket = [];
        var id = 0;
        var i = 0;
        $('.pole_basket').each(function(){
            if(id!=$(this).attr('product_id')){
                i++;
                pole_basket[i] = '';
            }
            id = $(this).attr('product_id');
            pole_basket[i] += $(this).html()+'; ';
        });
        return pole_basket;
    }

    function new_input(){
        var id = product_id();
        var pole = pole_basket();
        for(var i in id){
            $('input[name="pole[]"]').each(function(){
                if($(this).attr('product_id')==id[i]){
                    $(this).attr('value',pole[i]);
                    $(this).attr('name','pole['+id[i]+']');
                }
            })
        }
    }

    function product_id(){
        var product_id = [];
        var id = 0;
        var i = 0;
        $('.pole_basket').each(function(){
            if(id!=$(this).attr('product_id')){
                i++;
                product_id[i] = $(this).attr('product_id');
            }
            id = $(this).attr('product_id');
        })
        return product_id;
    }

</script>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'contact-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation' => true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
)); ?>


<?php
$product_id = array();
$kokoko = 1;
if($scenario=='basket'): ?>
    <div class="row wfg font_size-24" style="display: none">ПУСТО :(</div>
    <?php foreach($model as $val): ?>
        <div class="row margin_top-40 kokoko" kokoko="<?=$kokoko ?>">

            <div class="my-50">

                <?php
                $product = Product::model()->findByPk($val['product_id']);
                $product_id[] = $product->id;
                ?>
                <div class="my-40">
                    <img src="/uploads/product/preview/<?=$product->mainImage->image ?>" style="width: 100%;">
                </div>

                <div class="my-60" style="padding-left: 10px">
                    <div class="font_bold font_size-24 pn"><?=$product->product_name ?></div>

                    <?php
                        $price_change = 0;
                        foreach($val['pole_id'] as $key=>$val2):
                            $pole = ProductPole::model()->findByPk($val2);
                            if( $pole->price_change!='+0' ){
                                $price_change_id[$val['product_id']] = $pole->id;
                                $action = substr($pole->price_change,0,1);
                                $price_change = substr($pole->price_change,1);
                            }
                    ?>
                        <div class="pole_basket" style="font-size: 12px" product_id="<?=$product->id ?>" price_change="<?=$pole->price_change ?>"><?=$val['pole_name'][$key]; ?>: <?=$pole->value; ?></div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="my-50">
                <div class="row">
                    <div class="my-36 text-center">
                        <div class="row font_size-24">Количество</div>
                        <div class="row font_size-20">
                            <div class="my-333 text-right"><div class="basket_count bsr" product_id="<?=$product->id ?>">-</div></div>
                            <div class="my-333 text-center product_count" product_id="<?=$product->id ?>"><?=$val['count']; ?></div>
                            <div class="my-333 text-left"><div class="basket_count bsl" product_id="<?=$product->id ?>">+</div></div>
                        </div>
                    </div>

                    <div class="my-28 text-center">
                        <div class="row font_size-24">Цена</div>
                        
                        <?php
                        $cityPrice = $product->price;
                    
                        if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($product->productCityPrice) && !empty($product->productCityPrice))
                        {
                            foreach ($product->productCityPrice as $city_p)
                            {
                                if ($city_p->id_city == $_COOKIE['city'])
                                {
                                    $cityPrice = $city_p->price;
                                }
                            }
                        }
                        ?>
                        
                        <?php
                        if ($action == '+')
                        {
                            $base_price = round((100 - $product->productDiscount->discount*1)*($cityPrice + $price_change*1)/100,0);
                        }
                        else
                        {
                            $base_price = round((100 - $product->productDiscount->discount*1)*($cityPrice - $price_change*1)/100,0);
                        }
                        ?>
                        
                        <?php if( $product->productDiscount->discount!=null ){ ?>
                            <div class="discount ops"><?=$product->productDiscount->discount ?>%</div>
                            <div class="row font_size-20 main_price"><?= $base_price ?> руб.</div>
                        <?php }
                        else{
                            ?>

                            <div class="row font_size-20 main_price"><?=$base_price ?> руб.</div>

                        <?php } ?>



                    </div>

                    <div class="my-36 text-center">
                        <div class="row font_size-24">Стоимость</div>
                        <div class="row font_size-20 cost float_left" style="margin-left: 38px;" product_id="<?=$product->id ?>"><?=$base_price*($val['count']) ?> </div><span class="float_left font_size-20">&nbspруб.</span>

                    </div>
                </div>

                <div class="row marginsp">
                    <div class="font_size-20" style="color: #1d6887">Стоимость сборки мебели: <span style="color: #ff1e1e">10%</span> </div>
                    <div class="font_size-20" style="color: #1d6887">Доставка по Москве до подъезда <span style="color: #ff1e1e">950руб</span> </div>
                </div>
                <div class="delete_from_basket" product_id="<?=$product->id ?>">Убрать из корзины</div>
            </div>

        </div>
    <?php
    $kokoko++;
    endforeach ?>

    <div class="row spss" style="background: #f8f8f8;padding: 20px 0;margin-top: 95px;">
        <div class="row msps text-center">
            <div class="row font_size-30">Стоимость товаров: <span id="count_down"></span><span>&nbspруб.</span></div>
            
            
            <div class="row margin_top-20 font_size-24">Промокод для скидки: <input type="text" name="promocode"></div>
            
            
            <div class="row margin_top-20 font_size-24" id="next_order">Продолжить оформление заказа</div>
        </div>
    </div>
<?php endif ?>
<?php if($scenario == 'no_basket'): ?>
    <div class="row font_size-24">ПУСТО :(</div>
<?php endif ?>

<div class="row margin_top-20">
    <div class="row">
        <div class="my-24 font_size-24">Мы рекомендуем:</div>
        <hr class="my-69" style="border-top: 1px solid #d7d7d7;  margin-top: 19px">
    </div>
    <?php
    for ($i = 1; $i <= 6; $i++):
        $ar_rand = rand(0, count($all_product['name'])-1);
        ?>

        <div class="my-50 margin_top-30">
            <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>"><img src="/uploads/product/preview/<?=$all_product['image'][$ar_rand] ?>" style="width: 150px;height: 125px;float: left"></a>
            <div style="float: left" class="margin_left-15">
                <div><?=$all_product['name'][$ar_rand] ?></div>

                <?php if( $all_product['discount'][$ar_rand]!=0 ){ ?>
                    <div class="discount small_size">-<?=$all_product['discount'][$ar_rand] ?>%</div>
                    <div>Цена: <span style="text-decoration: line-through; color: #ff0000;font-size: 12px"><?=number_format($all_product['price'][$ar_rand], 0, '.', ' ') ?> р. </span><?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>
                <?php }
                else{
                    ?>

                    <div>Цена: <?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>

                <?php } ?>

                <div class="margin_top-30"><a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>" class="jopa">Подробнее</a></div>
            </div>
        </div>

    <?php endfor ?>
</div>

<div class="row margin_top-40 spss" style="background:#f8f8f8;padding: 20px 0;display: none;" id="next_step">
    <div class="row msps">

        <div class="my-50">
            <div class="row">
                <div class="my-11"><div class="red_circle font_size-30">1</div></div>
                <div class="my-85">
                    <div class="contact_client margin_left-25">
                        <div class="row font_size-30">Введите свои данные</div>

                        <div class="form">

                            
                            
                            
                            
                            
                            
                            
                            
                            

                            <?php
                            $spPopa = 0;
                            foreach($product_id as $key=>$val): ?>
                                <input type="hidden" name="product_id[]" style="display: none" value="<?=$val ?>"/>
                                <input type="hidden" name="product_count[]" style="display: none" value="" product_id="<?=$val ?>"/>
                                <input type="hidden" name="product_pole[]" style="display: none" value=""/>
                                <input type="hidden" name="pole[]" product_id="<?=$val ?>" style="display: none" value=""/>

                                <input type="hidden" name="pole_change[<?=$val ?>]" product_id="<?=$val ?>" style="display: none" value="<?=$price_change_id[$val] ?>"/>
                            <?php endforeach ?>

                            <div class="row">
                                <?php echo $form->textField($contact_client,'name',array('maxlength'=>64,'placeHolder'=>'Ваше имя', 'class'=>'contact_input')); ?>
                                <?php echo $form->error($contact_client,'name'); ?>
                            </div>

                            <div class="row">
                                <?php echo $form->textField($contact_client,'phone',array('maxLength'=>64, 'placeHolder'=>'Контактный телефон', 'class'=>'contact_input')); ?>
                                <?php echo $form->error($contact_client,'phone'); ?>
                            </div>

                            <div class="row">
                                <?php echo $form->textField($contact_client,'email',array('maxLength'=>64, 'placeHolder'=>'Ваш E-mail', 'class'=>'contact_input')); ?>
                                <?php echo $form->error($contact_client,'email'); ?>
                            </div>

                            <div class="row">
                                <?php echo $form->textArea($contact_client,'comment',array('maxLength'=>64, 'placeHolder'=>'Ваш комментарий', 'class'=>'contact_area')); ?>
                                <?php echo $form->error($contact_client,'comment'); ?>
                            </div>

                            <div id="lst"">
                                <?php
                                $capha = Capha::model()->findByPk(1);
                                if( $capha->capha==1 ):?>
                                    <?php if(CCaptcha::checkRequirements()):?>
                                        <?php $this->widget('CCaptcha')?>
                                        <?=CHtml::activeTextField($contact_client, 'verifyCode', array('placeHolder' => 'Код проверки'))?>
                                        <?= $form->error($contact_client,'verifyCode'); ?>
                                    <?php endif?>
                                <?php endif?>
                            </div>
                        </div><!-- form -->

                    </div>
                </div>
            </div>
        </div>

        <div class="my-50">
            <div class="row">
                <div class="my-11"><div class="red_circle font_size-30">2</div></div>

                <div class="my-87">
                    <div class="margin_left-25">
                        <div class="row font_size-30">Выберите удобный для вас способ доставки</div>
                        <?php
                        $regionFirst = Region::model()->find();
                        ?>
                        <div class="row client_region">
                            <?=BsHtml::radioButtonListControlGroup('region',$regionFirst->region,$all_region); ?>
                        </div>
                        <script>
                            $('input[type="radio"]:checked').parent().css('background','url(/images/sf.png) no-repeat 0 13px');
                        </script>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="my-11"><div class="red_circle font_size-30" style="margin-top: 5px">3</div></div>

                <div class="my-87">


                    <div class="margin_left-25">

                        <div class="row ">
                            <?php echo CHtml::SubmitButton('Оформить', array('class'=>'button_client')); ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php $this->endWidget(); ?>

