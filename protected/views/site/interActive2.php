<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerCssFile($pt.'css/catalog.css');

$cs
    ->registerScriptFile($pt.'js/catalog.js');
?>

<?php
    $cords = array(
        'woman' => array(
            'woman_plus1' => array(
                'top' => '16%',
                'left' => '4%',
            ),
            'woman_plus2' => array(
                'top' => '58%',
                'left' => '10%',
            ),
            'woman_plus3' => array(
                'top' => '29%',
                'left' => '13%',
            ),
            'woman_plus4' => array(
                'top' => '16.5%',
                'left' => '16.4%',
            ),
            'woman_plus5' => array(
                'top' => '41%',
                'left' => '22%',
            ),
            'woman_plus6' => array(
                'top' => '74.3%',
                'left' => '27%',
            ),
            'woman_plus7' => array(
                'top' => '24%',
                'left' => '36%',
            ),
            'woman_plus8' => array(
                'top' => '23%',
                'left' => '45.8%',
            ),
            'woman_plus9' => array(
                'top' => '63.6%',
                'left' => '62%',
            ),
            'woman_plus10' => array(
                'top' => '73%',
                'left' => '85.6%',
            ),
        )
    );

?>


<div class="my_row">

    <div class="bg_image room1">

        <div id="room_man">

            <div class="name_room">
                <div>Комната для девочек «Леди»</div>
                <a href="#" class="another_room">Посмотрите другие комнаты</a>
            </div>
            <div class="center">
                <?php
                foreach($cords['woman'] as $key => $val):
                    $top = $val['top'];
                    $left = $val['left'];
                    ?>
                    <a href="#" class="plus_room" id="<?= $key ?>" style="position: absolute;top:<?= $top ?>;left:<?= $left ?>"><img class="plus_room" src="/images/plus_room.png"></a>
                <?php endforeach ?>
            </div>
        </div>

        <div class="my-12 blue">
            <div class="rooms_padding man">
                <img src="/images/plus.png" class="plus_margin">
                <div class="rooms">Комната для мальчиков</div>
                <div class="after_rooms">(нажмите, чтобы посмотреть)</div>
                <img src="/images/rectangle.jpg" class="rectangle">
                <div class="description_rooms">
                    Сундуки с золотыми монетами, карты, компас, приключения... Пора в путь на остров сокровищ! Тем более, он, вернее, детская комната уже ждет своего маленького кладоискателя. Лучшая
                    шхуна-кровать и настоящий пиратский флаг, удобные шкафы-рифы и «трон» для отважного капитана —  только для смелых мореплавателей. А, может быть, ваш озорник поклонник автомобилей? Тогда кровать машинка и детская мебель в
                    авто-тематике специально для вашего сына. Для мальчика детская комната станет проявлением вашей любви и заботы. Возвратитесь всего лишь на миг в детство. Вы — мальчик. Не о такой ли комнате мечтаете? Конечно, о такой! Вся разница в том, что у вас ее не было, а у вашего сына она может быть, для этого всего лишь нужно заказать у нас качественную детскую мебель
                </div>
            </div>
        </div>
    </div>

    <div class="bg_image room2">


        <div id="room_woman">

            <div class="name_room">
                <div>Комната для девочек «Леди»</div>
                <a href="#" class="another_room">Посмотрите другие комнаты</a>
            </div>
            <div class="center">
                <?php
                    foreach($cords['woman'] as $key => $val):
                        $top = $val['top'];
                        $left = $val['left'];
                ?>
                <a href="#" class="plus_room" id="<?= $key ?>" style="position: absolute;top:<?= $top ?>;left:<?= $left ?>"><img class="plus_room" src="/images/plus_room.png"></a>
                <?php endforeach ?>
            </div>
        </div>


        <div class="my-12 pink">
            <div class="rooms_padding woman">
                <img src="/images/plus.png" class="plus_margin">
                <div class="rooms">Комната для девочек</div>
                <div class="after_rooms">(нажмите, чтобы посмотреть)</div>
                <img src="/images/rectangle.jpg" class="rectangle">
                <div class="description_rooms">
                    Что нравится девочкам? Красота и уют, нежность и безупречность. Ваша доча будет в восторге от спальни! Не нужно отдельно подбирать детскую мебель, чтобы все было в едином стиле. Вам, вместе со своей принцессой, нужно только
                    выбрать именно ту детскую комнату, о которой она так мечтала. Детская мебель должна радовать ребенка и приносить ему счастье. Девочка будет чувствовать себя любящей в детской
                    комнате, а вы будете спокойны от того, что детская мебель выполнена из качественных
                    безопасных материалов, что ей удобно спать и заниматься творчеством
                </div>
            </div>
        </div>
    </div>

</div>

