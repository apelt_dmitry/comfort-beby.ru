<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerCssFile($pt.'css/interActive.css');

$cs
    ->registerScriptFile($pt.'js/interActive.js');

$cords = array(
    'woman' => array(
        '1' => array(
            'top' => '42%',
            'left' => '2%',
        ),
        '2' => array(
            'top' => '16%',
            'left' => '4%',
        ),
        '3' => array(
            'top' => '58%',
            'left' => '10%',
        ),
        '4' => array(
            'top' => '29%',
            'left' => '13%',
        ),
        '5' => array(
            'top' => '16.5%',
            'left' => '16.4%',
        ),
        '6' => array(
            'top' => '41%',
            'left' => '22%',
        ),
        '7' => array(
            'top' => '74.3%',
            'left' => '27%',
        ),
        '8' => array(
            'top' => '24%',
            'left' => '36%',
        ),
        '9' => array(
            'top' => '23%',
            'left' => '45.8%',
        ),
        '10' => array(
            'top' => '63.6%',
            'left' => '62%',
        ),
        '11' => array(
            'top' => '39.6%%',
            'left' => '81%',
        ),
        '12' => array(
            'top' => '18.6%',
            'left' => '84%',
        ),
        '13' => array(
            'top' => '73%',
            'left' => '85.6%',
        ),
    )
);
$meta = Meta::model()->findByPk(7);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
$image = InterImage::model()->findByPk(1);
?>

<script>
    $(document).ready(function(){
        $('.bg_image_main').hide();
        $('body').scrollTop(124);
    })
</script>
<div class="woman_room size_room">
    <img src="/uploads/product/<?=$image->image ?>" style="position: absolute;left: 76%;"/>
    <div class="name_room" scenario="woman">
        <div>Комната для девочек «Леди»</div>
        <a href="/site/catalog#product" class="another_room">Посмотрите другие комнаты</a>
    </div>
    <?php
    foreach($cords['woman'] as $key => $val):
        $top = $val['top'];
        $left = $val['left'];
        ?>
        <div class="plus_room" id="<?= $key ?>" style="position: absolute;top:<?= $top ?>;left:<?= $left ?>"><img class="plus_room" src="/images/plus_room.png"></div>
    <?php endforeach ?>
    <div class="view">
        <div class="cross"></div>
        <div class="st"></div>
        <div class="description_room">

            <div class="description_text">
                Полка навесная «Леди»
                предоставляет возможность по максимуму использовать пространство детской
                комнаты, занимая свободное место на стенах. Легкая полка «Леди» непринужденно
                монтируется на любую стену
                и удобна во всём.
            </div>

            <div style="background: #006886;padding-bottom: 20px;">
                <!--<div class="after_description size">Размер</div>-->
                <div class="after_description price">Цена:</div>
                <a href="#" class="link_2 basket2 basket_href">В корзину</a>
                <a href="/site/catalog#product" class="link_2">Каталог</a>
            </div>

        </div>
    </div>
</div>