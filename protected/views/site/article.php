<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerCssFile($pt.'css/slider.css');


$cs
    ->registerScriptFile($pt.'js/slider.js');



$meta = Meta::model()->findByPk(3);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>
<?php
$view = '_view_article';
if($scenario == 'detail_news'){
    $view = '_view_detail_article';
}
mb_internal_encoding("UTF-8");
?>

<div class="news_top">
    <div class="container clear-fix">
        <div class="news_top_bgr"></div>
        <div class="news_top_yellow clear-fix"><p>ПОЛЕЗНЫЕ СТАТЬИ</p></div>
        <div class="news_top_black clear-fix"><p>КОТОРЫЕ ПОМОГУТ ВАМ ВЫБРАТЬ Детскую мебель</p></div>
    </div>
</div>
<div class="middle_news">
    <div class="container clear-fix">
        <div class="news_left_box clear-fix">
            <div class="news_left_box_title">
                Мы рекомендуем:
            </div>
            
            <?php for ($i = 1; $i <= 3; $i++):?>
            <?php  $ar_rand = rand(0, count($all_product['name'])-1); ?>        
                
                <div class="news_left_mini_box">
                    <a href="/site/productDetail/<?= $all_product['id'][$ar_rand] ?>" class="product_image">
                        <?php if ($all_product['discount'][$ar_rand] != 0): ?>
                            <div class="stock"><?= $all_product['discount'][$ar_rand]; ?>%</div>
                        <?php endif; ?>
                        <div class="glaz"></div>
                        <img class="news_left_mini_img" src="/uploads/product/preview/<?=$all_product['image'][$ar_rand] ?>" />
                    </a>
                    <div class="news_left_mini_name"><?=$all_product['name'][$ar_rand] ?></div>
                    
                    <?php if( $all_product['discount'][$ar_rand]!=0 ){ ?>
                        <div class="news_left_mini_price"><strike><?=number_format($all_product['price'][$ar_rand], 0, '.', ' ') ?> р.</strike> <span><?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</span></div>
        
                    <?php }
                    else{
                        ?>            
                        <div class="news_left_mini_price"><?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.</div>
                    <?php } ?>
            
                    <div class="product_line"></div>
                    <div product_id="<?=$all_product['id'][$ar_rand] ?>" class="buy_btn_main button_buy" onclick="animate_cart_my(this);"></div>
                </div>
            <?php endfor; ?>
            
        </div>
        
        <?php if($scenario != 'detail_news'): ?>
                <div class="article_news">
        <?php endif;?>

        <?php $this->widget('bootstrap.widgets.BsListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>$view,
            //'afterAjaxUpdate' => 'function(){new_css()}',
            'template' => '
            {items}
            <div class="article_navigate_box"> {pager}</div>',
            'pager' => array(
				//'afterAjaxUpdate' => 'function(){top_body()}',
                'header' => '',
                'prevPageLabel' => '<',
                'previousPageCssClass' => 'article_navigate_button',
                'nextPageLabel' => '>',
                'nextPageCssClass'=>'article_navigate_button',
                'maxButtonCount' => '6',
                'firstPageLabel' => '|<',
                'firstPageCssClass' => 'article_navigate_button',
                'lastPageLabel' => '>|',
                'lastPageCssClass' => 'article_navigate_button',
                'internalPageCssClass'=>'article_navigate_button',
                'htmlOptions' =>array(
                    'class' => 'article_navigate',
                )
            ),
        )); ?>
        
        <?php if($scenario != 'detail_news'): ?>
            </div>
        <?php endif;?>
        
        <!--
        <div class="article_navigate_box">
            <div class="article_navigate">
                <div class="article_navigate_button">1</div>
                <div class="article_navigate_button">2</div>
                <div class="article_navigate_button">3</div>
                <div class="article_navigate_button">4</div>
                <div class="article_navigate_button">5</div>
                <div class="article_navigate_button"><p>></p></div>
                <div class="article_navigate_button"><p>>|</p></div>
            </div>
        </div>
        -->
    </div>
</div>


<div class="cosmos">
    <div class="container clear-fix">
        <div class="cosmos_bgr"></div>
        <div class="cosmos_yellow clear-fix"><p>МЫ ВСЕГДА НАХОДИМ ОТВЕТЫ</p></div>
        <div class="cosmos_black clear-fix"><p>НАПИШИТЕ НАМ!</p></div>
        <form class="form_form" name="" action="">
            <div class="form_name_bgr">
                <input class="form_name cb" placeholder="Ваше Имя" name="form_name">

                <div class="red_star name_red_star"></div>
            </div>
            <div class="form_phone_bgr">
                <input class="form_phone cb" placeholder="Ваш телефон" name="form_phone">

                <div class="red_star phone_red_star"></div>
            </div>
            <div class="form_email_bgr">
                <input class="form_email cb" placeholder="Ваш email" name="form_email">

                <div class="red_star email_red_star"></div>
            </div>
            <div class="form_text_bgr">
                <textarea class="form_text cb" placeholder="Ваш вопрос" name="form_text"></textarea>
            </div>
            <input formaction="" type="submit" value="" class="form_submit" name="form_submit">
        </form>
        <div class="warning"><span>* </span>обязательные поля для заполнения</div>
    </div>
</div>