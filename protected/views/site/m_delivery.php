<div class="m_container m_delivery_car"></div>

<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_2" src="/images/m_advantage_img_2.png">
    <div class="m_advantage_text_box">
        <span>Доставка и оплата</span>
        Мы доставим детскую мебель по Москве и области уже на следующий день (у нас свой автопарк), а в любой уголок России надежной транспортной компанией. Оплата любым удобным для вас способом: в рублях при получении(для Москвы и МО) или по безналичному расчету для регионов.    </div>
</div>
<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_2" src="/images/m_advantage_img_5.png">
    <div class="m_advantage_text_box">
        <span>Логистика</span>
        За 10 лет работы нашего магазина, мы доставили детскую мебель в более чем 200 городов России. За это время мы проверили работу многих транспортных компаний.
    </div>
</div>
<div class="m_container clear-fix m_advantage">
    <img class="m_advantage_img_2" src="/images/m_advantage_img_6.png">
    <div class="m_advantage_text_box">
        <span>Вы ничем не рискуете</span>
        Потому что платите за детскую комнату или мебель только после того, как вам ее доставят. Никакой предоплаты!
    </div>
</div>

<div class="m_adv">
    <div class="m_container clear-fix">
        <img class="m_adv_img_1" src="/images/m_adv_img.png">
        <div class="m_adv_text_box">
            <span>Доставка мебели по Москве и <br/>Московской области</span>
            <span2>
                Москва: 950 р.<br/>
                за пределы МКАД:  к сумме доставки<br/>
                Вы доплачиваете +25 р./км от МКАД<br/>
            </span2>
            <span3>
                Доставка осуществляется до подъезда.<br/>
                Подъём мебели в квартиру оплачивается отдельно
            </span3>
        </div>
    </div>
    <div class=" m_container ">
        <div class="m_adv_line"></div>
    </div>
    <div class="m_container clear-fix">
        <img class="m_adv_img_2" src="/images/m_adv_img_2.png">
        <div class="m_adv_text_box">
            <span>Доставка мебели по России</span>
            <span4>
                Если Вы проживаете в регионах России, мы доставим Ваш заказ посредством транспортной компании в самые короткие сроки. Т.к. практически весь ассортимент детской мебели представленной на сайте есть на складе, мы готовы отгрузить его сразу после поступления оплаты на наш счет. Закажите детскую мебель через сайт или позвоните, мы выставим счет на оплату. Точные расценки по доставке Вы можете узнать на сайте транспортной компании. Не забудьте учесть стоимость обрешетки, которая обезопасит груз от повреждений. Доставка заказа до транспортной компании - 1000 р. Приятных покупок!
            </span4>

        </div>
    </div>
</div>

<div class="m_mi_rabotaem">
    <div class="m_container clear-fix">
        <div class="m_mi_rabotaem_text">
            Мы работаем только с проверенными<br/>
            транспортными компаниями
        </div>
    </div>
</div>

