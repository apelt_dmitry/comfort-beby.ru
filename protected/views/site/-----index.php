<?php
mb_internal_encoding("UTF-8");
$meta = Meta::model()->findByPk(1);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>



<div id="thx_wrapper">
    <div id="text_thx">Благодарим, Вас
        за покупку.
        Наш менеджер скоро
        свяжется с вами
    </div>
</div>
    <div class="row margin_top-20">
        <script>
            $(document).ready(function(){
                if($.get('thx')=='yes' ){
                    $('#thx_wrapper').show();
                    setTimeout(function(){
                        $('#thx_wrapper').animate({
                            'opacity': 0
                        },1000,function(){
                            $('#thx_wrapper').hide();
                        })
                    },4000);
                }
            });

        </script>
        <!--left content-->

        <div class="my-24">
            <div id="comment">
                <div id="slider1">
                    <table>
                        <tr>
                            <td class="comment_padding">
                                <a class="buttons prev" href="#"></a>
                            </td>
                            <td>
                                <div id="comment_header" class="font_bold"><a href="/site/comment" style="color: #fff">Все отзывы</a></div>
                            </td>
                            <td class="comment_padding">
                                <a class="buttons next" href="#"></a>
                            </td>
                        </tr>
                    </table>
                    <div class="clear"></div>
                    <div class="viewport">
                        <ul class="overview">
                            <?php foreach($comment as $val): ?>
                                <li><?= $val ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div style="display: table;margin-bottom: 40px;">
                <?php $comment_image = ImageComment::model()->findByPk(1) ?>
                <img src="/uploads/product/<?=$comment_image->image ?>" class="my-35" style="height: 80px;border-radius: 50px">
                <div class="my-60-ml-10"><?=$comment_image->title ?></div>
            </div>


            <div id="popular">

                <div id="slider2">
                    <div class="header_popular">
                        <table>
                            <tr>
                                <td class="comment_padding">
                                    <a class="buttons prev" href="#"></a>
                                </td>
                                <td>
                                    <div id="comment_header" class="font_bold">Популярное</div>
                                </td>
                                <td class="comment_padding">
                                    <a class="buttons next" href="#"></a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="clear"></div>
                    <div class="viewport">
                        <ul class="overview">
                            <?php foreach($popular as $val): ?>
                                <?php $product = $val->product?>
                                <?php if($product->mainImage!=null): ?>
                                    <li>
                                        <a href="/site/productDetail/<?=$product->id ?>"><?= BsHtml::image('/uploads/product/preview/'.$product->mainImage->image) ?></a>
                                        <div class="popular_footer">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <a href="/site/productDetail/<?=$product->id ?>">Подробнее</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--end left content-->

        <!--category content-->

        <div class="my-76">
            <div class="row under_line font_bold">Наш каталог</div>

            <div class="row">
                <div id="slider3">
                    <table>
                        <tr>
                            <td class="comment_padding">
                                <a class="buttons prev" href="#"></a>
                            </td>
                            <td class="comment_padding">
                                <a class="buttons next" href="#"></a>
                            </td>
                        </tr>
                    </table>
                    <div class="clear"></div>
                    <div class="viewport">
                        <ul class="overview">
                            <?php foreach($category_publish as $val): ?>
                                <li>
                                    <a href="/site/catalog/<?=$val->publish_id ?>/#product"><?= BsHtml::image('/uploads/product/preview/'.$val->category_publish->image) ?></a>
                                    <a href="/site/catalog/<?=$val->publish_id ?>/#product"><div class="category_slider_name"><?= $val->category_publish->name ?></div></a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row margin_top-40">
                <div class="my-100 chose_parents">
                    <div style="width: 75%;display: block;" class="font_bold"><?=$text[1]['title'] ?></div>
                </div>
                <div class="my-100 margin_top-30"><?=$text[1]['description'] ?></div>
            </div>

        </div>

        <!--end category content-->

    </div>

    <div class="row margin_top-58">

        <div class="my-333">
            <img src="/images/fast.png">
            <div class="margin_top-28 font_bold"><?=$text[2]['title'] ?></div>
            <img src="/images/under.png" style="margin: 13px 0">
            <div style="padding: 0 15px"><?=$text[2]['description'] ?></div>
        </div>

        <div class="my-333">
            <img src="/images/risk.png">
            <div class="margin_top-28 font_bold"><?=$text[3]['title'] ?></div>
            <img src="/images/under.png" style="margin: 13px 0">
            <div style="padding: 0 15px"><?=$text[3]['description'] ?></div>
        </div>

        <div class="my-333">
            <img src="/images/product.png">
            <div class="margin_top-28 font_bold"><?=$text[4]['title'] ?></div>
            <img src="/images/under.png" style="margin: 13px 0">
            <div style="padding: 0 15px"><?=$text[4]['description'] ?></div>
        </div>

    </div>

    <!--product content-->

    <div class="row margin_top-58">

        <div class="my-23">
            <?=$result_categories ?>
        </div>

        <div class="my-77">
            <?php
                for ($i = 1; $i <= 6; $i++):
                     $ar_rand = rand(0, count($all_product['name'])-1);
            ?>

                <div class="view_product margin_left-25" style="margin-bottom: 20px;position: relative">
                    <table class="table_name">
                        <tr>
                            <td style="text-align: center" class="font_bold"><?= $all_product['name'][$ar_rand] ?></td>
                        </tr>
                    </table>
                    <div class="view_img">
                        <a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>"><?= BsHtml::image('/uploads/product/preview/'.$all_product['image'][$ar_rand]) ?></a>
                    </div>
                    <table class="table_detail">
                        <tr>
                            <?php if( $all_product['act'][$ar_rand]!=0 ): ?>
                                <img src="/images/act.png" class="act_img"/>
                            <?php endif ?>
                            
                            <?php if ($all_product['discount'][$ar_rand] != 0): ?>
                                <div class="discount md">-<?=$all_product['discount'][$ar_rand] ?>%</div>
                                <td class="table_price">
                                    <?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.
                                </td>
                            <?php else: ?>
                                <td class="table_price">
                                    <?= number_format((100-$all_product['discount'][$ar_rand]*1)*$all_product['price'][$ar_rand]/100, 0, '.', ' ') ?> р.
                                </td>
                            <?php endif; ?>

                            <td class="table_link"><a href="/site/productDetail/<?=$all_product['id'][$ar_rand] ?>">Подробнее</a></td>
                        </tr>
                    </table>
                </div>

            <?php endfor ?>
        </div>
    </div>

    <!--end product content-->


    <div class="row margin_top-58">

        <div class="my-27" style="padding: 44px 0 0 24px">
            <img src="/images/shopping.png">
        </div>

        <div class="my-73">

            <div class="font_size-24 font_bold"><img src="/images/many_element.png" style="margin-right: 12px"><?=$text[5]['title'] ?></div>
            <div class="margin_top-20"><?=$text[5]['description'] ?></div>

            <div style="font-size: 24px" class="margin_top-30 font_bold"><img src="/images/many_element.png" style="margin-right: 12px"><?=$text[6]['title'] ?></div>
            <div class="margin_top-20"><?=$text[6]['description'] ?></div>

        </div>

    </div>
	
	<div class="row margin_top-58">
        <div class="font_size-24 font_bold border_bottom-3"><a href="/site/article" style="color:#000;">Статьи:</a></div>

        <div class="row">
            <div id="slider6">
                <table>
                    <tr>
                        <td class="comment_padding">
                            <a class="buttons prev" href="#"></a>
                        </td>
                        <td class="comment_padding">
                            <a class="buttons next" href="#"></a>
                        </td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="viewport">
                    <ul class="overview">
                        <?php foreach($article as $val): ?>
                            <li>
                                <?= BsHtml::image('/uploads/product/preview/'.$val->image) ?>
                                <div class="category_slider_name"><?=  mb_substr((preg_replace('/\<br(\s*)?\/?\>/i', "&nbsp", $val->description)), 0, 150).'...'; ?></div>
                                <a href="/site/article/<?=$val->id ?>" class="link_news">Подробнее</a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
	
    <div class="row margin_top-58">
        <div class="font_size-24 font_bold border_bottom-3"><a href="/site/news" style="color:#000;">Последние новости:</a></div>

        <div class="row">
            <div id="slider4">
                <table>
                    <tr>
                        <td class="comment_padding">
                            <a class="buttons prev" href="#"></a>
                        </td>
                        <td class="comment_padding">
                            <a class="buttons next" href="#"></a>
                        </td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="viewport">
                    <ul class="overview">
                        <?php foreach($news_publish as $val): ?>
                            <li>
                                <?= BsHtml::image('/uploads/product/preview/'.$val->news_publish->image) ?>
                                <div class="category_slider_name"><?=  mb_substr($val->news_publish->description, 0, 150).'...'; ?></div>
                                <a href="/site/news/<?=$val->news_publish->id ?>" class="link_news">Подробнее</a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>


