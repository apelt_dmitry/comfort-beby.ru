<?php
$meta = Meta::model()->findByPk(6);

Yii::app()->name = $meta->title;
Yii::app()->clientScript->registerMetaTag($meta->content, 'description');
?>

<script>
    $(document).ready(function(){
        $('.basket_count').click(function(){
            var cost = $('.cost[product_id='+$(this).attr('product_id')+']').html();
            var count =$('.product_count_new[product_id='+$(this).attr('product_id')+']').html();
            if($(this).hasClass('bsr')){
                if($('.product_count_new[product_id='+$(this).attr('product_id')+']').html()*1>=2){
                    $('.product_count_new[product_id='+$(this).attr('product_id')+']').html($('.product_count_new[product_id='+$(this).attr('product_id')+']').html()*1-1);
                }
            }
            else{
                $('.product_count_new[product_id='+$(this).attr('product_id')+']').html($('.product_count_new[product_id='+$(this).attr('product_id')+']').html()*1+1);
            }
            $('.cost[product_id='+$(this).attr('product_id')+']').html((cost/count)*$('.product_count_new[product_id='+$(this).attr('product_id')+']').html());
            count_down();
            count_post2();
        });
        $('#next_order').click(function(){
            $('#next_step').show();
            $('body').animate({scrollTop: 1150},1000);
			$('html').animate({scrollTop: 1150},1000)
        });
        count_down();

        $('.button_client').click(function(){
            $.cookie('basket', null, { path: '/' });
        });
        $('.kokoko[kokoko="1"]').css({
            'border-top':'none',
            'padding-top': 0
        });
        count_post();
        delete_from_basket();
        new_input();



    });

    function count_post2(){
        var i = 0;
        var count_post = [];
        $('.product_count_new').each(function(){
            count_post[$(this).attr('product_id')] = $(this).html();
            i++;
        });
        var id = product_id();
        for(var j in id){
            $('input[name="product_count_new['+id[j]+']"').attr('value',count_post[id[j]])
        }
        /*var j = 0;
         $('input[name="product_count_new[]"]').each(function(){
         $(this).attr('value',count_post[j]);
         $(this).attr('name','product_count_new['+$(this).attr('product_id')+']' );
         j++;
         })*/

    }


    function count_post(){
        var i = 0;
        var count_post = [];
        $('.product_count_new').each(function(){
            count_post[i] = $(this).html();
            i++;
        });
        var j = 0;
        $('input[name="product_count_new[]"]').each(function(){
            $(this).attr('value',count_post[j]);
            $(this).attr('name','product_count_new['+$(this).attr('product_id')+']' );
            j++;
        })

    }

    function delete_from_basket(){
        $('.delete_from_basket').click(function(){
            $(this).parent().parent().hide();
            var product_id = $(this).attr('product_id');
            $('.cost[product_id='+product_id+']').html('0');
            $('.product_count_new[product_id='+product_id+']').html('0');
            count_down();
            var new_product = [];
            var j=0;
            var products = JSON.parse($.cookie('basket'));
            for(var i in products){
                if(product_id!=products[i]['product_id']){
                    new_product[j] = products[i];
                    j++;
                }
            }
            $.cookie('basket', JSON.stringify(new_product), { expires: 30, path: '/' });
            basket();
            if(JSON.parse($.cookie('basket'))==''){
                $('.spss').hide();
                $('.wfg').show();
                $.cookie('basket', null, { path: '/' });
            }
        });
    }


    function count_down(){
        var html = 0/1;
        $('.cost').each(function(){
             html += $(this).html()*1;
        });
        $('#count_down').html(html);
    }

    function pole_basket(){
        var pole_basket = [];
        var id = 0;
        var i = 0;
        $('.pole_basket').each(function(){
            if(id!=$(this).attr('product_id')){
                i++;
                pole_basket[i] = '';
            }
            id = $(this).attr('product_id');
            pole_basket[i] += $(this).html()+'; ';
        });
        return pole_basket;
    }

    function new_input(){
        var id = product_id();
        var pole = pole_basket();
        for(var i in id){
            $('input[name="pole[]"]').each(function(){
                if($(this).attr('product_id')==id[i]){
                    $(this).attr('value',pole[i]);
                    $(this).attr('name','pole['+id[i]+']');
                }
            })
        }
    }

    function product_id(){
        var product_id = [];
        var id = 0;
        var i = 0;
        $('.pole_basket').each(function(){
            if(id!=$(this).attr('product_id')){
                i++;
                product_id[i] = $(this).attr('product_id');
            }
            id = $(this).attr('product_id');
        })
        return product_id;
    }

</script>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'contact-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation' => true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
)); ?>

<div class="cart">
    <div class="container clear-fix">
    <?php
    $product_id = array();
    $kokoko = 1;
    if($scenario=='basket'): ?>
        <div class="cart_head">
            <span>НАИМЕНОВАНИЕ</span>
            <span2>КОЛЛИЧЕСТВО</span2>
            <span3>ЦЕНА</span3>
        </div>
        <?php foreach($model as $val): ?>
            <?php
                $product = Product::model()->findByPk($val['product_id']);
                $product_id[] = $product->id;
            ?>
            <div class="cart_box kokoko" kokoko="<?=$kokoko ?>">
                <div class="cart_name_box">
                    <img src="/uploads/product/preview/<?=$product->mainImage->image ?>" />
    
                    <div class="cart_name"><?=$product->product_name ?></div>
                    
                    <div class="pole_div_wrap">
                    <?php
                        $price_change = 0;
                        foreach($val['pole_id'] as $key=>$val2):
                            $pole = ProductPole::model()->findByPk($val2);
                            if( $pole->price_change!='+0' ){
                                $price_change_id[$val['product_id']] = $pole->id;
                                $action = substr($pole->price_change,0,1);
                                $price_change = substr($pole->price_change,1);
                            }
                    ?>
                        <div class="pole_basket" style="font-size: 12px" product_id="<?=$product->id ?>" price_change="<?=$pole->price_change ?>"><?=$val['pole_name'][$key]; ?>: <?=$pole->value; ?></div>
                    <?php endforeach ?>
                    </div>
                    
                    <a href="/site/productDetail/<?=$product->id ?>"><div class="cart_see_button"></div></a>
                </div>
                
                <div class="cart_count_box">
                    <div class="box_count"><span class="product_count_new" product_id="<?=$product->id ?>"><?=$val['count']; ?></span></div>

                        <?php
                        $cityPrice = $product->price;
                    
                        if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($product->productCityPrice) && !empty($product->productCityPrice))
                        {
                            foreach ($product->productCityPrice as $city_p)
                            {
                                if ($city_p->id_city == $_COOKIE['city'])
                                {
                                    $cityPrice = $city_p->price;
                                }
                            }
                        }
                        ?>
                        
                        <?php
                        if ($action == '+')
                        {
                            $base_price = round((100 - $product->productDiscount->discount*1)*($cityPrice + $price_change*1)/100,0);
                        }
                        else
                        {
                            $base_price = round((100 - $product->productDiscount->discount*1)*($cityPrice - $price_change*1)/100,0);
                        }
                        ?>
                        

                    
                        <?php if( $product->productDiscount->discount!=null ){ ?>
                            <!-- <div class="discount ops"><?=$product->productDiscount->discount ?>%</div> -->
                            <div class="cart_count_price">х <?=$base_price ?> руб.</div>
                        <?php }
                        else{
                            ?>
                            <div class="cart_count_price">х <?=$base_price ?> руб.</div>

                        <?php } ?>
                        
                        
                        
                    <div class="plus_minus_box">
                        <div class="button_plus basket_count bsl" product_id="<?=$product->id ?>"></div>
                        <div class="button_minus basket_count bsr" product_id="<?=$product->id ?>"></div>
                    </div>
                </div>
                
                <div class="cart_price_box">

                        <div class="cart_price_price" ><span class="cost" product_id="<?=$product->id ?>"><?=$base_price*($val['count']) ?></span><span>руб.</span></div>
                        
                    <div class="button_delete delete_from_basket" product_id="<?=$product->id ?>"></div>
                </div>
            </div>
        <?php $kokoko++; endforeach; ?>

        <?php endif; ?>
        
        <?php if($scenario == 'no_basket'): ?>
            <div class="row font_size-24">ПУСТО :(</div>
        <?php endif ?>
    </div>
</div>
<div class="cart_banner_and_sum">
    <div class="container clear-fix">
        <div class="cart_banner_and_sum_banner">
            <img src="/images/cart_banner_1.jpg"/>
        </div>
        <div class="cart_banner_and_sum_sum">
            <div class="cart_sum">
                Сумма заказа: <span id="count_down"></span><span>&nbspруб.</span>
            </div>
            <div class="csrt_sum_grey">
                В сумме заказа не учтены: акции на доставку и сборку;<br/>
                cтоимость доставки за пределами МКАД
            </div>
            <div class="promo_cod_box">
                <div class="i_have_promo">У меня есть промокод</div>
                <input class="promo" placeholder="Введите промокод"><input type="button" value="Ввести"
                                                                           class="promo_success">
            </div>
            <div class="button_oformlenie" id="next_order"></div>
        </div>
    </div>
</div>
<div class="we_commend">
    <div class="container clear-fix">
        <img class="we_commend_img" src="/images/banner_phone.jpg">

        <div class="we_commend_title"><p>Мы рекомендуем</p></div>
        <div class="we_recommend_line"></div>
        <div class="we_recommend_product">
        <!--
    <?php //for($i = 1; $i <= 3; $i++):?>
        
            <?php// $this->renderPartial('_all_product_view', array('all_product'=>$all_product)); ?>

    <?php //endfor ?>
    -->
            <div class="catalog_slider">
                <?php for ($i = 1; $i <= 6; $i++):?>        
                    <?php $this->renderPartial('_all_product_view', array('all_product'=>$all_product)); ?>
                <?php endfor; ?>
            </div>

        </div>
    </div>
</div>

<div class="ordering" id="next_step" style="display: none;">
    <div class="container clear-fix">
        <div class="ordering_title">
            <div class="ordering_title_top"><img src="/images/ordering_cart.png">Оформление заказа</div>
            <div class="ordering_title_bot">
                Заполните поля с данными и выберите удобный для Вас способ доставки.<br/>
                После подтверждения заказа наш менеджер свяжеться с вами для уточнения<br/>
                деталей
            </div>
        </div>
        <div class="left_form">
            <div class="ordering_data"><img src="/images/ordering_data.png"/>Ваши данные</div>
            <div class="ordering_form" name="" action="">
            
        <?php
        $spPopa = 0;
        foreach($product_id as $key=>$val): ?>
            <input type="hidden" name="product_id[]" style="display: none" value="<?=$val ?>"/>
            <input type="hidden" name="product_count_new[]" style="display: none" value="" product_id="<?=$val ?>"/>
            <input type="hidden" name="product_pole[]" style="display: none" value=""/>
            <input type="hidden" name="pole[]" product_id="<?=$val ?>" style="display: none" value=""/>
        
            <input type="hidden" name="pole_change[<?=$val ?>]" product_id="<?=$val ?>" style="display: none" value="<?=$price_change_id[$val] ?>"/>
        <?php endforeach ?>
        
                <div class="form_name_bgr">
                    <!-- <input class="form_name cb" placeholder="Ваше Имя" name="ordering_name"> -->
                    <?php echo $form->textField($contact_client,'name',array('maxlength'=>64,'placeHolder'=>'Ваше Имя', 'class'=>'form_name cb')); ?>
                    <div class="red_star name_red_star"></div>
                    <?php echo $form->error($contact_client,'name'); ?>
                </div>
                <div class="form_phone_bgr">
                    <!-- <input class="form_phone cb" placeholder="Ваш телефон" name="ordering_phone">-->
                    <?php echo $form->textField($contact_client,'phone',array('maxLength'=>64, 'placeHolder'=>'Ваш телефон', 'class'=>'form_phone cb')); ?>
                    <?php echo $form->error($contact_client,'phone'); ?>

                    <div class="red_star phone_red_star"></div>
                </div>
                <div class="form_email_bgr">
                    <!-- <input class="form_email cb" placeholder="Ваш email" name="ordering_email"> -->
                    <?php echo $form->textField($contact_client,'email',array('maxLength'=>64, 'placeHolder'=>'Ваш email', 'class'=>'form_email')); ?>
                    <?php echo $form->error($contact_client,'email'); ?>

                    <div class="red_star email_red_star"></div>
                </div>
                <div class="form_text_bgr">
                    <!-- <textarea class="form_text cb" placeholder="Здесь Вы можете написать Ваш комментарий к заказу" name="ordering_text"></textarea>-->
                    <?php echo $form->textArea($contact_client,'comment',array('maxLength'=>64, 'placeHolder'=>'Здесь Вы можете написать Ваш комментарий к заказу', 'class'=>'form_text cb')); ?>
                    <?php echo $form->error($contact_client,'comment'); ?>
                </div>
                <div class="warning2"><span>* </span>обязательные поля для заполнения</div>
                <!-- <input formaction="" type="submit" value="" class="ordering_submit" name="ordering_submit">-->
                    <?php echo CHtml::SubmitButton('', array('class'=>'ordering_submit')); ?>
            </div>
        </div>
        <div class="right_form">
            <div class="ordering_data"><img src="/images/ordering_car.png"/>Выберите способ доставки</div>
            <div class="form-group"><label class="control-label" for="region"></label>
            
                <div style="position: relative">

                    <div class="radio">
                        <label>
                            <input id="region_0" value="Москва(За МКАД + 25руб/км)" id="region_0" checked="checked" type="radio" name="region">
                            <p><span></span></p>
                            <div class="text_inp_moss">Москва(за МКАД + 25 руб/км)<br/><span>Доставка: 950 рублей</span></div>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input id="region_1" value="Самовывоз" id="region_1" type="radio" name="region">
                            <p><span></span></p>
                            <div class="text_inp_sam">Самовывоз<br/><span>Доставка: бесплатно</span></div>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input id="region_2" value="До ТК(для регионов)" id="region_2" type="radio" name="region">
                            <p><span></span></p>
                            <div class="text_inp_tk">До ТК(для регионов)<br/><span>Доставка: 1000 руб.)</span></div>
                        </label>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
