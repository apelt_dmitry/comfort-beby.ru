<?php

class PageController extends Controller
{
    public function actionSearch()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            
            if (isset($_REQUEST['product_name']))
            {
                $product_name = $_REQUEST['product_name'];
                            
          		$criteria=new CDbCriteria;
                //$criteria->compare('region_id', $region_id, false);
                $criteria->compare('product_name', $product_name, true);
                $criteria->order = 'id';
                $criteria->limit = 10;
            
                $mdl_prod = Product::model()->findAll($criteria);
                $mdl_prod = CHtml::listData($mdl_prod, 'id', 'product_name');
                
                echo json_encode($mdl_prod); // возвраащем данные в JSON формате;
            }
            else
            {
                echo json_encode(array('Что вы хотите найти?'));
            }
            
        }  
    }
    
    public function actionIndex()
    {
        $basket_arr = json_decode($_COOKIE['basket']);
        echo "<pre>";
        var_dump($basket_arr);     
    }
    
    public function actionGetprice()
    {
        $basket_arr = json_decode($_COOKIE['basket']);
        
        $full_price = 0;
        foreach($basket_arr as $product)
        {
            $data =  Product::model()->findByPk($product->product_id);
            
            $cityPrice = $data->price;
        
            if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($data->productCityPrice) && !empty($data->productCityPrice))
            {
                foreach ($data->productCityPrice as $city_p)
                {
                    if ($city_p->id_city == $_COOKIE['city'])
                    {
                        $cityPrice = $city_p->price;
                    }
                }
            }        
            
            $full_price = $full_price + $product->count * $cityPrice;
        }
        
        echo $full_price; 
    }
    
}