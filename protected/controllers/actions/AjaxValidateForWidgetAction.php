<?php

class AjaxValidateForWidgetAction extends CAction
{
    /**
     * AJAX validation for Yii widgets
     */
    public function run($model, $scenario = null)
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['ajax']) && preg_match("/^[0-9a-zA-Z-_]+$/", $model) && (preg_match("/^[0-9a-zA-Z-_]+$/", $scenario) || $scenario == null))
        {
            $model = new $model;
            $model->scenario = $scenario;
        
            echo CActiveForm::validate($model);
            
            Yii::app()->end();
        } 
        else {throw new CHttpException(404, 'Такой страницы не существует');}
    }
}