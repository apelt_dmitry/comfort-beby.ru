<?php

class SiteController extends Controller
{
    public $categories = array();
    public $result_categories = '';
    public $parent_category = array();

    public function actions()
    {
        return array(
            'ajaxwidgetvalidate' => 'application.controllers.actions.AjaxValidateForWidgetAction'
        );
    }
    
    public function actionDelivery()
    {
        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_delivery');
        }
        else{
            $this->render('delivery');
        }


    }
    
    /**
     * Главная
     */
	public function actionIndex()
	{
        $slide = Slide::model()->findAll();
		$criteria = new CDbCriteria();
		$criteria->order = '`id` DESC';
        $config = ConfigMain::model()->findAll();
        
        $i = 1;
        foreach($config as $val)
        {
            $text[$i]['title'] = $val->title;
            $text[$i]['description'] = $val->description;
            $i++;
        }
        
		//$model = Comment::model()->findAll(); //NSV комит
        $comment = UserComment::model()->findAll('state=1');
        
        $popular = Popular::model()->findAll();
        
		$article = Article::model()->findAll($criteria);
		$criteria->order = '`news_id` DESC';
		$news_publish = PublishNews::model()->findAll($criteria);
        $all_product = $this->all_product2();
        $category_publish = PublishCategory::model()->findAll();
        $this->catalog();
//NSV комит
//        $comment = array();
//
//        foreach($model as $val)
//        {
//            $comment[] = $val->comment;
//        }

        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_index',array(
                'slide' => $slide,
                'article' => $article,
                'text' => $text,
                'category_publish' => $category_publish,
                'comment' => $comment,
                'popular' => $popular,
                'news_publish' => $news_publish,
                'all_product' => $all_product,
                'result_categories' => $this->result_categories,
            ));
        }else{
            $this->render('index',array(
                'slide' => $slide,
                'article' => $article,
                'text' => $text,
                'category_publish' => $category_publish,
                'comment' => $comment,
                'popular' => $popular,
                'news_publish' => $news_publish,
                'all_product' => $all_product,
                'result_categories' => $this->result_categories,
            ));
        }


	}

    public function outTree($parent_id) 
    {
        $category_arr = $this->categories; //Делаем переменную $category_arr видимой в функции
        
        if (isset($category_arr[$parent_id])) { //Если категория с таким parent_id существует
            $this->result_categories .= '<ul class="left_catalog">';
            
            foreach ($category_arr[$parent_id] as $value) 
            {
                $this->result_categories .= '<li>
                    <a href="/site/catalog/?id='.$value["id"].'" category_id="'.$value["id"].'"><span>' . $value["name"] . '</span></a></li>';
                $this->outTree($value["id"]);
            }
            
            $this->result_categories .= "</ul>";
        }
    }

    public function actionConnection() 
    {
        $model = new Connection();
        
		if(isset($_POST['ajax']) && $_POST['ajax']==='quick-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
       
        
        $model->attributes=$_POST['Connection'];
        $model->data_create = time();
        $model->save();
        Yii::app()->user->setState('thx_for_contac_form_submt', 'thx_for_contac_form_submt');
        $model->goContact();

        $this->redirect(array('site/index'));
    }

    public function catalog()
    {
		$criteria = new CDbCriteria;
		$criteria->order = '`position` ASC';
        $parent_categories = PublishCategory::model()->findAll($criteria);
        
        foreach($parent_categories as $val){
            $this->categories[$val->category_publish->category_id][] = array(
                'name' => $val->category_publish->name,
                'id' => $val->category_publish->id,
                'category_id' => $val->category_publish->category_id,
            );
        }
        
        $this->outTree(0);
    }

    // Вывод каталога
    public function actionCatalog($id = null)
    {
        $criteria = new CDbCriteria;
        
        if ($id != null)
        {
            $criteria->condition = 'category=:id';
            $criteria->params = array(':id'=>$id);
            
            $criteria->with = array(
                'Product', 
                'Product.productCityPrice', 
                'Product.productDiscount'
            );
			
			if ($_GET['from']*1 > 0 && $_GET['to']*1 > 0 && $_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`price`>='.($_GET['from']*1).' AND `price`<='.($_GET['to']*1));
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            } 
            elseif ($_GET['from']*1 > 0) 
            {
                $criteria->addCondition('`price`>='.($_GET['from']*1));
            } 
            elseif ($_GET['to']*1 > 0) 
            {
                $criteria->addCondition('`price`<='.($_GET['to']*1));
            } 
            elseif ($_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            }  
            elseif ($_GET['to']*1 > 0 && $_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`price`<='.($_GET['to']*1));
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            }  
            elseif ($_GET['from']*1 > 0 && $_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`price`<='.($_GET['to']*1));
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            }
            
			$_GET['id'] = $id;
            
            if (isset($_GET['catalogsort']) && !empty($_GET['catalogsort']))
            {
                switch ($_GET['catalogsort'])
                {
                    case 'price_up':
                        $criteria->order = '(100-IFNULL(productDiscount.discount, 0))*Product.price/100 ASC';
                        break;
                    case 'price_down':
                        $criteria->order = '(100-IFNULL(productDiscount.discount, 0))*Product.price/100 DESC';
                        break;
                }
            }
			else
            {
                $criteria->order = 'Product.position ASC';
            }
			
            //$criteria->order = 'Product.position ASC';
            
            $dataProvider = new CActiveDataProvider('ProductCategory', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 15,
                ),
            ));
            
            $scenario = 'category';
        }
        else
        {
            $criteria->with = array(
                'productCityPrice', 'productDiscount'
            );
            
            if (isset($_GET['catalogsort']) && !empty($_GET['catalogsort']))
            {
                switch ($_GET['catalogsort'])
                {
                    case 'price_up':
                        $criteria->order = '(100-IFNULL(productDiscount.discount, 0))*price/100 ASC';
                        break;
                    case 'price_down':
                        $criteria->order = '(100-IFNULL(productDiscount.discount, 0))*price/100 DESC';
                        break;
                }
            }
			else
            {
                $criteria->order = 'position ASC';
            }
            
            //$criteria->order = 'position ASC';

            if(isset($_GET['production'])){

                $criteria->condition = 'production_id=:PI';
                $criteria->params = array(':PI'=>$_GET['production']);
            }
			
			if ($_GET['from']*1 > 0 && $_GET['to']*1 > 0 && $_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`price`>='.($_GET['from']*1).' AND `price`<='.($_GET['to']*1));
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            } 
            elseif ($_GET['from']*1 > 0) 
            {
                $criteria->addCondition('`price`>='.($_GET['from']*1));
            } 
            elseif ($_GET['to']*1 > 0) 
            {
                $criteria->addCondition('`price`<='.($_GET['to']*1));
            } 
            elseif ($_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            }  
            elseif ($_GET['to']*1 > 0 && $_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`price`<='.($_GET['to']*1));
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            }  
            elseif ($_GET['from']*1 > 0 && $_GET['productions']*1 > 0) 
            {
                $criteria->addCondition('`price`<='.($_GET['to']*1));
                $criteria->addCondition('`production_id`='.$_GET['productions']*1);
            }
            
            
            
            /* ------------------------------- */
            
            if(isset($_REQUEST['manuf']))
            {
                //exit;
                $criteria->condition = 'production_id=:PI';
                $criteria->params = array(':PI'=>$_REQUEST['manuf']);
            }
            
            if(isset($_REQUEST['product_name']))
            {
                //exit;
                $criteria->compare('product_name', $_REQUEST['product_name'], true);
                $criteria->compare('product_name', ucfirst($_REQUEST['product_name']), true);
            }
            
			
            $dataProvider = new CActiveDataProvider('Product', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 15,
                ),
            ));
            
            $scenario = 'all';
        }

        $this->catalog();
        /* NSV закомит
        $product = Product::model()->findAll();
        $all_product = array();
        
        foreach ($product as $val)
        {
            $all_product['name'][] = $val->product_name;
            $all_product['price'][] = $val->price;
            $all_product['id'][] = $val->id;
            $all_product['description'][] = $val->description;
            $all_product['image'][] = $val->mainImage->image;
        }
        */
        $all_product = $this->all_product2();//NSV

        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_catalog',array(
                'result_categories' => $this->result_categories,
                'dataProvider' => $dataProvider,
                'all_product' => $all_product,
                'scenario' => $scenario,
            ));
        }else{
            $this->render('catalog',array(
                'result_categories' => $this->result_categories,
                'dataProvider' => $dataProvider,
                'all_product' => $all_product,
                'scenario' => $scenario,
            ));
        }

    }

    public function actionNews($id = null)
    {
        $scenario = '';
        $criteria = new CDbCriteria;
		$criteria->order = '`news_id` DESC';
        
        if ($id != null)
        {
            $criteria->condition = 'news_id=:id';
            $criteria->params = array(':id'=>$id);
            $scenario = 'detail_news';
        }
	
        $product = Product::model()->findAll();
        $all_product = array();
        
        foreach ($product as $val)
        {
            $all_product['name'][] = $val->product_name;
            $all_product['price'][] = $val->price;
            $all_product['id'][] = $val->id;
            $all_product['description'][] = $val->description;
            $all_product['image'][] = $val->mainImage->image;
        }

        $this->catalog();

        $dataProvider=new CActiveDataProvider('PublishNews', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 6,
            ),
        ));

        $this->render('news', array(
            'result_categories' => $this->result_categories,
            'dataProvider' => $dataProvider,
            'all_product' => $all_product,
            'scenario' => $scenario,
        ));
    }
	

    public function actionAbout()
    {
        $this->catalog();
        //$product = Product::model()->findAll();
        $all_product = $this->all_product2();

        $comment = array();
        $model = Comment::model()->findAll();
        
        foreach ($model as $val)
        {
            $comment[] = $val->comment;
        }
        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_about',array(
                'comment' => $comment,
                'all_product' => $all_product,
                'result_categories' => $this->result_categories,
            ));
        }else{
            $this->render('about',array(
                'comment' => $comment,
                'all_product' => $all_product,
                'result_categories' => $this->result_categories,
            ));
        }
    }

    public function actionHow()
    {
        $popular = Popular::model()->findAll();
        $this->catalog();
        $product = Product::model()->findAll();
        $all_product = array();
        foreach($product as $val){
            $all_product['name'][] = $val->product_name;
            $all_product['price'][] = $val->price;
            $all_product['id'][] = $val->id;
            $all_product['description'][] = $val->description;
            $all_product['image'][] = $val->mainImage->image;
        }

        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_howMake',array(
                'all_product' => $all_product,
                'result_categories' => $this->result_categories,
                'popular' => $popular,
            ));
        }else{
            $this->render('howMake',array(
                'all_product' => $all_product,
                'result_categories' => $this->result_categories,
                'popular' => $popular,
            ));
        }
    }

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function actionInter_woman()
    {
        $this->render('interActiveWoman');
    }

    public function actionInter_man()
    {
        $this->render('interActiveMan');
    }

    public function action_ajax()
    {
        mb_internal_encoding("UTF-8");
        $result = array();
        $query = '';
        if($_POST['scenario']=='woman'){
            $womanInterRoom = InterWoman::model()->findAll();
            foreach($womanInterRoom as $val){
                if($_POST['plus_id'] == $val->id){
                    $query = $val->product_id;
                }
            };
        }
        if($_POST['scenario']=='man'){
            $manInterRoom = InterMan::model()->findAll();
            foreach($manInterRoom as $val){
                if($_POST['plus_id'] == $val->id){
                    $query = $val->product_id;
                }
            };
        }

        if($model = Product::model()->findByAttributes(array('id' => $query))){
            if(strlen($model->description)<=275){
                $description = $model->description;
            }
            else{
                $description = mb_substr($model->description, 0, 275).'...';
            }
            $result[1] = $model->product_name;
            $result[2] = $description;
            $result[3] = 'Цена: '.number_format($model->price, 0, '.', ' ').' руб.';
            $result[4] = $model->_onePole;
            $result[5] = $query;
        };

        echo json_encode($result);
    }

    // Вывод товара
    public function actionProductDetail($id)
    {
        $model = Product::model()->findByPk($id);
        
  		// Хлебные крошки
        $sum = '<a href="/site/catalog">Каталог</a>';
        $catP = Category::model()->findByPk($model->oneProductCategory->category);
        $t3 = ' - ';
        $t3 .= '<a href="/site/catalog/?id='.$catP->id.'">'.$catP->name.'</a>';
        $main_cat = $catP->id;
        
        if ($catP->category_id != 0)
        {
            $secP = Category::model()->findByAttributes(array('id' => $catP->category_id));
            $t2 = ' - ';
            $t2 .= '<a href="/site/catalog/?id='.$secP->id.'">'.$secP->name.'</a>';
            $main_cat = $secP->id;
        
            if ($secP->category_id != 0)
            {
                $frP = Category::model()->findByAttributes(array('id' => $secP->category_id));
                $t1 = ' - ';
                $t1 .= '<a href="/site/catalog/?id='.$frP->id.'">'.$frP->name.'</a>';
                $main_cat = $frP->id;
            }
        }
        $sum .= $t1;
        $sum .= $t2;
        $sum .= $t3;
        
        // Узнаем вышестоящую категорию для Рекомендуемых товаров
        $beforeCategory = 1;
        $allMainCategory = Category::model()->findAll('category_id=0');
        
        if (isset($allMainCategory) && !empty($allMainCategory))
        {
            $arrayCat = array();
            foreach ($allMainCategory as $aCat)
            {
                $arrayCat[$aCat->publish->position] = $aCat->id;
            }
            if (!empty($arrayCat))
            {
                ksort($arrayCat); $catCounter = 0;
                foreach ($arrayCat as $key => $item)
                {
                    if ($catCounter == 0 && $main_cat == $item)
                    {
                        end($arrayCat);
                        $beforeCategory = current($arrayCat);
                    }
                    else
                    {
                        if ($main_cat == $item) 
                        {
                            prev($arrayCat);
                            prev($arrayCat);
                            $beforeCategory = current($arrayCat);
                        }
                    }
                    $catCounter++;
                }
            }
        }
        
        // Рекомендуемые товары
        $i = 0;
        $allR = array();
        //$allRoom = Product::model()->findAll();
        $allRoom = Product::model()->with(array(
            'aC' => array('condition' => 'category='.$beforeCategory),
        ))->findAll();
        shuffle($allRoom); // Чтобы каждй раз случайный порядок
        foreach($allRoom as $val)
        {
            //if ($val->room == 1)
            //{
                $recPrice = $val->price;
                if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($val->productCityPrice) && !empty($val->productCityPrice))
                {
                    foreach ($val->productCityPrice as $city_p)
                    {
                        if ($city_p->id_city == $_COOKIE['city'])
                        {
                            $recPrice = $city_p->price;
                        }
                    }
                }
                $allR[$i]['price'] = $recPrice;
                $allR[$i]['image'] = $val->mainImage->image;
                $allR[$i]['id'] = $val->id;
                $allR[$i]['discount'] = $val->productDiscount->discount;
                $i++;
            //}
        }
        $sql = '
            SELECT pp.*
            FROM ProductPole AS pp
            INNER JOIN pole AS p ON pp.pole_id = p.id
            WHERE pp.product_id = '.$id.' AND p.pole_type != 2
        ';
//        $pole = ProductPole::model()->findAllByAttributes(array(
//            'product_id' => $id,
//        ));
        $pole = ProductPole::model()->findAllBySql($sql);
        $name_pole = '';
        $mas_pole = array();
        foreach ($pole as $val)
        {
            if ($val->PoleName->pole_name != $name_pole)
            {
                $mas_pole['name'][] = $val->PoleName->pole_name;
                $mas_pole['id'][] = $val->id;
                //$mas_pole['price_change'][] = $val->price_change;
                $mas_name = $val->PoleName->pole_name;
            }
            
            $name_pole = $val->PoleName->pole_name;
            $mas_pole['val'][$mas_name][$val->id] = $val->value;
            $mas_pole['price_change'][$mas_name][$val->id] = $val->price_change;
        }
        
        //Вытаскиваем поля характеристик с картинками
        $sql = '
            SELECT pp.*
            FROM ProductPole AS pp
            INNER JOIN pole AS p ON pp.pole_id = p.id
            WHERE pp.product_id = '.$id.' AND p.pole_type = 2
        ';
        $img_pole_arr = ProductPole::model()->findAllBySql($sql);
         
        $name_pole = ''; 
        $img_pole  = array();     
        foreach ($img_pole_arr as $val)
        {
            if ($val->PoleName->pole_name != $name_pole)
            {
                $img_pole['name'][] = $val->PoleName->pole_name;
                $img_pole['id'][] = $val->id;
                //$img_pole['price_change'][] = $val->price_change;
                $mas_name = $val->PoleName->pole_name;
            }
            
            $name_pole = $val->PoleName->pole_name;
            $img_pole['val'][$mas_name][$val->id] = $val->value;
            $img_pole['img'][$mas_name][$val->id] = $val->img;
            $img_pole['price_change'][$mas_name][$val->id] = $val->price_change;
        }
        
//        echo "<pre>";
//        var_dump($img_pole); exit;
        

        $all_product = $this->all_product2();
		$res_cat = array();
		$i = 1;
        
		foreach ($model->aC as $val)
        {
			$res_cat[$i] = $val->category;
			$i++;
		}
        
		$new_all_product = $this->new_all_product($res_cat[1]);
		$article = Article::model()->findAll();

        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_main_product',array(
                'new_all_product' => $new_all_product,
                'mas_pole' => $mas_pole,
                'all_product' => $all_product,
                'allR' => $allR,
                'model' => $model,
                'sum' => $sum,
                'article' => $article,
                'img_pole' => $img_pole,
            ));
        }else{
            $this->render('main_product',array(
                'new_all_product' => $new_all_product,
                'mas_pole' => $mas_pole,
                'all_product' => $all_product,
                'allR' => $allR,
                'model' => $model,
                'sum' => $sum,
                'article' => $article,
                'img_pole' => $img_pole,
            ));
        }
    }

    public $price_crazy = array();
    
    public function actionBasket()
    {
        $scenario = 'no_basket';
        $good_mas = array();
        
        if ($_COOKIE['basket'] != null)
        {
            $model = json_decode($_COOKIE['basket']);
            $i = 0;
            
            foreach ($model as $val)
            {
                if ($model = Product::model()->findByPk($val->product_id))
                {
                    $good_mas[$i]['product_id'] = $val->product_id;
                    
                    foreach ($val->pole_id as $key=>$val2)
                    {
                        $good_mas[$i]['pole_id'][$key] = $val2;
                    }
                    
                    foreach($val->pole_name as $key=>$val2)
                    {
                        $good_mas[$i]['pole_name'][$key] = $val2;
                    }
                    
                    $good_mas[$i]['count'] = $val->count;
                    $i++;
                    $scenario = 'basket';
                }
                else 
                {
                    unset(Yii::app()->request->cookies['basket']);
                }
            }
        }

        $contact_client = new ClientContact();
        $region = Region::model()->findAll();
        $all_product = $this->all_product3();
        $all_region = array();
        
        foreach ($region as $val) 
        {
            if ($val->price != 0) 
            {
                $all_region[$val->region] = $val->region . '</br>' . '<div style="font-size: 14px">(Стоимость доставки ' . $val->price . ' руб.)</div>';
            }
            else
            {
                $all_region[$val->region] = $val->region . '</br>' . '<div style="font-size: 14px">(Доставка бесплатно)</div>';
            }
        }

        if (isset($_POST['ClientContact']))
        {
            $order = new Order();
            
            // Не забываем про выбранный из списка заказчиком город
            $order->id_city = 0;
            if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0) // Если город не дефолтный, то узнаем какой
            {
                $check_city = City::model()->findByPk($_COOKIE['city']);
                
                if (isset($check_city->id_city) && !empty($check_city->id_city))
                {
                    $order->id_city = $check_city->id_city;
                }
            }
            
            // Контактные данные
            $order->name = $_POST['ClientContact']['name'];
            $order->email = $_POST['ClientContact']['email'];
            $order->phone = $_POST['ClientContact']['phone'];
            $order->comment = $_POST['ClientContact']['comment'];
            $allCount = 0;
            
            $product_model = Product::model()->findAllByPk($_POST['product_id']);
            
            $counter_city = 0;
            foreach ($product_model as $key => $val)
            {
                
                if ($_POST['product_count_new'][$val->id] != 0)
                {
                    
//                                var_dump($_POST['product_id']);
//            exit;
                    $polePr = ProductPole::model()->findByPk($_POST['pole_change'][$val->id]);
                    $price_change = $polePr->price_change;
                    
                    $cityPrice = $val->price;
                    $cityName = 'Москва';
                    
                    if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($val->productCityPrice) && !empty($val->productCityPrice))
                    {
                        foreach ($val->productCityPrice as $city_p)
                        {
                            if ($city_p->id_city == $_COOKIE['city'])
                            {
                                $cityPrice = $city_p->price;
                                $getCity = City::model()->findByPk($city_p->id_city);
                                $cityName = $getCity->title;
                            }
                        }
                    }
                    
                    if ($counter_city == 0) {$order->product_name .= '<div style="margin-bottom: 10px;">Город: <b>'.$cityName.'</b></div>';}

                    $order->product_name .= '
                        <div>Продукт №'.($key+1).'</div>
                        <table id="super_table" border="1px">
                            <tr>
                                <td>Продукт</td>
                                <td>Кол.</td>
                                <td>Общая стоимость</td>
                                <td>Хар. товара</td>
                            </tr>
                            <tr>
                                <td>'.$val->product_name.'</td>
                                <td>'.$_POST['product_count_new'][$val->id].'</td>
                                <td>'.$_POST['product_count_new'][$val->id]*round((100-$val->productDiscount->discount*1)*($cityPrice+$price_change*1)/100,0).'</td>
                                <td>'.$_POST['pole'][$val->id].'</td>
                            </tr>
                        </table>';

                    $allCount = $allCount + $_POST['product_count_new'][$val->id]*round((100-$val->productDiscount->discount*1)*($cityPrice+$price_change*1)/100,0);
                    $order->pole .= '<div>'.$val->product_name.' поля: '.$_POST['pole'][$val->id].'</div>';
                
                    $counter_city++;
                }
            }
            
            $priceRegion = Region::model()->findByAttributes(array('region'=>$_POST['region']));
            
            // Скидка за промокод
            $promodisc = 0; $promotext1 = ''; $promotext2 = '';
            if (isset($_POST['promocode']) && !empty($_POST['promocode']))
            {
                $postcode = trim($_POST['promocode']);
                $promomodel = Promo::model()->find('promocode=:promocode AND status=0', array(':promocode' => $postcode));
                
                if (isset($promomodel->discount) && !empty($promomodel->discount))
                {
                    $promodisc = ($allCount * $promomodel->discount) / 100;
                    $promotext1 = 'Скидка к товарам за промокод '.$promomodel->promocode.': '.$promomodel->discount.'% (-'.$promodisc.' р.)<br>';
                    $promotext2 = ' + скидка за промокод';
                }
            }
            
            $order->product_name .= '
                <div style="margin-top: 25px">
                    Стоимость товаров: '.$allCount.' р. + <br>
                    Стоимость доставки('.$_POST['region'].'): '.$priceRegion->price.' р.<br>
                    '.$promotext1.'
                    Общая стоимость с доставкой'.$promotext2.': '.($allCount*1 + $priceRegion->price*1 - $promodisc).' р.<br></br>
                </div>
            ';

            if (isset($_POST['region']))
            {
                $order->region = $_POST['region'].'<br/>Стоимость доставки: '.$priceRegion->price.' р.';
            }
            
            $order->date = time();
            
            if ($order->save())
            {
                unset(Yii::app()->request->cookies['basket']);
                if ($promodisc > 0 && isset($promomodel)) 
                {
                    $promomodel->status = 1;
                    $promomodel->update();
                }
                
                $order->goContact();
            }
            
            $this->redirect(array('/site/index?thx=yes'));
        }

        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->render('m_basket',array(
                'model'=>$good_mas,
                'all_product' => $all_product,
                'scenario' => $scenario,
                'contact_client' => $contact_client,
                'all_region' => $all_region,
            ));
        }else{
            $this->render('basket',array(
                'model'=>$good_mas,
                'all_product' => $all_product,
                'scenario' => $scenario,
                'contact_client' => $contact_client,
                'all_region' => $all_region,
            ));
        }

    }

    public function actionThx()
    {
        $this->render('thx');
    }

    public function all_product()
    {
        $popular = Popular::model()->findAll();
        $all_product = array();
        foreach($popular as $val){
            $all_product['name'][] = $val->product->product_name;
            $all_product['price'][] = $val->product->price;
            $all_product['id'][] = $val->product->id;
            $all_product['discount'][] = $val->product->productDiscount->discount;
            $all_product['act'][] = $val->product->act_id;
            $all_product['image'][] = $val->product->mainImage->image;

            $name_pole = ProductPole::model()->findByAttributes(array(
                'product_id' => $val->product->id,
            ));

            if($name_pole->PoleName->pole_name!=null){
                $all_product['size'][] = $name_pole->PoleName->pole_name.': '.$name_pole->value;
            }
            else{
                $all_product['size'][] = '&nbsp';
            }

        }

        return $all_product;
    }

    public function all_product3()
    {
        $popular = Product::model()->findAllByAttributes(array('recom'=>1));
        
        $all_product = array();
        
        foreach($popular as $val)
        {
            $cityPrice = $val->price;
        
            if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($val->productCityPrice) && !empty($val->productCityPrice))
            {
                foreach ($val->productCityPrice as $city_p)
                {
                    if ($city_p->id_city == $_COOKIE['city'])
                    {
                        $cityPrice = $city_p->price;
                    }
                }
            }
            
            $all_product['name'][] = $val->product_name;
            $all_product['price'][] = $cityPrice;
            $all_product['id'][] = $val->id;
            $all_product['discount'][] = $val->productDiscount->discount;
            $all_product['act'][] = $val->act_id;
            $all_product['description'][] = $val->description;
            $all_product['image'][] = $val->mainImage->image;
            
            //$all_product['city_price'][] = $val->productCityPrice;

            $name_pole = ProductPole::model()->findByAttributes(array(
                'product_id' => $val->id,
            ));

            if ($name_pole->PoleName->pole_name != null)
            {
                $all_product['size'][] = $name_pole->PoleName->pole_name.': '.$name_pole->value;
            }
            else
            {
                $all_product['size'][] = '&nbsp';
            }
        }

        return $all_product;
    }

    public function all_product2()
    {
        $popular = Product::model()->findAll();
        
        $all_product = array();
        
        foreach($popular as $val)
        {
            $cityPrice = $val->price;
        
            if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($val->productCityPrice) && !empty($val->productCityPrice))
            {
                foreach ($val->productCityPrice as $city_p)
                {
                    if ($city_p->id_city == $_COOKIE['city'])
                    {
                        $cityPrice = $city_p->price;
                    }
                }
            }
            
            $all_product['name'][] = $val->product_name;
            $all_product['price'][] = $cityPrice;
            $all_product['id'][] = $val->id;
            $all_product['discount'][] = $val->productDiscount->discount;
            $all_product['act'][] = $val->act_id;
            $all_product['description'][] = $val->description;
            $all_product['image'][] = $val->mainImage->image;
            
            //$all_product['city_price'][] = $val->productCityPrice;

            $name_pole = ProductPole::model()->findByAttributes(array(
                'product_id' => $val->id,
            ));

            if ($name_pole->PoleName->pole_name != null)
            {
                $all_product['size'][] = $name_pole->PoleName->pole_name.': '.$name_pole->value;
            }
            else
            {
                $all_product['size'][] = '&nbsp';
            }
        }

        return $all_product;
    }
	
	public function new_all_product($category_id)
    {
        $pop = ProductCategory::model()->findAllByAttributes(array('category' => $category_id));
		
        $all_product = array();
		
        foreach ($pop as $val)
        {
            $cityPrice = $val->Product->price;

            if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && $_COOKIE['city'] > 0 && isset($val->Product->productCityPrice) && !empty($val->Product->productCityPrice))
            {
                foreach ($val->Product->productCityPrice as $city_p)
                {
                    if ($city_p->id_city == $_COOKIE['city'])
                    {
                        $cityPrice = $city_p->price;
                    }
                }
            }
            
            $all_product['name'][] = $val->Product->product_name;
            $all_product['price'][] = $cityPrice;
            $all_product['id'][] = $val->Product->id;
            $all_product['discount'][] = $val->Product->productDiscount->discount;
            $all_product['act'][] = $val->Product->act_id;
            $all_product['description'][] = $val->Product->description;
            $all_product['image'][] = $val->Product->mainImage->image;

            $name_pole = ProductPole::model()->findByAttributes(array(
                'product_id' => $val->Product->id,
            ));

            if($name_pole->PoleName->pole_name!=null){
                $all_product['size'][] = $name_pole->PoleName->pole_name.': '.$name_pole->value;
            }
            else{
                $all_product['size'][] = '&nbsp';
            }

        }

        return $all_product;
    }
	
    function actionComment()
    {
        $all_product = $this->all_product2();
        $this->catalog();
        $form_comment = new UserComment();

        $criteria = new CDbCriteria;
        $criteria->params = array(':state' => 1);
        $criteria->condition = 'state=:state';
        
        $dataProvider=new CActiveDataProvider('UserComment', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>6,
            ),
        ));

        if (isset($_POST['UserComment']))
        {
            $form_comment->attributes = $_POST['UserComment'];
            $form_comment->date = time();
            
            if ($_POST['name_image']!='no')
            {
                $IconUser = IconComment::model()->findByAttributes(array('icon' => $_POST['name_image']));
                $id_icon = $IconUser->id;
                $form_comment->icon_id = $id_icon;
            }
            
            if ($form_comment->save())
            {
                $this->redirect('/site/comment');
            };
        }

        $this->render('comment',array(
            'result_categories' => $this->result_categories,
            'form_comment' => $form_comment,
            'all_product' => $all_product,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAjd()
    {
        $this->render('lol');
    }

    public function actionUpload()
    {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder='uploads/icon/preview/';// folder for uploaded files
        $allowedExtensions = array("jpg", "jpeg", "gif", "png");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
        $fileName=$result['filename'];//GETTING FILE NAME
        $UserIcon = new IconComment();
        $UserIcon->icon = $result['filename'];
        $UserIcon->icon_user = 0;
        $UserIcon->save(false);

        echo json_encode($fileName);// it's array
    }
    
    // Страница поиска
    public function actionSearch()
    {
        $product = Product::model()->findAll();
        $all_product = array();
        
        foreach ($product as $val)
        {
            $all_product['name'][] = $val->product_name;
            $all_product['price'][] = $val->price;
            $all_product['id'][] = $val->id;
            $all_product['description'][] = $val->description;
            $all_product['image'][] = $val->mainImage->image;
        }
        
        $page_size = 64;
        
        if (isset($_GET['data_search']) && !empty($_GET['data_search'])) 
        {
            // Текущая страница и номер начальной записи
            if (isset($_GET['page'])) 
            {
                if (preg_match("/^[0-9]+$/", $_GET['page'])) 
                {
                    $page = $_GET['page'];
                    $num = 1+$page_size*($_GET['page']-1);
                }
            } 
            else {$page = 1; $num = 1;}
            
            // Фильтруем поисковый запрос
            $search_title = new CHtmlPurifier();
            $search_title = $search_title->purify($_GET['data_search']);
            $search_title = str_replace("&amp;", "&", $search_title);
            $search_title = trim($search_title);
            
            // Подключаем Sphinx
            Yii::import('application.vendor.*');
            include('sphinxapi.php');
            
            $cl = new SphinxClient();
            $cl->SetServer("localhost", 3312);
            $cl->SetLimits(($page-1)*$page_size, $page_size);
            $cl->SetMatchMode("SPH_MATCH_EXTENDED2");
            
            $result = $cl->Query($search_title, "comfortbeby");
            
            // Пагинация
            $criteria = new CDbCriteria();
            $count = $result["total"];
            $pages = new CPagination($count);
            $pages->pageSize = $page_size;
            $pages->applyLimit($criteria);
            
            if ($result["total"] == 0) {$result['matches'] = null;}
        }
        else 
        {
            $result['matches'] = null; $pages = null; $count = null; $page = null; $num = null;
        }
        
        $this->render('search', array(
            'result' => $result['matches'],
            'all_product' => $all_product,
            // Для пагинации
            'pages' => $pages,
            'page_size' => $page_size,
            'count' => $count,
            'page' => $page,
            'num' => $num,
        ));
    }
    
    // Подсказки для поиска
    public function actionHints()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            // Фильтруем поисковый запрос
            $search_title = new CHtmlPurifier();
            $search_title = $search_title->purify($_GET['data_search']);
            $search_title = str_replace("&amp;", "&", $search_title);
            $search_title = trim($search_title);
            
            // Подключаем Sphinx
            Yii::import('application.vendor.*');
            include('sphinxapi.php');
            
            $cl = new SphinxClient();
            $cl->SetServer("localhost", 3312);
            $cl->SetLimits(0, 6);
            $cl->SetMatchMode(SPH_MATCH_EXTENDED2);
            $cl->SetSortMode(SPH_SORT_RELEVANCE);
            
            $result = $cl->Query($search_title, "comfortbeby");
            
            if (!empty($result["matches"])) 
            {
                echo '<div>Возможно вы искали &darr;</div>';
                
                $i = 0;
                foreach ($result["matches"] as $info) 
                {
                    if ($i < 6)
                    {
                        echo '<a href="/site/productDetail/'.$info["attrs"]["idproduct"].'">'.$info["attrs"]["productname"].'</a>';
                    }
                    
                    $i++;
                }
            }
            else
            {
                echo '0';
            }
        }
        else {throw new CHttpException(404, 'Такой страницы не существует');}
    }

    public function actionTestMail()
    {
        /*if( mail('serjeku@mail.ru', 'test', '<body>sdfs</body>') ){
            echo 'OK';
        }*/
        include_once "libmail.php";
        $u = new Mail;
        //$u->From('serjeku@mail.ru');
        $u->To(array('serjeku@mail.ru'));
        $u->Subject('Заказ покупки');
        $u->Body('lol');
        if( $u->Send() ){
            echo 'OK';
        }
    }
    
	public function actionArticle($id=null)
    {
        $scenario = '';
        $criteria = new CDbCriteria;
        $criteria->order = '`id` DESC';

        if ($id != null)
        {
            $criteria->condition = 'id=:id';
            $criteria->params = array(':id'=>$id);
            $scenario = 'detail_news';
        }

        $all_product = $this->all_product2();

        $this->catalog();

        $dataProvider=new CActiveDataProvider('Article', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>6,
            ),
        ));

        if(Yii::app()->mobileDetect->isMobile() && !isset($_COOKIE['m_full_button'])){
            if($id != null){

                $this->render('m_article',array(
                    'result_categories' => $this->result_categories,
                    'dataProvider' => $dataProvider,
                    'all_product' => $all_product,
                    'scenario' => $scenario,
                ));
            }else{
                $this->redirect('index');
            }
        }else{
            $this->render('article',array(
                'result_categories' => $this->result_categories,
                'dataProvider' => $dataProvider,
                'all_product' => $all_product,
                'scenario' => $scenario,
            ));
        }


    }
    
//    public function actionArticles()
//    {
//        $criteria = new CDbCriteria;
//		$criteria->order = '`id` DESC';
//        $dataProvider=new CActiveDataProvider('Article', array(
//            'criteria'=>$criteria,
//            'pagination'=>array(
//                'pageSize'=>10,
//            ),
//        ));
//        
//        $this->render('articles', array(
//            'dataProvider' => $dataProvider,
//        ));
//    }
}