<script>
    $(function(){
        // Сохраняем в куки выбранный город
        $('#city').change(function(){
            var city_id = $.trim($(this).val());
            $.cookie('city', city_id, {expires: 30, path: '/'});
            location.reload();
        });
    });
</script>

<div class="select_city_box">
    <select name="city" id="city" class="styled">
        <option value="0">Москва</option>
        <?php if (isset($all_city) && !empty($all_city)): ?>
            <?php foreach ($all_city as $city): ?>
                <option value="<?php echo $city->id_city; ?>"  
                    <?php if (isset($_COOKIE['city']) && !empty($_COOKIE['city']) && ($_COOKIE['city'] == $city->id_city)): ?>
                        selected
                    <?php endif; ?>
                ><?php echo $city->title; ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>
</div>