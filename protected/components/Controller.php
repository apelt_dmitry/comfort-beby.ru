<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }

    public $mobile;
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
    
    public $urlvar = '';
    
    public function init()
    {
        $detect = Yii::app()->mobileDetect;
        if($detect->isMobile() && !isset($_COOKIE['m_full_button'])){
            $this->layout ='//layouts/m_main';
        }
        else{
            $this->layout = '//layouts/main';
        }


        // Устанавливаем город по умолчанию
        if (!isset($_COOKIE['city']))
        {
            $check = City::model()->find('type=1');
            
            if (isset($check->id_city))
            {
                setcookie("city", $check->id_city, time() + 3600*24*30, '/');
                
                $this->refresh(); 
            }
        }
    }
}