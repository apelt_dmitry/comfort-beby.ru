<?php

class CityWidget extends CWidget
{
    public function run()
    {
        $all_city = City::model()->findAll();
        
        $this->render('citywidget', array(
            'all_city' => $all_city
        ));
    }
}