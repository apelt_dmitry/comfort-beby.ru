<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'promo-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'promocode', array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'discount', array('maxlength'=>2)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'status', array(0 => 'Активен', 1 => 'Погашен')) ?>

    <?=
        BsHtml::formActions(array(
        BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
        BsHtml::linkButton('Назад', array(
            'color' => BsHtml::BUTTON_COLOR_DEFAULT,
            'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
            'url' => Yii::app()->request->urlReferrer,
        )),
        ));
    ?>

<?php $this->endWidget(); ?>