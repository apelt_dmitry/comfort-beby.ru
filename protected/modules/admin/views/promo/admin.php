<script>
    $(document).ready(function(){
        $('.page-header h1').text('Промокоды')
    })
</script>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#promo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= BsHtml::ajaxButton('Удалить', array('/admin/promo/_ajaxDelete'),array(
            'type' => 'Post',
            'data' => array(
                'id' => 'js:$.fn.yiiGridView.getSelection("promo-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("promo-grid")}',
            'dataType'=>'json',
        )) ?>
        
        <?= BsHtml::linkButton('Добавить промокод', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
            'url' => '/admin/promo/create',
        )); ?>

        <?php $this->widget('bootstrap.widgets.BsGridView', array(
            'selectableRows' => 2,
			'id' => 'promo-grid',
            'nullDisplay' => '-',
			'dataProvider' => $model->search(),
			'filter' => $model,
			'columns' => array(
                array(
                    'class'=>'CCheckBoxColumn',
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                ),
        		'id_promo',
                'promocode',
                'discount',
                array(
                    'name' => 'status',
                    'value' => '($data->status == 0) ? "Активен" : "Погашен"',
                    'filter' => CHtml::activeDropDownList($model, "status", array(0 => 'Активен', 1 => 'Погашен'), array('prompt' => 'Все статусы')),
                ),
				array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'deleteButtonOptions'=>array('style'=>'display: none'),
                    'viewButtonOptions'=>array('style'=>'display: none'),
                ),
			),
        )); ?>
    </div>
</div>




