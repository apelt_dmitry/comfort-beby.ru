<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => true,
    'id' => 'user_form',
    'htmlOptions' => array(
        'class' => 'bs-example'
    )
));
?>
    <fieldset>
        <legend>Авторизация</legend>
        <?php
        echo $form->textFieldControlGroup($model, 'username', array(
            'AUTOCOMPLETE'=>'off',
        ));
        ?>
        <?php
        echo $form->passwordFieldControlGroup($model, 'password', array(
            'AUTOCOMPLETE'=>'off',
        ));
        ?>
        <?php
        echo BsHtml::formActions(array(
            BsHtml::submitButton('Вход', array(
                'color' => BsHtml::BUTTON_COLOR_PRIMARY
            ))
        ));
        ?>
    </fieldset>
<?php
$this->endWidget();
?>