<script>
    $(function() {
        
        // Скрыть/Показать варианты для поле выпадающего списка
        $('#Pole_pole_type').change(function() {
            var type = $(this).val();
            if (type == 1) {$('#show_options').show();} else {$('#show_options').hide(); $('#show_options .form-group').remove();}
        });
        
        // Добавление варианта для поля
        $('#link_add_pole').click(function() {
            
            var html = '';
            html += '<div class="form-group">';
            html += '<div class="col-lg-2"></div>';
            html += '<div class="col-lg-6">';
            html += '<input value="" name="Variant[]" class="form-control" type="text" maxlength="255">';
            html += '</div>';
            html += '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
            html += '</div>';

            $('.addVariant:last').after(html);
            
            delete_input();
        });
        
        // Удалить вариант
        function delete_input(){
            $('.delete_pole').click(function(){
                $(this).parent().parent().remove();
                return false;
            });
        }
        $('.delete_pole').click(function(){
            $(this).parent().parent().remove();
            return false;
        });
    });
</script>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    'enableClientValidation'=>true,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<?= $form->textFieldControlGroup($model, 'pole_name', array(
    'style'=>'width:100%',
    'placeHolder'=>'Имя поля',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
    ),

)); ?>

<?= $form->dropDownListControlGroup($model, 'price_change', array(0 => 'Нет', 1 => 'Да')) ?>

<?= $form->dropDownListControlGroup($model, 'pole_type', array(0 => 'Текстовое поле', 1 => 'Выпадающий список с вариантами', 2 => 'Текстовое поле с картинкой')) ?>

<div <?php if (!isset($allVariants)): ?>class="dnone"<?php endif; ?> id="show_options">
    <div class="addVariant"></div>
    
    <?php if (isset($allVariants) && !empty($allVariants)): ?>
        <?php foreach ($allVariants as $variant): ?>
            <div class="form-group addVariant">
                <div class="col-lg-2"></div>
                <div class="col-lg-6">
                    <input value="<?php echo $variant->variant; ?>" name="Variant[]" class="form-control" type="text" maxlength="255">
                </div>
                <div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    
    <div class="col-lg-2"></div>
    <div class="col-lg-4" style="margin-bottom: 15px">
        <a href="#" id="link_add_pole">Добавить вариант</a>
    </div>
    <div class="clear"></div>
</div>

<?=
    BsHtml::formActions(array(
    BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
    ));
?>
<?php
$this->endWidget();
?>