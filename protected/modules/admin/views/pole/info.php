<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= BsHtml::ajaxButton('Удалить', array('/admin/pole/_ajaxDelete'),array(
            'type'=>'Post',
            'data'=>array(
                'news_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                ),
                'pole_name',
                array(
                    'name' => 'price_change',
                    'value' => '( $data->price_change==1 )?"Да":"Нет"',
                ),
                array(
                    'name' => 'pole_type',
                    'value' => '$data->getType()',
                    'filter' => CHtml::activeDropDownList($model, "pole_type", Pole::getTypeArr(), array('prompt' => 'Все поля')),
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'deleteButtonOptions'=>array('style'=>'display: none'),
                    'viewButtonOptions'=>array('style'=>'display: none'),
                ),
            )
        ))   ; ?>
    </div>
</div>




