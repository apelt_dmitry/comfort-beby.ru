<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<?= $form->textFieldControlGroup($model, 'title', array(
    'style'=>'width:100%',
    //'class'=>'',
    'placeHolder'=>'Загаловок',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
        //'class'=>'col-md-9',
    ),

)); ?>

<?= $form->textFieldControlGroup($model, 'meta_title', array(
    'style'=>'width:100%',
    //'class'=>'',
    'placeHolder'=>'Мета загаловок',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
        //'class'=>'col-md-9',
    ),

)); ?>
<div class="form-group">
    <?= $form->labelEx($model, 'description', array(
        'class'=>'control-label col-lg-2',
    )) ?>
    <div class="col-lg-10">
        <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
            'model' => $model,
            'attribute' => 'description',
            'defaultValue' => $model->description,
            'config' => array(
                //'height' => '400px',
                //'width' => '100%',
            ),
        )); ?>
    </div>
</div>

<?= $form->fileFieldControlGroup($model,'_image',array(
    'help'=>'До 1 изображения любого размера.'.( (!$model->isNewRecord)?' Выбор нового изображения удалит текущие':'' ),
    'multiple'=>'multiple',
)); ?>

<?=
    BsHtml::formActions(array(
    BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
    ));
?>
<?php
$this->endWidget();
?>