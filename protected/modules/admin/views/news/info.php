<?php
/* @var $this ProductController */
/* @var $model Product */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= BsHtml::ajaxButton('Опуб./Не опуб.', array('/admin/news/publish'),array(
            'type'=>'Post',
            'data'=>array(
                'news_publish' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <?= BsHtml::ajaxButton('Удалить', array('/admin/news/_ajaxDelete'),array(
            'type'=>'Post',
            "onclick"=>"if ( !confirm('Действительно удалить?') ) return false;",
            'data'=>array(
                'news_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    //'' => 2,
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                ),
                array(
                    'header'=>BsHtml::icon(BsHtml::GLYPHICON_LIST_ALT),
                    'headerHtmlOptions'=>array(
                        'data-toggle'=>'tooltip',
                        'title'=>'Опубликовано.',
                    ),
                    'type' => 'raw',
                    'value'=>'($data->publish!=null)?BsHtml::icon(BsHtml::GLYPHICON_LIST_ALT,array(\'data-toggle\'=>\'tooltip\',\'title\'=>\'Опубликовано.\')):null',
                ),
                array(
                    'name' => 'title',
                   
                ),
				array(
                    'name' => 'meta_title',
                   
                ),
                array(
                    'name' => 'description',
                    'type' => 'raw',
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'deleteButtonOptions'=>array('style'=>'display: none'),
                    'viewButtonOptions'=>array('style'=>'display: none'),
                ),
            )
        ))   ; ?>
    </div>
</div>




