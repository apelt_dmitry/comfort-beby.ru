<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'city-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="form-group validating">
        <label class="control-label col-lg-2 required" for="City_title"></label>
        <div class="col-lg-10">
            <i style="margin-bottom: 20px;">Для всех товаров дефолтный город - Москва. Поэтому нет нужды в создании этого города, он уже есть в списке как дефолтный.</i>
        </div>
    </div>
    
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>255)); ?>

    <?=
        BsHtml::formActions(array(
        BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
        BsHtml::linkButton('Назад', array(
            'color' => BsHtml::BUTTON_COLOR_DEFAULT,
            'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
            'url' => Yii::app()->request->urlReferrer,
        )),
        ));
    ?>

<?php $this->endWidget(); ?>