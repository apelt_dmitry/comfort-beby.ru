<script>
    $(document).ready(function(){
        $('.page-header h1').text('Список городов')
    })
</script>

<?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
    	$('.search-form').toggle();
    	return false;
    });
    $('.search-form form').submit(function(){
    	$('#city-grid').yiiGridView('update', {
    		data: $(this).serialize()
    	});
    	return false;
    });
");
?>

<div class="panel panel-default">
    <div class="panel-body" style="text-align:  center;">
        <?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array()); ?>
            <p style="font-size: 20px;">Город по умолчанию</p>
            
            <?php $check = City::model()->find('type=1'); ?>
            
            
            <p>
            <?php
            if (isset($check->id_city))
            {
                echo 'Сейчас город по умолчанию: <b>'.$check->title.'</b>';
            }
            else
            {
                echo 'Сейчас город по умолчанию: <b>Москва</b>';
            }
            ?>
            
            </p>
            Изменить город по умолчанию: <select name="defcity">
                <?php
                $cityarray = City::model()->findAll('type=0');
                
                if (isset($check->id_city))
                {
                    echo '<option value="0">Москва</option>';
                }
                
                if (!empty($cityarray))
                {
                    foreach ($cityarray as $item)
                    {
                        echo '<option value="'.$item->id_city.'">'.$item->title.'</option>';
                    }
                }
                
                ?>
            </select>
            
            
            <br /><br />
            <i>Для всех новых посетителей город по умолчанию выставляется основным (хранится в браузере пользователя 30 дней). Если пользователь ранее уже заходил на сайт и выбрал другой 
            город (или раньше был другой город по умолчанию и ему был выставлен именно он), отличный от текущего города по умолчанию, то мы ему 
            ничего не меняем. Опция действительна только для новых посетителей, впервые зашедших на сайт.
            <br /><br />
            В админке: <br />
            - город по умолчанию нельзя удалить<br />
            - может быть лишь один город по умолчанию</i>
            <br /><br />
            <input type="submit" value="Сохранить изменения" class="btn btn-success">
        <?php $this->endWidget(); ?>
    </div>
</div>

<div class="panel panel-default">

    <div class="panel-body">
        <?= BsHtml::ajaxButton('Удалить', array('/admin/city/_ajaxDelete'),array(
            'type' => 'Post',
            'data' => array(
                'id' => 'js:$.fn.yiiGridView.getSelection("city-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("city-grid")}',
            'dataType'=>'json',
        )) ?>
        
        <?= BsHtml::linkButton('Добавить город', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
            'url' => '/admin/city/create',
        )); ?>

        <?php $this->widget('bootstrap.widgets.BsGridView', array(
            'selectableRows' => 2,
			'id'=>'city-grid',
            'nullDisplay'=> '-',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                    'disabled' => '$data->type == 1 ? true : false',
                ),
                'id_city',
                'title',
                /*
                array(
                    'name' => 'type',
                    'value' => '($data->type == 0) ? "Обычный" : "Дефолтный"',
                    'filter' => CHtml::activeDropDownList($model, "type", array(0 => 'Обычный', 1 => 'Дефолтный'), array('prompt' => 'Все города')),
                ),*/
				array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'deleteButtonOptions'=>array('style'=>'display: none'),
                    'viewButtonOptions'=>array('style'=>'display: none'),
                ),
			),
        )); ?>
    </div>
</div>