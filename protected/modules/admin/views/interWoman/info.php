<?php
/* @var $this ProductController */
/* @var $model Product */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$dataProvider=new CActiveDataProvider($model, array(
    'pagination'=>array(
        'pageSize'=>16,
    ),
));

?>

<div class="panel panel-default">
    <div class="panel-body">
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'id'=>'interWoman-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$dataProvider,
            'filter'=>$model,
            'columns'=>array(

                array(
                    'name' => 'id',
                ),
                array(
                    'name' => 'product_id',
                    'value' => '$data->Product->product_name'
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'deleteButtonOptions'=>array('style'=>'display: none'),
                    'viewButtonOptions'=>array('style'=>'display: none'),
                ),

            )
        ))   ; ?>
    </div>
</div>


<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>


<?= $form->fileFieldControlGroup($image,'_image',array(
    'help'=>'До 1 изображения любого размера.'.( (!$model->isNewRecord)?' Выбор нового изображения удалит текущие':'' ),
    'multiple'=>'multiple',
)); ?>

<?=

BsHtml::formActions(array(
    BsHtml::submitButton('Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
));
?>
<?php
$this->endWidget();
?>

