<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    //'id' => 'user_form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
    )
));
?>


<?= $form->dropDownListControlGroup($model, 'capha', array(0=>'Нет',1=>'Да'));

?>


<?= BsHtml::formActions(array(
    BsHtml::submitButton('Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
), array('class'=>'form-actions')); ?>


<?php
$this->endWidget();
?>