<?php
/* @var $this ProductController */
/* @var $model Product */

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Product', 'url'=>array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Product', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?=BsHtml::linkButton('Добавить иконку', array(
            'icon' => BsHtml::GLYPHICON_PLUS,
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            //'size' => BsHtml::BUTTON_SIZE_MINI,
            'url' => array('/admin/comment/add_icon'),)); ?>

        <?= BsHtml::ajaxButton('Удалить', array('/admin/comment/delete_icon'),array(
            'type'=>'Post',
            "onclick"=>"if ( !confirm('Действительно удалить?') ) return false;",
            'data'=>array(
                'icon_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>

        <?= BsHtml::ajaxButton('Всем/Админ', array('/admin/comment/view_icon'),array(
            'type'=>'Post',
            "onclick"=>"if ( !confirm('Действительно удалить?') ) return false;",
            'data'=>array(
                'icon_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    //'' => 2,
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                    'headerHtmlOptions' => array(
                        'style' => 'width:20px',
                    ),
                ),
                array(
                    'headerHtmlOptions' => array(
                        'style' => 'width:80px',
                    ),
                    'name' => 'icon',
                    'type' => 'raw',
                    'filter' => false,
                    'value' => '$data->_image',
                ),
                array(
                    'name' => 'icon_user',
                    'filter' => false,
                    'value' => '( $data->icon_user!=0 )?"Всем пользователям":"Только админу"',
                    'headerHtmlOptions' => array(
                        'style' => 'width:120px',
                    ),

                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'template'=>''
                ),
            )
        ))   ; ?>
    </div>
</div>



