<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
'enableAjaxValidation' => true,
//'id' => 'user_form',
'htmlOptions' => array(
'class' => 'bs-example'
)
));
?>

<div class="form-group">
    <?= $form->labelEx($model, 'comment', array(
        'class'=>'control-label col-lg-2',
    )) ?>
    <div class="col-lg-10">
        <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
            'model' => $model,
            'attribute' => 'comment',
            'defaultValue' => $model->comment,
            'config' => array(
                //'height' => '400px',
                //'width' => '100%',
            ),
        )); ?>
    </div>
</div>


<?=
    BsHtml::formActions(array(
    BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
    ));
?>
<?php
$this->endWidget();
?>