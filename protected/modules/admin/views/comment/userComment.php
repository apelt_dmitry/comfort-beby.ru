<?php
/* @var $this ProductController */
/* @var $model Product */

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Product', 'url'=>array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Product', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= BsHtml::ajaxButton('Опуб./Не опуб.', array('/admin/comment/publish_comment'),array(
            'type'=>'Post',
            'data'=>array(
                'comment_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <?= BsHtml::ajaxButton('Удалить', array('/admin/comment/delete_comment'),array(
            'type'=>'Post',
            "onclick"=>"if ( !confirm('Действительно удалить?') ) return false;",
            'data'=>array(
                'comment_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    //'' => 2,
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                    'headerHtmlOptions' => array(
                        'style' => 'width:12px'
                    )
                ),
                array(
                    'name' => 'icon_id',
                    'type' => 'raw',
                    'value' => '$data->icon->_image',
                    'filter' => false,
                    'headerHtmlOptions' => array(
                        'style' => 'width:80px'
                    )
                ),
                array(
                    'name' => 'name',
                    'type' => 'raw',
                    'filter' => false,
                    'headerHtmlOptions' => array(
                        'style' => 'width:180px'
                    )
                ),
                array(
                    'name' => 'comment',
                    'type' => 'raw',
                    'filter' => false,
                    'headerHtmlOptions' => array(
                        'style' => 'width:700px'
                    )
                ),
                array(
                    'name' => 'state',
                    'value' => '( $data->state!=0 )?"Опубликованно":"Не опуб."',
                    'filter' => false,
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'template'=>''
                ),
            )
        ))   ; ?>
    </div>
</div>



