<?php
/* @var $this ProductController */
/* @var $model Product */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(

            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'id',
                array(
                    'name' => 'comment',
                    'type' => 'raw',
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'template'=>' {update} {delete}'
                ),
            )
        ))   ; ?>
    </div>
</div>


<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>

<?= $form->textFieldControlGroup($image, 'title', array(
    'style'=>'width:100%',
    //'class'=>'',
    'placeHolder'=>'Загаловок',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
        //'class'=>'col-md-9',
    ),

)); ?>

<?= $form->fileFieldControlGroup($image,'_image',array(
    'help'=>'До 1 изображения любого размера.'.( (!$model->isNewRecord)?' Выбор нового изображения удалит текущие':'' ),
    'multiple'=>'multiple',
)); ?>

<?=

BsHtml::formActions(array(
    BsHtml::submitButton('Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
));
?>
<?php
$this->endWidget();
?>

