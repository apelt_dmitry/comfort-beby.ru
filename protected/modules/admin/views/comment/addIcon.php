<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>

<?= $form->fileFieldControlGroup($model,'image[]',array(
    'multiple'=>'multiple',
)); ?>

<?=$form->dropDownListControlGroup($model, 'icon_user', array('0'=>'Только админу', '1'=>'Всем пользователям'));

?>

<?= BsHtml::formActions(array(
    BsHtml::submitButton('Добавить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
), array('class'=>'form-actions')); ?>

<?php
$this->endWidget();
?>