<?php

    $cs = Yii::app()->clientScript;
    $pt = Yii::app()->homeUrl;

    $cs
        // bootstrap 3.3.1
        ->registerCssFile($pt.'css/bootstrap.min.css')
        ->registerCssFile($pt.'css/admin.css')
        // bootstrap theme
        ->registerCssFile($pt.'css/bootstrap-editable.css')
        ->registerCssFile($pt.'css/bootstrap-theme.min.css');
    
    $cs
        ->registerCoreScript('jquery',CClientScript::POS_END)
        ->registerCoreScript('jquery.ui',CClientScript::POS_END)
        ->registerCoreScript('cookie',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/bootstrap-editable.min.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/admin.js');

    //$config = $this->config;

?>

<!DOCTYPE html>
<html>
<head>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <!--link rel="icon" type="image/png" href="/favicon.png" /-->

    <title><?= CHtml::encode($this->pageTitle); ?></title>

</head>

<body>
    <div class="container-fluid">
        
        <div class="row">
            
            <div id="header">
                <?php
                    $this->widget('bootstrap.widgets.BsNavbar', array(
                        'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
                        'brandUrl' => Yii::app()->homeUrl,
                        'items' => array(
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Конфигурации сайта',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Мета описание',
                                                'url' => array('/admin/meta/index'),
                                            ),
                                            array(
                                                'label' => 'Баннера',
                                                'url' => array('/admin/banner/index'),
                                            ),
                                            array(
                                                'label' => 'Слайдер на главной',
                                                'url' => array('/admin/slide/index'),
                                            ),
                                            array(
                                                'label' => 'Главная страница',
                                                'url' => array('/admin/config/info'),
                                            ),
                                            array(
                                                'label' => 'Контакты',
                                                'url' => array('/admin/contact/create'),
                                            ),
                                            array(
                                                'label' => 'Капча',
                                                'url' => array('/admin/config/capha'),
                                            ),
                                            array(
                                                'label' => 'Список городов',
                                                'url' => array('/admin/city'),
                                            ),
                                            array(
                                                'label' => 'Промокоды',
                                                'url' => array('/admin/promo'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Категории',
                                        //'url' => array('/admin/default/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Создать категорию',
                                                'url' => array('/admin/category/create'),
                                            ),
                                            array(
                                                'label' => 'Список категорий',
                                                'url' => array('/admin/category/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Товары',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Создать товар',
                                                'url' => array('/admin/product/create'),
                                            ),
                                            array(
                                                'label' => 'Список товаров',
                                                'url' => array('/admin/product/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Заказы',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'url' => array('/admin/order/info'),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Отзывы',
                                        //'url' => array('/admin/default/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить отзыв',
                                                'url' => array('/admin/comment/create'),
                                            ),
                                            array(
                                                'label' => 'Список отзывов',
                                                'url' => array('/admin/comment/info'),
                                            ),
                                            array(
                                                'label' => 'Отзывы посетителей',
                                                'url' => array('/admin/comment/userComment'),
                                            ),
                                            array(
                                                'label' => 'Список иконок',
                                                'url' => array('/admin/comment/icon_list'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Новости',
                                        //'url' => array('/admin/default/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить новость',
                                                'url' => array('/admin/news/create'),
                                            ),
                                            array(
                                                'label' => 'Список новостей',
                                                'url' => array('/admin/news/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
							array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Статьи',
                                        //'url' => array('/admin/default/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить статью',
                                                'url' => array('/admin/article/create'),
                                            ),
                                            array(
                                                'label' => 'Список статей',
                                                'url' => array('/admin/article/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Интер. комнаты',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Для мальчиков',
                                                'url' => array('/admin/interMan/info'),
                                            ),
                                            array(
                                                'label' => 'Для девочек',
                                                'url' => array('/admin/interWoman/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Регион',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить регион',
                                                'url' => array('/admin/region/create'),
                                            ),
                                            array(
                                                'label' => 'Список регионов',
                                                'url' => array('/admin/region/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Поля',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить поле',
                                                'url' => array('/admin/pole/create'),
                                            ),
                                            array(
                                                'label' => 'Список полей',
                                                'url' => array('/admin/pole/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Акции',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить акцию',
                                                'url' => array('/admin/act/create'),
                                            ),
                                            array(
                                                'label' => 'Список акций',
                                                'url' => array('/admin/act/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Производители',
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items' => array(
                                            array(
                                                'label' => 'Добавить производителя',
                                                'url' => array('/admin/production/create'),
                                            ),
                                            array(
                                                'label' => 'Список производителей',
                                                'url' => array('/admin/production/info'),
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Вход',
                                        'url' => array('/admin/default/login'),
                                        'visible'=>Yii::app()->user->isGuest,
                                    ),
                                    array(
                                        'label' => 'Выход',
                                        'url' => array('/admin/default/logout'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                ),

                                'htmlOptions' => array(
                                    'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                                    'style' => 'margin-right: 10px;',
                                ),
                            ),

                        ),
                    ));
                ?>
            </div>

            <?php if ( Yii::app()->user->getFlash('success') ): ?>
                <?= BsHtml::alert(Bshtml::ALERT_COLOR_SUCCESS, 'Запись сохранена.') ?>
            <?php endif; ?>

            <?php if ( Yii::app()->user->hasFlash('success_img') ): ?>
                <?= BsHtml::alert(Bshtml::ALERT_COLOR_SUCCESS, 'Картинка загружена') ?>
            <?php endif; ?>
            
            <div id="content">
                <?= BsHtml::pageHeader('Добавление страницы / пункта меню') ?>
                <?= $content ?>
            </div>

        </div>

        <div id="footer" class="panel panel-footer panel-default row">
            <div id="timeInfo" class="col_l col-md-6"><small>Время сервера: <?= date('d.m.Y, H:i') ?>.</small></div>
            <div id="logInfo" class="col_r col-md-6"><small>Страница сгенерирована за: <?=round(Yii::getLogger()->executionTime, 3).' сек' ?>.</small></div>
        </div>
        
    </div>
    
</body>
</html>