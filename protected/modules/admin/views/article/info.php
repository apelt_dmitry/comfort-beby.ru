<?php
/* @var $this ProductController */
/* @var $model Product */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= BsHtml::ajaxButton('Удалить', array('/admin/article/_ajaxDelete'),array(
            'type'=>'Post',
            "onclick"=>"if ( !confirm('Действительно удалить?') ) return false;",
            'data'=>array(
                'news_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    //'' => 2,
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
					 'headerHtmlOptions' => array(
                        'style' => 'width:20px',
                    ),
                ),
                array(
                    'name' => 'title',
                    'type' => 'raw',
					 'headerHtmlOptions' => array(
                        'style' => 'width:200px',
                    ),
                ),
				array(
                    'name' => 'meta_title',
                    'type' => 'raw',
					 'headerHtmlOptions' => array(
                        'style' => 'width:200px',
                    ),
                ),
                array(
                    'name' => 'description',
                    'type' => 'raw',
					 'headerHtmlOptions' => array(
                        'style' => 'width:800px',
                    ),
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'deleteButtonOptions'=>array('style'=>'display: none'),
                    'viewButtonOptions'=>array('style'=>'display: none'),
                ),
            )
        ))   ; ?>
    </div>
</div>




