<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'bs-example',
        //'enctype'=>'multipart/form-data',
    )
));
?>
<?= $form->textFieldControlGroup($model, 'name', array(
    'style'=>'width:100%',
    //'class'=>'',
    'placeHolder'=>'Производитель',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
        //'class'=>'col-md-9',
    ),

)); ?>

<?=
    BsHtml::formActions(array(
    BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
    ));
?>
<?php
$this->endWidget();
?>