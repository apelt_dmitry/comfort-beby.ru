<?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'menu-form',
    'enableAjaxValidation' => false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnChange' => false,
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype' => 'multipart/form-data',
    )
)); ?>

<?php echo $form->errorSummary($model); ?>

<?= $form->fileFieldControlGroup($model, 'image[]'/*, array('multiple'=>'multiple')*/); ?>

<?= BsHtml::formActions(array(
    BsHtml::resetButton('Сброс', array(
        'color' => BsHtml::BUTTON_COLOR_WARNING,
        'icon' => BsHtml::GLYPHICON_REFRESH,
    )),
    BsHtml::submitButton('Готово', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
), array('class' => 'form-actions')); ?>

<?php $this->endWidget(); ?>
