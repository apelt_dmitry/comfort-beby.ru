<script>
    $(document).ready(function(){

        delete_input();
        teh_des();
        
        $('.addPole2').click(function(){
            $(this).attr('pole_id',$('.listPole').val());
            if($(this).attr('pole_id').length!=0){
                $.ajax({
                    'url': '/admin/product/_ajax',
                    'type': 'POST',
                    'dataType': 'json',
                    'data': {
                        'pole_id': $(this).attr('pole_id')
                    },
                    'success': function(data){
                        var html = '';
                        if( data[3]==1 ){
                            html += '<div class="form-group">';
                            html += '<div class="col-lg-2"></div>';
                            html += '<label class="control-label col-lg-2" for="Category_name">'+data[1]+'</label>';
                            html += '<div class="col-lg-3">';
                            html += '<input label="'+data[1]+'" style="display:none" name="Pole_id[]" type="text" maxlength="255" value="'+data[2]+'">';
                            
                            if (data[4] == 1) 
                            {
                                html += data[5];
                            }
                            else
                            {
                                html += '<input label="'+data[1]+'" placeholder="'+data[1]+'" autocomplete="off" name="Pole[]"  class="form-control" type="text" maxlength="255">';
                            }
                            
                            html += '</div>';
                            html += '<label class="control-label col-lg-1" for="Category_name">Из. цены</label>';
                            html += '<div class="col-lg-2">';
                            html += '<input label="'+data[1]+'" placeholder="Изменение цены" autocomplete="off" name="price_change[]"  class="form-control" type="text" maxlength="255">';
                            html += '</div>';
                            html += '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
                            html += '</div>';
                        }
                        else{
                            html += '<div class="form-group">';
                            html += '<div class="col-lg-2"></div>';
                            html += '<label class="control-label col-lg-2" for="Category_name">'+data[1]+'</label>';
                            html += '<div class="col-lg-6">';
                            html += '<input label="'+data[1]+'" style="display:none" name="Pole_id[]" type="text" maxlength="255" value="'+data[2]+'">';
                            
                            if (data[4] == 1) 
                            {
                                html += data[5];
                            }
                            else
                            {
                                html += '<input label="'+data[1]+'" placeholder="'+data[1]+'" autocomplete="off" name="Pole[]"  class="form-control" type="text" maxlength="255">';
                            }
                            
                            html += '<input name="price_change[]"  class="form-control" type="hidden" maxlength="255" value="+0">';
                            
                            html += '</div>';
                            html += '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
                            html += '</div>';
                        }

                        $('.addPole').append(html);

                        delete_input();
                    }
                });
            }
            return false;
        });

        $('#link_add_pole').click(function(){
            $(this).attr('category_id',$('.listCategory').val());
            if($(this).attr('category_id').length!=0){
                $.ajax({
                    'url': '/admin/product/_ajaxCategory',
                    'type': 'POST',
                    'dataType': 'json',
                    'data': {
                        'category_id': $(this).attr('category_id')
                    },
                    'success': function(data){

                        var html = '';
                        html += '<div class="form-group">';
                        html += '<div class="col-lg-2"></div>';
                        html += '<div class="col-lg-6">';
                        html += '<input label="'+data[1]+'" style="display:none" name="Category_id[]" type="text" maxlength="255" value="'+data[2]+'">';
                        html += '<input label="'+data[1]+'" value="'+data[1]+'" autocomplete="off" name="Category[]" readonly="readonly" class="form-control" type="text" maxlength="255">';
                        html += '</div>';
                        html += '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
                        html += '</div>';

                        $('.addCategory').append(html);

                        delete_input();
                    }
                });
            }
            return false;
        });

        $('.addRoom_link').click(function(){
            $(this).attr('room_id',$('.listRoom').val());
            if($(this).attr('room_id').length!=0){
                $.ajax({
                    'url': '/admin/product/_ajaxRoom',
                    'type': 'POST',
                    'dataType': 'json',
                    'data': {
                        'room_id': $(this).attr('room_id')
                    },
                    'success': function(data){

                        var html = '';
                        html += '<div class="form-group">';
                        html += '<div class="col-lg-2"></div>';
                        html += '<div class="col-lg-6">';
                        html += '<input label="'+data[1]+'" style="display:none" name="room_id[]" type="text" maxlength="255" value="'+data[2]+'">';
                        html += '<input label="'+data[1]+'" value="'+data[1]+'" autocomplete="off" name="room[]" readonly="readonly" class="form-control" type="text" maxlength="255">';
                        html += '</div>';
                        html += '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
                        html += '</div>';

                        $('.addRoom').append(html);

                        delete_input();
                    }
                });
            }
            return false;
        });
        
        $('.addCity_link').click(function(){
            
            $(this).attr('city', $('.listCity').val());
            
            if ($(this).attr('city').length != 0) {
                
                $.ajax({
                    'url': '/admin/product/_ajaxCity',
                    'type': 'POST',
                    'dataType': 'json',
                    'data': {
                        'city': $(this).attr('city')
                    },
                    'success': function(data){
                        
                        var cityNum = $('body').find('.city_' + data[1]);
                        
                        if (cityNum.length > 0) {alert('Такой город уже есть!'); return false;}
                        
                        var html = '';
                        
                        html += '<div class="form-group city_'+data[1]+'">';
                        html += '<div class="col-lg-2"></div>';
                        html += '<label class="control-label col-lg-2">Город</label>';
                        html += '<div class="col-lg-3">';
                        html += '<input label="" style="display:none" name="City_id['+data[1]+'][city]" type="text" maxlength="255" value="'+data[1]+'">';
                        html += '<input readonly="readonly" class="form-control" type="text" maxlength="255" value="'+data[2]+'">';
                        html += '</div>';
                        html += '<label class="control-label col-lg-1">Цена</label>';
                        html += '<div class="col-lg-2">';
                        html += '<input placeholder="Цена" autocomplete="off" name="City_id['+data[1]+'][price]" class="form-control" type="text" maxlength="255">';
                        html += '</div>';
                        html += '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
                        html += '</div>';
                        
                        $('.addCity').append(html);

                        delete_input();
                    }
                });
            }
            return false;
        });

    });

    function teh_des(){
        if( $('#Product_room').val()==1 ){
            $('#teh_des').show();
        }

        $('#Product_room').change(function(){
            if( $(this).val()==1 ){
                $('#teh_des').show();
            }
            else{
                $('#teh_des').hide();
            }
        })
    }

    function delete_input(){
        $('.delete_pole').click(function(){
            $(this).parent().parent().remove();
            return false;
        })
    }

</script>

<?php if ( Yii::app()->user->getFlash('success') ): ?>
    <?= BsHtml::alert(Bshtml::ALERT_COLOR_SUCCESS, 'Информация добавлена.') ?>
<?php endif; ?>
<?php if ( Yii::app()->user->getFlash('error') ): ?>
    <?= BsHtml::alert(Bshtml::ALERT_COLOR_ERROR, 'Информация не добавлена, повторить попытку или свяжитесь с разработчиком.') ?>
<?php endif; ?>


<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>

    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldControlGroup($model, 'product_name', array(
        'style'=>'width:100%',
        'placeHolder'=>'Название товара',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>

<!--Add Category-->
<?php $SpecialPC = new ProductCategory(); ?>
<?= $form->dropDownListControlGroup($SpecialPC, 'category', $category, array(
        'empty' => '--Нет--',
        'name' => 'Category[]',
        'class' => 'listCategory'
    )
); ?>

<?php if($scenario=='update'){
    $html ='';
    foreach($ProductCategory as $val){
        $html .= '<div class="form-group">';
        $html .= '<div class="col-lg-2"></div>';
        $html .= '<div class="col-lg-6">';
        $html .= '<input label="'.$val->CategoryName->name.'" style="display:none" name="Category_id[]" type="text" maxlength="255" value="'.$val->category.'">';
        $html .= '<input label="'.$val->CategoryName->name.'" autocomplete="off" name="Category[]" readonly="readonly" class="form-control" type="text" maxlength="255" value="'.$val->CategoryName->name.'">';
        $html .= '</div>';
        $html .= '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
        $html .= '</div>';
    }
    echo $html;
}
?>

<div class="addCategory"></div>

<div class="col-lg-2"></div>

<div class="col-lg-4" style="margin-bottom: 15px">
    <a href="#" id="link_add_pole" ">Добавить в категорию</a>
</div>
<div class="clear"></div>

<!--End Add Category-->

<!--Add Pole-->

<?php
    $pole = new Pole();
    $SpecialPP = new ProductPole();
    $specialCity = new City();
?>

<?= $form->dropDownListControlGroup($SpecialPP, 'pole_id', $pole->_pole_list, array(
        'empty' => '--Нет--',
        'class' => 'listPole',
    )
); ?>

<?php if($scenario=='update'){
    $html ='';
    foreach($ProductPole as $val){
        if( $val->PoleName->price_change==1 ){
            $html .= '<div class="form-group">';
            $html .= '<div class="col-lg-2"></div>';
            $html .= '<label class="control-label col-lg-2" >'.$val->PoleName->pole_name.'</label>';
            $html .= '<div class="col-lg-3">';
            $html .= '<input label="'.$val->PoleName->pole_name.'" style="display:none" name="Pole_id[]" type="text" maxlength="255" value="'.$val->pole_id.'">';
            
            if ($val->PoleName->pole_type == 1)
            {
                $allvariants = PoleVariants::model()->findAll('id_pole=:id_pole', array(':id_pole' => $val->PoleName->id));
                
                if (isset($allvariants) && !empty($allvariants))
                {
                    $html .= '<select class="form-control" name="Pole[]">';
                    
                    foreach ($allvariants as $v)
                    {
                        $html .= '<option value="'.$v->variant.'"';
                        
                        if ($val->value == $v->variant) {$html .= ' selected="selected"';}
                        
                        $html .= '>'.$v->variant.'</option>';
                    }
                    
                    $html .= '</select>';
                }
            }
            else
            {
                $html .= '<input label="'.$val->PoleName->pole_name.'" autocomplete="off" name="Pole[]" class="form-control" type="text" maxlength="255" value="'.$val->value.'">';
            }
            
            $html .= '</div>';
            $html .= '<label class="control-label col-lg-1" for="Category_name">Из. цены</label>';
            $html .= '<div class="col-lg-2">';
            $html .= '<input label="'.$val->PoleName->pole_name.'" placeholder="Изменение цены" autocomplete="off" name="price_change[]"  class="form-control" type="text" maxlength="255" value="'.$val->price_change.'">';
            $html .= '</div>';
            $html .= '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
            $html .= '</div>';
        }
        else{
            $html .= '<div class="form-group">';
            $html .= '<div class="col-lg-2"></div>';
            $html .= '<label class="control-label col-lg-2" >'.$val->PoleName->pole_name.'</label>';
            $html .= '<div class="col-lg-6">';
            $html .= '<input label="'.$val->PoleName->pole_name.'" style="display:none" name="Pole_id[]" type="text" maxlength="255" value="'.$val->pole_id.'">';
            
            if ($val->PoleName->pole_type == 1)
            {
                $allvariants = PoleVariants::model()->findAll('id_pole=:id_pole', array(':id_pole' => $val->PoleName->id));
                
                if (isset($allvariants) && !empty($allvariants))
                {
                    $html .= '<select class="form-control" name="Pole[]">';
                    
                    foreach ($allvariants as $v)
                    {
                        $html .= '<option value="'.$v->variant.'"';
                        
                        if ($val->value == $v->variant) {$html .= ' selected="selected"';}
                        
                        $html .= '>'.$v->variant.'</option>';
                    }
                    
                    $html .= '</select>';
                }
            }
            else
            {
                $html .= '<input label="'.$val->PoleName->pole_name.'" autocomplete="off" name="Pole[]" class="form-control" type="text" maxlength="255" value="'.$val->value.'">';
            }
            
            $html .= '<input name="price_change[]"  class="form-control" type="hidden" maxlength="255" value="+0">';

            $html .= '<input  autocomplete="off" name="Pole_img[]" class="form-control" type="hidden" maxlength="255" value="'.$val->img.'">';
                        
            $html .= '</div>';
            $html .= '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
            $html .= '</div>';
        }
    }
    echo $html;
}
?>

<div class="addPole"></div>

<div class="col-lg-2"></div>

<div class="col-lg-4" style="margin-bottom: 15px">
    <a href="#" class="addPole2">Добавить поле</a>
</div>
<div class="clear"></div>

<!--End Add Pole-->
    <?= $form->dropDownListControlGroup($model, 'type_pole', array('0'=>'Пункты','1'=>'Выпадающий список')) ?>

    <?= $form->dropDownListControlGroup($model, 'room', array('0'=>'Нет','1'=>'Да')) ?>

<!--In Room-->

    <?php $inRoom = new InRoom(); ?>
    <?= $form->dropDownListControlGroup($inRoom, 'room_id', $allProduct, array(
            'empty' => '--Нет--',
            'class' => 'listRoom'
        )
    ); ?>

    <?php if($scenario=='update'){
        $html ='';
        foreach($room as $val){
            $html .= '<div class="form-group">';
            $html .= '<div class="col-lg-2"></div>';
            $html .= '<div class="col-lg-6">';
            $html .= '<input label="'.$val->RoomName->product_name.'" style="display:none" name="room_id[]" type="text" maxlength="255" value="'.$val->room_id.'">';
            $html .= '<input label="'.$val->RoomName->product_name.'" autocomplete="off" name="room[]" readonly="readonly" class="form-control" type="text" maxlength="255" value="'.$val->RoomName->product_name.'">';
            $html .= '</div>';
            $html .= '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
            $html .= '</div>';
        }
        echo $html;
    }
    ?>

<div class="addRoom"></div>

<div class="col-lg-2"></div>

<div class="col-lg-4" style="margin-bottom: 15px">
    <a href="#" class="addRoom_link">Добавить в комнату</a>
</div>
<div class="clear"></div>

<!--End In Room-->

    <?= $form->textFieldControlGroup($model, 'price', array(
        'style'=>'width:100%',
        'placeHolder'=>'Цена',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>
    
    
    <?= $form->dropDownListControlGroup($model, 'city', CHtml::listData(City::model()->findAll(), 'id_city', 'title'), array(
        'empty' => '--Нет--',
        'class' => 'listCity',
    )); ?>
    
    <?php 
    if ($scenario == 'update')
    {
        $html = '';
        
        foreach ($ProductCity as $val)
        {
            $html .= '<div class="form-group city_'.$val->id_city.'">';
            $html .= '<div class="col-lg-2"></div>';
            $html .= '<label class="control-label col-lg-2">Город</label>';
            $html .= '<div class="col-lg-3">';
            $html .= '<input label="" style="display:none" name="City_id['.$val->id_city.'][city]" type="text" maxlength="255" value="'.$val->id_city.'">';
            $html .= '<input readonly="readonly" class="form-control" type="text" maxlength="255" value="'.$val->city->title.'">';
            $html .= '</div>';
            $html .= '<label class="control-label col-lg-1">Цена</label>';
            $html .= '<div class="col-lg-2">';
            $html .= '<input placeholder="Цена" autocomplete="off" name="City_id['.$val->id_city.'][price]" value="'.$val->price.'" class="form-control" type="text" maxlength="255">';
            $html .= '</div>';
            $html .= '<div class="col-lg-2"><a href="#" class="delete_pole">Удалить</a></div>';
            $html .= '</div>';
            
        }
        
        echo $html;
    }
    ?>
    
    <div class="addCity"></div>

    <div class="col-lg-2"></div>
    
    <div class="col-lg-4" style="margin-bottom: 15px">
        <a href="#" class="addCity_link">Добавить город и цену для него</a>
    </div>
    <div class="clear"></div>
    
    
    
    
    <?php $act = new Act(); ?>
    <?= $form->dropDownListControlGroup($model, 'discount_id', $allDiscount, array('empty' => '--Нет--')); ?>
    <?= $form->dropDownListControlGroup($model, 'act_id', $act->_act, array('empty' => '--Нет--')); ?>

    <?= $form->textAreaControlGroup($model, 'description', array(
        'style'=>'width:100%',
        'placeHolder'=>'Описание товара',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>
<div id="teh_des">
    <?= $form->textAreaControlGroup($model, 'left_description', array(
        'style'=>'width:100%',
        'placeHolder'=>'Техническое описание',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>

    <?= $form->textAreaControlGroup($model, 'right_description', array(
        'style'=>'width:100%',
        'placeHolder'=>'Техническое описание',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>
</div>

    <?= $form->textFieldControlGroup($model, 'count', array(
        'style'=>'width:100%',
        'placeHolder'=>'Колличество на складе',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>

    <?= $form->textFieldControlGroup($model, 'position', array(
        'style'=>'width:100%',
        'placeHolder'=>'Позиция товара',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>
    <?php $production = new Production(); ?>
    <?= $form->dropDownListControlGroup($model, 'production_id', $production->allProduction, array('empty' => '--Нет--')); ?>

    <div class="form-group">
    <label class="control-label col-lg-2" for="Product_production_id"><?php echo $form->label($model, 'recom'); ?></label>
        <div class="col-lg-10">
            <?php echo $form->checkBox($model, 'recom'); ?>
        </div>
    </div>
    
    <div class="form-group">
    <label class="control-label col-lg-2" for="Product_production_id"><?php echo $form->label($model, 'palette'); ?></label>
        <div class="col-lg-10">
            <?php if(file_exists('./uploads/palette/'.$model->palette)):?>
            <div class="img_form">
                <img src="/uploads/palette/<?php echo $model->palette;?>" style="max-width: 100px; max-heigth: 100px;" />
            </div>
            <?php endif;?>
        
            <?php echo CHtml::FileField('palette');?>
        </div>
    </div>
    
    <?= $form->textAreaControlGroup($model, 'delivery', array(
        'style'=>'width:100%',
        'placeHolder'=>'Доставка и сборка',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>
    <!--Meta Product-->

    <hr/>
    <div class="col-lg-2"></div>
    <div class="col-lg-10" style="margin-bottom: 5px"><h2>Мета теги товара</h2>(будут взяты из названия и описания товара если не прописанны)</div>

    <?= $form->textFieldControlGroup($model, 'meta_title', array(
        'style'=>'width:100%',
        'placeHolder'=>'Мета название',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>

    <?= $form->textAreaControlGroup($model, 'meta_content', array(
        'style'=>'width:100%',
        'placeHolder'=>'Мета описание',
        'AUTOCOMPLETE'=>'off',
        'groupOptions'=>array(
        ),
    )); ?>

    <hr/>

    <!--End Meta Product-->

    <?= $form->fileFieldControlGroup($model,'image[]',array(
        'help'=>'До 12 изображений любого размера.'.( (!$model->isNewRecord)?' Выбор новых изображений удалит текущие (кол-во: '.count($model->productImage).').':'' ),
        'multiple'=>'multiple',
    )); ?>

<script>
    $(document).ready(function(){
        $('.product_image').hover(function(){
            $(this).parent().find('.delete_image').show();
        }, function(){
            $(this).parent().find('.delete_image').hide();
        });
        $('.delete_image').hover(function(){
            $(this).show();
        }, function () {
            $(this).hide();
        });
        $('.delete_image').click(function(){
            $.ajax({
                'url': '/admin/product/_ajaxDeleteImage',
                'type': 'POST',
                //'dataType': 'json',
                'data': {
                    'image_id': $(this).attr('image_id')
                },
                'success': function(data){
                    $('.delete_image[image_id="'+data+'"]').parent().hide();
                }
            });
        });
    })
</script>
<?php
if($scenario=='update'){
    $allImage = $model->productImage;
    foreach($allImage as $val){
        echo '<div class="for_image">';
        echo BsHtml::image('/uploads/product/preview/'.$val->image, '', array('class' => 'product_image'));
        echo '<div class="delete_image" image_id="'.$val->id.'">X</div>';
        echo '</div>';
    }

} ?>



    <?= BsHtml::formActions(array(
        BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
        BsHtml::linkButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
            'url' => '#',
            'onclick' => 'location.reload();return false;'
        )),
        BsHtml::linkButton('Назад', array(
            'color' => BsHtml::BUTTON_COLOR_DEFAULT,
            'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
            'url' => Yii::app()->request->urlReferrer,
        )),
    ), array('class'=>'form-actions')); ?>

<?php
$this->endWidget();
?>

