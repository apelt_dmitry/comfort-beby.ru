<style>
.page-header2 {
    border-bottom: 1px solid #eee;
    margin: 40px 0 20px;
    padding-bottom: 9px;
}
</style>
<div class="">
    <a href="/admin/product/update/id/<?php echo $_REQUEST['id'];?>">Редактировать товар&nbsp;<?php echo $_REQUEST['id'];?></a>
</div>
<div class="page-header2">
    <h1>Список полей характеристик с картинкой товара <?php echo $_REQUEST['id'];?></h1>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <!-- search-form -->
        <?php
//        echo BsHtml::ajaxButton('Добавить/Убрать', array('/admin/product/popular'),array(
//            'type'=>'Post',
//            'data'=>array(
//                'popular_product' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
//            ),
//            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
//            'dataType'=>'json',
//        )) 
        ?>
        <?php
//        echo  BsHtml::ajaxButton('Удалить', array('/admin/product/_ajaxDelete'),array(
//            'type'=>'Post',
//            'data'=>array(
//                'product_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
//            ),
//            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
//            'dataType'=>'json',
//        )) 
        ?>
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
			'id'=>'product-grid',
            'nullDisplay'=> '-',
			'dataProvider'=>$model->search_img_pole(),
			'columns'=>array(
                array(
                    'header' => 'Название поля',
                    'name' => 'pole_id',
                    'value' => '$data->PoleName->pole_name',
                    'filter' => false,
                ),
                array(
                    'header' => 'Значение',
                    'name' => 'value',
                    'filter' => false,
                ),
                array(
                    'header' => 'Изображение',
                    'type' => 'image',
                    'name' => 'img',
                    //'value' => '/uploads/pole/$data->img',
                    'value'=>'"/uploads/pole/".$data->img',
                ),

                    array(
                        'class'=>'bootstrap.widgets.BsButtonColumn',
                        'template'=>'{update} {delete}',
                        'buttons'=>array
                        (
                            'update' => array
                            (
                                'label'=>'Изменить Изображение',
                                'icon'=>false,
                                'url'=>'Yii::app()->createUrl("/admin/product/updateimgpole", array("id"=>$data->id))',
//                                'options'=>array(
//                                    'class'=>'btn btn-small btn-success',
//                                ),
                            ),
                            'delete' => array
                            (
                                'label'=>'Удалить Изображение',
                                'icon'=>false,
                                'url'=>'Yii::app()->createUrl("/admin/product/deleteimgpole", array("id"=>$data->id))',
//                                'options'=>array(
//                                    'class'=>'btn btn-small btn-success',
//                                ),
                            ),
                        
                        ),
                        
                    ),
			    ),
            )
        ); ?>
    </div>
</div>




