<?php
/* @var $this ProductController */
/* @var $model Product */

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-list','label'=>'List Product', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Product', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <!-- search-form -->
        <?= BsHtml::ajaxButton('Добавить/Убрать', array('/admin/product/popular'),array(
            'type'=>'Post',
            'data'=>array(
                'popular_product' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <?= BsHtml::ajaxButton('Удалить', array('/admin/product/_ajaxDelete'),array(
            'type'=>'Post',
            'data'=>array(
                'product_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
			'id'=>'product-grid',
            'nullDisplay'=> '-',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
            'afterAjaxUpdate' => 'function(){set_editable();}',
			'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    //'' => 2,
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                ),
                array(
                    'header'=>BsHtml::icon(BsHtml::GLYPHICON_STAR),
                    'headerHtmlOptions'=>array(
                        'data-toggle'=>'tooltip',
                        'title'=>'Популярный товар.',
                    ),
                    'type' => 'raw',
                    'value'=>'($data->popular!=null)?BsHtml::icon(BsHtml::GLYPHICON_STAR,array(\'data-toggle\'=>\'tooltip\',\'title\'=>\'Популярный товар.\')):null',
                ),
                'product_name',
                /*array(
                    'name' => 'category_id',
                    'filter'=>BsHtml::activeDropDownList($model, 'category_id', Category::model()->_list_category2),
                    'value' => '$data->productCategory->name',
                ),*/
                'categorySTR',
                array(
                    'name' => 'count',
                    'filter' => false,
                ),
                array(
                    'type' => 'raw',
                    'name' => 'price',
                    'value' => '"<span class=\"editable editable-required\" data-type=\"text\" data-name=\"price\" data-pk=\"".$data->id."\" data-url=\"".Yii::app()->controller->createUrl("_ajaxCGUpdate")."\">".$data->price."</span>"',
                    'filter' => false,
                    /*'htmlOptions' => array(
                        'class' => 'editable editable-required'
                    )
                    /*'editable' => array(    //editable section
                        //'apply'      => '$data->user_status != 4', //can't edit deleted users
                        'url'        => $this->createUrl('site/updateUser'),
                        //'placement'  => 'right',
                    )*/
                ),
                array(
                    'header' => 'Поля',
                    'value' => '($data->_productPole!="")?"$data->_productPole":"Нет"',
                    'type' => 'raw',
                ),
                array(
                    'name' => 'room',
                    'value' => '($data->room==1)?"Да":"Нет"',
                    'filter' => false,
                ),
                array(
                    'header' => 'Пр. к ком.',
                    'value' => '($data->_inRoom!="")?"$data->_inRoom":"Нет"',
                ),
                array(
                    'name' => 'act_id',
                    'value' => '$data->act->act_title',
                    'filter' => false,
                ),
                array(
                    'type' => 'raw',
                    'name' => 'position',
                    'value' => '"<span class=\"editable editable-required\" data-type=\"text\" data-name=\"position\" data-pk=\"".$data->id."\" data-url=\"".Yii::app()->controller->createUrl("_ajaxCGUpdate")."\">".$data->position."</span>"',
                    'filter' => false,
                ),
                array(
                    'name' => 'production_id',
                    'filter' => false,
                    'value' => '($data->production_id==0)?"Не указан":$data->production->name',
                ),
                array(
                    'name' => 'discountValue',
                    'filter' => false,
                ),

                    array(
                        'class'=>'bootstrap.widgets.BsButtonColumn',
                        'template'=>'{update} {view}',
                        'buttons'=>array
                        (
                            'view' => array
                            (
                                'label'=>'Изменить Катинки Полей Характеристик(Цвет)',
                                'icon'=>'plus',
                                'url'=>'Yii::app()->createUrl("/admin/product/listimgpole", array("id"=>$data->id))',
//                                'options'=>array(
//                                    'class'=>'btn btn-small btn-success',
//                                ),
                            ),
                        
                        ),
                        
                    ),
			    ),
            )
        ); ?>
    </div>
</div>




