<script>
    $(document).ready(function(){
        $('.page-header h1').text('Изменить товар')
    })
</script>
<?php $this->renderPartial('_form',array(
    'model' => $model,
    'category' => $category,
    'scenario' => $scenario = 'update',
    'allDiscount' => $allDiscount,
    'allProduct' => $allProduct,
    'ProductCategory' => $ProductCategory,
    'ProductPole' => $ProductPole,
    'room' => $room,
    'ProductCity' => $ProductCity
)); ?>