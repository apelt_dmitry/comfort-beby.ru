<style>
.page-header2 {
    border-bottom: 1px solid #eee;
    margin: 40px 0 20px;
    padding-bottom: 9px;
}
.img_form
{
    margin: 15px;
}
</style>
<div class="">
    <a href="/admin/product/update/id/<?php echo $model->product_id?>">Редактировать товар&nbsp;<?php echo $model->product_id?></a><br />
    <a href="/admin/product/listimgpole/id/<?php echo $model->product_id?>">Список полей характеристик с картинкой товара&nbsp;<?php echo $model->product_id?></a>
</div>
<div class="page-header2">
    <h1>Загрузить картинку для поля <?php echo $model->value;?></h1>
</div>
        
<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?> 
    <?php if(file_exists('/uploads/pole/'.$model->img)):?>
    <div class="img_form">
        <img src="/uploads/pole/<?php echo $model->img;?>" />
    </div>
    <?php endif;?>

    <?php echo CHtml::FileField('image');?>
    
    <br />
    
	<div class="form-actions">
    <?php echo  BsHtml::submitButton('Загрузить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => false,
        )); ?>
    </div>

<?php
$this->endWidget();
?>