<?php
/* @var $this ProductController */
/* @var $model Product */

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Product', 'url'=>array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Product', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-body">
        <?= BsHtml::ajaxButton('Обработать заказ', array('/admin/order/state'),array(
            'type'=>'Post',
            'data'=>array(
                'order_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <?= BsHtml::ajaxButton('Удалить', array('/admin/order/_ajaxDelete'),array(
            'type'=>'Post',
            'data'=>array(
                'order_id' => 'js:$.fn.yiiGridView.getSelection("product-grid")',
            ),
            'success' => 'js:function(){$.fn.yiiGridView.update("product-grid")}',
            'dataType'=>'json',
        )) ?>
        <!-- search-form -->
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
            'selectableRows'=>2,
            'id'=>'product-grid',
            'nullDisplay'=> '-',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'class'=>'CCheckBoxColumn',
                    //'' => 2,
                    'checkBoxHtmlOptions' => array('class' => 'lol'),
                ),
                'id',
                'name',
                'phone',
                'email',
                array(
                    'name' => 'region',
                    'type' => 'raw',
                    'filter' => false,
                ),
                array(
                    'name' => 'product_name',
                    'type' => 'raw',
                    'headerHtmlOptions' => array(
                        'style' => 'width:400px'
                    ),
                    'filter' => false,
                ),
                /*array(
                    'name' => 'pole',
                    'type' => 'raw',
                ),*/
                array(
                    'name' => 'state',
                    'value' => '($data->state==0)?"Не обработан":"Обработан"',
                    'filter' => false,
                ),
                array(
                    'name' => 'date',
                    'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm", $data->date)',
                    'filter' => false,

                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                    'template'=>'',
                ),
            )
        ))   ; ?>
    </div>
</div>




