<script>
    $(document).ready(function(){
        $('.delete_pole').click(function(){
            $.ajax({
                url: '/admin/contact/_ajax',
                type: 'POST',
                //dataType: 'json',
                data: {
                    phone_id: $(this).attr('phone_id')
                },
                success: function(data){
                    $('.delete_pole[phone_id="'+data+'"]').parent().parent().hide();
                }
            })
            return false;
        });
        $('#link_add_pole2').click(function(){
            generate();
            return false;
        });
        function generate(){
            var html = '<div class="form-group">';
            html += '<div class="col-lg-9">';
            html += '<input style="width:100%" placeholder="Телефон" autocomplete="off" name="phone_new[]"  class="form-control" type="text" maxlength="255">';
            html += '</div>';
            html += '<div class="col-lg-1"><a href="#" class="create_pole">Удалить</a></div>';
            html += '</div>';
            $('#add_pole').append(html);
            $('.create_pole').click(function(){
                $(this).parent().parent().remove();
                return false;
            })
        }

        $('.delete_pole_adress').click(function(){
            $.ajax({
                url: '/admin/contact/_ajax_adress',
                type: 'POST',
                //dataType: 'json',
                data: {
                    adress_id: $(this).attr('adress_id')
                },
                success: function(data){
                    $('.delete_pole_adress[adress_id="'+data+'"]').parent().parent().hide();
                }
            })
            return false;
        });
        $('#link_add_pole_adress').click(function(){
            generate2();
            return false;
        });
        function generate2(){
            var html = '<div class="form-group">';
            html += '<div class="col-lg-9">';
            html += '<input style="width:100%" placeholder="Адрес" autocomplete="off" name="adress_new[]"  class="form-control" type="text" maxlength="255">';
            html += '</div>';
            html += '<div class="col-lg-1"><a href="#" class="create_pole_adress">Удалить</a></div>';
            html += '</div>';
            $('#add_pole_adress').append(html);
            $('.create_pole_adress').click(function(){
                $(this).parent().parent().remove();
                return false;
            })
        }

        $('.delete_pole_email').click(function(){
            $.ajax({
                url: '/admin/contact/_ajax_email',
                type: 'POST',
                //dataType: 'json',
                data: {
                    email_id: $(this).attr('email_id')
                },
                success: function(data){
                    $('.delete_pole_email[email_id="'+data+'"]').parent().parent().hide();
                }
            })
            return false;
        });
        $('#link_add_pole_email').click(function(){
            generate3();
            return false;
        });
        function generate3(){
            var html = '<div class="form-group">';
            html += '<div class="col-lg-9">';
            html += '<input style="width:100%" placeholder="email" autocomplete="off" name="email_new[]"  class="form-control" type="text" maxlength="255">';
            html += '</div>';
            html += '<div class="col-lg-1"><a href="#" class="create_pole_email">Удалить</a></div>';
            html += '</div>';
            $('#add_pole_email').append(html);
            $('.create_pole_email').click(function(){
                $(this).parent().parent().remove();
                return false;
            })
        }
    })
</script>


<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<div class="row">
    <div><h2>Телефон:</h2></div>
    <?php

    $phone = Phone::model()->findAll();
    foreach($phone as $val) {
        $html = '<div class="form-group">';
        $html .= '<div class="col-lg-9">';
        $html .= '<input style="width:100%" placeholder="Телефон" value="'.$val->phone.'" autocomplete="off" name="phone['.$val->id.']"  class="form-control" type="text" maxlength="255">';
        $html .= '</div>';
        $html .= '<div class="col-lg-1"><a href="#" class="delete_pole" phone_id="'.$val->id.'">Удалить</a></div>';
        $html .= '</div>';
        echo $html;
    }
    ?>

    <div id="add_pole"></div>
    <a href="#" id="link_add_pole2" class="control-label col-lg-2 required">Добавить Телефон</a>
</div>


<div class="row">
    <div><h2>Адрес:</h2></div>
    <?php

    $adress = Adress::model()->findAll();
    foreach($adress as $val) {
        $html = '<div class="form-group">';
        $html .= '<div class="col-lg-9">';
        $html .= '<input style="width:100%" placeholder="Адрес" value="'.$val->adress.'" autocomplete="off" name="adress['.$val->id.']"  class="form-control" type="text" maxlength="255">';
        $html .= '</div>';
        $html .= '<div class="col-lg-1"><a href="#" class="delete_pole_adress" adress_id="'.$val->id.'">Удалить</a></div>';
        $html .= '</div>';
        echo $html;
    }
    ?>

    <div id="add_pole_adress"></div>
    <a href="#" id="link_add_pole_adress" class="control-label col-lg-2 required">Добавить Адрес</a>
</div>

<div class="row">
    <div><h2>E-mail:</h2></div>
    <?php

    $email= Email::model()->findAll();
    foreach($email as $val) {
        $html = '<div class="form-group">';
        $html .= '<div class="col-lg-9">';
        $html .= '<input style="width:100%" placeholder="Email" value="'.$val->email.'" autocomplete="off" name="email['.$val->id.']"  class="form-control" type="text" maxlength="255">';
        $html .= '</div>';
        $html .= '<div class="col-lg-1"><a href="#" class="delete_pole_email" email_id="'.$val->id.'">Удалить</a></div>';
        $html .= '</div>';
        echo $html;
    }
    ?>

    <div id="add_pole_email"></div>
    <a href="#" id="link_add_pole_email" class="control-label col-lg-2 required">Добавить Email</a>
</div>





<?=
BsHtml::formActions(array(
    BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
));
?>
<?php
$this->endWidget();
?>