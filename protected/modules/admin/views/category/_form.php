<script>
    $(document).ready(function(){
        $('.delete_pole').click(function(){
            $.ajax({
                url: '/admin/category/_ajax',
                type: 'POST',
                //dataType: 'json',
                data: {
                    pole_id: $(this).attr('pole_id')
                },
                success: function(data){
                    $('.delete_pole[pole_id="'+data+'"]').parent().parent().hide();
                }
            })
            return false;
        });
    })
</script>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    //'id' => 'user_form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<?= $form->errorSummary($model); ?>
<?= $form->textFieldControlGroup($model, 'name', array(
    'style'=>'width:100%',
    //'class'=>'',
    'placeHolder'=>'Название категории',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
        //'class'=>'col-md-9',
    ),

)); ?>
<?= $form->textAreaControlGroup($model, 'category_description', array(
    'style'=>'width:100%',
    //'class'=>'',
    'placeHolder'=>'Описание категории',
    'AUTOCOMPLETE'=>'off',
    'groupOptions'=>array(
        //'class'=>'col-md-9',
    ),

)); ?>
<?= $form->dropDownListControlGroup($model, 'category_id', $parent_category, array(
        'empty' => '--Нет--'
    ));

?>
<?php 
if( $scenario == 'update' ): 
	if( $valCat = PublishCategory::model()->findByPk($model->id) ):
?>
		<div class="form-group">
			<label class="control-label col-lg-2 required" for="Category_name">Позиция категории</label>
			<div class="col-lg-10"><input style="width:100%" placeholder="Позиция категории" autocomplete="off" name="position" id="Category_position" class="form-control" type="text" maxlength="255" value="<?= $valCat->position;?>"></div>
		</div>
	<?php endif; ?>
<?php endif; ?>

    <?= $form->dropDownListControlGroup($model, 'global_discount', $allDiscount, array('empty' => '--Нет--')); ?>

    <?= $form->fileFieldControlGroup($model,'_image',array(
        'help'=>'До 1 изображения любого размера.'.( (!$model->isNewRecord)?' Выбор нового изображения удалит текущие':'' ),
        'multiple'=>'multiple',
    )); ?>

	
	
    <?= BsHtml::formActions(array(
        BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
        BsHtml::resetButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
        )),
        BsHtml::linkButton('Назад', array(
            'color' => BsHtml::BUTTON_COLOR_DEFAULT,
            'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
            'url' => Yii::app()->request->urlReferrer,
        )),
    ), array('class'=>'form-actions')); ?>


<?php
$this->endWidget();
?>