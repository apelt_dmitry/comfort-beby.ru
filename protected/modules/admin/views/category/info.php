
<script>
    $(document).ready(function() {
        $( ".sortable" ).sortable({
                connectWith: ".connectedSortable",
                dropOnEmpty: true,
                change: function( event, ui ) {
                  console.log(ui);
                }
            }
        ).disableSelection();
       // $( ".sortable" ).disableSelection();
    });
</script>

<div class="col-md-2">
    <div style="margin-bottom: 10px">
        <?=BsHtml::linkButton('Добавить категорию', array(
            'icon' => BsHtml::GLYPHICON_PLUS,
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'size' => BsHtml::BUTTON_SIZE_MINI,
            'url' => array('/admin/category/create'),)); ?>
    </div>
    <?= $result_categories ?>
</div>
<div class="col-md-8">
    <?php
        if($model!==null) {
            $this->widget('BsDetailView', array(
                'nullDisplay'=> '-',
                'data'=>$model,
                'attributes'=>array(
                    'name',
                    'category_description',
                    array(
                        'name' => 'category_id',
                        'value' => ($model->category_id!=0)?$model->parentCategory->name: '-',
                    ),
                    array(
                        'name' => 'global_discount',
                        'value' => ($model->all_discount->discount!=0)?$model->all_discount->discount: '-',
                    ),
                    array(
                        'label' => 'Кол-во товаров',
                        'value' => ($model->productCount!=0)?$model->_productCategory: $model->_subsidiaryCategoryProductCount,
                    ),
                    array(
                        'label' => 'Опубликовано',
                        'value' => ($model->publish!=null)?'Да':'Нет',
                    ),
					array(
                        'label' => 'Позиция',
                        'value' => ($model->publish->position),
                    ),
                ),
            ));
            echo BsHtml::linkButton('Изменить', array(
                'icon' => BsHtml::GLYPHICON_PENCIL,
                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                'url' => array('/admin/category/update','id'=>$model->id),
                //'target'=>'_blank',
                //'block' => true,
            ));
            echo BsHtml::linkButton('Опуб./Не опуб.', array(
                'icon' => BsHtml::GLYPHICON_LIST_ALT,
                'color' => BsHtml::BUTTON_COLOR_WARNING,
                'url' => array('/admin/category/publish','id'=>$model->id),
                'style' => 'margin-left:10px',
                //'target'=>'_blank',
                //'block' => true,
            ));
            echo BsHtml::linkButton('Удалить', array(
                'color' => BsHtml::BUTTON_COLOR_DANGER,
                'icon' => BsHtml::GLYPHICON_REMOVE_SIGN,
                'url' => array('/admin/category/delete','id'=>$model->id),
                'style' => 'margin-left:10px',
                "onclick"=>"if ( !confirm('Действительно удалить?') ) return false;",
            ));
        }
    ?>
</div>



