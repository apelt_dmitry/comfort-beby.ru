<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'htmlOptions' => array(
        'class' => 'bs-example',
    )
));
?>
<div class="row">

<?php

$model = Meta::model()->findAll();
foreach($model as $val){

    $html .= '<div style="margin-left: 80px"><h3>'.$val->name.'</h3></div>';

    $html .= '<div class="col-lg-2"></div>';
    $html .= '<div class="col-lg-10">';
    $html .= '<div>Заголовок</div>';
    $html .= '<div class="form-group">';
    $html .= '<div class="col-lg-9">';
    $html .= '<input style="width:100%" placeholder="Заголовок" value="'.$val->title.'" autocomplete="off" name="meta_title['.$val->id.']"  class="form-control" type="text" maxlength="255">';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';

    $html .= '<div class="col-lg-2"></div>';
    $html .= '<div class="col-lg-10">';
    $html .= '<div>Описание</div>';
    $html .= '<div class="form-group">';
    $html .= '<div class="col-lg-9">';
    $html .= '<input style="width:100%" placeholder="Описание" value="'.$val->content.'" autocomplete="off" name="meta_content['.$val->id.']"  class="form-control" type="textarea">';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';

}
echo $html;
?>

<?=
BsHtml::formActions(array(
    BsHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array(
        'color' => BsHtml::BUTTON_COLOR_SUCCESS,
        'icon' => BsHtml::GLYPHICON_OK,
    )),
    BsHtml::linkButton('Назад', array(
        'color' => BsHtml::BUTTON_COLOR_DEFAULT,
        'icon' => BsHtml::GLYPHICON_CIRCLE_ARROW_LEFT,
        'url' => Yii::app()->request->urlReferrer,
    )),
));
?>
<?php
$this->endWidget();
?>