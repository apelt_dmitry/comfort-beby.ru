<?php
Class BannerController extends AdminController
{
    public function actionIndex()
    {
        $model = Banner::model()->findByPk(1);

        if(isset($_POST['Banner'])){
            $model->attributes =  $_POST['Banner'];
            if($model->save()){
                Yii::app()->user->setFlash('success', true);
            }
        }

        $this->render('index',array(
            'model'=>$model,
        ));
    }
}