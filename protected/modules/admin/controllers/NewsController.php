<?php
Class NewsController extends AdminController
{

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['News']))
        {
            $model->attributes=$_POST['News'];
			if($model->meta_title == ''){
				$model->meta_title = $model->title;
			}
            $model->_image=CUploadedFile::getInstance($model,'_image');
            $file = $model->_image;
            if($model->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $model->image = $imageName;
                }
                if($model->save(false)){
                    Yii::app()->user->setFlash('success', true);

                };
            }
            $this->redirect(array('/admin/news/info'));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionCreate()
    {
        $model = new News();
        if ( isset($_POST['News']) ) {
            $model->attributes = $_POST['News'];
			if($model->meta_title == ''){
				$model->meta_title = $model->title;
			}
            $model->_image=CUploadedFile::getInstance($model,'_image');
            $file = $model->_image;
            if($model->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $model->image = $imageName;
                }
                if($model->save(false)){
                    Yii::app()->user->setFlash('success', true);

                };
            }
            $this->redirect('/admin/news/info');
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }


    public function actionInfo()
    {
        $model=new News('search');
        $model->unsetAttributes();
        if(isset($_GET['News']))
            $model->attributes=$_GET['News'];
        $this->render('info',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=News::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            PublishNews::model()->deleteByPk($id);
            ProductDetails::model()->deleteAllByAttributes(array(
                'id' => $id,
            ));
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }


    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function action_ajaxDelete()
    {
        News::model()->deleteByPk($_POST['news_id']);
        PublishNews::model()->deleteByPk($_POST['news_id']);
        echo json_encode('');
    }

    public function actionPublish()
    {

        if( isset($_POST['news_publish']) ){
            foreach($_POST['news_publish'] as $val){
                if(!PublishNews::model()->findByPk($val)){
                    $model = new PublishNews();
                    $model->news_id = $val;
                    $model->save();
                }
                else{
                    PublishNews::model()->deleteByPk($val);
                }
            }
        }
        echo json_encode($_POST['popular_product']);
    }

}