<?php

Class CategoryController extends AdminController
{

    public $categories = array();
    public $result_categories = '';
    public $parent_category = array();

    public function actionUpdate($id)
    {
        $this->parent_category();
        $allDiscount = Discount::model()->_discount;
        $model = $this->loadModel($id);
        if ( isset($_POST['Category']) ) {
            $model->attributes = $_POST['Category'];

            if( $_POST['Category']['global_discount']!='' ){
                $pr = ProductCategory::model()->findAllByAttributes(array('category'=>$model->id));
                foreach($pr as $val){
                    Product::model()->updateByPk($val->Product->id,array('discount_id'=>$_POST['Category']['global_discount']));
                }
            }
            else{
                $pr = ProductCategory::model()->findAllByAttributes(array('category'=>$model->id));
                foreach($pr as $val){
                    Product::model()->updateByPk($val->Product->id,array('discount_id'=>null));
                }
            }

            $model->_image=CUploadedFile::getInstance($model,'_image');
            $file = $model->_image;
            if($model->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $model->image = $imageName;
                }
                if($model->save(false)){
                    Yii::app()->user->setFlash('success', true);

                };
            }
			if( isset($_POST['position']) ){
				$pcp = PublishCategory::model()->findByPK($id);
				$pcp->position = $_POST['position'];
				$pcp->save(false);
			}
            $this->redirect('/admin/category/info');
        }
        $this->render('update', array(
			'scenario' => 'update',
            'model' => $model,
            'parent_category' => $this->parent_category,
            'allDiscount' => $allDiscount,
        ));
    }
    public function actionCreate()
    {
        $this->parent_category();
        $allDiscount = Discount::model()->_discount;
        $model = new Category();
        if ( isset($_POST['Category']) ) {
            $model->attributes = $_POST['Category'];

            if( $_POST['Category']['global_discount']!='' ){
                $pr = ProductCategory::model()->findAllByAttributes(array('category'=>$model->id));
                foreach($pr as $val){
                    Product::model()->updateByPk($val->Product->id,array('discount_id'=>$_POST['Category']['global_discount']));
                }
            }

            $model->_image=CUploadedFile::getInstance($model,'_image');
            $file = $model->_image;
            if($model->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $model->image = $imageName;
                }
                if($model->save(false)){
                    Yii::app()->user->setFlash('success', true);

                };
            }
            $this->redirect('/admin/category/info');
        }

        $this->render('create', array(
            'model' => $model,
            'parent_category' => $this->parent_category,
            'allDiscount' => $allDiscount,
        ));

    }
    public function actionInfo()
    {
        $model = Category::model()->findAll();
        foreach($model as $val){
            $this->categories[$val->category_id][] = array(
                'name' => $val->name,
                'id' => $val->id,
                'category_id' => $val->category_id,
            );
        }

        $model = null;
        if( isset($_GET['id']) ){
            $model = $this->loadModel($_GET['id']);
        }
        $this->outTree(0);
        $this->render('info', array(
            'result_categories' => $this->result_categories,
            'model' => $model,
        ));
    }

     public function outTree($parent_id) {
        $category_arr = $this->categories; //Делаем переменную $category_arr видимой в функции
        if (isset($category_arr[$parent_id])) { //Если категория с таким parent_id существует
            $this->result_categories .= '<ul class="sortable connectedSortable">';
            foreach ($category_arr[$parent_id] as $value) {
                $this->result_categories .= '<li><a href="/admin/category/info?id='.$value["id"].'">' . $value["name"] . '</a></li>';
                $this->outTree($value["id"]);
            }
            $this->result_categories .= "</ul>";
        }
     }
    public function actionPublish($id){
        if($model = PublishCategory::model()->findByPK($id)){
            $model->delete();
        }
        else{
            $model = new PublishCategory();
            $model->publish_id = $id;
        }
        $model->save();
        $this->redirect('/admin/category/info?id='.$id);
    }
    public function parent_category(){
        $category = Category::model()->findAll();
        foreach($category as $val){
            $this->parent_category[$val->id] = $val->name;
        }
        return $this->parent_category;
    }

    public function loadModel($id)
    {
        $model=Category::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    public function action_ajax()
    {
        ProductCategory::model()->deleteAllByAttributes(array(
            'category' => $_POST['pole_id']));
        PublishCategory::model()->deleteByPk((int)$_POST['pole_id']);
        echo $_POST['pole_id'];
    }

    public function actionDelete($id)
    {
        if( isset($_GET['id']) ){
            $modelCategory = new Category();
            ProductCategory::model()->deleteAllByAttributes(array(
                'category' => $_POST['pole_id']));
            PublishCategory::model()->deleteByPk((int)$_POST['pole_id']);

            $modelCategory->deleteByPk($id);
            $modelCategory->updateAll(array('category_id'=>0),'category_id=:id', array(':id' => $id));

            PublishCategory::model()->deleteByPk($id);

            $this->redirect('/admin/category/info');
        }
        else
        {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }

    }
}