<?php

class CityController extends AdminController
{
    public $defaultAction = 'admin';
    
	public function actionCreate()
	{
		$model = new City;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['City']))
		{
			$model->attributes = $_POST['City'];
            
            if ($model->validate())
            {
                if ($model->save())
                {
                    Yii::app()->user->setFlash('success', true);
                }
            }
            
            $this->redirect('/admin/city/admin');
		}

		$this->render('create', array(
            'model' => $model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['City']))
		{
			$model->attributes = $_POST['City'];
            
			if ($model->save())
            {
                Yii::app()->user->setFlash('success', true);
            }
            
            $this->redirect('/admin/city/admin');
		}

		$this->render('update',array(
			'model' => $model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionAdmin()
	{
		$model = new City('search');
		$model->unsetAttributes();  // clear any default values
        
		if (isset($_GET['City']))
        {
            $model->attributes = $_GET['City'];
        }
		
        if (isset($_POST['defcity']))
        {
            $up_old_default_city = City::model()->find('type=1');
            
            if (isset($up_old_default_city->id_city))
            {
                $up_old_default_city->type = 0;
                $up_old_default_city->update();
            }
            
            if ($_POST['defcity'] != 0)
            {
                $up_new_default_city = City::model()->findByPk($_POST['defcity']);
                $up_new_default_city->type = 1;
                $up_new_default_city->update();
            }
        }
        
		$this->render('admin',array(
			'model' => $model,
		));
	}

	public function loadModel($id)
	{
		$model=City::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='city-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
    public function action_ajaxDelete()
    {
        City::model()->deleteByPk($_POST['id']);

        echo json_encode('OK');
    }
}