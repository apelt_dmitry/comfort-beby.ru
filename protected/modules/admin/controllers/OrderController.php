<?php
Class OrderController extends AdminController
{
    public function actionInfo()
    {
        $model=new Order('search');
        $model->unsetAttributes();
        if(isset($_GET['Order']))
            $model->attributes=$_GET['Order'];
        $this->render('info',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Order::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }


    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function action_ajaxDelete()
    {
        Order::model()->deleteByPk($_POST['order_id']);

        echo json_encode('');
    }

    public function actionState()
    {

        $model = Order::model()->findByPk($_POST['order_id']);
        $model->state = 1;
        $model->save();

        echo json_encode($_POST['order_id']);
    }

}