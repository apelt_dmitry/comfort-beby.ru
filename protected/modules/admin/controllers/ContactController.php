<?php
Class ContactController extends AdminController
{

    public function actionCreate()
    {

        if ( isset($_POST['phone_new']) && count($_POST['phone_new'])>0 ) {
            foreach($_POST['phone_new'] as $val){
                $phone = new Phone();
                $phone->phone = $val;
                $phone->save();
            }
        }
        if ( isset($_POST['phone']) && count($_POST['phone'])>0 ){
            foreach($_POST['phone'] as $key => $val){
                Phone::model()->updateByPk($key, array('phone'=>$val));
            }
        }
        if ( isset($_POST['adress_new']) && count($_POST['adress_new'])>0 ) {
            foreach($_POST['adress_new'] as $val){
                $phone = new Adress();
                $phone->adress = $val;
                $phone->save();
            }
        }
        if ( isset($_POST['adress']) && count($_POST['adress'])>0 ){
            foreach($_POST['adress'] as $key => $val){
                Adress::model()->updateByPk($key, array('adress'=>$val));
            }
        }
        if ( isset($_POST['email_new']) && count($_POST['email_new'])>0) {
            foreach($_POST['email_new'] as $val){
                $phone = new Email();
                $phone->email = $val;
                $phone->save();
            }
        }
        if ( isset($_POST['email']) && count($_POST['email'])>0 ){
            foreach($_POST['email'] as $key => $val){
                Email::model()->updateByPk($key, array('email'=>$val));
            }
        }

        $this->render('contact',array());
    }

    public function action_ajax()
    {
        Phone::model()->deleteByPk($_POST['phone_id']);
        echo $_POST['phone_id'];
    }

    public function action_ajax_adress()
    {
        Adress::model()->deleteByPk($_POST['adress_id']);
        echo $_POST['adress_id'];
    }

    public function action_ajax_email()
    {
        Email::model()->deleteByPk($_POST['email_id']);
        echo $_POST['email_id'];
    }
}