<?php
Class ConfigController extends AdminController
{

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['ConfigMain']))
        {
            $model->attributes=$_POST['ConfigMain'];
            if($model->save()){
                Yii::app()->user->setFlash('success', true);
            };
            $this->redirect(array('/admin/config/info'));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionInfo()
    {
        $model=new ConfigMain('search');
        $model->unsetAttributes();
        if(isset($_GET['ConfigMain']))
            $model->attributes=$_GET['ConfigMain'];
        $this->render('info',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=ConfigMain::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCapha(){
        $model = Capha::model()->findByPk(1);
        if( isset($_POST['Capha']) ){
            $model->attributes = $_POST['Capha'];
            if( $model->save() ){
                Yii::app()->user->setFlash('success', true);
            }
        }
        $this->render('capha', array(
            'model' => $model,
        ));
    }
}