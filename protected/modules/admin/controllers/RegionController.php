<?php
Class RegionController extends AdminController
{

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['Region'])) {
            $model->attributes=$_POST['Region'];
            if($model->save()){
                Yii::app()->user->setFlash('success', true);
                $this->redirect(array('/admin/region/info'));
            }
        }
        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionCreate()
    {
        $model = new Region();
        if(isset($_POST['Region'])) {
            $model->attributes=$_POST['Region'];
            if($model->save()){
                Yii::app()->user->setFlash('success', true);
                $this->redirect(array('/admin/region/info'));
            }
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }


    public function actionInfo()
    {
        $model=new Region('search');
        $model->unsetAttributes();
        if(isset($_GET['Region']))
            $model->attributes=$_GET['Region'];
        $this->render('info',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Region::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }


    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}