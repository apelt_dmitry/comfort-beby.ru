<?php

class PromoController extends AdminController
{
	public $defaultAction = 'admin';

	public function actionCreate()
	{
		$model = new Promo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Promo']))
		{
			$model->attributes = $_POST['Promo'];
			
            if ($model->save())
			{
                Yii::app()->user->setFlash('success', true);
            }
            
            $this->redirect('/admin/promo/admin');
		}

		$this->render('create',array(
            'model' => $model,
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Promo']))
		{
			$model->attributes = $_POST['Promo'];
            
			if ($model->save())
			{
                Yii::app()->user->setFlash('success', true);
            }
            
            $this->redirect('/admin/promo/admin');
		}

		$this->render('update',array(
			'model' => $model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionAdmin()
	{
		$model=new Promo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Promo']))
			$model->attributes=$_GET['Promo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Promo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='promo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
    public function action_ajaxDelete()
    {
        Promo::model()->deleteByPk($_POST['id']);

        echo json_encode('OK');
    }
}