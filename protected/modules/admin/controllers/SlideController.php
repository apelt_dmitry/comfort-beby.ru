<?php

class SlideController extends AdminController
{


    public function actionCreate()
    {
        $model = new Slide();
        if ( isset($_POST['Slide'])) {
            $model->attributes = $_POST['Slide'];
            $model->image = CUploadedFile::getInstances($model, 'image');
            if($model->validate()){

                if (count($model->image) != 0) {
                    foreach ($model->image as $file) {
                        if ($file->name != '') {
                            $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                            $imageName = substr(md5($file->name . microtime()), 0, 27) . '.' . $imageExtention;
                            $image = Yii::app()->image->load($file->tempName);
//                            $image->resize(self::$image_w, self::$image_h);
                            $image->save('./uploads/Slide/' . $imageName);
                            $image->resize(300,100);
                            $image->save('./uploads/Slide/preview/' . $imageName);
                            $model->preview = $imageName;
                        }
                        break;
                    }
                }

                if($model->save(FALSE)){

                    $this->redirect(array('index'));
                }
            }
        }
        
        $this->render('create', array(
            'model'=>$model,
        ));
    }


    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        if ( isset($_POST['Slide'])) {
            $model->attributes = $_POST['Slide'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/Slide/'.$imageName);
                    $image->resize(300, 100);
                    $image->save('./uploads/Slide/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){

                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Slide('search');
        $model->unsetAttributes();
        if ( isset($_GET['Slide']) ) {
            $model->attributes=$_GET['Slide'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Slide::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
