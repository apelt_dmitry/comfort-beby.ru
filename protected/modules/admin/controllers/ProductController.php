<?php

Class ProductController extends AdminController
{
    public function actionCreate()
    {
        $model = new Product();
        $allProduct = Product::model()->_list_product_room;
        $allDiscount = Discount::model()->_discount;
        $category = Category::model()->_list_category;
        $ProductCategory = new ProductCategory();

        if ( isset($_POST['Product']) ) {
            
            $model->attributes = $_POST['Product'];
            if( $_POST['Product']['meta_title']=='' ){
                $model->meta_title = $model->product_name;
            }
            if( $_POST['Product']['position']=='' ){
                $model->position = 1;
            }
            if( $_POST['Product']['meta_content']=='' ){
                $model->meta_content = $model->description;
            }
            $model->image=CUploadedFile::getInstances($model,'image');
            
            if($model->save())
            {
                
                //saving imgage
                $img = Yii::app()->imagemod->load($_FILES['palette']);
                if ($img->uploaded) 
                {
//                    $img->image_resize          = true;
//                    $img->image_ratio_crop      = true;
//                    $img->image_y               = 46;
//                    $img->image_x               = 46;
                    
                    $img->file_new_name_body = $model->id;
                    $img->file_new_name_ext = $img->file_src_name_ext;
                    $full_name = $img->file_new_name_body.".".$img->file_new_name_ext;
                    
        
                    @unlink('./uploads/palette/'.$full_name); 
        
                    
                    $img->process("./uploads/palette");
                    
                    if ($img->processed)
                    {
                        $model->palette = $full_name;
                        $model->save(false);
                        $img->clean();
        
                    } 
                    else
                    {
                        echo 'error : ' . $img->error;
                    }
                }
                //saving photo end 
                
                

                if(isset($_POST['Category_id'])){
                    foreach($_POST['Category_id'] as $val){
                        $PC = new ProductCategory();
                        $PC->category = $val;
                        $PC->product = $model->id;
                        $PC->save();
                    }
                }

                if( isset($_POST['Pole']) ){
                    foreach($_POST['Pole'] as $key => $val){
                        $ProductPole = new ProductPole();
                        $ProductPole->product_id = $model->id;
                        $ProductPole->pole_id = $_POST['Pole_id'][$key];
                        $ProductPole->value = $val;
                        if( isset($_POST['price_change']) ){
                            $ProductPole->price_change = $_POST['price_change'][$key];
                        }
                        $ProductPole->save();
                    }
                }

                if( isset($_POST['room_id']) ){
                    foreach($_POST['room_id'] as $val){
                        $inRoom = new InRoom();
                        $inRoom->room_id = $val;
                        $inRoom->product_id = $model->id;
                        $inRoom->save();
                    }
                }
                
                // Добавляем к товару цены для других городов
                if (isset($_POST['City_id']) && !empty($_POST['City_id']))
                {
                    foreach ($_POST['City_id'] as $a_city)
                    {
                        $new_city = new ProductCity();
                        $new_city->id_product = $model->id;
                        $new_city->id_city = $a_city['city'];
                        
                        if (isset($a_city['price']) && !empty($a_city['price']) && $a_city['price'] > 0)
                        {
                            $new_city->price = $a_city['price'];
                        }
                        else
                        {
                            $new_city->price = 0;
                        }
                        
                        $new_city->save();
                    }
                }

                $i = 1;
                $image_id = array();
                Yii::app()->user->setFlash('success', true);
                foreach($model->image as $file) {
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/product/'.$imageName);
                        $image->resize(256, 256);
                        $image->save('./uploads/product/preview/'.$imageName);
                        $productImage = new ProductImage();
                        $productImage->product_id = $model->id;
                        $productImage->image = $imageName;
                        $productImage->save();
                        $image_id[$i] = $productImage->id;
                        $i++;
                    }
                }
                foreach($image_id as $val){
                    $productImage = ProductImage::model()->findByPk($val);
                    $stamp = imagecreatefrompng('images/water_mark.png');
                    $im = imagecreatefromjpeg('uploads/product/'.$productImage->image);
                    $imageSize = getimagesize('uploads/product/'.$productImage->image);
                    //print_r($image_id);

                   for( $j = 1; $j<=floor($imageSize[0]/221)+2; $j++ ){
                        $marge_right = 230*($j-1);
                        for( $k = 1; $k<=floor($imageSize[1]/191)+2; $k++ ){
                            $marge_bottom = 170*($k-1);
                            $sx = imagesx($stamp);
                            $sy = imagesy($stamp);
                            imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
                        }
                    }
// Установка полей для штампа и получение высоты/ширины штампа
                    //$marge_right = 10;
                    //$marge_bottom = 10;
                    //$sx = imagesx($stamp);
                    //$sy = imagesy($stamp);
// Копирование изображения штампа на фотографию с помощью смещения края
// и ширины фотографии для расчета позиционирования штампа.
                    //imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
// Вывод и освобождение памяти
                    //header('Content-type: image/png');
                    imagepng($im, 'uploads/product/'.$productImage->image);
                    imagedestroy($im);

                }

                //Yii:app()->end();


            };



            $this->redirect('/admin/product/info');
        }
        $this->render('create',array(
            'ProductCategory' => $ProductCategory,
            'allProduct' => $allProduct,
            'model' => $model,
            'category' => $category,
            'allDiscount' => $allDiscount,
        ));
    }

    public function actionUpdate($id)
    {
        $model = Product::model()->findByPk($id);
        $allProduct = Product::model()->_list_product_room;
        $allDiscount = Discount::model()->_discount;
        $category = Category::model()->_list_category;
        $ProductCategory = ProductCategory::model()->findAllByAttributes(array('product'=>$id));
        $ProductPole = ProductPole::model()->findAllByAttributes(array('product_id'=>$id));
        $ProductCity = ProductCity::model()->findAllByAttributes(array('id_product' => $id));
        $room = InRoom::model()->findAllByAttributes(array('product_id'=>$id));

        if ( isset($_POST['Product']) ) {
            ProductPole::model()->deleteAllByAttributes(array('product_id'=>$id));
            ProductCategory::model()->deleteAllByAttributes(array('product'=>$id));
            InRoom::model()->deleteAllByAttributes(array('product_id'=>$id));
            ProductCity::model()->deleteAllByAttributes(array('id_product' => $id));
            $model->attributes = $_POST['Product'];
            if( $_POST['Product']['meta_title']=='' ){
                $model->meta_title = $model->product_name;
            }
            if( $_POST['Product']['meta_content']=='' ){
                $model->meta_content = $model->description;
            }
            if( $_POST['Product']['position']=='' ){
                $model->position = $model->id;
            }
            $model->image=CUploadedFile::getInstances($model,'image');
            if($model->save()){
                
                //saving imgage
                $img = Yii::app()->imagemod->load($_FILES['palette']);
                if ($img->uploaded) 
                {
//                    $img->image_resize          = true;
//                    $img->image_ratio_crop      = true;
//                    $img->image_y               = 46;
//                    $img->image_x               = 46;
                    
                    $img->file_new_name_body = $model->id;
                    $img->file_new_name_ext = $img->file_src_name_ext;
                    $full_name = $img->file_new_name_body.".".$img->file_new_name_ext;
                    
        
                    @unlink('./uploads/palette/'.$full_name); 
        
                    
                    $img->process("./uploads/palette");
                    
                    if ($img->processed)
                    {
                        $model->palette = $full_name;
                        $model->save(false);
                        $img->clean();
        
                    } 
                    else
                    {
                        echo 'error : ' . $img->error;
                    }
                }
                //saving photo end 

                if(isset($_POST['Category_id'])){
                    foreach($_POST['Category_id'] as $val){
                        $PC = new ProductCategory();
                        $PC->category = $val;
                        $PC->product = $model->id;
                        $PC->save();
                    }
                }

                if( isset($_POST['Pole']) ){
                    foreach($_POST['Pole'] as $key => $val){
                        $ProductPole = new ProductPole();
                        if( isset($_POST['price_change']) ){
                            $ProductPole->price_change = $_POST['price_change'][$key];
                        }
                        $ProductPole->product_id = $model->id;
                        $ProductPole->pole_id = $_POST['Pole_id'][$key];
                        $ProductPole->img = $_POST['Pole_img'][$key];
                        $ProductPole->value = $val;
                        $ProductPole->save();
                    }
                }

                if( isset($_POST['room_id']) ){
                    foreach($_POST['room_id'] as $val){
                        $inRoom = new InRoom();
                        $inRoom->room_id = $val;
                        $inRoom->product_id = $model->id;
                        $inRoom->save();
                    }
                }
                
                // Добавляем к товару цены для других городов
                if (isset($_POST['City_id']) && !empty($_POST['City_id']))
                {
                    foreach ($_POST['City_id'] as $a_city)
                    {
                        $new_city = new ProductCity();
                        $new_city->id_product = $model->id;
                        $new_city->id_city = $a_city['city'];
                        
                        if (isset($a_city['price']) && !empty($a_city['price']) && $a_city['price'] > 0)
                        {
                            $new_city->price = $a_city['price'];
                        }
                        else
                        {
                            $new_city->price = 0;
                        }
                        
                        $new_city->save();
                    }
                }

                $i = 1;
                $image_id = array();
                Yii::app()->user->setFlash('success', true);
                foreach($model->image as $file) {
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/product/'.$imageName);
                        $image->resize(256, 256);
                        $image->save('./uploads/product/preview/'.$imageName);
                        $productImage = new ProductImage();
                        $productImage->product_id = $model->id;
                        $productImage->image = $imageName;
                        $productImage->save();
                        $image_id[$i] = $productImage->id;
                        $i++;
                    }
                }
                foreach($image_id as $val){
                    $productImage = ProductImage::model()->findByPk($val);
                    $stamp = imagecreatefrompng('images/water_mark.png');
                    $im = imagecreatefromjpeg('uploads/product/'.$productImage->image);
                    $imageSize = getimagesize('uploads/product/'.$productImage->image);
                    //print_r($image_id);

                    for( $j = 1; $j<=floor($imageSize[0]/221)+2; $j++ ){
                        $marge_right = 230*($j-1);
                        for( $k = 1; $k<=floor($imageSize[1]/191)+2; $k++ ){
                            $marge_bottom = 170*($k-1);
                            $sx = imagesx($stamp);
                            $sy = imagesy($stamp);
                            imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
                        }
                    }
// Установка полей для штампа и получение высоты/ширины штампа
                    //$marge_right = 10;
                    //$marge_bottom = 10;
                    //$sx = imagesx($stamp);
                    //$sy = imagesy($stamp);
// Копирование изображения штампа на фотографию с помощью смещения края
// и ширины фотографии для расчета позиционирования штампа.
                    //imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
// Вывод и освобождение памяти
                    //header('Content-type: image/png');
                    imagepng($im, 'uploads/product/'.$productImage->image);
                    imagedestroy($im);
                }

                //Yii:app()->end();

            };


            $this->redirect('/admin/product/info');
        }
        $this->render('update',array(
            'room' => $room,
            'ProductCategory' => $ProductCategory,
            'ProductPole' => $ProductPole,
            'allProduct' => $allProduct,
            'model' => $model,
            'category' => $category,
            'allDiscount' => $allDiscount,
            'ProductCity' => $ProductCity
        ));
    }

    public function action_ajaxDelete()
    {
        Product::model()->deleteByPk($_POST['product_id']);
        Popular::model()->deleteByPk($_POST['product_id']);
        ProductCategory::model()->deleteAllByAttributes(array(
            'product' => $_POST['product_id'],
        ));
        ProductImage::model()->deleteAllByAttributes(array(
            'product_id' => $_POST['product_id'],
        ));
        ProductPole::model()->deleteAllByAttributes(array(
            'product_id' => $_POST['product_id'],
        ));
        InRoom::model()->deleteAllByAttributes(array(
            'product_id' => $_POST['product_id'],
        ));
        echo json_encode('OK');
    }

    public function actionInfo()
    {
        SearchCategory::model()->deleteAll();
        $addModel = Product::model()->findAll();
        foreach($addModel as $val){
            $searchModel = new SearchCategory();
            $searchModel->id = $val->id;
            $searchModel->category = $val->_myStr;
            $searchModel->save();
        }

        $model=new Product('search');
        $model->unsetAttributes();
        if(isset($_GET['Product']))
            $model->attributes=$_GET['Product'];
        $this->render('info',array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Product::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function action_ajax()
    {
        $result = array();
        $a = Pole::model()->findByPk($_POST['pole_id']);
        $result[1] = $a->pole_name;
        $result[2] = $a->id;
        
        if ($a->price_change == 1) {$result[3] = $a->price_change;}
        
        $result[4] = $a->pole_type;
        
        if ($a->pole_type == 1) 
        {
            $variants = PoleVariants::model()->findAll('id_pole=:id_pole', array(':id_pole' => $a->id));
            
            if (isset($variants) && !empty($variants))
            {
                $select = '<select class="form-control" name="Pole[]">';
                
                foreach ($variants as $v)
                {
                    $select .= '<option value="'.$v->variant.'">'.$v->variant.'</option>';
                }
                
                $select .= '</select>';
            }
            
            $result[5] = $select;
        }
        
        echo json_encode($result);
    }

    public function action_ajaxCategory()
    {
        $result = array();
        $a = Category::model()->findByPk($_POST['category_id']);
        $result[1] = $a->name;
        $result[2] = $a->id;
        echo json_encode($result);
    }

    public function action_ajaxRoom()
    {
        $result = array();
        $a = Product::model()->findByPk($_POST['room_id']);
        $result[1] = $a->product_name;
        $result[2] = $a->id;
        echo json_encode($result);
    }
    
    public function action_ajaxCity()
    {
        $result = array();
        $a = City::model()->findByPk($_POST['city']);
        $result[1] = $a->id_city;
        $result[2] = $a->title;
        
        echo json_encode($result);
    }

    public function actionPopular()
    {

        if( isset($_POST['popular_product']) ){
            foreach($_POST['popular_product'] as $val){
                if(!Popular::model()->findByPk($val)){
                    $model = new Popular();
                    $model->product_id = $val;
                    $model->save();
                }
                else{
                    Popular::model()->deleteByPk($val);
                }
            }
        }
        echo json_encode($_POST['popular_product']);
    }

    public function action_ajaxCGUpdate(){
        Product::model()->updateByPk($_POST['pk'], array(
            $_POST['name'] => $_POST['value'],
        ));

        echo 'ok';
    }

    public function action_ajaxDeleteImage(){
        ProductImage::model()->deleteByPk($_POST['image_id']);
        echo $_POST['image_id'];
    }
    
    public function actionListImgPole()
    {
        //var_dump($_REQUEST);
        $_GET['ProductPole']['product_id'] = $_REQUEST['id'];
        
        $model=new ProductPole('search');
        $model->unsetAttributes();
        if(isset($_GET['ProductPole']))
            $model->attributes=$_GET['ProductPole'];
        $this->render('list_img_pole',array(
            'model'=>$model,
        ));
    }
    
    public function actionUpdateImgPole()
    {
     //  var_dump($_REQUEST['ProductPole']); exit;
        $id = $_REQUEST['id'];
        
        $model = ProductPole::model()->findByPk($id);
        
        if(isset($_REQUEST['ProductPole']))
        {
             $model->attributes=$_REQUEST['ProductPole'];           
        }

        
        //saving imgage
        $img = Yii::app()->imagemod->load($_FILES['image']);
        if ($img->uploaded) 
        {
            $img->image_resize          = true;
            $img->image_ratio_crop      = true;
            $img->image_y               = 46;
            $img->image_x               = 46;
            
            $img->file_new_name_body = $model->id;
            $img->file_new_name_ext = $img->file_src_name_ext;
            $full_name = $img->file_new_name_body.".".$img->file_new_name_ext;
            

            @unlink('./uploads/pole/'.$full_name); 

            
            $img->process("./uploads/pole");
            
            if ($img->processed)
            {
                $model->img = $full_name;
                $model->save(false);
                Yii::app()->user->setFlash('success_img', true);
                $img->clean();
                $this->redirect('/admin/product/listimgpole/id/'.$model->product_id);
            } 
            else
            {
                echo 'error : ' . $img->error;
            }
        }
        //saving photo end 
       
       
        if ($model === null)
        {
            throw new CHttpException(404, 'The requested page does not exist.');            
        }
            
        $this->render('update_img_pole',array(
            'model'=>$model,
        ));
        
    }
    
    public function actionDeleteImgPole()
    {
        if(isset($_REQUEST['id']))
        {
            $id = $_REQUEST['id'];
            $model = ProductPole::model()->findByPk($id);
            @unlink('./uploads/pole/'.$model->img); 
            $model->img = NULL;
            $model->save(false);
        }
        $this->redirect('/admin/product/listimgpole/id/'.$model->product_id);
        
    }
}