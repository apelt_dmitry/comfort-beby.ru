<?php
Class CommentController extends AdminController
{

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['Comment']))
        {
            $model->attributes=$_POST['Comment'];
            $model->save();
            $this->redirect(array('/admin/Comment/info'));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionCreate()
    {
        $model = new Comment();
        if ( isset($_POST['Comment']) ) {
            $model->attributes = $_POST['Comment'];
            $model->save();
            $this->redirect('/admin/comment/info');
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }


    public function actionInfo()
    {
        $model=new Comment('search');
        $model->unsetAttributes();
        if(isset($_GET['Comment']))
            $model->attributes=$_GET['Comment'];

        $inter_image = ImageComment::model()->findByPk(1);
        if(isset($_POST['ImageComment']))
        {
            $inter_image->attributes = $_POST['ImageComment'];
            $inter_image->_image=CUploadedFile::getInstance($inter_image,'_image');
            $file = $inter_image->_image;
            if($inter_image->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $inter_image->image = $imageName;
                }
                if($inter_image->save(false)){
                    Yii::app()->user->setFlash('success', true);
                };
            }
        }

        $this->render('info',array(
            'image' => $inter_image,
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model=Comment::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();
            ProductDetails::model()->deleteAllByAttributes(array(
                'id' => $id,
            ));
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }


    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function action_Ajax()
    {
        $a = PoleCategory::model()->findAllByAttributes(array('category_id' => $_POST['category_id']));
        $result = array();
        foreach($a as $val){
            $result[$val->id] = $val->name;
        }
        echo json_encode($result);
    }


    public function actionPublish_comment(){
        if( isset($_POST['comment_id']) ){
            $model = UserComment::model()->findByPk($_POST['comment_id']);

            if( $model->state==0 ){
                $model->state = 1;
            }
            else{
                $model->state =0;
            }
            $model->save();
        }
        echo json_encode('OK');
    }

    public function actionDelete_comment(){
        if( isset($_POST['comment_id']) ){
            UserComment::model()->deleteByPk($_POST['comment_id']);
        }
        echo json_encode('OK');
    }


    public function actionUserComment(){

        $model=new UserComment('search');
        $model->unsetAttributes();
        if(isset($_GET['UserComment']))
            $model->attributes=$_GET['UserComment'];

        $this->render('userComment', array(
            'model' => $model,
        ));
    }

    public function actionIcon_list(){

        $model=new IconComment('search');
        $model->unsetAttributes();
        if(isset($_GET['IconComment']))
            $model->attributes=$_GET['IconComment'];

        $this->render('iconList', array(
            'model' => $model,
        ));
    }

    public function actionDelete_icon(){
        if( isset($_POST['icon_id']) ){
            IconComment::model()->deleteByPk($_POST['icon_id']);
        }
        echo json_encode('OK');
    }

    public function actionAdd_icon(){
        $model = new IconComment();

        if( isset($_POST['IconComment']) ){

            $model->image=CUploadedFile::getInstances($model,'image');

            foreach($model->image as $file) {
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->resize(77, 77);
                    $image->save('./uploads/icon/preview/'.$imageName);
                    $icon = new IconComment();
                    $icon->icon = $imageName;
                    $icon->save();
                }

            }
            $this->redirect('/admin/comment/icon_list');
        }

        $this->render('addIcon', array(
            'model' => $model,
        ));
    }

    public function actionView_icon(){
        if( isset($_POST['icon_id']) ){
            $model = IconComment::model()->findByPk($_POST['icon_id']);
            if( $model->icon_user == 0 ){
                $model->icon_user = 1;
            }
            else{
                $model->icon_user = 0;
            }
            $model->save();
        }
        echo json_encode('OK');
    }

}