<?php

Class PoleController extends AdminController
{

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        
        $allVariants = PoleVariants::model()->findAll('id_pole=:id_pole', array(':id_pole' => $id));

        if (isset($_POST['Pole'])) 
        {
            $model->attributes = $_POST['Pole'];

            if ($model->save()) 
            {
                if (isset($_POST['Variant']))
                {
                    PoleVariants::model()->deleteAllByAttributes(array('id_pole' => $id));
                    
                    foreach($_POST['Variant'] as $val)
                    {
                        $val = trim($val);
                        
                        if (!empty($val))
                        {
                            $variant = new PoleVariants();
                            $variant->id_pole = $model->id;
                            $variant->variant = $val;
                            $variant->save();
                        }
                    }
                }
                
                Yii::app()->user->setFlash('success', true);
            };

            $this->redirect(array('/admin/pole/info'));
        }

        $this->render('update', array(
            'model' => $model,
            'allVariants' => $allVariants
        ));
    }

    public function actionCreate()
    {
        $model = new Pole();
        
        if (isset($_POST['Pole'])) 
        {
            $model->attributes = $_POST['Pole'];

            if ($model->save()) 
            {
                if (isset($_POST['Variant']))
                {
                    foreach($_POST['Variant'] as $val)
                    {
                        $val = trim($val);
                        
                        if (!empty($val))
                        {
                            $variant = new PoleVariants();
                            $variant->id_pole = $model->id;
                            $variant->variant = $val;
                            $variant->save();
                        }
                    }
                }
                
                Yii::app()->user->setFlash('success', true);
            }

            $this->redirect('/admin/pole/info');
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionInfo()
    {
        $model = new Pole('search');
        $model->unsetAttributes();
        if (isset($_GET['Pole']))
            $model->attributes = $_GET['Pole'];
        $this->render('info', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Pole::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function action_ajaxDelete()
    {
        Pole::model()->deleteByPk($_POST['news_id']);

        echo json_encode('OK');
    }
}