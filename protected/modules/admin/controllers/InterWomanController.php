<?php
Class InterWomanController extends AdminController
{

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $allProduct = Product::model()->_list_product;
        if(isset($_POST['InterWoman']))
        {
            $model->attributes=$_POST['InterWoman'];
                if($model->save()){
                    Yii::app()->user->setFlash('success', true);

                };
            $this->redirect(array('/admin/interWoman/info'));
        }

        $this->render('update',array(
            'allProduct' => $allProduct,
            'model'=>$model,
        ));
    }

    public function actionInfo()
    {
        $model=new InterWoman();
        $model->unsetAttributes();
        if(isset($_GET['InterWoman']))
            $model->attributes=$_GET['InterWoman'];

        $inter_image = InterImage::model()->findByPk(1);
        if(isset($_POST['InterImage']))
        {
            $inter_image->_image=CUploadedFile::getInstance($inter_image,'_image');
            $file = $inter_image->_image;
            if($inter_image->validate()){
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/product/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/product/preview/'.$imageName);
                    $inter_image->image = $imageName;
                }
                if($inter_image->save(false)){
                    Yii::app()->user->setFlash('success', true);
                };
            }
        }

        $this->render('info',array(
            'image' => $inter_image,
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model=InterWoman::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}