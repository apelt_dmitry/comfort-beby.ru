<?php
Class MetaController extends AdminController
{

    public function actionIndex()
    {
        if ( isset($_POST['meta_title']) && count($_POST['meta_title'])>0 ) {
            foreach($_POST['meta_title'] as $key=>$val){
                $model = Meta::model()->findByPk($key);
                $model->title = $val;
                if($model->save()){
                    Yii::app()->user->setFlash('success', true);
                }
            }
        }

        if ( isset($_POST['meta_content']) && count($_POST['meta_content'])>0 ) {
            foreach($_POST['meta_content'] as $key=>$val){
                $model = Meta::model()->findByPk($key);
                $model->content = $val;
                if($model->save()){
                    Yii::app()->user->setFlash('success', true);
                }
            }
        }

        $this->render('index',array());
    }
}