<?php

/**
 * This is the model class for table "connection".
 *
 * The followings are the available columns in table 'connection':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $data_create
 */
class Connection extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'connection';
	}

	/**
	 * @return array validation rules for model attributes.
	 */

    public $verifyCode;

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email, phone', 'required'),
			array('data_create, phone', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('email', 'length', 'max'=>64),
            array('email', 'email'),
//            array(
//                'verifyCode',
//                'captcha',
//                // авторизованным пользователям код можно не вводить
//                //'allowEmpty'=>!CCaptcha::checkRequirements(),
//            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, phone, email, data_create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Ваше имя',
			'phone' => 'Ваш телефон',
			'email' => 'e-mail',
			'data_create' => 'Data Create',
            'verifyCode' => 'Код проверки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('data_create',$this->data_create);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Connection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function goContact()
    {
        $email = Email::model()->find();
        include_once "libmail.php";
        $m = new Mail;
        $m->From('shop@comfort-beby.ru'.';u0068489@server114.hosting.reg.ru');
        $m->To(array($email->email));
        $m->Subject('Заказ звонка');
        $m->Body('Отправитель: '.$this->name.'. '.$this->phone.'. '.$this->email);
        $m->Send();

        $u = new Mail;
        $u->From('shop@comfort-beby.ru'.';u0068489@server114.hosting.reg.ru');
        $u->To(array($this->email));
        $u->Subject('Заказ звонка');
        $u->Body('Ваш запрос принят. Наш менеджер свяжется с вами в ближайшее время');
        $u->Send();
    }
}
