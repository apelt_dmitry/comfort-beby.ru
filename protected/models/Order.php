<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $region
 * @property integer $date
 * @property integer $state
 * @property string $product_name
 * @property integer $count
 * @property double $price
 */
class Order extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone, date, product_name', 'required'),
			array('date, state, id_city, id_promo', 'numerical', 'integerOnly'=>true),
			array('name, phone', 'length', 'max'=>32),
            array('product_id, pole', 'length', 'max'=>255),
			array('email, region', 'length', 'max'=>255),
            array('product_name', 'length', 'max'=>10000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, phone, email, comment, region, date, state, product_name, count, price, product_id, pole, id_city, id_promo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Номер заказа',
			'name' => 'Имя заказчика',
			'phone' => 'Телефон',
			'email' => 'Email',
			'comment' => 'Комментарий',
			'region' => 'Регион',
			'date' => 'Дата заказа',
			'state' => 'Состояние',
			'product_name' => 'Продукт * Колличество * Общая стоимость * Хар. товара',
            'pole' => 'Характеристики товара',
            'product_id' => 'product_id',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = '`id` DESC';
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('date',$this->date);
		$criteria->compare('state',$this->state);
		$criteria->compare('product_name',$this->product_name,true);
        $criteria->compare('product_id',$this->product_id,true);
        $criteria->compare('pole',$this->pole,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function goContact()
    {
        $email = Email::model()->find();
        include_once "libmail.php";


        $m = new Mail;
        $m->From('shop@comfort-beby.ru'.';u0068489@server114.hosting.reg.ru');

        $m->To(array($email->email));

        $m->Subject('Заказ покупки');
        /*$m->Body('Отправитель: '
            .$this->name.'. Телефон:'
            .$this->phone.'. E-mail:'
            .$this->email.'. Регион:'
            .$this->region.'. '
            .$this->product_name.'. '
        );*/
        //$m->Body('Поступил заказ на покупку. Зайдите в панель администратора что-бы посмотреть детали заказа.');
        $m->Body('
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <title>Заказ покупки</title>
                    </head>
                    <body>
                        <div>Поступил заказ на покупку.</div>
						<div>Имя заказчика: '.$this->name.'</div>
                        <div>Email заказчика: '.$this->email.'</div>
						<div>Телефон: '.$this->phone.'</div>
						<div>Номер заказа: '.$this->id.'</div></br></hr>
						<div>Информация о заказе: '.$this->product_name.'</div>
                    </body>
                </html>
                ', 'html');
        $m->Send();


        $u = new Mail;
        $u->From('shop@comfort-beby.ru'.';u0068489@server114.hosting.reg.ru');
        $u->To(array($this->email));
        $u->Subject('Заказ покупки');
        $phone = Phone::model()->findAll();
        $phoneAll = '';
        foreach($phone as $val){
            $phoneAll .= '<div>'.$val->phone.'</div>';
        }
        $u->Body('
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <title>Заказ покупки</title>
                    </head>
                    <body>
						<div>Здраствуйте, '.$this->name.'</div>
						<div>Это сообщение автоматически создано в ответ на покупку оформленную на сайте <a href="http://comfort-beby.ru/" target="_blank">comfort-beby.ru</a></div>
						<div>Вашему заказу присвоен номер: '.$this->id.'</div>
						<div>Ваш контактный номер телефона: '.$this->phone.'</div></br></hr>
                        <div>Пожалуйста, проверьте информацию о Вашем заказе.'.$this->product_name.'</div></br></hr>
                        <div>С Уважением, мебельный магазин Комфорт.<div>
                        <div>Телефон:</div>
                        '.$phoneAll.'
                        <div>Наш e-mail: shop@comfort-beby.ru</div>
                        <div>Посетите наш сайт: <a href="http://comfort-beby.ru/" target="_blank">comfort-beby.ru</a></div>
                    </body>
                </html>
                ', 'html');

        $u->Send();
    }
}
