<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property integer $z_index
 * @property integer $category_id
 */
class Category extends CActiveRecord
{
    public $_image;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name','required'),
			array('z_index, category_id, room, global_discount', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
            array('image', 'length', 'max'=>255),
            array('category_description', 'length', 'max'=>5000),
            array('_image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,room, name, z_index, global_discount, category_id, image, category_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'parentCategory'=>array(self::BELONGS_TO, 'Category', 'category_id'),
            'productCount'=>array(self::STAT, 'ProductCategory', 'category'),
            'publish' => array(self::BELONGS_TO, 'PublishCategory', 'id'),
            'all_discount' => array(self::BELONGS_TO, 'Discount', 'global_discount'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название категории',
			'z_index' => 'Z Index',
			'category_id' => 'Родительская категория',
            'poleCategory',
            'image' => 'Изображение',
            '_image' => 'Изображение',
            'room' => 'Комната',
            'global_discount' => 'Скидка для категории',
            'category_description' => 'Описание категории',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('z_index',$this->z_index);
		$criteria->compare('category_id',$this->category_id);
        $criteria->compare('image',$this->image);
        $criteria->compare('room',$this->room);
        $criteria->compare('global_discount',$this->global_discount);
        $criteria->compare('category_description',$this->category_description);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function get_list_category()
    {
        $all_category = Category::model()->findAll();
        
        foreach ($all_category as $val)
        {
            $category[$val->id] = $val->name;
        }
        
        return $category;
    }

    public function get_productCategory(){
        return $this->productCount;
    }

    public function get_subsidiaryCategoryProductCount(){
        $model = $this->findAllByAttributes(array(
            'category_id' => $this->id
        ));
        $result = null;
        foreach($model as $val){
            $result = $result + $val->productCount;
        }
        return $result;
    }
    
    public static function getImgById($id)
    {
        $cat_arr = array(
            '16' => 'furniture_child_bed.png',
            '4' => 'furniture_bed_top.png',
            '7' => 'furniture_bed_top.png',
            '5' => 'furniture_bed_car.png',
            '1' => 'furniture_child_room.png',
            '6' => 'furniture_child_sofas.png',
            '10' => 'furniture_mattress.png',
            '27' => 'furniture_20.png',
        
        );
        if(array_key_exists($id, $cat_arr))
        {
            return $cat_arr[$id];
        }
        else
        {
            return 'furniture_module.png';
        }
         
    }
}