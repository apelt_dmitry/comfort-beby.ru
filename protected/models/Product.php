<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $category_id
 * @property integer $count
 */
class Product extends CActiveRecord
{
    public $image;
    
    public $city;
    
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_name, price', 'required'),
			array('count, price, production_id, discount_id, room, position, act_id, type_pole, recom', 'numerical', 'integerOnly'=>true),
			array('product_name', 'length', 'max'=>32),
            array('meta_title', 'length', 'max'=>64),
            array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'maxFiles'=>12),
            array('description, meta_content, left_description, right_description', 'length', 'max'=>5000),
            array('delivery, palette', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('city, id, name, production_id, room, description, count, price, type_pole, act_id, categoryName, categorySTR, discount_id, discountValue, left_description, right_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'productImage' => array(self::HAS_MANY, 'ProductImage', 'product_id'),
            'mainImage' => array(self::HAS_ONE, 'ProductImage', 'product_id'),
            'productDetails' => array(self::HAS_MANY, 'ProductDetails', 'product_id'),
            'productDetailsOne' => array(self::HAS_ONE, 'ProductDetails', 'product_id'),
            'productDiscount' => array(self::BELONGS_TO, 'Discount', 'discount_id'),
            'InRoom' => array(self::HAS_MANY, 'InRoom', 'product_id'),
            'popular'=>array(self::HAS_ONE, 'Popular','product_id'),
            'allCategory'=>array(self::BELONGS_TO, 'SearchCategory', 'id'),
            'aC' => array(self::HAS_MANY, 'ProductCategory', 'product'),
            'oneProductCategory' => array(self::HAS_ONE, 'ProductCategory', 'product'),
            'ProductPole' => array(self::HAS_MANY, 'ProductPole', 'product_id'),
            'ProductOnePole' => array(self::HAS_ONE, 'ProductPole', 'product_id'),
            //'thisCategoryId' => array(self::)
            'act' => array(self::BELONGS_TO, 'Act', 'act_id'),
            'production' => array(self::BELONGS_TO, 'Production', 'production_id'),
            
            'productCityPrice' => array(self::HAS_MANY, 'ProductCity', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'room'=>'Комната',
			'product_name' => 'Название товара',
			'description' => 'Описание',
			'count' => 'На складе',
            'price' => 'Цена (Москва)',
            'image[]' => 'Изображение',
            '_productDetails' => 'Поля',
            'discount_id' => 'Скидка',
            'discountValue' => 'Скидка',
            'categorySTR' => 'Категория',
            'meta_content' => 'Мета описание',
            'meta_title' => 'Мета название',
            'position' => 'Позиция товара',
            'act_id' => 'Акция',
            'type_pole' => 'Тип поля в товаре',
            'left_description' => 'Техническое описание слева',
            'right_description' => 'Техническое описание справа',
            'production_id' => 'Производитель',
            'city' => 'Добавить цену для других городов',
            'recom' => 'Рекомендуем товар',
            'palette' => 'Цветовая палитра',
            'delivery' => 'Доставка и сборка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
    private $myStr;
    public function get_myStr(){
        $model = $this->aC;
        foreach($model as $val){
            $this->myStr .= $val->CategoryName->name.'; ';
        }
        return $this->myStr;
    }

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('meta_title',$this->meta_title,true);
        $criteria->compare('meta_content',$this->meta_content,true);
		$criteria->compare('count',$this->count);
        $criteria->compare('price',$this->price);
        $criteria->compare('discount',$this->discount_id);
        $criteria->compare('room',$this->room);
        $criteria->compare('position',$this->position);
        $criteria->compare('type_pole',$this->type_pole);
        $criteria->compare('left_description',$this->left_description);
        $criteria->compare('right_description',$this->right_description);




        $criteria->with = array(
            'productDiscount',
            'allCategory',
        );
        //$criteria->with = 'productDiscount';
        $criteria->compare('productDiscount.discount', $this->discountValue, true);
        $criteria->compare('allCategory.category', $this->categorySTR, true);


        $dataProvider = new CActiveDataProvider($this, array(
            'sort' => array(
                'attributes' => array(
                    '*',
                    'categorySTR' => array(
                        'asc' => 'allCategory.category ASC',
                        'desc' => 'allCategory.category DESC',
                    ),
                    'discountValue' => array(
                        'asc' => 'productDiscount.discount ASC',
                        'desc' => 'productDiscount.discount DESC',
                    ),
                ),
            ),
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));

        $dataProvider->sort->defaultOrder = '`t`.`id` DESC';
        //$dataProvider->sort->defaultOrder = '`t`.`name` DESC';

        return $dataProvider;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function get_productDetails(){
        $productDetails = $this->productDetails;
        $html ='';
        foreach($productDetails as $val){
            $html .= '<div style="width: 150px;"><strong>'.$val->poleCategory->name.'</strong>: '.$val->value.'</div>';
        }
        if($html==null){
            $html='-';
        }
        return $html;
    }

    private  $_discountValue = null;
    public function getDiscountValue(){
        if ($this->_discountValue === null && $this->productDiscount !== null) {
            $this->_discountValue = $this->productDiscount->discount;
        }
        return $this->_discountValue;
    }

    public function setDiscountValue($value){
        $this->_discountValue = $value;
    }

    private  $_categorySTR = null;
    public function getCategorySTR(){
        if ($this->_categorySTR === null && $this->allCategory !== null) {
            $this->_categorySTR = $this->allCategory->category;
        }
        return $this->_categorySTR;
    }

    public function setCategorySTR($value){
        $this->_categorySTR = $value;
    }

    public function get_list_product(){
        $all_Product = Product::model()->findAll();
        foreach($all_Product as $val){
            $result[$val->id] = $val->product_name;
        }
        return $result;
    }

    public function get_list_product_room(){
        $all_Product = Product::model()->findAllByAttributes(array(
            'room'=>'1',
        ));
        foreach($all_Product as $val){
            $result[$val->id] = $val->product_name;
        }
        return $result;
    }

    public function get_inRoom(){
        $model = $this->InRoom;
        $result = '';
        foreach($model as $val){
            $result .= $val->RoomName->product_name.'; ';
        }
        return $result;
    }

    public function get_productPole(){
        $result = '';
        $model = $this->ProductPole;
        foreach($model as $val){
            $result .= $val->PoleName->pole_name.': '.$val->value.'. <br/>';
        }
        return $result;
    }

    public function  get_onePole(){

        $pole = $this->ProductOnePole;
        if( $pole->PoleName->pole_name!=null ){
            $result = $pole->PoleName->pole_name.': '.$pole->value;
        }
        else{
            $result = '&nbsp';
        }
        return $result;
    }
}
