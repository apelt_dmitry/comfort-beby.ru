<?php

/**
 * This is the model class for table "icon_comment".
 *
 * The followings are the available columns in table 'icon_comment':
 * @property integer $id
 * @property string $icon
 */
class IconComment extends CActiveRecord
{
    public $image;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'icon_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('icon', 'length', 'max'=>32),
            array('icon_user', 'numerical', 'integerOnly'=>true),
            array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'maxFiles'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, icon, icon_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'image[]' => 'Изображение',
			'id' => 'ID',
			'icon' => 'Изображение иконки',
            'icon_user' => 'Кому видна',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('icon',$this->icon,true);
        $criteria->compare('icon_user',$this->icon_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IconComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function get_image(){
        return BsHtml::image('/uploads/icon/preview/'.$this->icon, '', array('style'=>'width:77px;height:77px'));
    }
}
