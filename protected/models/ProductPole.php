<?php

/**
 * This is the model class for table "ProductPole".
 *
 * The followings are the available columns in table 'ProductPole':
 * @property integer $id
 * @property integer $product_id
 * @property integer $pole_id
 * @property string $value
 */
class ProductPole extends CActiveRecord
{
    
    public $image;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ProductPole';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, pole_id', 'numerical', 'integerOnly'=>true),
			array('value, price_change', 'length', 'max'=>255),
            array('image, img', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, pole_id, value, price_change', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'PoleName' => array(self::BELONGS_TO, 'Pole', 'pole_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Продукт',
			'pole_id' => 'Поля',
			'value' => 'Value',
            'price_change' => 'Изменение цены'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('pole_id',$this->pole_id);
		$criteria->compare('value',$this->value,true);
        $criteria->compare('price_change',$this->price_change,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    public function search_img_pole()
    {
        $mdl_arr_pole_img = Pole::model()->findAllByAttributes(array('pole_type'=>2));
        $mdl_arr_pole_img_ids = CHtml::listData($mdl_arr_pole_img, 'id', 'id');
        
		$criteria=new CDbCriteria;
		$criteria->compare('product_id',$this->product_id);
        $criteria->addInCondition('pole_id', $mdl_arr_pole_img_ids, 'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));        
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductPole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
