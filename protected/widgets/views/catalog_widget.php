<div class="main_catalog_left" id="main_catalog_menu_left">
    <div class="catalog_furniture">Каталог мебели</div>
    <ul class="left_catalog">
        <li>
            <img src="/images/furniture_20.png"/>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 27)); ?>"></a>
            <span>Разпродажа! -20%</span>
        </li>
        <li>
            <img src="/images/furniture_hit.png"/></a>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 32)); ?>"></a>
            <span>Хиты продаж</span>
        </li>
        <li>
            <img src="/images/furniture_new.png"/>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 33)); ?>"></a>
            <span>Новинки</span></li>
        <li>
            <img src="/images/furniture_module.png">
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 34)); ?>"></a>
            <span>Модульная мебель</span></li>
        </li>
        <li>
            <img src="/images/furniture_child_room.png"/>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 1)); ?>"></a>
            <span>Детские комнаты</span>

            <div class="inside">
                <div class="furniture_category_box">
                    <?php foreach ($pod_category[1] as $key => $val): ?>
                        <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => $key)); ?>">
                            <span><?php echo $val; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
                <img src="/images/furniture_img_child_room.png"/>
            </div>
        </li>
        <li>
            <img src="/images/furniture_child_bed.png">
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 16)); ?>"></a>
            <span>Детские кровати</span>
            <div class="inside">
                <div class="furniture_category_box">
                    <?php foreach ($pod_category[16] as $key => $val): ?>
                        <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => $key)); ?>">
                            <span><?php echo $val; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
                <img src="/images/furniture_img_child_bed.png"/>
            </div>
        </li>
        <li>
            <img src="/images/furniture_bed_top.png">
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 4)); ?>"></a>
            <span>Кровати чердаки</span>
            <div class="inside">
                <div class="furniture_category_box">
                    <?php foreach ($pod_category[4] as $key => $val): ?>
                        <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => $key)); ?>">
                            <span><?php echo $val; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
                <img src="/images/furniture_img_bad_top.png"/>
            </div>
        </li>
        <li>
            <img src="/images/furniture_2_bed.png">
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 7)); ?>"></a>
            <span>2 ярусные кровати</span>
            <div class="inside">
                <div class="furniture_category_box">
                    <?php foreach ($pod_category[7] as $key => $val): ?>
                        <a href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => $key)); ?>">
                            <span><?php echo $val; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
                <img src="/images/furniture_img_2_bed.png"/>
            </div>
        </li>
        <li>
            <img src="/images/furniture_bed_car.png"/>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 5)); ?>"></a>
            <span>Кровать машина</span>
        </li>
        <li>
            <img src="/images/furniture_mattress.png"/>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 10)); ?>"></a>
            <span>Матрасы</span>
        </li>
        <li>
            <img src="/images/furniture_child_sofas.png"/>
            <a class="catalog_menu_link"
               href="<?php echo Yii::app()->createUrl('/site/catalog', array('id' => 6)); ?>"></a>
            <span>Детские диваны</span>
        </li>
    </ul>
</div>