<?php

/**
 * Левое меню
 */
class CatalogWidget extends CWidget
{
    public $wid_view = 'catalog_widget';

    public function run()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'category_id= 0';
        $criteria->order = 't.id DESC';
        $base_category = Category::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $criteria->condition = 'category_id!= 0';
        //$criteria->order = 't.id DESC';
        $pod_category = Category::model()->findAll($criteria);
        $pod_category = CHtml::listData($pod_category, 'id', 'name', 'category_id');

        //echo $this->viewPath;
        $this->render($this->wid_view,
            array(
                'base_category' => $base_category,
                'pod_category' => $pod_category,
            ));
    }
}