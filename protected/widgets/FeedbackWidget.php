<?php

/**
 * Обратная связь
 */
class FeedbackWidget extends CWidget
{
    public function run()
    {
        if (Yii::app()->user->isGuest) {
            $model = new FeedbackGuestForm();
        } else {
            $model = new FeedbackUserForm();
        }

        if (isset($_POST['FeedbackGuestForm']) || isset($_POST['FeedbackUserForm'])) {
            if (Yii::app()->user->isGuest) {
                $model->attributes = $_POST['FeedbackGuestForm'];
            } else {
                $model->attributes = $_POST['FeedbackUserForm'];
            }

            if ($model->validate()) {
                if (Yii::app()->user->isGuest) {
                    $request = ApiRequests::sendRequest('RegisterTicket', array(
                        'channel' => 'S',
                        'email' => $model->email,
                        'theme_id' => $model->theme,
                        'message' => $model->message,
                        'fio' => $model->name
                    ));
                } else {
                    $request = ApiRequests::sendRequest('RegisterTicket', array(
                        'token' => Yii::app()->user->id,
                        'channel' => 'S',
                        'theme_id' => $model->theme,
                        'message' => $model->message
                    ));
                }

                if (isset($request['error']) && isset($request['result']) && $request['error'] == 1 && $request['result'] == 1) {
                    ApiRequests::UpdateStatusApiRequest($request['id_api_requests'], 1);
                    Yii::app()->user->setFlash('success', 'Ваш вопрос успешно отправлен.');
                } else {
                    ApiRequests::UpdateStatusApiRequest($request['id_api_requests'], 2);
                    Yii::app()->user->setFlash('success', 'Что-то пошло не так, повторите попытку позже.');
                }

                $this->owner->refresh();
            }
        }

        $this->render('index', array(
            'model' => $model
        ));
    }
}