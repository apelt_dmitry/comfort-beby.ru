$(document).ready(function(){
    click_plus();
    size();
    setTimeout(function(){
        $('.plus_room').css('width', parseFloat($('.plus_room').css('width'))*($(window).width())/1920);
    },400)
	
	$('#hide_slides').hide();
	
})


function click_plus(){

    $('.plus_room').click(function(){
        $('.description_text').html('');
        $('.price').html('');
        $.ajax({
            'url': '/site/_ajax',
            'type': 'POST',
            'dataType': 'json',
            'data': {
                'plus_id': $(this).attr('id'),
                'scenario': $('.name_room').attr('scenario')
            },
            'success': function(data){
                $('.description_text').html(data[2]);
                $('.price').html(data[3]);
                $('.size').html(data[4]);
                $('.basket_href').attr('href','/site/productDetail/'+data[5]+'')
            }
        });

        if(parseFloat($(this).css('left'))/$(window).width()>0.5){
            $('.st').css({
                'float':'right',
                'background':'url("/images/st_right.png") center no-repeat'
            });
            $('.view').css('left',parseFloat($(this).css('left'))-310)
            $('.cross').css('left','266px');
        }
        else{
            $('.st').css({
                'float':'left',
                'background':'url("/images/st_left.png") center no-repeat'
            });
            $('.view').css('left',parseFloat($(this).css('left'))+40)
            $('.cross').css('left','286px');
        }

        if(parseFloat($(this).css('top'))/parseFloat($('.size_room').css('height'))>0.5){
            $('.view').css('top',parseFloat($(this).css('top'))-300);
            $('.st').css('margin-top',295);
        }
        else{
            $('.view').css('top',parseFloat($(this).css('top'))-30);
            $('.st').css('margin-top',25);
        }

        $('.view').show();

    })

    $('.cross').click(function(){
        $(this).parent('.view').hide();
    })


}

function size(){

    $('.size_room').css({
        'width':$(window).width(),
        'height':$(window).width()*895/1920
    });

    $('.size_room').parent('.my_container').css({
        'width':$(window).width(),
        'padding': 0,
        'margin': '40px auto 78px auto'
    })
}