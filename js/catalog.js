$(document).ready(function(){
    var font = [];
    var margin_top = [];
    var margin_bottom = [];
    //$('body').css('width',$(window).width())
    $('body *').each(function(){
        font.push($(this).css('font-size'));
        margin_bottom.push($(this).css('margin-bottom'));
        margin_top.push($(this).css('margin-top'));
    })
    size(font,margin_bottom,margin_top);
    setTimeout(function () {
        size(font,margin_bottom,margin_top);
    }, 20);

    $('#loader-wrapper').animate({
        opacity: 0
    },1000,null,function(){
        $(this).hide();
    });

    //$('.my_row').css('height',$(window).height()*895/1920);

    $('.room1').click(function(){
        if($(this).hasClass('after_animate')){
            return false;
        }

        $('#room_man').show();
        $('#room_man').css('height',($(window).width())*895/1920);

        $(this).addClass('after_animate');

        $('.man .description_rooms').hide();
        $('.man .after_rooms').hide();
        $('.man .rectangle').hide()
        $(".room2").css({
            'position': 'absolute',
            'left': ($(window).width())/2
        })
        $('.blue').animate({
            opacity: 0
        },1000,null,function(){
            setTimeout(function(){
                $('.blue').hide()
            },5)
        })
        $('.rooms_padding').animate({
            padding: '20% 0'
        },1000)
        $('.room1').animate({
            width: '100%',
            height: ($(window).width())*895/1920
        },1000);

        $('#room_man').animate({
            opacity: 1
        },1000)

        $(".room2").hide("slide", { direction: "right" }, 1000);
    })

    $('.room2').click(function(){
        if($(this).hasClass('after_animate')){
            return false;
        }
        
        $('#room_woman').show();
        $('#room_woman').css('height',($(window).width())*895/1920);

        $(this).addClass('after_animate');

        $('.woman .description_rooms').hide();
        $('.woman .after_rooms').hide();
        $('.woman .rectangle').hide()
        $(".room1").css({
            'position': 'absolute'
        })
        $('.pink').animate({
            opacity: 0
        },1000,null,function(){
            setTimeout(function(){
                $('.pink').hide()
            },5)
        })
        $('.rooms_padding').animate({
            padding: '20% 0'
        },1000)
        $('.room2').animate({
            'width': '100%'
        },1000,null,(function(){
            $('.plus_room').show();
            $('.plus_room').each(function(){
                $(this).css('top',$(this).css('top'));
                $(this).css('left',$(this).css('left'));
            });
            $('.plus_room').animate({
                opacity: 1
            },500)}
        ));



        $('#room_woman').animate({
            opacity: 1
        },1000)


        $(".room1").hide("slide", { direction: "left" }, 1000);
    })

    $(window).resize(function(){
        size(font,margin_bottom,margin_top);
    });

})


function size(font,margin_bottom,margin_top){

        var i=0;
        if($(window).width()<1700 && $(window).width()>=600){
            $('.my_row *').each(function(){
                $(this).css('font-size', parseFloat(font[i])*(($(window).width())/1890).toString()+'px');
                $(this).css('margin-bottom', parseFloat(margin_bottom[i])*(($(window).width())/1920).toString()+'px');
                $(this).css('margin-top', parseFloat(margin_top[i])*(($(window).width())/1920).toString()+'px');
                i+=1;
            })
        }
    var j=0;
    if($(window).width()<900){
        $('.description_rooms').hide();
        $('.my_row *').each(function(){
            $(this).css('font-size', (parseFloat(font[i])*600/1750).toString()+'px');
            $(this).css('margin-bottom', (parseFloat(margin_bottom[i])*600/1920).toString()+'px');
            $(this).css('margin-top', (parseFloat(margin_top[i])*600/1920).toString()+'px');
            i+=1;
        })
    }

    //$('.pink').css('height', ($(window).width())*895/1920);
    //$('.blue').css('height', ($(window).width())*895/1920);
    //$('.bg_image').css('height', ($(window).width())*895/1920);
    //$('.my_row').css('height',($(window).width())*895/1920);
    $('.plus_room').css('width', parseFloat($('.plus_room').css('width'))*($(window).width())/1920);
    $('.plus_room').css('height', parseFloat($('.plus_room').css('height'))*($(window).width())/1920);
    $('.room2').css('height', ($(window).width())*895/1920);
    $('.room1').css('height', ($(window).width())*895/1920);



    $('.my-9').css('height', $('.my-3').height());
    var center = ($('.my-3').height()/2) - ($('#main_menu').height()/2);
    $('#main_menu').css('top',center);
}
