$(document).ready(function() {
    fixed_menu();
    body_resize();
    resize_slider();
    $(window).resize(function(){
        resize_slider();
        body_resize();
        fixed_menu()
    });

    $.extend({
        getUrlVars: function() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
        }
        return vars;
    },
    get: function(name) {
        return $.getUrlVars()[name];
    },
    rand: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    });


    $('#loader-wrapper').animate({
        opacity: 0
    }, 1000, null, function () {
        $(this).hide();
    });


    $('#slider1').tinycarousel();
    $('#slider2').tinycarousel();
    $('#slider3').tinycarousel({
        interval: 10000,
        intervaltime: 10000
    });
    $('#slider4').tinycarousel();
    $('#slider5').tinycarousel();
	$('#slider6').tinycarousel();





    $('.sortable img').each(function(){
        $(this).attr('title','Показать подкатегории');
    });

    $('.sortable img').click(function(){
        if($(this).parent().next().hasClass('sortable') && $(this).parent().next().css('display')=='block'){
            $(this).attr('src','/images/list_style_catalog.png');
            $(this).parent().next().hide();
            return false;
        }

        if($(this).parent().next().hasClass('sortable')){
            $(this).attr('src','/images/list_style_catalog_down.png');
            $(this).parent().next().css('display','block');
            return false;
        }

    });

    $('#my_drop_down').click(function(){
        $('.ul_color').fadeIn(300);
    });

    $('.ul_color li').click(function(){
        $('#my_drop_down').html($(this).html());
        $(this).parent().fadeOut(300);
    });

    $('.spss').css({
        'margin-left': -($(window).width()-960)/2,
        'margin-right': -($(window).width()-960)/2
    });
    $('.msps').css({
        'margin-left':($(window).width()-960)/2,
        'width': 960
    });
    /*$('.listPole').change(function(){
        new_price();
    });*/
    $('input[type="radio"]').click(function(){
        $('input[type="radio"]').parent().css('background','url(/images/el.png) no-repeat 0 7px');
        $('input[type="radio"]:checked').parent().css('background','url(/images/sf.png) no-repeat 0 7px');
        /*var price_change = $('input[type="radio"]:checked').attr('price_change').substr(1);
        var action = $('input[type="radio"]:checked').attr('price_change').substr(0,1);
        var base_price = $('.price_js').attr('base_price');

        if( action=='+' ){
            $('.price_js').html('Цена: ');
            $('.price_js').append( base_price*1+price_change*1+' р.')
        }
        else{
            $('.price_js').html('Цена: ');
            $('.price_js').append( base_price*1-price_change*1+' р.')
        }*/
        new_price();



    });

    $('.client_region input[type="radio"]').click(function(){
        $('input[type="radio"]').parent().css('background','url(/images/el.png) no-repeat 0 13px');
        $('input[type="radio"]:checked').parent().css('background','url(/images/sf.png) no-repeat 0 13px');

    });
    basket();

});

function fixed_menu(){
    if( $(window).width()>1000 ){
        $(window).scroll(function(){
            if($(this).scrollTop() > 123){
                $('.fix2').show();
                $('.fix2').addClass('row_menu_header_fixed');
            }
            else{
                $('.fix2').removeClass('row_menu_header_fixed');
                $('.fix2').hide();
            }
        });
    }
}

function body_resize(){
    if( $(window).width()<1000 ){
        $('body').css('width', 1000);
    }
    else{
        $('body').css('width', $(window).width());
    }
}

function new_price(){
    $('input[type="radio"]:checked').each(function(){

        if( $(this).attr('price_change').length ){
            var price_change = $('input[type="radio"]:checked').attr('price_change').substr(1);
            var action = $('input[type="radio"]:checked').attr('price_change').substr(0,1);
            var base_price = $('.price_js').attr('base_price');
            var discount = 1;
            if( $('.price_js').attr('discount')!=undefined ){
                discount = (100-$('.price_js').attr('discount'))/100;
            }
            //console.log($(this).attr('price_change'));
        }
        if( action=='+' ){
            $('.price_js').html('Цена: ');
            $('.price_js').append( base_price*1+price_change*discount+' р.')
        }
        else{
            $('.price_js').html('Цена: ');
            $('.price_js').append( base_price*1-price_change*discount+' р.')
        }
    });
}

function resize_slider(){
    if( $(window).width()<1300 ){
        $('#banner1').hide();
        $('#banner2').hide();
        $('#slides').hide();
    }
    else{
        $('#slides').show();
        $('#banner2').show();
        $('#banner1').show();
    }
}
/*<script>
 $('.pole_basket').each(function(){
 if( $(this).attr('price_change').substr(1)*1>0 ){
 var price_change = $(this).attr('price_change').substr(1);
 var action = $(this).attr('price_change').substr(0,1);
 if( action=='+' ){
 $('.main_price').html($('.main_price').html()*1+price_change*1);
 }
 else{
 $('.main_price').html($('.main_price').html()*1+price_change*1);
 }
 }
 })
 </script>*/


function basket(){
    var products = JSON.parse($.cookie('basket'));
    var count = 0;
    for(var i in products){
        count++;
    }
    $('.circle_basket').html(count);
}