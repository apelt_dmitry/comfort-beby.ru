$(document).ready(function(){
    $('.page-header h1').text($('ul[role="menu"] .active:last').text())

    $('[data-toggle="tooltip"]').tooltip({
        html: true,
        container: 'body'
    });

    set_editable();



});


function set_editable(){
    $('.editable').editable({
        validate: function(value) {
            if ( $(this).hasClass('editable-required') && $.trim(value) == '' ) {
                return 'Поле не может быть пустым.';
            }
        }
    });
}
