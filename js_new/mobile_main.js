$(document).ready(function () {
    //cookies
    $('.m_full_button').click(function(){
        //alert('123');
        $.cookie('m_full_button', '1');
        location.reload();
    });


    //slick slider mobile_banner
    $('.m_banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 15000,
        dots: false,
        infinite: true,
        arrows:false
    });
    $('.m_main_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 15000,
        dots: false,
        infinite: true,
        arrows:true,
        fade:true
    });
    $('.m_popular_slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 15000,
        dots: false,
        infinite: true,
        arrows:true,
        fade:false
    });
    $('.m_we_recomended_slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 15000,
        dots: false,
        infinite: true,
        arrows:true,
        fade:false
    });

    $('.button_call_back').click(function(){
        $('#phoneModal').arcticmodal();
    });

    $('.m_drop_slide_title').click(function(){
        $(this).find('.m_drop_slide_arrow').toggleClass('rotate');
        $(this).find('.m_drop_slide_arrow').toggleClass('rotate-reset');
        $(this).next().slideToggle();
    });
    $('.m_drop_slide_title').click(function(){
        $('.choise_arrow_size').toggleClass('rotate');
        $('.choise_arrow_size').toggleClass('rotate-reset');
        $(".choice_size_sizes").slideToggle();
    });

    $('.m_i_have_promo').click(function(){
        $(this).hide();
        $('.m_promo').show();
        $('.m_promo_success').show();
    });

    $('.qwe').focus(function(){
        $(this).parent().css('background-position','0 -50px');
    });
    $('.qwe').keyup(function(){
        if($(this).val()!=''){
            $(this).next().hide();
        }
    });
    $('.qwe').focusout(function(){
        if($(this).val()==''){
            $(this).parent().css('background-position','0 0');
            $(this).next().show();
        }
    });

    //    при клике добавляем выбранный цвет
    $('.box_color').click(function(){
        $(this).parent().find('.box_color.chic').removeClass('chic');
        //$('.box_color').each(function(){
        //    $(this).removeClass('chic');
        //});
        $(this).addClass('chic');
        var color = $('.color', this ).attr('color');

        var need_title = $(this).attr('for_title');
        $('span','.' + need_title).html(color);
        $(this).find('input[type="radio"]').prop("checked", true);
    });
    //$('.box_color').click(function(){
    //    $('.box_color').each(function(){
    //        $(this).removeClass('chic');
    //    })
    //    $(this).addClass('chic');
    //    var color = $('.color', this ).attr('color');
    //    $('span','.choise_color_colors_title').html(color);
    //});
    //$('.box_color_fasad').click(function(){
    //    $('.box_color_fasad').each(function(){
    //        $(this).removeClass('chic');
    //    })
    //    $(this).addClass('chic');
    //    var color = $('.color', this ).attr('color');
    //    $('span','.choise_color_colors_title_fasad').html(color);
    //});
//    выбираем первый цвет
    var color = $('.color', '.box_color' ).attr('color');
    $('span','.choise_color_colors_title').html(color);
    $('.box_color:first').addClass('chic');
    var color = $('.color', '.box_color_fasad' ).attr('color');
    $('span','.choise_color_colors_title_fasad').html(color);
    $('.box_color_fasad:first').addClass('chic');
//    var color = $('.color', '.box_color' ).attr('color');
//    $('span','.choise_color_colors_title').html(color);
//    $('.box_color:first').addClass('chic');
//    var color = $('.color', '.box_color_fasad' ).attr('color');
//    $('span','.choise_color_colors_title_fasad').html(color);
//    $('.box_color_fasad:first').addClass('chic');


    $(".autocomplete").autocomplete({
        source: function (request, response) {
            // запрос
            $.ajax({
                url: "/page/search",
                dataType: "json",
                // параметры запроса, передаваемые на сервер :
                data: {
                    product_name: request.term,
                    //region_id: $(".region_id_class").val(),
                },
                success: function (data) {
                    response($.map(data, function (title, id) {
                        return {
                            label: title,
                            value: title,
                            id: id
                        };
                    }));
                }
            });
        },
        minLength: 1,
        delay: 300,
        select: function (event, ui) {
            // $(".city_class").val(ui.item.label);
            // $(".city_id_class").val(ui.item.id);
        },
        close: function (event, ui) {
            // $('#autocomplete').val($(".city_class").val());
        },
    }).data('ui-autocomplete')._renderItem = function (ul, item) {
        return $("<li>")
            .attr("data-value", item.value)
            .attr("city-id", item.id)
            .append(item.label)
            .appendTo(ul);
    };

    basket();


    $(".buy_btn_main").live("click", function () {

        /* Добовление в корзину!!!! */
        var pole_name = [];
        var pole_id = [];
        var i = 0;

        var product = JSON.stringify([{
            'product_id': $(this).attr('product_id'),
            'pole_name': pole_name,
            'pole_id': pole_id,
            'count': 1
        }]);

        //alert($.cookie('basket'));

        if ($.cookie('basket') == 'null' || !IsJsonString($.cookie('basket'))) {
            $.cookie('basket', product, {expires: 30, path: '/'});
        }
        else {
            var products = JSON.parse($.cookie('basket'));
            var hasProduct = false;
            for (var i in products) {
                if ($(this).attr('product_id') == products[i]['product_id']) {
                    products[i]['count'] = products[i]['count'] * 1 + 1;
                    hasProduct = true;
                }
            }
            if (!hasProduct) {
                products.push({
                    'product_id': $(this).attr('product_id'),
                    'pole_name': pole_name,
                    'pole_id': pole_id,
                    'count': 1
                });
            }
            $.cookie('basket', JSON.stringify(products), {expires: 30, path: '/'});
            $('#add_basket').css({
                'left': ($(window).width() / 2) - 120,
                'top': ($(window).height() / 2) - 100
            });

        }

        basket();
        return false;
    });

});



/* функция для корзины */
function basket(){

    if(IsJsonString($.cookie('basket'))){
        var products = JSON.parse($.cookie('basket'));
        var count = 0;
        for(var i in products){
            //count++;
            count = count + products[i]['count'];
        }

        $('.menu_count_basket').html(count);

        if(count > 0)
        {
            $.ajax({
                type: "POST",
                url: "/page/getprice",
                data:  $.cookie('basket'),
                success: function(full_price){

                    $('.drop_count').show();
                    $('.drop_count_count').html(count);
                    $('.drop_count_cost').html(full_price);

                }
            });

        }
        else
        {
            $('.drop_count').hide();
        }
    }

}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}