$(document).ready(function () {
    //slick slider
    $('.top_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        infinite: true

    });
    $('.catalog_slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true

    });
    //slick slider
    $('.reviews_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        infinite: true,
        arrows: true
    });
    //slick slider
    $('.similarSlider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        //autoplaySpeed: 10000,
        //infinite: true,
        arrows: true
    });
    //slick slider
    $('.flex_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        //fade: true,
        //autoplaySpeed: 10000,
        //infinite: true,
        arrows: true
    });
    //показываем кнопку поиска при нажатии на input
    $('.search_input').click(function () {
        $('.search_button').show();
    });
    //изменение цвета города при выборе
    $('#city').change(function () {
        $('.select').css('background-position', '0 -30px');
    });
    
    //Переходы по ссылкам на баннере на главной
    $('.promo_button_1').click(function(){
           window.location = '/site/catalog/27';
    });
    $('.promo_button_2').click(function(){
           window.location = '/site/catalog/1';
    });
    $('.promo_button_3').click(function(){
           window.location = '/site/catalog/5';
    });
    $('.promo_button_4').click(function(){
           window.location = '/site/catalog/32';
    });
    
    //прилипающая менюшка
    //var start_pos = $('#header_menu_2').offset().top;
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 158) {
            if ($('#header_menu_2').hasClass() == false){
                $('#header_menu_2').addClass('to_top');
                $('#header_menu_2').slideDown('fast');
                //$('#header_menu_2').show();
                $('.category_drop').hide();
            }
        }
        else {
            $('#header_menu_2').removeClass('to_top');
            $('#header_menu_2').hide();
            $('.category_drop').hide();
        }
    });
    //открываем каталог меню
    $('.open_drop').hover(function (){
        if(!$('.open_drop').hasClass('now_animate')){
            $('.open_drop').addClass('now_animate');
            $('.category_drop').slideDown(400, function () {
                $('.open_drop').removeClass('now_animate');
            });
        }

    },function (){
        if(!$('.open_drop').hasClass('now_animate')){
            $('.open_drop').addClass('now_animate');
            $('.category_drop').slideUp(400, function () {
                $('.open_drop').removeClass('now_animate');
            });
        }
    });
    //открываем каталог меню-2
    $('.open_drop_2').hover(function (){
        if(!$('.open_drop_2').hasClass('now_animate')){
            $('.open_drop_2').addClass('now_animate');
            $('.category_drop').slideDown(400, function () {
                $('.open_drop_2').removeClass('now_animate');
            });
        }
    },function (){
        if(!$('.open_drop_2').hasClass('now_animate')){
            $('.open_drop_2').addClass('now_animate');
            $('.category_drop').slideUp(400, function () {
                $('.open_drop_2').removeClass('now_animate');
            });
        }
    });

    $('.drop_count_top_close').click(function(){
        $('.drop_count_top ').slideUp();
        $('.drop_count ').animate({
            height: '33px'         // ширина станет 70%
        }, 400);
    });
    //arcticmodal модальные окна
    $('.button_buy').click(function(){
        $('#exampleModal').arcticmodal();
    });
    $('.button_call_back').click(function(){
        $('#phoneModal').arcticmodal();
    });
    $('#header_call_back').click(function(){
        $('#phoneModal').arcticmodal();
    });
    $('#alwaystop_call_back').click(function(){
        $('#phoneModal').arcticmodal();
    });
    
//    $('.phone_submit').click(function(){
//        $('#TYModal').arcticmodal();
//    });
    $('.big_red_car_button').click(function() {
        var c = $('<div class="box-modal" />');
        c.html($('.b-text').html());
        $.arcticmodal({
            content: c
        });
    });

    $('.cb').focus(function(){
        $(this).parent().css('background-position','0 -50px');
    });
    $('.cb').keyup(function(){
        if($(this).val()!=''){
            $(this).next().hide();
        }
    });
    $('.cb').focusout(function(){
        if($(this).val()==''){
            $(this).parent().css('background-position','0 0');
            $(this).next().show();
        }
    });

    $('.i_have_promo').click(function(){
        $(this).hide();
        $('.promo').show();
        $('.promo_success').show();
    });

//    вращаем отзывы
    $('.slick-next').click(function(){
        $('.review_img').addClass('flip');
        setTimeout(function(){
            $('.review_img').removeClass('flip')
        },1000)
    })
    $('.slick-prev').click(function(){
        $('.review_img').addClass('flip');
        setTimeout(function(){
            $('.review_img').removeClass('flip')
        },1000)
    })
//  добавляем тень при наведении на цвет
    $('.box_color').hover(function (){
        $(this).css("box-shadow","0px 0px 10px 0px rgba(0,0,0,0.75)")
    },function (){
        $(this).css("box-shadow","none")
    });
//    при клике добавляем выбранный цвет
    $('.box_color').click(function(){
        $(this).parent().find('.box_color.chic').removeClass('chic');
        //$('.box_color').each(function(){
        //    $(this).removeClass('chic');
        //});
        $(this).addClass('chic');
        var color = $('.color', this ).attr('color');
        
        var need_title = $(this).attr('for_title');
        $('span','.' + need_title).html(color);
        $(this).find('input[type="radio"]').prop("checked", true);
    });
    
//    $('.box_color_fasad').click(function(){
//        $('.box_color_fasad').each(function(){
//            $(this).removeClass('chic');
//        })
//        $(this).addClass('chic');
//        var color = $('.color', this ).attr('color');
//        $('span','.choise_color_colors_title_fasad').html(color);
//    });
//    выбираем первый цвет
    var color = $('.color', '.box_color' ).attr('color');
    $('span','.choise_color_colors_title').html(color);
    $('.box_color:first').addClass('chic');
    var color = $('.color', '.box_color_fasad' ).attr('color');
    $('span','.choise_color_colors_title_fasad').html(color);
    $('.box_color_fasad:first').addClass('chic');
//    закрываем колорпикер
    $('.choice_color_title').click(function(){
        $('.choise_arrow').toggleClass('rotate');
        $('.choise_arrow').toggleClass('rotate-reset');
        $(".choise_color_colors").slideToggle();
    });
    $('.choice_size_title').click(function(){
        $('.choise_arrow_size').toggleClass('rotate');
        $('.choise_arrow_size').toggleClass('rotate-reset');
        $(".choice_size_sizes").slideToggle();
    });
//    расчитываем отступ для плавающего блока
//    var h = $(window).height();
//    var top = (h-695)/2;
//    if(top>0){
//        $('.flex-tab').css('top',top);
//    }else{
//        $('.flex-tab').hide();
//    }
//   закрываем плавающий блок
    $('.flex-tab').css('opacity', '0');
    setTimeout(function () {
        $('.flex-tab').animate({opacity: 1})
    }, 100);
    $('.flex_income').hide(50);
    $('.flex_open').click(function () {
        //$('.flex_slider .slick-next').click();
        $('.flex_income').animate({width: 'toggle'}, 350)
    });

//    расчитываем ширину тени боковой менюшки
    setTimeout(function(){
        $('.left_catalog li div.inside').each(function () {
            var total = 0;
            $(this).find('>*').each(function () {
                total += $(this).width();
            });
            console.log(total);
            $(this).width(total);
        });
    },300)

    setTimeout(function(){
        $('.left_catalog li div.inside').each(function () {
            $(this).addClass('dn')
        })
    },500)


    basket();       
    //$.cookie('basket', null);

    
    $('#TYModal').arcticmodal(); //показывать всегда окно, 'Cпасибо за заказной звонок!' (показывается с помощью php)
    
    /* ДОБОВЛЕНИЕ ТОВАРА Для всех кнопок в списках "Купить"  */
    $(".buy_btn_main").live("click", function () {

        /* Добовление в корзину!!!! */
        var pole_name = [];
        var pole_id = [];
        var i = 0;

        var product = JSON.stringify([{
            'product_id': $(this).attr('product_id'),
            'pole_name': pole_name,
            'pole_id': pole_id,
            'count': 1
        }]);

        //alert($.cookie('basket'));

        if ($.cookie('basket') == 'null' || !IsJsonString($.cookie('basket'))) {
            $.cookie('basket', product, {expires: 30, path: '/'});
        }
        else {
            var products = JSON.parse($.cookie('basket'));
            var hasProduct = false;
            for (var i in products) {
                if ($(this).attr('product_id') == products[i]['product_id']) {
                    products[i]['count'] = products[i]['count'] * 1 + 1;
                    hasProduct = true;
                }
            }
            if (!hasProduct) {
                products.push({
                    'product_id': $(this).attr('product_id'),
                    'pole_name': pole_name,
                    'pole_id': pole_id,
                    'count': 1
                });
            }
            $.cookie('basket', JSON.stringify(products), {expires: 30, path: '/'});
            $('#add_basket').css({
                'left': ($(window).width() / 2) - 120,
                'top': ($(window).height() / 2) - 100
            });

        }

        basket();
        return false;
    });

    $('#main_catalog_menu_left ul li').on('click', function () {
        location.href = $(this).find('.catalog_menu_link').attr('href');

    });

    $(".autocomplete").autocomplete({
        source: function (request, response) {
            // запрос
            $.ajax({
                url: "/page/search",
                dataType: "json",
                // параметры запроса, передаваемые на сервер :
                data: {
                    product_name: request.term,
                    //region_id: $(".region_id_class").val(),
                },
                success: function (data) {
                    response($.map(data, function (title, id) {
                        return {
                            label: title,
                            value: title,
                            id: id
                        };
                    }));
                }
            });
        },
        minLength: 1,
        delay: 300,
        select: function (event, ui) {
            // $(".city_class").val(ui.item.label);
            // $(".city_id_class").val(ui.item.id);
        },
        close: function (event, ui) {
            // $('#autocomplete').val($(".city_class").val());
        },
    }).data('ui-autocomplete')._renderItem = function (ul, item) {
        return $("<li>")
            .attr("data-value", item.value)
            .attr("city-id", item.id)
            .append(item.label)
            .appendTo(ul);
    };

});



/* функция для корзины */
function basket(){
    
    if(IsJsonString($.cookie('basket'))){
        var products = JSON.parse($.cookie('basket'));
        var count = 0;
        for(var i in products){
            //count++;
            count = count + products[i]['count'];
        }
        
        $('.menu_count_basket').html(count); 
        
        if(count > 0)
        {
            $.ajax({
              type: "POST",
              url: "/page/getprice",
              data:  $.cookie('basket'),
              success: function(full_price){
                
                $('.drop_count').show();
                $('.drop_count_count').html(count);
                $('.drop_count_cost').html(full_price); 
                
              }
            });               
                    
        }
        else
        {
                $('.drop_count').hide();            
        }       
    }

}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
